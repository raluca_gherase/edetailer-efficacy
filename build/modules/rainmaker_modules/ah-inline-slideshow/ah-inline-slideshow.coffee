app.register 'ah-inline-slideshow', ->
  {
    events: {}
    states: []

    onRender: () ->
      app.on('init:inlineSlideshow', @init.bind(@))
      app.on('open:inlineSlideshow', @show.bind(@))
      app.on('close:inlineSlideshow', @hide.bind(@))

    getSlideIndex: (id = @currentSlideDOM.el.id)->
      @slides.indexOf id

    getCurrentSlide: ()->
      @currentSlideDOM.el.id

    presentAnimation: ($slide)->
      setTimeout ()->
        $slide.classList.remove("future", "past")
        $slide.classList.add("present")
      , 50

    triggerEvent: (type = 'Enter')->
      @currentSlideDOM['on' + type]()
      app.trigger('slide' + type + ':inlineSlideshow', {slide: @currentSlideDOM})
      console.log 'slide' + type + ':inlineSlideshow  --- ' + @currentSlideDOM.id

    animate:(id)->
      isNext = false
      for slide in @slides
        $slide = app.slide.get(slide).el
        if id is slide
          isNext = true
          @presentAnimation $slide
        else if isNext
          $slide.classList.remove("past", "present")
          $slide.classList.add("future")
        else
          $slide.classList.remove("future", "present")
          $slide.classList.add("past")

    goTo: (id)->
      unless id is @getCurrentSlide()
        @triggerEvent('Exit')
        @animate(id)
        @currentSlideDOM = app.slide.get(id)
        @triggerEvent()

    openNextSlide: (that = @)->
      app.lock()
      nextSlide = that.slides[that.getSlideIndex() + 1]
      @goTo(nextSlide) if nextSlide

    openPrevSlide: (that = @)->
      app.lock()
      nextSlide = that.slides[that.getSlideIndex() - 1]
      @goTo(nextSlide) if nextSlide


    _setClasses: ()->
      window.viewer.classList.add 'inline-slideshow', 'custom-popup', 'state-default'
      @inlineSlideshowWrapper.classList.add 'inline-slideshow-wrapper', 'slides'

    _clearDOM: (slides)->
      slides.forEach (slide)->
        app.slide.remove slide, true

    _setCurrentSlide: (slide)->
      @currentSlide = slide

    _insertDOMSlides: (slides)->
      slidesStructure = slides.map (slide)->
        {"id": slide}
      app.dom.insert slidesStructure, false, @inlineSlideshowWrapper

    show: (props)->
      setTimeout ()=>
        @currentSlideDOM = app.slide.get @currentSlide
        @currentSlideDOM.el.classList.add "present"
        @currentSlideDOM.onEnter()
        @currentSlideDOM.parent.el.dataset.transitionSpeed = app.config.get("transitionSpeed")
        app.trigger('slideEnter:inlineSlideshow', {slide: @currentSlideDOM})
        props.data.view.container.classList.add 'visible'
        props.data.view.container.classList.add 'loaded'
        window.viewer.classList.add 'state-ag-overlay-open'
        window.viewer.classList.remove 'state-default'
      , 100

    hide: ()->
      app.unlock()
      app.trigger('slideExit:inlineSlideshow', {slide: @currentSlideDOM})

    addEventsHandler: ()->
      that = @
      @inlineSlideshowWrapper.addEventListener 'swipeleft', ()=>
        @openNextSlide(that)
      @inlineSlideshowWrapper.addEventListener 'swiperight', ()=>
        @openPrevSlide(that)

    init: (props)->
      viewPort = document.createElement 'div'
      @inlineSlideshowWrapper = document.createElement 'div'

      @_setClasses()
      viewPort.appendChild @inlineSlideshowWrapper
      viewPort.classList.add 'viewport'
      @slides = app.model.get().structures[props.data.slideshow].content
      @_clearDOM(@slides)
      @_setCurrentSlide(props.data.slide || @slides[0])
      @_insertDOMSlides(@slides)
      props.data.view.container.appendChild viewPort

      @addEventsHandler()
  }