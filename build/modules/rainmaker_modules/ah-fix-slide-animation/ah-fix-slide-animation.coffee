app.register "ah-fix-slide-animation", ()->
    onRender: () ->
      @setAnimation(@getSB())
      app.slideshow.on 'load', (data)=>
        @setAnimation(@getSB())

    getSB: ()->
      app.model.get().storyboards[app.slideshow.getId()].linear

    setAnimation: (isLinear)->
      presentation.classList.remove 'linear-animation'
      presentation.classList.add 'linear-animation' if isLinear
