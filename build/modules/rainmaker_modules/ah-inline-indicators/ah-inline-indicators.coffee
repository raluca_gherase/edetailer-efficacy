app.register 'ah-inline-indicators', ->
  {
    events: {}
    states: []
    excludedSlides: []
    updatedSlide: []

    onRender: () ->
      app.on('init:inlineSlideshow', @load.bind(@))
      app.on('slideEnter:inlineSlideshow', @setActive.bind(@))

    load: (props)->
      @indicatorWrappers = props.data.view.container.getElementsByClassName('inline-indicators')[0]
      inlineSlideshow = app.model.get().structures[props.data.slideshow]
      return if !@indicatorWrappers or !inlineSlideshow.indicators

      indicatorWrapper = document.createElement 'ul'
      @slides = inlineSlideshow.content
      @slides.forEach (slide)=>
        indicator = document.createElement 'li'
        indicator.classList.add 'inline-indicator'
        indicator.setAttribute 'indicator', slide
        indicatorWrapper.appendChild indicator
      @indicatorWrappers.appendChild indicatorWrapper

    setActive: (props)->
      @indicatorWrappers.getElementsByClassName('active')?[0]?.classList.remove 'active'
      @indicatorWrappers.querySelector('[indicator="' + props.slide.el.id + '"]')?.classList.add 'active'
  }