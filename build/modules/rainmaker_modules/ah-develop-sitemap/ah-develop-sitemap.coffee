# * Provides sitemap Navigation.
# * -------------------------------------
# * API:
# * exclude: Some content that should not be in the Sitemap? "slide_id1 slide_id2"
# * popUps: {true,false} - load Popups from structure
# * @module ah-develop-sitemap.coffee
# * @requires module.js, jquery.js, iscroll.js, touchy.js
# * if need popups require ag-overlay


class AhDevelopSitemap
  constructor: ()->
    @template = '<button class="close">X</button>'+
                '<header>'+
                '<p>FILTERS:</p>'+
                '<div class="rounded" title="Slides">'+
                  '<input type="checkbox" value="None" id="rounded" name="slides" checked />'+
                  '<label for="rounded"></label>'+
                '</div>'+
                '<div class="rounded" title="Popups">'+
                '<input type="checkbox" value="None" id="rounded1" name="popups" checked />'+
                '<label for="rounded1"></label>'+
                '</div>'+
                '<input type="text" placeholder="slideId..." />'+
                '<output></output>'+
                '<button class="sort"></button>'+
                '</header>'+
                '<div class="wrapper"><ul class="links"></ul></div>'
    @collections = []
    @currentPopup = null
    @currentSlide = null
    @scroll = null
    @canUsePopups = false

    @states = [
      id: 'show'
      onEnter: (e)->
        @currentSlide = app.slide.get().id
        @filter()
        @scrollToActive()
    ]
    @events = {
      'tap .close': 'close'
      'tap .sort': 'changeSort'
      'tap li': 'navigate'
      'change [name="slides"]': 'toggleSlides'
      'keyup [type="text"]': 'filterByName'
      'change [name="popups"]': 'togglePopups'
      "swipeleft": (event)-> event.stopPropagation()
      "swiperight": (event)-> event.stopPropagation()
    }
    @publish = {
      exclude : ''
      popUps: true
    }

  onRender: (element)->
    @params = {
      isShowPopups: on
      isShowSlides: on
      searchValue: ''
      sort: 'desc'
    }
    @$output = element.getElementsByTagName('output')[0]
    @$sort = element.getElementsByClassName('sort')[0]
    if !app.view.get("overlay-popup-custom") and @props.popUps
      throw new Error("Can't find overlay-popup or off this.props.popUps!")
    else
      @canUsePopups = true
    window['presentation'].addEventListener('doubleTap', ()=>
      @goTo('show')
    ,false)
    @scroll = new IScroll($(".wrapper")[0], scrollbars: false, bounce: false)

    @getCollections()
    @collections = @collections.concat(@getPopups()) if @props.popUps
    @filtered = [].concat(@collections)

    #@scroll.refresh()

  filterByName: (event)->
    value = event.target.value
    @params.searchValue = value
    @filter()

  toggleSlides: (event)->
    @params.isShowSlides = event.target.checked
    @filter()

  togglePopups: (event)->
    @params.isShowPopups = event.target.checked
    @filter()

  changeSort: ()->
    @params.sort = if @params.sort is '' then 'desc' else ''
    @$sort.classList.toggle('active')
    @filter()

  filter: ()->
    compareSort = (a, b)=>
      if (a.id < b.id)
        return -1
      if (a.id > b.id)
        return 1
      return 0

    if !@params.isShowPopups or !@params.isShowSlides
      @filtered = @collections.filter (slide)=>
        if @params.isShowPopups
          compare = slide.isPopup
        if @params.isShowSlides
          compare = !slide.isPopup
        compare
    else
      @filtered = @collections

    res = @filtered.filter (slide)=>
                      slide.id.toLowerCase().indexOf(@params.searchValue) > -1
    res.sort(compareSort)
    if(@params.sort is 'desc')
      res.reverse()

    @createLinks(res)
    @$output.innerText = res.length
    @scrollToActive()

  close: (e)->
    e.stopPropagation()
    @reset()

  scrollToActive: ()->
    @scroll.refresh()
    @scroll.scrollTo(0, 0, 500)
    #@scroll.scrollToElement('li.active', 500)

  navigate: (event)->
    event.stopPropagation()
    target = event.delegateTarget
    path = target.dataset.goto
    popupId = target.dataset.popup

    if @canUsePopups
      app.view.get("overlay-popup-custom").reset()
      app.view.get("overlay-popup-custom").load(popupId) if popupId

    app.goTo(path) if path

    @reset()

  createLinks: (slides)->
    list = @el.getElementsByClassName('links')[0]
    html = ''
    while (list.firstChild)
      list.removeChild(list.firstChild)

    excludedLinks = @props.exclude.split(' ')

    for key of slides
      slide = slides[key]
      continue if excludedLinks.indexOf(slide.id) > -1
      className = ''
      if slide.id is @currentSlide
        className = 'active'
      if slide.isPopup
        html += "<li class='#{className}' data-popup='#{slide.path}'> <img src='slides/#{slide.id}/#{slide.id}.png'> [POPUP]<h2>#{slide.id}</h2></li>"
      else
        html += "<li class='#{className}' data-goto='#{slide.path}'> <img src='slides/#{slide.id}/#{slide.id}.png'><h2>#{slide.id}</h2></li>"

    list.appendChild(app.dom.parse(html))

  getCollections: ()->
    collections = app.model.get().storyboards
    slideshows = app.model.get().structures
    @collections.length = 0
    for collectionName of collections
      slideShowStructure = collections[collectionName].content

      for slideshow in slideShowStructure
        slides = slideshows[slideshow].content

        for slide in slides
          @collections.push ({id:slide, isPopup:false, path:"#{collectionName}/#{slideshow}/#{slide}"})
    @collections

  getPopups: ()->
    slides = app.model.get().slides
    popups = []
    for slide of slides
      popups.push({id:slide, isPopup: true, path: slide}) if slide.indexOf('PopupSlide') > -1
    popups

app.register "ah-develop-sitemap", -> new AhDevelopSitemap()