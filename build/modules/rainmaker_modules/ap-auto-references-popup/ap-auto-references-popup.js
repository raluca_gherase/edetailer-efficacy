/**
 * Implements auto reference popups.
 * ---------------------------------
 *
 * Adds tap handlers to every element that has the [data-reference-id] data attribute.
 * On tap a popup with a list of all references in the slide appears.
 *
 * The module is loaded within index.html
 *
 * @module ap-auto-references-popup.js
 * @requires jquery.js, touchy.js
 * @author Andreas Tietz, antwerpes ag
 */
app.register("ap-auto-references-popup", function () {

    return {
        publish: {},
        events: {},
        states: [],
        onRender: function (el) {
            app.listenTo(app.slide, 'slide:enter', this.autoReferencePopupHandler.bind(this));
        },
        onRemove: function (el) {

        },
        onEnter: function (el) {

        },
        onExit: function (el) {

        },

        autoReferencePopupHandler: function (data) {
            var $slide = $("#" + data.id);
            var self = this;

            $slide.find("[data-reference-id]")
                .off('tap.auto-references-popup')
                .on('tap.auto-references-popup', function () {

                    // Collect unique reference ids:
                    var referenceIds = {};
                    $slide.find("[data-reference-id]").each(function () {

                        var referenceId = $(this).attr("data-reference-id");

                        if (referenceId.indexOf('-') > -1) {
                            var range = referenceId.split('-');
                            var from = parseInt(range[0]);
                            var to = parseInt(range[1]);

                            for(var i = from; i <= to; i++){
                                referenceIds[i] = true;
                            }
                        }
                        else
                        {
                            var ids = referenceId.split(',');


                            $.each(ids, function (index, value) {
                                referenceIds[value] = true;
                            });
                        }
                    });
                    referenceIds = Object.keys(referenceIds);

                    // Find media resources associated with the collected reference ids:
                    var references = {};
                    $.each(window.mediaRepository.metadata(), function (file, meta) {
                        if (referenceIds.indexOf("" + meta.referenceId) > -1) {
                            references[file] = meta;
                        }
                    });

                    // Render all references into a list:
                    var $scroll = $('<div class="scroll"/>');
                    var $list = $('<ul class="references"/>');
                    $list.append($.map(references, function (meta, file) {
                        return window.mediaRepository.render(file, meta);
                    }));
                    $scroll.append($list);



                    // Put list in popup:
                    var $popup = $('<div class="auto-references-popup" />')
                        .append('<header><div class="x">⊗</div><h1>References</h1></header>')
                        .append($scroll);

                    // Put popup in overlay:
                    $('<div class="auto-references-popup-overlay" />')
                        .append($popup)
                        .on("swipedown swipeup swiperight swipeleft", function (e) {
                            e.stopPropagation();
                        })
                        .on("tap", function (event) {
                            if ($(event.target).is(":not(.auto-references-popup, .auto-references-popup *) .x")) $(this).remove();
                        }).appendTo("#presentation");

                      // self.scroll.refresh();
                      if(self.scroll) self.scroll.destroy();
                      self.scroll = new IScroll($scroll[0], {scrollbars: true});

                });
        }
    }

});
