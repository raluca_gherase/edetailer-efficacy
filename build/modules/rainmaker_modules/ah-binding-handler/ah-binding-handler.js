/**
 * -----------------------------------------------------------------
 * @module ah-binding-handler.js
 * @requires
 * @author
 */

app.register("ah-binding-handler", function () {
    return {
        publish: {},
        events: {},
        states: [],
        onRender: function (el) {
            this.currentSlide = app.slide.get();
            this.applyBindings(el);
            var self = this;
            app.listenTo(app, 'history:update', function (data) {
                self.currentSlide = app.view.views[data.id];
                self.applyBindings(self.currentSlide.el);
            });
            app.on('open:inlineSlideshow', function (data) {
	            self.applyBindings(viewer);
            });
        },
        applyBindings: function (el) {
            if (!el) el = document;
            var self = this;
            Object.keys(this.handlers()).forEach(function (bindKey) {
                var attribute = 'data-' + bindKey,
                    handler = self.handlers()[bindKey],
                    currentList = el.querySelectorAll('[' + attribute + ']');
                arryaList = Array.prototype.slice.call(currentList);
                if (el.hasAttribute(attribute)) {
                    arryaList.push(el);
                }
                arryaList.forEach(function (el) {
                    if (!el.bindingFlag) {
                        handler(el, attribute);
                        el.bindingFlag = true;
                    }
                });
            })
        },
        handlers: function () {
            var self = this;

            function handlerController(el, attribute, tapHandler) {
                el.addEventListener('tap', function (event) {
                    event.preventDefault();
                    tapHandler(el, el.attributes[attribute].value);
                });
            }

            return {
                'popup': function (element, attributes) {
                    handlerController(element, attributes, function (el, slideId) {
                        self._openOverlay(el, 'overlay-popup-custom', slideId);
                    });
                },
                'goto-state': function (element, attributes) {
                    handlerController(element, attributes, function (el, attribute) {
                        self.currentSlide.goTo(attribute);
                    })
                },
                'goto-slide': function (element, attributes) {
                    handlerController(element, attributes, function (el, attribute) {
                        app.goTo(attribute);
                    })
                },
                'goto-inline-slide': function (element, attributes) {
                    handlerController(element, attributes, function (el, attribute) {
	                    app.module.get('inlineSlideshow').goTo(attribute);
                    })
                },
                'stop-swipe': function (element) {
                    ['swipeup', 'swipedown', 'swipeleft', 'swiperight'].forEach(function (event) {
                        element.addEventListener(event, function (event) {
                            event.stopPropagation()
                        });
                    });
                }
            }
        },
        _openOverlay: function (el, overlayId, slideId) {
            var popup = app.view.get(overlayId),
                attributes = el.attributes,
                customClass;
            if (attributes["data-custom"]) {
                customClass = attributes["data-custom"].value;
                popup.el.classList.add(customClass);
            }
            if (popup && slideId) popup.load(slideId);
        },
        onRemove: function (el) {
        },
        onEnter: function (el) {
            this.applyBindings(el);
        },
        onExit: function (el) {
        }
    }
});
