app.register("ag-cleanup", function () {

    // Get the unique entries of an array
    function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }

    return {
        publish: {
            purgeOnLoad: false,
            timeout: 40, // seconds to wait since last purge.
            limit: 10
        },
        events: {},
        states: [],
        history: [],
        archived: [],
        lastPurge: 0,
        onRender: function (el) {
            var self = this;
            app.slideshow.on('update:current', function (data) {
                var unique, clean;
                var cleanupLength = parseInt(self.props.limit / 2); // We'll keep half our limit as active
                // Add current slide to beginning of history
                if (data.current) {
                    self.history.unshift(data.current.id);
                }
                // Allow animation to run first
                setTimeout(function () {
                    // Make sure we don't have the new slide in history already
                    self.history = self.history.filter(onlyUnique);
                    // console.log("SLIDE HISTORY:", self.history);
                    // If our history is larger than limit, then send for cleanup
                    if (self.history.length > parseInt(self.props.limit)) {
                        clean = self.history.splice(cleanupLength - 1, self.history.length - cleanupLength);
                        console.log("CLEAN FROM HISTORY:", clean);
                        self.clean(clean);
                    }
                }, 800);
            });
            // Fix for bug in Accelerator when slideshow is loaded and first slide has been archived
            app.slide.on('slide:enter', function (data) {
                var el = app.dom.get(data.id);
                if (el) el.classList.remove('archived');
            });
            app.slideshow.on('load', function (data) {
                var info = app.slideshow.resolve();
                var el = app.dom.get(info.slide);
                if (el) el.classList.remove('archived');
            });
            if (this.props.purgeOnLoad) {
                app.slideshow.on('load', function (data) {
                    console.log("Unloading slideshow");
                    // Remove all slides except the newly loaded one
                    // TODO: implement
                });
            }
        },
        onRemove: function (el) {

        },
        // Archive old slides and remove previously archived ones
        clean: function (slides) {
            var now = new Date().getTime();
            var currentSlide = app.slideshow.get();
            slides.forEach(function (id) {
                app.dom.archive(id);
            });
            // Only remove prevously archived if timeout has expired
            if (this.archived.length && now > (this.lastPurge + parseInt(this.props.timeout * 1000))) {
                this.archived.forEach(function (id) {
                    if (id !== currentSlide) app.slide.remove(id, true);
                });
                this.archived = slides;
                this.lastPurge = now;
            }
            else {
                this.archived = this.archived.concat(slides);
            }
        }
    };

});
