
class UniqueMoA50B3PopupSlide
  constructor: ()->
    @events = {}
    @states = [
      {
        id: "mead"
        name: "DME_Eff_DEXDurability_State1_PopUp",
        onEnter: ()-> @onChangeState()
      }
      {
        id: "bevordex",
        name: "DME_Eff_DEXDurability_State2_PopUp",
        onEnter: ()-> @onChangeState()
      },
      {
        id: "callanan",
        name: "DME_Eff_DEXDurability_State3_PopUp",
        onEnter: ()-> @onChangeState()
      }
    ]

  onRender: (element)->
    @el = element
    @analitycsModule = app.module.get 'ahAnalitycs'
    @refs = app.module.get 'ah-auto-references-popup'
    @extedNotes = app.module.get 'notes'
    app.module.get('ahUtils').getSlideData @el.id, (data)=>
      @texts = data
    @headline = element.getElementsByTagName('h1')[0]

  changeText: (element, text) -> element.innerHTML = text

  onEnter: (element)-> @goTo 'mead'

  updateNotes: (module, element)->
    module.updateNotes $(element)
    module.handleEnter(element)

  onChangeState: ()->
    textHeadline = if @stateIsnt 'mead' then @texts.headline_1 else @texts.headline
    @changeText(@headline, textHeadline)
    @refs.autoReferences @el
    @updateNotes @extedNotes, @el
    app.trigger('state:update',{slide: {el: @el}})
    @submitMonitoring()

  onExit: (element)->

  submitMonitoring: ()->
    @analitycsModule.submitCustomSlideMonitoring {id: @el.id, name: @getState().name}

app.register('UniqueMoA50B3PopupSlide', -> new UniqueMoA50B3PopupSlide())