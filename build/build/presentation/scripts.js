function SlideList(t){var t=t||[];this.init(t)}Function.prototype.bind||(Function.prototype.bind=function(t){if("function"!=typeof this)throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");var e=Array.prototype.slice.call(arguments,1),i=this,n=function(){},s=function(){return i.apply(this instanceof n&&t?this:t,e.concat(Array.prototype.slice.call(arguments)))};return n.prototype=this.prototype,s.prototype=new n,s}),function(t,e){try{t.querySelector(":scope body")}catch(i){["querySelector","querySelectorAll"].forEach(function(i){var n=e[i];e[i]=function(e){if(/(^|,)\s*:scope/.test(e)){var s=this.id;this.id="ID_"+Date.now(),e=e.replace(/((^|,)\s*):scope/g,"$1#"+this.id);var r=t[i](e);return this.id=s,r}return n.call(this,e)}})}}(window.document,Element.prototype);var app=app||function(){"use strict";function t(){if(w.wrapper){var t=s(),e=app.config.get("maxScale"),i=app.config.get("minScale");w.slides.style.width=t.slideWidth+"px",w.slides.style.height=t.slideHeight+"px",y=Math.min(t.availableWidth/t.slideWidth,t.availableHeight/t.slideHeight),y=Math.max(y,i),y=Math.min(y,e),"undefined"==typeof w.slides.style.zoom||navigator.userAgent.match(/(iphone|ipod|ipad|android)/gi)?c(w.slides,"translate(-50%, -50%) scale("+y+") translate(50%, 50%)"):w.slides.style.zoom=y,app.trigger("update:layout",{scale:y})}}function e(){return y}function i(){function t(){!window.Promise&&window.ES6Promise&&(window.Promise=window.ES6Promise.Promise),n.length&&head.js.apply(null,n),app.start.init()}function e(e){head.ready(e.src.match(/([\w\d_\-]*)\.?js$|[^\\\/]*$/i)[0],function(){"function"==typeof e.callback&&e.callback.apply(this),0===--s&&t()})}var i=[],n=[],s=0,r=app.config.get("dependencies"),a=app.config.get("transition"),o=app.config.get("transitionSpeed"),p=app.config.get("monitoringAPI");a?w.wrapper.classList.add(a):w.wrapper.classList.add("linear"),o&&w.wrapper.setAttribute("data-transition-speed",o),app.start||r.unshift({src:"accelerator/js/core.js"}),window.touchy||r.push({src:"accelerator/lib/touchy.js"}),!window.ag&&p&&r.unshift({src:p}),window.Promise||r.unshift({src:"accelerator/lib/promise.min.js"});for(var l=0,c=r.length;l<c;l++){var d=r[l];d.condition&&!d.condition()||(d.async?n.push(d.src):i.push(d.src),e(d))}i.length?(s=i.length,head.js.apply(null,i)):t()}function n(){var t=w.slides.querySelectorAll(".slide");return t.length>0&&p(t).forEach(function(t,e){t.id&&(w[t.id]=t)}),t}function s(){var t=w.wrapper.offsetWidth,e=w.wrapper.offsetHeight,i=app.config.get();t-=e*i.margin,e-=e*i.margin;var n=i.width,s=i.height,r=20;return"string"==typeof n&&/%$/.test(n)&&(n=parseInt(n,10)/100*t),"string"==typeof s&&/%$/.test(s)&&(s=parseInt(s,10)/100*e),{availableWidth:t,availableHeight:e,slideWidth:n,slideHeight:s,slidePadding:r}}function r(e){var s=app.config.get();e&&app.config.add(e),e.setup&&e.setup(app.config.get()),app.env&&s[app.env]&&app.config.add(s[app.env]),!app.lang&&s.lang&&(app.lang=s.lang),t();n();app.config.add({cachedElements:w}),setTimeout(function(){i()},50)}function a(t,e){t=t||null,e=e||{},app.initialize=!1;window.location.port||null;w.theme=document.querySelector("#theme"),w.wrapper=document.querySelector(".accelerator"),w.slides=document.querySelector(".accelerator .slides"),w.template=document.querySelector(".accelerator .template"),app.queryParams=f(window.location.search),app.queryParams&&(app.queryParams.env&&(app.env=app.queryParams.env),app.queryParams.lang&&(app.lang=app.queryParams.lang),app.queryParams.mode&&(app.mode=app.queryParams.mode)),t?app.config.fetch(t,function(){r(e)}):r(e)}function o(t){return S.call(arguments,1).forEach(function(e){for(var i in e)t[i]=e[i]}),t}function p(t){return Array.prototype.slice.call(t)}function l(t){var e=0;if(t){var i=0;p(t.childNodes).forEach(function(t){"number"==typeof t.offsetTop&&t.style&&("absolute"===t.style.position&&(i+=1),e=Math.max(e,t.offsetTop+t.offsetHeight))}),0===i&&(e=t.offsetHeight)}return e}function c(t,e){t.style.WebkitTransform=e,t.style.MozTransform=e,t.style.msTransform=e,t.style.OTransform=e,t.style.transform=e}function d(){if(window.XMLHttpRequest)return new XMLHttpRequest;try{return new ActiveXObject("Microsoft.XMLHTTP")}catch(t){}try{return new ActiveXObject("Msxml2.XMLHTTP.6.0")}catch(t){}try{return new ActiveXObject("Msxml2.XMLHTTP.3.0")}catch(t){}try{return new ActiveXObject("Msxml2.XMLHTTP")}catch(t){}return!1}function u(t,e){var i=d();if(!i)throw new Error("XMLHttpRequest or ActiveXObject is not available. Cannot get file.");i.open("GET",t),i.onreadystatechange=function(){4===i.readyState&&(i.onreadystatechange=function(){},i.status>=200&&i.status<300||304==i.status||0==i.status&&"file:"==window.location.protocol?e(null,i.responseText):400===i.status?e({error:"Could not locate file"},null):e({error:i.status},null))},i.send(null)}function h(t){var e,i=document.createElement("div"),n=i.style;return t.toLowerCase()in n?e="":"Webkit"+t in n?e="-webkit-":"Moz"+t in n?e="-moz-":"ms"+t in n?e="-ms-":"O"+t in n&&(e="-o-"),e}function f(t){return(t||document.location.search).replace(/(^\?)/,"").split("&").map(function(t){return t=t.split("="),this[t[0]]=t[1],this}.bind({}))[0]}function v(){function t(t){var e=_.template(t);return e()}return t}var g,m="1.4.4",w={},y=1,b=[],S=b.slice;return window.head||(g=document.createElement("script"),g.setAttribute("src","accelerator/lib/head.min.js"),document.head.appendChild(g)),window.addEventListener("resize",t,!1),setTimeout(function(){app.initialize&&app.initialize("config.json")},100),{initialize:a,layout:t,getScale:e,util:{extend:o,getAbsoluteHeight:l,toArray:p,transformElement:c,getFile:u,getBrowserPrefix:h,_templateParser:v},version:m}}();!function(){function t(t){var e=++s+"";return t?t+e:e}var e=[],i=e.push,n=e.slice,s=({}.toString,0),r=app.events={on:function(t,e,i){if(!o(this,"on",t,[e,i])||!e)return this;this._events||(this._events={});var n=this._events[t]||(this._events[t]=[]);return n.push({callback:e,context:i,ctx:i||this}),this},once:function(t,e,i){if(!o(this,"once",t,[e,i])||!e)return this;var n,s=this,r=function(){n||(n=!0,s.off(t,r),e.apply(this,arguments))};return r._callback=e,this.on(t,r,i)},off:function(t,e,i){var n,s,r,a,p,l,c,d;if(!this._events||!o(this,"off",t,[e,i]))return this;if(!t&&!e&&!i)return this._events=void 0,this;for(a=t?[t]:Object.keys(this._events),p=0,l=a.length;p<l;p++)if(t=a[p],r=this._events[t]){if(this._events[t]=n=[],e||i)for(c=0,d=r.length;c<d;c++)s=r[c],(e&&e!==s.callback&&e!==s.callback._callback||i&&i!==s.context)&&n.push(s);n.length||delete this._events[t]}return this},trigger:function(t){if(!this._events)return this;var e=n.call(arguments,1);if(!o(this,"trigger",t,e))return this;var i=this._events[t],s=this._events.all;return i&&p(i,e),s&&p(s,arguments),this},stopListening:function(t,e,i){var n=this._listeningTo;if(!n)return this;var s=!e&&!i;i||"object"!=typeof e||(i=this),t&&((n={})[t._listenId]=t);for(var r in n)t=n[r],t.off(e,i,this),!s&&Object.keys(t._events).length||delete this._listeningTo[r];return this}},a=/\s+/,o=function(t,e,n,s){if(!n)return!0;var r;if("object"==typeof n){for(var o in n)r=[o,n[o]],i.apply(r,s),t[e].apply(t,r);return!1}if(a.test(n)){for(var p=n.split(a),l=0,c=p.length;l<c;l++)r=[p[l]],i.apply(r,s),t[e].apply(t,r);return!1}return!0},p=function(t,e){var i,n=-1,s=t.length,r=e[0],a=e[1],o=e[2];switch(e.length){case 0:for(;++n<s;)(i=t[n]).callback.call(i.ctx);return;case 1:for(;++n<s;)(i=t[n]).callback.call(i.ctx,r);return;case 2:for(;++n<s;)(i=t[n]).callback.call(i.ctx,r,a);return;case 3:for(;++n<s;)(i=t[n]).callback.call(i.ctx,r,a,o);return;default:for(;++n<s;)(i=t[n]).callback.apply(i.ctx,e)}},l={listenTo:"on",listenToOnce:"once"};Object.keys(l).forEach(function(e){var i=l[e];r[e]=function(e,n,s){var r=this._listeningTo||(this._listeningTo={}),a=e._listenId||(e._listenId=t("l"));return r[a]=e,s||"object"!=typeof n||(s=this),e[i](n,s,this),this}})}(),app.util.extend(app,app.events),app.config=function(){function t(t,e){for(var i in e)t[i]=e[i]}function e(t,e){var n=app.cache.get(t);n?(i(JSON.parse(n)),e()):app.util.getFile(t,function(n,s){var r;if(n)throw new Error("Unable to fetch configuration "+t,n);r=JSON.parse(s),r&&i(r),e()})}function i(e){t(p,e)}function n(t){return t?!!p[t]&&p[t]:p}function s(t,e){return!(!t||!e)&&(!(!p[t]||"object"!=typeof p[t]||!p[t][e])&&p[t][e])}function r(t,e){p[t]=e}function a(t,e,i){p[t]&&"object"==typeof p[t]?p[t][e]=i:(p[t]={},p[t][e]=i)}function o(t){p[t]&&i(p[t])}var p={paths:{slides:"slides/<id>/",modules:"modules/<id>/",scripts:"slides/<id>/<id>.js",styles:"slides/<id>/<id>.css",thumbs:"slides/<id>/<id>.png",placeholderThumb:"placeholder_thumb.png",references:"shared/references/<id>.pdf"},width:960,height:700,margin:.1,minScale:.2,maxScale:1,history:!1,keyboard:!0,center:!0,touch:!0,loop:!1,rtl:!1,fragments:!0,embedded:!1,autoSlide:0,mouseWheel:!1,rollingLinks:!1,hideAddressBar:!0,theme:null,transition:"linear",transitionSpeed:"default",backgroundTransition:"default",viewDistance:3,preload:!1,pathToSlides:"slides/<id>/",pathToModules:"modules/<id>/",monitoringAPI:"accelerator/lib/agnitio.js",dependencies:[],lazy:!1,remote:!1,editableTag:"data-ag-editable",csvHeaders:{fieldName:"field_name",originalContent:"original_value",localContent:"localized_value"},iPlanner:{lockedOrientation:null,bounce:!1}};return{fetch:e,add:i,get:n,pluck:s,set:r,update:a,storyboardSetup:o}}(),app.registry=function(){function t(t,e,i){!n[t]||i?(n[t]=e(app),app.registry.trigger("register",t)):window.console&&console.log("Registration of "+t+" ignored. It's already registered.")}function e(t){return!!n[t]}function i(t){return t?e(t)?n[t]:null:n}var n={},s={add:t,exist:e,get:i};return app.util.extend(s,app.events),s}(),app.register=app.registry.add,app.util.extend(app.registry,app.events),app.cache=function(){function t(t,e){s[t]=e}function e(t){return!!s[t]}function i(t){return t?e(t)?s[t]:void 0:s}function n(t){return e(t)&&(s[t]=null),!0}var s={};return{put:t,exist:e,get:i,remove:n}}(),app.remote=function(){function t(t){i=t.role,n=t.path||null,app.config.set("remote",!0)}function e(){if(window.ag&&i){var t=function(){};app.slideshow.on("update:current",function(t){ag.msg.send({name:"slideEnter",value:app.getPath()})}),app.slide.on("state:enter",function(t){ag.msg.send({name:"stateEnter",value:JSON.stringify(t)})}),app.slide.on("reset",function(t){ag.msg.send({name:"stateExit",value:JSON.stringify(t)})}),"contact"===i&&(app.removeNavListeners(),app.slideshow.load=t,app.slideshow.goTo=t,app.slideshow.step=t,app.slideshow.next=t,app.slideshow.prev=t,app.slideshow.left=t,app.slideshow.right=t,app.slideshow.up=t,app.slideshow.down=t,ag.on("goTo",function(t){app.slideshow.__load__(t)}),ag.on("enterState",function(t){var e=JSON.parse(t),i=app.slide.get(e.view);i&&i.goTo(e.id,e.data)}),ag.on("resetState",function(t){var e=JSON.parse(t),i=app.slide.get(e.view);i&&i.reset()})),n&&app.slideshow.__load__(n)}}var i,n;return window.ag&&window.ag.on&&ag.on("registerUser",function(e){t(e)}),{init:t,setup:e}}(),SlideList.prototype.init=function(t){this.list=t,this.current={h:0,v:0}},SlideList.prototype.size=function(){var t=[];return t=t.concat.apply(t,this.list),t.length},SlideList.prototype.get=function(t,e){var i;if(void 0===t&&(t=this.current.h,e=this.current.v),e=e||0,i=this.getType(t)){if("list"===i)return this.list[t][e];if(!e)return this.list[t]}},SlideList.prototype.getIndex=function(t){var e,i=this.list.indexOf(t);return t?i>-1?{h:i,v:0}:(this.list.forEach(function(n,s){"string"!=typeof n&&(i=n.indexOf(t),i>-1&&(e={h:s,v:i}))}),e):this.current},SlideList.prototype.getType=function(t){if(t>-1&&t<this.list.length)return"string"==typeof this.list[t]?"item":"list"},SlideList.prototype.inRange=function(t){return t>-1&&t<this.list.length},SlideList.prototype.isEqual=function(t,e){return JSON.stringify(t)===JSON.stringify(e)},SlideList.prototype._set=function(t){this.current;this.current=t},SlideList.prototype.getList=function(){return this.list},SlideList.prototype.setList=function(t){var e,i=this.get();this.list=t,e=this.getIndex(i),this.inRange(this.current.h)&&e?this._set(e):this._set({h:0,v:0})},SlideList.prototype.goTo=function(t){var t=t||{h:0,v:0};"string"==typeof t&&(t=this.getIndex(t)),t.h=t.h||0,t.v=t.v||0,this.get(t.h,t.v)&&this._set(t)},SlideList.prototype.left=function(){var t={h:this.current.h-1,v:0};this.get(t.h)&&this._set(t)},SlideList.prototype.right=function(){var t={h:this.current.h+1,v:0};this.get(t.h)&&this._set(t)},SlideList.prototype.down=function(){var t={h:this.current.h,v:this.current.v+1};this.get(t.h,t.v)&&this._set(t)},SlideList.prototype.up=function(){var t={h:this.current.h,v:this.current.v-1};this.get(t.h,t.v)&&this._set(t)},SlideList.prototype.getUp=function(){var t={h:this.current.h,v:this.current.v-1},e=this.get(t.h,t.v);if(e)return t},SlideList.prototype.getDown=function(){var t={h:this.current.h,v:this.current.v+1},e=this.get(t.h,t.v);if(e)return t},SlideList.prototype.getRight=function(){var t={h:this.current.h+1,v:0},e=this.get(t.h,0);if(e)return t},SlideList.prototype.getLeft=function(){var t={h:this.current.h-1,v:0},e=this.get(t.h,0);if(e)return t},SlideList.prototype.getNeighbors=function(t){var e=[];return e[0]=this.getLeft(),e[1]=this.getUp(),e[2]=this.getRight(),e[3]=this.getDown(),e},SlideList.prototype.getNext=function(){var t,e={h:this.current.h,v:this.current.v+1},i={h:this.current.h+1,v:0},n=this.get(e.h,e.v);return n?e:(t=this.get(i.h,0))?i:void 0},SlideList.prototype.next=function(){var t=this.getNext();t&&this._set(t)},SlideList.prototype.previous=function(){var t,e={h:this.current.h,v:this.current.v-1},i={h:this.current.h-1,v:0},n=this.get(e.h,e.v);n?this._set(e):(t=this.getType(i.h),"item"===t?this._set(i):"list"===t&&(i.v=this.list[i.h].length-1,this._set(i)))},SlideList.prototype.gotoFirst=function(){this._set({h:0,v:0})},SlideList.prototype.gotoLast=function(){this._set({h:this.list.length-1,v:0})},SlideList.prototype.append=function(t){this.list.push(t)},SlideList.prototype.prepend=function(t){this.list.unshift(t),this._set({h:this.current.h+1,v:0})},SlideList.prototype.insert=function(t,e){this.getType(e.h);this.list.splice(e.h,0,t),e.h<=this.current.h&&this._set({h:this.current.h+1,v:0})},SlideList.prototype.insertNested=function(t,e){var i=this.getType(e.h);"list"===i&&"string"==typeof t&&(this.list[e.h].splice(e.v,0,t),e.h===this.current.h&&e.v<=this.current.v&&this._set({h:e.h,v:this.current.v+1}))},SlideList.prototype.replace=function(t,e){var i,n;"string"==typeof t?(i=t,t=this.getIndex(t)):i=this.get(t.h,t.v),n=this.getType(t.h),"list"===n&&"string"==typeof e?this.list[t.h].splice(t.v,1,e):"item"!==n&&"list"!==n||this.list.splice(t.h,1,e)},SlideList.prototype.remove=function(t){var e,i;"string"==typeof t?(e=t,t=this.getIndex(t)):e=this.get(t.h,t.v),i=this.getType(t.h),"list"===i&&void 0!==t.v?this.list[t.h].splice(t.v,1):"item"!==i&&"list"!==i||this.list.splice(t.h,1)},SlideList.prototype.move=function(t,e){var i,n,s,r;"string"==typeof t?(i=t,t=this.getIndex(t)):i=this.get(t.h,t.v),"string"==typeof e?(n=e,e=this.getIndex(e)):n=this.get(e.h,e.v),s=this.getType(t.h),r=this.getType(e.h),"item"===s&&"item"===r?(t.h<e.h&&(e.h=e.h-1),this.list.splice(t.h,1),this.list.splice(e.h,0,i)):"item"===s&&"list"===r?void 0!==e.v?(t.h<e.h&&(e.h=e.h-1),this.list.splice(t.h,1),this.list[e.h].splice(e.v,0,i)):(t.h<e.h&&(e.h=e.h-1),this.list.splice(t.h,1),this.list.splice(e.h,0,i)):"list"===s&&"item"===r?void 0!==t.v?(this.list[t.h].splice(t.v,1),this.list.splice(e.h,0,i)):(t.h<e.h&&(e.h=e.h-1),this.list.splice(t.h,1),this.list.splice(e.h,0,i)):"list"===s&&"list"===r&&(void 0!==e.v&&void 0!==t.v?(t.h===e.h&&t.v<e.v&&(e.v=e.v-1),this.list[t.h].splice(t.v,1),this.list[e.h].splice(e.v,0,i)):void 0===e.v&&void 0===t.v?(t.h<e.h&&(e.h=e.h-1),this.list.splice(t.h,1),this.list.splice(e.h,0,i)):void 0===e.v&&void 0!==t.v&&(this.list[t.h].splice(t.v,1),this.list.splice(e.h,0,i)))},app.model=function(){function t(t,e){var n=app.cache.get(t);e=e||function(){},n?(i(JSON.parse(n)),e()):app.util.getFile(t,function(n,s){if(n)throw new Error("Unable to fetch model "+t,n);i(JSON.parse(s)),e()})}function e(t){t=t||"local",app.model.trigger("save",{model:j,storage:t})}function i(t){if(!(t.slides&&t.structures&&t.storyboard))throw new Error("Presentation model is incorrectly formatted");j=t,app.model.trigger("set:model")}function n(){return j}function s(t){return!!j.slides[t]&&j.slides[t]}function r(t){return!!j.modules[t]&&j.modules[t]}function a(t){return j.slides[t]?j.slides[t]:!!j.modules[t]&&j.modules[t]}function o(t){return j.slides[t]?j.slides[t]:j.modules[t]?j.modules[t]:j.structures[t]?j.structures[t]:!!j.storyboards[t]&&j.storyboards[t]}function p(t){return!!j.structures[t]&&j.structures[t]}function l(t){return!t&&j.storyboard?j.storyboard:!(!j.storyboards||!j.storyboards[t])&&j.storyboards[t]}function c(t,e){if(!t)return!1;var i=p(t)||l(t);return i?i.content.indexOf(e):void 0}function d(t){return!!(j.slides[t]||j.structures[t]||j.modules[t]||j.storyboards[t])}function u(t){return!!j.slides[t]}function h(t){return!!j.structures[t]}function f(t){return!!j.storyboards[t]}function v(t){return!(!j.storyboards[t]||!j.storyboards[t].linear)}function g(t,e){j.storyboards[t]&&(j.storyboards[t].linear=e)}function m(t){var e=o(t);return!!e.shareable}function w(t){var e;j.slides[t]?e=j.slides[t]:j.structures[t]?e=j.structures[t]:j.storyboards[t]&&(e=j.storyboards[t]),e.shareable||(e.shareable={})}function y(t){var e;j.slides[t]?e=j.slides[t]:j.structures[t]?e=j.structures[t]:j.storyboards[t]&&(e=j.storyboards[t]),e.shareable&&delete e.shareable}function b(t,e){u(t)||(j.slides[t]=e)}function S(t,e){if(!e.name||!e.content)throw new Error("Name and content must be specified when adding structure");h(t)||(j.structures[t]=e)}function E(t,e){if(!e.name||!e.content)throw new Error("Name and content must be specified when adding storyboard");f(t)||(j.storyboards[t]=e)}function _(t){f(t)&&delete j.storyboards[t]}function L(t,e,i){var n=l(t)||p(t);b(e.id,e),n&&n.content.splice(i,0,e.id)}function x(t,e,i,n){var s=l(t);b(e.id,e),s&&s.content.splice(i,1,n)}function T(t,e,i){var n,s=l(t)||p(t);s&&(n=s.content,"string"==typeof n[i.h]?h(n[i.h])?(s=p(n[i.h]),s.content.splice(i.v,1)):n.splice(i.h,1):(n=n[i.h],n.splice(i.v,1)))}function k(t,e,i){var n=o(t);n&&n[e]&&(n[e]=i)}function P(t,e){var i=l(t)||p(t);i&&e&&(i.content=e,app.model.trigger("update:content",{id:t,content:e}))}var j={slides:{},modules:{},structures:{},storyboard:[],storyboards:{}};return{save:e,set:i,get:n,getSlide:s,getModule:r,getView:a,getItem:o,getStructure:p,getStoryboard:l,getSlidePosition:c,fetch:t,exist:d,hasSlide:u,hasStructure:h,hasStoryboard:f,isLinear:v,setLinear:g,isShareable:m,setShareable:w,removeShareable:y,addSlide:b,addStructure:S,addStoryboard:E,insertSlide:L,insertNested:x,removeSlide:T,update:k,updateContent:P,deleteStoryboard:_}}(),app.util.extend(app.model,app.events),app.slideshow=function(t){function e(e){var i=t.getStoryboard(e),n=i.content||[];n.length&&(B[e]={},n.forEach(function(i,n){t.hasStructure(i)&&(t.hasSlide(i)||(B[e][n]=i))}))}function i(e,i){i=i||t.getStoryboard(e);var n=i.content||[],s=n.slice(0);if(B[e]={},i)s.forEach(function(i,n){t.hasStructure(i)&&(t.hasSlide(i)||(s.splice(n,1,t.getStructure(i).content.slice(0)),B[e][n]=i))}),R[e]=new SlideList(s),app.dom.add(e,app.dom.get("wrapper")),app.dom.trigger("new:elements",{views:[{id:e,parent:"presentation"}]});else{if(i=t.getItem(e),!i)throw new Error('Not able to find content with id "'+e+'"');i.content?R[e]=new SlideList(i.content):R[e]=new SlideList([e])}if(R[e])return R[e]}function n(e){var n,s,r={},a={},o=b(e),c=o.slideshow,d=o.chapter,u=o.slide,h={h:0,v:0},f=!1;c!==F&&(s=app.view.get(F),f=!0,s&&s.onExit&&s.onExit(),app.slideshow.trigger("unload",{id:F})),F&&(r={index:l(),id:p(),classes:["present","past"]}),R[c]||i(c),R[c]&&(F=c,u&&!d?h=l(u):d&&!u?h.h=y(d,c):d&&u&&(t.isLinear(c)?h=l(u):(h.h=y(d,c),h.v=t.getSlidePosition(d,u),h.v=h.v>-1?h.v:0)),R[c].goTo(h),a={index:l(),id:p(),classes:["past future","present"]},f&&(app.slideshow.trigger("load",{id:c}),n=app.view.get(c),n&&n.onEnter&&n.onEnter()),app.slideshow.trigger("update:current",{current:a,prev:r}))}function s(t){n(t)}function r(t){var t=t||F;if(t)return R[t]}function a(t){var e=l();R[t.id]&&(i(t.id),g({h:0,v:0},e))}function o(){if(F)return F}function p(t,e){if(F)return R[F].get(t,e)}function l(t){if(F)return R[F].getIndex(t)}function c(){if(F)return R[F].list.length}function d(){if(F)return R[F].size()}function u(t){if(F)return R[F].getType(t)}function h(t,e){return JSON.stringify(t)===JSON.stringify(e)}function f(t,e,i){var n="",s="";return i||t.h===e.h&&(t.v>0||e.v>0)&&(n+="stack"),h(e,t)?(s+=" past future archived",n+=" present"):t.h<e.h?(s+=" past present stack archived",n+=" future"):t.h===e.h&&t.v<e.v?(s+=" past present archived",n+=" future"):(s+=t.h===e.h?" past present archived":" present future stack archived",n+=" past"),[s,n]}function v(t){var e=R[F].getNeighbors(),i=[],n=app.slideshow.getIndex(),s=R[F].getIndex(t.prevId),r=app.model.isLinear(F);e.forEach(function(e){var a,o;e&&!h(s,e)&&(a=R[F].get(e.h,e.v),o={index:e,id:a,classes:f(n,e,r)},a!==t.id&&i.push(o))}),app.slideshow.trigger("load:neighbors",{updates:i})}function g(t,e){var i,n,s=app.model.isLinear(F);i={index:t,id:R[F].get(),classes:f(t,t,s)},e&&(n={index:e,id:R[F].get(e.h,e.v),classes:f(t,e,s)}),app.slideshow.trigger("update:current",{current:i,prev:n})}function m(){var t=F,e=B[F][l().h]||null;return e&&(t+="/"+e),t+="/"+R[F].get()}function w(t,e,i){var n,s,i=i||F,r=app.model.isLinear(i);if(!R[i])throw new Error("Slideshow is not initialized. Use 'app.slideshow.init(id)' to initialize.");s=l(),r&&("right"===t&&(t="next"),"left"===t&&(t="previous")),R[i][t].apply(R[i],e),n=l(),g(n,s),R[i].isEqual.apply(R[i],[s,n])?("next"!==t&&"right"!==t||app.slideshow.trigger("slideshow:end",{id:i}),"down"===t&&app.slideshow.trigger("chapter:end",{id:i}),"previous"!==t&&"left"!==t||app.slideshow.trigger("slideshow:start",{id:i}),"up"===t&&app.slideshow.trigger("chapter:start",{id:i})):app.slideshow.trigger("navigate")}function y(e,i){if(!e)return-1;var i=i||F,n=t.getStoryboard(i);return n?n.content.indexOf(e):-1}function b(e){e=e||app.getPath();var i,n,s,r=e.split("/");return""===r[0]&&r.splice(0,1),i=r[0],r[2]?(n=r[1],s=r[2]):r[1]?t.hasStructure(r[1])&&!t.hasSlide(r[1])?(n=r[1],s=null):(n=null,s=r[1]):(n=null,s=null),{slideshow:i,chapter:n,slide:s}}function S(e){if(!e)return!1;var i=!0,n=b(e);return n.slideshow?(t.hasStoryboard(n.slideshow)||(i=!1),t.hasSlide(n.slideshow)&&(i=!0)):i=!1,n.chapter&&!t.hasStructure(n.chapter)&&(i=!1),n.slide&&!t.hasSlide(n.slide)&&(i=!1),i}function E(t,e,i){var n,s,i=i||F,r={h:0,v:0};if(e){if(n=y(e,i),n<0)return!1;if(r.h=n,s=R[i].list[n].indexOf(t),s<0)return!1;r.v=s}else if(s=R[i].getIndex(t))r=s;else{if(n=y(t,i),n<0)return!1;r.h=n}return w("goTo",[r],i),!0}function _(t,e){w(t,[],e)}function L(){_("next")}function x(){_("previous")}function T(){_("left")}function k(){_("right")}function P(){_("up")}function j(){_("down")}function I(){_("gotoFirst")}function N(){_("gotoLast")}function q(t,e,i){var i=i||F;R[i]&&R[i][t].apply(R[i],e)}function A(t,e){q("append",[t],e)}function M(t,e){q("prepend",[t],e)}function O(t,e,i){q("insert",[t,e],i)}function C(t,e,i){q("insertNested",[t,e],i)}function $(t,e,i){q("move",[t,e],i)}function H(t,e,i){q("replace",[t,e],i)}function z(t,e){q("remove",[t],e),p()?update({index:l(),id:p(),classes:["past future","present"]}):x()}if(!t)throw new Error("app.model module is required for app.slideshow");var s,R={},F="",B={};return app.listenTo(app.model,"update:content",a),app.on("enter:element",v),{init:i,load:s,__load__:n,inspect:r,getId:o,get:p,getIndex:l,getPath:m,getLength:c,getSize:d,getType:u,resolve:b,pathExist:S,updateChapterMap:e,goTo:E,step:_,next:L,prev:x,left:T,right:k,up:P,down:j,first:I,last:N,prepend:M,append:A,insert:O,insertNested:C,move:$,remove:z,replace:H}}(app.model),app.util.extend(app.slideshow,app.events),app.dom=function(t,e,i){function n(t){return t&&T[t]?T[t]:t?void 0:T}function s(t,e){e&&!T[t]&&(T[t]=e)}function r(){return P}function a(t){P=t}function o(t,e,i){var n=[];return t?(n=t.className.split(" "),e.forEach(function(t){var e=n.indexOf(t);e>-1&&n.splice(e,1)}),i.forEach(function(t){var e=n.indexOf(t);e===-1&&t&&n.push(t)}),n):[]}function p(){var t=app.slideshow.get(),e=O?O.id:null;M&&M.id===t||(O=M||null,M=T[t],$&&clearTimeout($),P.push(t),app.dom.trigger("element:enter",{id:t,prevId:e}),app.trigger("enter:element",{id:t,prevId:e}))}function l(t){$&&clearTimeout($),$=setTimeout(function(){p()},q)}function c(t){return new Promise(function(n,s){var r,a=t.id,o=t.model||{},p=t.type||"slide",l="",c=null,h=null,f=app.registry.get(a)||{},v=e.get("lazy");T[t.id]&&n(T[t.id]);var g=function(t){if(t&&"<"===t.trim().charAt(0)||n(null),app.lang){var e="module"===p?I:j;e=e.replace("<id>",a)+"translations/"+app.lang+".json";var i=app.cache.get(e);i&&(t=app.template(t,JSON.parse(i)))}else H&&(t=H(t));if(h=z.parse(t),c=h.querySelector("#"+a)||h,!c.tagName&&(c=document.createElement("article"),c.setAttribute("id",a),"slide"===p&&c.classList.add("slide"),c.appendChild(h),c.querySelector("title")))return console.error("Could not get template for "+a),void n(null);if(i){i=JSON.parse(i);var s=c.querySelectorAll("[data-ag-local]");app.util.toArray(s).forEach(function(t){var e=t.getAttribute("data-ag-local");i[e]&&(t.innerHTML=i[e])})}n(c)};f.hasOwnProperty("template")?g(f.template):(f.hasOwnProperty("templateUrl")?l=f.templateUrl:o.files&&o.files.templates&&(l=o.files.templates[0],/\.html$/.test(l)||(l=l.split("."),l[l.length-1]="html",l=l.join("."))),app.cache.exist(l)?g(app.cache.get(l)):l?i.getFile(l,function(t,e){t||!e?("slide"===p&&(c=document.createElement("div"),c.id=a,c.classList.add("slide"),c.innerHTML="<h2>Missing template: "+l+"</h2>"),n(c)):v?g(e):(r=u([{id:a}],!0),r.length?d(r).then(function(){g(e)}):g(e))}):n(null))})}function d(t){return new Promise(function(e,i){t.reduce(function(t,e){return t.indexOf(e)<0&&t.push(e),t},[]);head.load(t,function(){e()})})}function u(t,i){var n=[];return i=i||e.get("lazy"),i&&t.forEach(function(t){var e=t.id;if(t.getAttribute&&(e=t.getAttribute("data-module")||t.getAttribute("data-partial")),!k[e]){var i=app.model.getView(e)||null,s=[];i&&i.files&&(i.files.styles=i.files.styles||[],i.files.scripts=i.files.scripts||[],s=s.concat(i.files.styles),s=s.concat(i.files.scripts)),n=n.concat(s)}}),n}function h(t){return new Promise(function(e,i){var n=t.getAttribute("data-module")||t.getAttribute("data-partial"),s=app.model.getView(n),r=n+"_"+(new Date).getTime(),a=t.id||r,o=t.hasAttribute("data-module")?"module":"slide";c({id:n,model:s,type:o}).then(function(i){i&&(i.id&&i.removeAttribute("id"),t.appendChild(i)),t.id=a,T[a]=t,e({id:a,moduleId:n,el:i})},function(i){t.id=a,T[a]=t,e({id:a,moduleId:n,el:null})})})}function f(t){var e=Array.prototype.slice.call(t.querySelectorAll("[data-module]")),i=Array.prototype.slice.call(t.querySelectorAll("[data-partial]")),n=e.concat(i);return n}function v(t,e){var i,n=f(t);n.length&&(i=u(n),i.length?d(i).then(function(){n.forEach(function(t){h(t).then(function(t){t.el&&v(t.el,t.id),app.dom.trigger("new:elements",{views:[{id:t.id,scriptId:t.moduleId,parent:e,el:t.el}]})})})}):n.forEach(function(t){h(t).then(function(t){t.el&&v(t.el,t.id),app.dom.trigger("new:elements",{views:[{id:t.id,scriptId:t.moduleId,parent:e,el:t.el}]})})}))}function g(t,e){var i=n(t);e=e||T.slides,i&&(e.appendChild(i),app.dom.trigger("new:elements",{views:[{id:t}]}))}function m(t,e){e=e||T.slides;var i=T[t]||null;i&&e.removeChild(i),T[t]=null,k[t]=!1}function w(t){T[t]&&T[t].classList.add("archived")}function y(t,e,i){e=e||!1,i=i||T.slides,S(t).then(function(t){b(t.elements,t.views,e,i)})}function b(t,e,i,n){n.appendChild(t),app.dom.trigger("new:elements",{views:e}),i&&(E(),$&&clearTimeout($),$=setTimeout(function(){p()},q))}function S(t){return new Promise(function(e,i){var n=document.createDocumentFragment(),s=0,r=t.length,a=[],p=u(t)||[],l=function(){s+=1,s===r&&(p.length?d(p).then(function(){e({elements:n,views:a})}):e({elements:n,views:a}))},g=function(t,e){var i=f(t);i.length&&(r+=i.length,p=p.concat(u(i)),i.forEach(function(t){h(t).then(function(t){t.el&&v(t.el,t.id),a.push({id:t.id,scriptId:t.moduleId,parent:e}),l()},function(){l()})}))};t.forEach(function(t){var e=app.model.getSlide(t.id),i=[];k[t.id]?(T[t.id]&&t.classes&&(i=o(T[t.id],t.classes[0].split(" "),t.classes[1].split(" ")),T[t.id].className=i.join(" ")),l()):(k[t.id]=!0,c({id:t.id,model:e,type:"slide"}).then(function(e){return e?(t.classes&&(i=o(e,t.classes[0].split(" "),t.classes[1].split(" ")),e.className=i.join(" ")),T[t.id]=e,g(e,t.id),n.appendChild(e),a.push({id:t.id,parent:"presentation"}),void l()):new Error("Not able to get element for "+t.id)},function(e){l(),console.log("Not able to get element for",t.id)}))})})}function E(t){var t=t||app.slideshow.get(),e=T[t],i=e.getAttribute("data-state");C&&C.split(" ").forEach(function(t){document.documentElement.classList.remove(t)}),i&&(C=i,i.split(" ").forEach(function(t){document.documentElement.classList.add(t)}))}function _(t){var e=[];T[t.current.id]||y([t.current],!0);var i=function(t,e,i,n){if(!n){t.setAttribute("data-incorrect","ultra");var s=function(t){t.classList.contains("past")&&(t.classList.remove("past"),t.classList.add("future"))},r=function(t){t.classList.contains("future")&&(t.classList.remove("future"),t.classList.add("past"))},a=function(t){t.classList.contains("stack")||t.classList.add("stack")},o=function(t){t.classList.contains("stack")&&t.classList.remove("stack")};i.h>e.h?(o(t),s(t)):i.h<e.h?(o(t),r(t)):i.v<e.v?(a(t),r(t)):i.v>e.v&&(a(t),s(t))}},n=function(){l(t.current.id),e=o(T[t.current.id],t.current.classes[0].split(" "),t.current.classes[1].split(" ")),T[t.current.id].className=e.join(" "),E(t.current.id)},s=function(t){t?document.getElementById("presentation").setAttribute("data-transition-speed",app.config.get("transitionSpeed")):document.getElementById("presentation").setAttribute("data-transition-speed","ultra")};if(T[t.prev.id]&&(s(!0),e=o(T[t.prev.id],t.prev.classes[0].split(" "),t.prev.classes[1].split(" ")),T[t.prev.id].className=e.join(" ")),M&&T[t.current.id]){var r=new Date,a=T[t.current.id];if(r-A<400)s(!1),x&&clearTimeout(x),n();else{var c=t.prev.id==t.current.id;i(a,t.prev.index,t.current.index,c),x=setTimeout(function(){s(!0),a.removeAttribute("data-incorrect"),n()},20)}A=r}else T[t.current.id]&&($&&clearTimeout($),p(),E(t.current.id))}function L(t){var e,i=[],n=!1,s=t.updates.length,r=function(t){var e=T[t.id],n=[];e?(n=o(e,t.classes[0].split(" "),t.classes[1].split(" ")),e.className=n.join(" ")):(i.push(t),P.push(t.id))};for(e=0;e<s;e++)r(t.updates[e]);y(i,n)}if(!t||!e||!i)throw new Error("app.dom requires following modules to be loaded: app.slideshow, app.config, app.util");app.listenTo(app.slideshow,"update:current",function(t){_(t)}),app.listenTo(app.slideshow,"load:neighbors",function(t){L(t)});var x,T=e.get("cachedElements"),k={},P=[],j=e.pluck("paths","slides")||"slides/<id>/",I=e.pluck("paths","modules")||"modules/<id>/",N=e.get("transitionSpeed")||"default",q=800,A=(document.createElement("div"),new Date);T||(T={},T.theme=document.querySelector("#theme"),T.wrapper=document.querySelector(".accelerator"),T.slides=document.querySelector(".accelerator .slides"));var M,O,C,$;"default"!==N&&("fast"===N?q=300:"slow"===N&&(q=1100));var H=(function(){var t=document.createElement("div");return void 0!==t.style.transform?"transform":void 0!==t.style.webkitTransform?"-webkit-transform":void 0!==t.style.MozTransform?"-moz-transform":void 0!==t.style.msTransform?"-ms-transform":void 0!==t.style.OTransform?"-o-transform":"transform"}(),function(){var t=app.registry.get("templateParser");return t}()),z=function(){function t(t,e){if("string"!=typeof t)throw new TypeError("String expected");e||(e=document);var i=/<([\w:]+)/.exec(t);
if(!i)return e.createTextNode(t);t=t.replace(/^\s+|\s+$/g,"");var s=i[1];if("body"==s){var r=e.createElement("html");return r.innerHTML=t,r.removeChild(r.lastChild)}var a=n[s]||n._default,o=a[0],p=a[1],l=a[2],r=e.createElement("div");for(r.innerHTML=p+t+l;o--;)r=r.lastChild;if(r.firstChild==r.lastChild)return r.removeChild(r.firstChild);for(var c=e.createDocumentFragment();r.firstChild;)c.appendChild(r.removeChild(r.firstChild));return c}var e=document.createElement("div");e.innerHTML='  <link/><table></table><a href="/a">a</a><input type="checkbox"/>';var i=!e.getElementsByTagName("link").length;e=void 0;var n={legend:[1,"<fieldset>","</fieldset>"],tr:[2,"<table><tbody>","</tbody></table>"],col:[2,"<table><tbody></tbody><colgroup>","</colgroup></table>"],_default:i?[1,"X<div>","</div>"]:[0,"",""]};return n.td=n.th=[3,"<table><tbody><tr>","</tr></tbody></table>"],n.option=n.optgroup=[1,'<select multiple="multiple">',"</select>"],n.thead=n.tbody=n.colgroup=n.caption=n.tfoot=[1,"<table>","</table>"],n.text=n.circle=n.ellipse=n.line=n.path=n.polygon=n.polyline=n.rect=[1,'<svg xmlns="http://www.w3.org/2000/svg" version="1.1">',"</svg>"],{parse:t}}();return{get:n,add:s,getElement:c,getHistory:r,setHistory:a,load:S,insert:y,remove:m,archive:w,render:g,resolveModules:v,parse:z.parse}}(app.slideshow,app.config,app.util),app.util.extend(app.dom,app.events),app.slide=app.view=app.module=function(t){function e(t,e){return Object.hasOwnProperty.call(t,e)}function i(t,e){var i=t?t[e]:void 0;return"function"==typeof i?t[e]():i}function n(t){var t=t||T;if(t)return k[t]}function s(t){if(!t)throw new Error("Need to supply slide id when creating new instance");var e=app.model.getView(t);if(e)return e}function r(t){t.views.length&&t.views.forEach(function(t){p(t)})}function a(t){return new Promise(function(e,i){k[t]?e(k[t]):app.dom.load([{id:t,classes:["present","future"]}]).then(function(t){e(t.views[0])})})}function o(t,e){var i,n=k[e.id];n&&(e.parent&&(i=k[e.parent],i||(i=k.presentation),n.states&&i._injectStates(e.id,n.states),n.parentId=e.parent,n.parent=i,i.children&&i.children.push(e.id)),n.onRender&&n.onRender(t),app.slide.trigger("slide:render",{id:e.id}))}function p(e){var i,n,s=t.get(e.id)||null,r={},a=e.scriptId||e.id;if(!k[e.id]){if(s||(s=document.getElementById(e.id)),r.el=s,r.id=e.id,r.scriptId=a,e.el&&(r.$el=e.el),n=app.registry.get(a)||{},n.children=[],n&&app.util.extend(r,n),i=_.extend(r),k[e.id]=new i,k[e.id].publish){k[e.id].props=app.util.extend({},k[e.id].publish);for(prop in k[e.id].props)k[e.id].props[prop]&&k[e.id].props[prop].push&&(k[e.id].props[prop]=k[e.id].props[prop][0])}else k[e.id].props={};app.util.toArray(s.attributes).forEach(function(t){var i=t.name.replace(/-([a-z])/g,function(t){return t[1].toUpperCase()});k[e.id].props[i]=t.value||!0})}window.setTimeout(function(){o(s,e)})}function l(e,i){var n=e.id,s=k[n]||{},r=t.get(n)||null,i=i||!1;return T&&!i&&c(T),s&&(0===s._state&&r.classList.add("state-default"),s.onEnter&&s.onEnter(r),s.children&&s.children.length&&s.children.forEach(function(t){l({id:t},!0)})),i||(T=n,app.slide.trigger("slide:enter",e)),s}function c(e,i){var n=k[e]||{},s=t.get(e)||null,i=i||!1;return n&&(n.onExit&&n.onExit(s),n.children&&n.children.length&&n.children.forEach(function(t){c(t,!0)})),i||app.slide.trigger("slide:exit",{id:e}),n}function d(){var t;if(T)return t=k[T],!!t&&t.next()}function u(){var t;if(T)return t=k[T],!!t&&t.previous()}function h(t){var e;T&&(e=k[T],e.goTo(t))}function f(t){var e;T&&(e=k[T],e.toggle(t))}function v(){var t;T&&(t=k[T],t.reset())}function g(t,e){var i=k[t]||null;i&&(e||i.el.classList.contains("archived"))&&(i.children&&i.children.forEach(function(t){app.slide.remove(t,!0)}),i._removeElement())}var m=/^\s*</,w="undefined"!=typeof Element&&Element.prototype||{},y=w.addEventListener||function(t,e){return this.attachEvent("on"+t,e)},b=w.removeEventListener||function(t,e){return this.detachEvent("on"+t,e)},S=function(t,e){for(var i=0,n=t.length;i<n;i++)if(t[i]===e)return i;return-1},E=w.matches||w.webkitMatchesSelector||w.mozMatchesSelector||w.msMatchesSelector||w.oMatchesSelector||function(t){var e=(this.parentNode||document).querySelectorAll(t)||[];return~S(e,this)};app.extend=function(t,i){var n,s=this;n=t&&e(t,"constructor")?t.constructor:function(){return s.apply(this,arguments)},app.util.extend(n,s,i);var r=function(){this.constructor=n};return r.prototype=s.prototype,n.prototype=new r,t&&app.util.extend(n.prototype,t),n.__super__=s.prototype,n};var _=function(t){t&&Object.keys(t).forEach(function(e){x.indexOf(e)!==-1&&(this[e]=t[e])},this),this._elements={},this._ensureElement(),this._createStateMap(),this.initialize.apply(this,arguments)},L=/^(\S+)\s*(.*)$/,x=["model","collection","el","id","attributes","className","tagName","events"];if(app.util.extend(_.prototype,app.events,{initialize:function(){},_state:0,states:[],rotate:!1,_domEvents:[],$:function(t){var e="querySelectorAll";return this._elements[t]?this._elements[t]:("#"===t[0]&&(e="querySelector"),"<"===t[0]?app.dom.parse(t):(this._elements[t]=this.el[e](t),this._elements[t]))},getState:function(){return this.states[this._state-1]?this.states[this._state-1]:null},addState:function(t){t.id&&this.states.push(t),this._createStateMap()},stateIs:function(t){var e=this.states[this._state-1];return!(!e||e.id!==t)},stateIsnt:function(t){var e=this.states[this._state-1];return!e||e.id!==t},next:function(){return this._setState(this._state+1)},previous:function(){return this._setState(this._state-1)},goTo:function(t,e){var i;"string"==typeof t?(i=this._stateMap[t],i>0&&this._setState(i,e)):this._setState(t,e)},getIndex:function(t){return t?this._stateMap[t]?this._stateMap[t]:-1:this._state},reset:function(){this._setState(0)},toggle:function(t){var e=this._stateMap[t];e===this._state?this.reset():this.goTo(t)},_setState:function(t,e){var i,n=this._state,s={},r=this.states[t-1];if(t!==n)return this.states.length&&(r||0===t)?(i=r||{id:null},n&&(s=this.states[n-1],this.el.classList.remove("state-"+s.id),s.onExit&&s.onExit.bind(this)(i.id),0===t&&(this.el.classList.add("state-default"),this.trigger("reset",{id:s.id,next:i.id,view:this.id}),app.slide.trigger("reset",{id:s.id,next:i.id,view:this.id}))),this._state=t,0!==t&&(s.id=s.id||null,this.el.classList.remove("state-default"),this.el.classList.add("state-"+i.id),i.onEnter&&i.onEnter.bind(this)(s.id,e),this.trigger("state:enter",{id:i.id,previous:s.id,view:this.id,data:e}),app.slide.trigger("state:enter",{id:i.id,previous:s.id,view:this.id,data:e})),!0):!(!this.rotate||t!==this.states.length+1)&&(this._setState(1),!0)},_injectStates:function(t,e){var i=this._stateMap[t],n=this,s=1;i&&(e.forEach(function(e,r){n.states.splice(i+r-1,s,{id:t+"-"+e.id,onEnter:function(e,i){var n=app.slide.get(t);n.goTo(r+1)},onExit:function(e,i){var n=app.slide.get(t);n.reset()}}),0===r&&(s=0)}),this._createStateMap())},_createStateMap:function(){var t=this;this._stateMap={},this.states.forEach(function(e,i){e.id&&(t._stateMap[e.id]=i+1),e.include&&(t._stateMap[e.include]=i+1)})},_removeElement:function(){var e=this;this.onRemove&&this.onRemove(this.el),this.undelegateEvents(),Object.keys(this._elements).forEach(function(t){e._elements[t]=null}),t.remove(this.id,this.el.parentNode),k[this.id]=null},_setElement:function(t){if("string"==typeof t)if(m.test(t)){var e=document.createElement("div");e.innerHTML=t,this.el=e.firstChild}else this.el=document.querySelector(t);else this.el=t},setElement:function(t){return this.undelegateEvents(),this._setElement(t),this.delegateEvents(),this},_setAttributes:function(t){for(var e in t)e in this.el?this.el[e]=t[e]:this.el.setAttribute(e,t[e])},delegate:function(t,e,i){"function"==typeof e&&(i=e,e=null);var n=this.el,s=e?function(t){for(var s=t.target||t.srcElement;s&&s!=n;s=s.parentNode)E.call(s,e)&&(t.delegateTarget=s,i(t))}:i;return y.call(this.el,t,s,!1),this._domEvents.push({eventName:t,handler:s,listener:i,selector:e}),s},delegateEvents:function(t){if(!t&&!(t=i(this,"events")))return this;this.undelegateEvents();for(var e in t){var n=t[e];"function"!=typeof n&&(n=this[t[e]]?this[t[e]]:function(){});var s=e.match(L);this.delegate(s[1],s[2],n.bind(this))}return this},undelegate:function(t,e,i){if("function"==typeof e&&(i=e,e=null),this.el)for(var n=this._domEvents.slice(),s=0,r=n.length;s<r;s++){var a=n[s],o=!(a.eventName!==t||i&&a.listener!==i||e&&a.selector!==e);o&&(b.call(this.el,a.eventName,a.handler,!1),this._domEvents.splice(S(n,a),1))}return this},undelegateEvents:function(){if(this.el){for(var t=0,e=this._domEvents.length;t<e;t++){var i=this._domEvents[t];b.call(this.el,i.eventName,i.handler,!1)}this._domEvents.length=0}return this},_createElement:function(t){return document.createElement(t)},_ensureElement:function(){if(this.el)this.setElement(i(this,"el"));else{var t=app.util.extend({},i(this,"attributes"));this.id&&(t.id=i(this,"id")),this.className&&(t["class"]=i(this,"className")),this.setElement(this._createElement(i(this,"tagName"))),this._setAttributes(t)}}}),_.extend=app.extend,!t)throw new Error("app.dom module is required for app.slide");var T,k={};return app.listenTo(t,"new:elements",r),app.listenTo(t,"element:enter",l),{get:n,getInstance:s,load:a,remove:g,views:k,enter:l,exit:c,next:d,prev:u,goTo:h,toggle:f,reset:v}}(app.dom),app.util.extend(app.slide,app.events),app.history=function(t){function e(t){a||(r=t?t+"&":"?",app.listenTo(app.slide,"slide:enter",s),app.listenTo(app.slide,"state:enter",i),app.listenTo(app.slide,"reset",n),a=!0)}function i(t){var e=app.slide.get();if(e&&e.stateIs(t.id)){var i={stateId:t.id,path:app.getPath()};window.history.replaceState(i,"",r+"path="+i.path+"&state="+t.id)}}function n(t){var e={stateId:"default",path:app.getPath()};window.history.replaceState(e,"",r+"path="+e.path)}function s(t){var e=app.slide.get().getState(),i={stateId:"default",path:app.getPath()};t.view&&(e=t);var n=r+"path="+i.path;e&&(i.stateId=e.id,n+="&state="+e.id),window.history.pushState(i,"",n)}if(!t)throw new Error("app.slide module is required for app.history");var r,a=!1;return window.addEventListener("popstate",function(t){t.state&&t.state.path!==app.getPath()&&app.goTo(t.state.path)}),{init:e,setStates:i}}(app.slide),app.template=function(){"use strict";var t={evaluate:/<%([\s\S]+?)%>/g,interpolate:/<%=([\s\S]+?)%>/g,escape:/<%-([\s\S]+?)%>/g},e=/.^/,i={"\\":"\\","'":"'",r:"\r",n:"\n",t:"\t",u2028:"\u2028",u2029:"\u2029"};for(var n in i)i[i[n]]=n;var s=/\\|'|\r|\n|\t|\u2028|\u2029/g,r=function(n,r,a){t.variable=a;var o="__p+='"+n.replace(s,function(t){return"\\"+i[t]}).replace(t.escape||e,function(t,e){return"'+\n_.escape("+unescape(e)+")+\n'"}).replace(t.interpolate||e,function(t,e){return"'+\n("+unescape(e)+")+\n'"}).replace(t.evaluate||e,function(t,e){return"';\n"+unescape(e)+"\n;__p+='"})+"';\n";t.variable||(o="with(obj||{}){\n"+o+"}\n"),o="var __p='';var print=function(){__p+=Array.prototype.join.call(arguments, '')};\n"+o+"return __p;\n";var p=new Function(t.variable||"obj",o);if(r)return p(r);var l=function(t){return p.call(this,t)};return l.source="function("+(t.variable||"obj")+"){\n"+o+"}",l};return window._&&window._.template?window._.template:(window._={template:r},r)}(),app.msg=function(){function t(){var t=app.dom.get("wrapper"),e=document.createElement("ul");return e.classList.add("app-messages"),t.appendChild(e),e}function e(t,e){var i='<div class="notice-msg">'+e+"</div>";return i+='<div class="notice-accept"><button class="pure-button ag-button-success" onclick="app.msg.accept(\''+t+"')\">Got it!</button></div>"}function i(t,e){var i='<div class="confirm-msg">'+e+"</div>";return i+='<div class="confirm-cancel"><button class="pure-button ag-button-warning" onclick="app.msg.cancel(\''+t+"')\">Cancel</button></div>",i+='<div class="confirm-confirm"><button class="pure-button ag-button-success" onclick="app.msg.accept(\''+t+"')\">Confirm</button></div>"}function n(t){var e='<div class="notice-msg">'+t+"</div>";return e+='<div class="notice-accept"><button class="pure-button ag-button-success" onclick="app.msg.reload()">Refresh</button></div>'}function s(t){var e=document.getElementById("app-msg-"+t);app.msg.trigger("msg:confirmed",{id:t}),window.localStorage.setItem(t,!0),e&&u(e)}function r(t){var e=document.getElementById("app-msg-"+t);app.msg.trigger("msg:cancelled",{id:t}),e&&u(e)}function a(){window.location.reload()}function o(t,i){var n=window.localStorage.getItem(t);n||(i=e(t,i),h(t,i))}function p(t){var e="confirm_"+(new Date).getTime();return t=i(e,t),h(e,t),e}function l(t){t=n(t),f(t)}function c(t){f(t,"alert")}function d(t){f(t)}function u(t){t.classList.remove("active-msg"),setTimeout(function(){v.removeChild(t)},500)}function h(e,i,n){var s=document.createElement("li");s.id="app-msg-"+e,n&&s.classList.add(n+"-msg"),s.innerHTML=i,v||(v=t()),v.insertBefore(s,v.firstChild),setTimeout(function(){s.classList.add("active-msg")},100)}function f(e,i){var n=document.createElement("li");i&&n.classList.add(i+"-msg"),n.innerHTML=e,v||(v=t()),v.insertBefore(n,v.firstChild),setTimeout(function(){n.classList.add("active-msg")},100),setTimeout(function(){n.classList.remove("active-msg")},4e3),setTimeout(function(){v.removeChild(n)},4500)}var v;return{alert:c,notice:o,send:d,accept:s,confirm:p,update:l,cancel:r,reload:a}}(),app.util.extend(app.msg,app.events),app.$=function(){function t(t){t=t||"builder";var i="agnitio-info.js",n=window.location.pathname.match(/\/presentations\/([a-z0-9]+)\/index.html/);l=document.createElement("div"),l.setAttribute("id","builder-plugins"),document.body.appendChild(l),n&&n.length&&(p=n[1]),p&&(i="../viewer/js/agnitio-info.js"),window.CKEDITOR_BASEPATH="ckeditor/",a="https:"===window.location.protocol?"prod":"dev","prod"===a&&/sqa/.test(window.location.host)&&(a="qa"),head.load([i],function(){var i;window.agnitioInfo&&("Builder Online"===window.agnitioInfo.applicationName?i=window.agnitioInfo.socket[a]+"socket.io/socket.io.js":"Builder CLI"===window.agnitioInfo.applicationName&&(i=["http://"+(location.host||"localhost")+window.agnitioInfo.socket[a]+"socket.io/socket.io.js"],"develop"===t&&i.push("http://"+(location.host||"localhost").split(":")[0]+":35729/livereload.js")),i&&head.load(i,function(){e(t)}))})}function e(t){var e,i="?mode="+t;window.agnitioInfo.socket[a].length>1&&(e=window.agnitioInfo.socket[a]),app.lang&&(i+="&lang="+app.lang),o=app.$.socket=window.io(e),app.$.send=function(t,e){e=e||{},app.$.session&&app.$.session.user&&(e.user=app.$.session.user),p&&(e.pid=p),c&&(e.bundle=c),o.emit(t,e)},"builder"===t&&app.$.send("init:builder"),app.history.init(i),o.on("server:error",function(t){app.msg.alert(t.msg)}),o.on("server:confirmation",function(t){app.msg.alert(t.msg)}),o.on("load:plugins",function(t){app.$.trigger("load:plugins",t)}),o.on("initialize:builder",function(t){app.$.isBuilder=!0,document.childNodes[1].classList.add("mode-builder"),t.basePath&&(window.CKEDITOR_BASEPATH=t.basePath),t.tmpPath&&(app.$.tmpPath=t.tmpPath),app.$.trigger("load:plugins",t)})}function i(t){return new Promise(function(e,i){var n=app.registry.get(t);l=l||document.body,n&&!app.$[t]?(app.dom.insert([{id:t}],!1,l),e(!0)):app.$[t]?e(app.$[t]):i("No plugin named "+t+" is registered")})}function n(t,e){ag&&ag.msg.send({name:t,value:e})}function s(){var t={presentation:{styles:["accelerator/css/styles.css","templates/master/**/*.{css,styl}","modules/**/*.{css,styl}","slides/**/*.{css,styl}","shared/**/*.{css,styl}"],scripts:["accelerator/lib/head.min.js","accelerator/js/init.js","templates/master/**/*.{js,coffee}","modules/**/*.{js,html,json,coffee,md,jade}","slides/**/*.{js,html,json,coffee,md,jade}","slides/**/translations/*.json","shared/**/*.{js,html,json,coffee,md,jade}","config.json","presentation.json","references.json"]}},e=app.config.get("bundle")||t;return e}function r(t){t.data&&("string"==typeof t.data&&(t.data=JSON.parse(t.data)),app.$.trigger(t.data.name,t.data))}var a,o,p,l,c=s();return window.addEventListener("message",r,!1),{isBuilder:!1,init:t,load:i,send:n}}(),app.util.extend(app.$,app.events),app.$.on("load:plugins",function(t){t.files&&head.load(t.files,function(){setTimeout(function(){app.$.trigger("plugins:ready")},500)})}),app.$.on("navigate",function(t){t.data&&t.data.path&&app.goTo(t.data.path)}),function(){function t(t){var e=location.hash.substring(1);app.goTo(e)}function e(t){var e=(document.activeElement,!(!document.activeElement||!document.activeElement.type&&!document.activeElement.href&&"inherit"===document.activeElement.contentEditable)),i=app.config.get("keyboard");if(!(e||t.shiftKey&&32!==t.keyCode||t.altKey||t.ctrlKey||t.metaKey)){if(app.isPaused&&[66,190,191].indexOf(t.keyCode)===-1)return!1;var n=!1;if(i&&"object"==typeof i)for(var s in i)if(parseInt(s,10)===t.keyCode){var r=i[s];"function"==typeof r?r.apply(null,[t]):"string"==typeof r&&"function"==typeof app[r]&&app[r].call(),n=!0}if(n===!1)switch(n=!0,t.keyCode){case 33:app.prev();break;case 34:app.next();break;case 37:app.slideshow.step("left");break;case 39:app.slideshow.step("right");break;case 38:app.slideshow.step("up");break;case 40:app.slideshow.step("down");break;case 36:app.slideshow.first();break;case 35:app.slideshow.last();break;case 32:t.shiftKey?app.prev():app.next();break;case 190:case 191:app.togglePause();break;case 70:app.fullScreen();break;default:n=!1}n&&t.preventDefault()}}function i(){document.addEventListener("swipeleft",app.slideshow.right),document.addEventListener("swiperight",app.slideshow.left),document.addEventListener("swipeup",app.slideshow.down),document.addEventListener("swipedown",app.slideshow.up)}function n(){document.removeEventListener("swipeleft",app.slideshow.right),document.removeEventListener("swiperight",app.slideshow.left),document.removeEventListener("swipeup",app.slideshow.down),document.removeEventListener("swipedown",app.slideshow.up)}function s(t){var e=app.config.get();app.model.isLinear(t.id)?(document.removeEventListener("swipeup",app.slideshow.down),document.removeEventListener("swipedown",app.slideshow.up)):e.touch&&(document.removeEventListener("swipeup",app.slideshow.down),document.removeEventListener("swipedown",app.slideshow.up),document.addEventListener("swipeup",app.slideshow.down),document.addEventListener("swipedown",app.slideshow.up))}function r(t,n){t&&document.addEventListener("keydown",e,!1),n&&i()}app.isPaused=!1,app.goTo=function(t,e){e&&app.slide.once("slide:enter",function(){app.slide.goTo(e)}),app.slideshow.load(t)},app.getPath=app.slideshow.getPath,app.addNavListeners=function(){var t=app.config.get(),e=app.slideshow.getId();r(t.keyboard,t.touch),s({id:e})},app.removeNavListeners=function(){document.removeEventListener("keydown",e,!1),n()},app.lock=function(){app.removeNavListeners(),app.trigger("locked")},app.unlock=function(){app.addNavListeners(),app.trigger("unlocked")},app.dispatchEvent=function(t,e){var i=document.createEvent("HTMLEvents",1,2),n=app.dom.get("wrapper")||document;i.initEvent(t,!0,!0),app.util.extend(i,e),n.dispatchEvent(i)},app.fullScreen=function(){var t=document.body,e=t.requestFullScreen||t.webkitRequestFullscreen||t.webkitRequestFullScreen||t.mozRequestFullScreen||t.msRequestFullScreen;e&&e.apply(t)},app.togglePause=function(){var t=app.dom.get("wrapper");t.classList.toggle("paused"),app.isPaused=!app.isPaused,app.isPaused?n():i()},app.autoRun=function(t){setInterval(function(){app.next()},t)},app.next=function(){app.slide.next()||app.slideshow.step("next")},app.prev=function(){app.slide.prev()||app.slideshow.step("previous")},app.start=function(t,e,i,n){function a(){var a=e.get();app.queryParams&&app.queryParams.mode&&app.$.init(app.queryParams.mode),window.ag&&window.ag.on&&(app.config.get("remote")?app.remote.setup():ag.on("registerUser",function(t){app.remote.init(t),app.remote.setup()})),app.initialize?app.listenTo(i,"register",function(t){app.slide.enter({id:t},!0)}):(a.history&&app.history.init(),a.preload&&app.listenTo(n,"load",function(e){var i=[],n=[],s=app.slideshow.get(),r="past",a="future present";i=i.concat.apply(i,app.slideshow.inspect().list),i.forEach(function(e){var i=t.getSlide(e);i.id=i.id||e,e===s?(i.classes=["past future","present"],r="future",a="past present"):i.classes=[a,r],n.push(i)}),app.dom.insert(n)}),a.model=a.model||"presentation.json",r(a.keyboard,a.touch),app.listenTo(n,"load",s),app.model.once("set:model",function(t){var e=window.location.hash,i=(window.location.search,a.startPath||null);e&&e.length>2?i=e.replace("#",""):app.queryParams&&(app.queryParams.path&&app.slideshow.pathExist(app.queryParams.path)?i=app.queryParams.path:app.queryParams.slide&&(i=app.queryParams.slide.replace(/%2F/g,"/"))),app.start.at(i),app.dispatchEvent("ready"),app.trigger("presentation:ready"),app.dom.trigger("new:elements",{views:[{id:"presentation"}]}),a.autoSlide&&app.autoRun(a.autoSlide)}),/\.json$/.test(a.model)?t.fetch(a.model):"object"==typeof a.model?t.set(a.model):(slides=a.model.replace(/,/g," ").replace("  "," ").split(" "),t.set({slides:{},modules:{},structures:{},storyboard:["presentation"],storyboards:{presentation:{content:slides}}})))}function o(e){var i,s=app.dom.get("slides"),r=app.queryParams.state||null;if(!e)if(app.model.hasStoryboard(app.env))e=app.env;else{if(i=t.getStoryboard(),!i||!i[0])throw new Error("No default storyboard or start path defined");e=i[0]}r?app.goTo(e,r):n.__load__(e),app.dom.resolveModules(app.dom.get("wrapper"),"accelerator"),s&&s.classList.remove("no-transition"),app.config.get("startPath")||app.config.set("startPath",e)}return{init:a,at:o}}(app.model,app.config,app.registry,app.slideshow),"onhashchange"in window&&(window.onhashchange=t)}();
/*! head.core - v1.0.2 */
(function(n,t){"use strict";function r(n){a[a.length]=n}function k(n){var t=new RegExp(" ?\\b"+n+"\\b");c.className=c.className.replace(t,"")}function p(n,t){for(var i=0,r=n.length;i<r;i++)t.call(n,n[i],i)}function tt(){var t,e,f,o;c.className=c.className.replace(/ (w-|eq-|gt-|gte-|lt-|lte-|portrait|no-portrait|landscape|no-landscape)\d+/g,"");t=n.innerWidth||c.clientWidth;e=n.outerWidth||n.screen.width;u.screen.innerWidth=t;u.screen.outerWidth=e;r("w-"+t);p(i.screens,function(n){t>n?(i.screensCss.gt&&r("gt-"+n),i.screensCss.gte&&r("gte-"+n)):t<n?(i.screensCss.lt&&r("lt-"+n),i.screensCss.lte&&r("lte-"+n)):t===n&&(i.screensCss.lte&&r("lte-"+n),i.screensCss.eq&&r("e-q"+n),i.screensCss.gte&&r("gte-"+n))});f=n.innerHeight||c.clientHeight;o=n.outerHeight||n.screen.height;u.screen.innerHeight=f;u.screen.outerHeight=o;u.feature("portrait",f>t);u.feature("landscape",f<t)}function it(){n.clearTimeout(b);b=n.setTimeout(tt,50)}var y=n.document,rt=n.navigator,ut=n.location,c=y.documentElement,a=[],i={screens:[240,320,480,640,768,800,1024,1280,1440,1680,1920],screensCss:{gt:!0,gte:!1,lt:!0,lte:!1,eq:!1},browsers:[{ie:{min:6,max:11}}],browserCss:{gt:!0,gte:!1,lt:!0,lte:!1,eq:!0},html5:!0,page:"-page",section:"-section",head:"head"},v,u,s,w,o,h,l,d,f,g,nt,e,b;if(n.head_conf)for(v in n.head_conf)n.head_conf[v]!==t&&(i[v]=n.head_conf[v]);u=n[i.head]=function(){u.ready.apply(null,arguments)};u.feature=function(n,t,i){return n?(Object.prototype.toString.call(t)==="[object Function]"&&(t=t.call()),r((t?"":"no-")+n),u[n]=!!t,i||(k("no-"+n),k(n),u.feature()),u):(c.className+=" "+a.join(" "),a=[],u)};u.feature("js",!0);s=rt.userAgent.toLowerCase();w=/mobile|android|kindle|silk|midp|phone|(windows .+arm|touch)/.test(s);u.feature("mobile",w,!0);u.feature("desktop",!w,!0);s=/(chrome|firefox)[ \/]([\w.]+)/.exec(s)||/(iphone|ipad|ipod)(?:.*version)?[ \/]([\w.]+)/.exec(s)||/(android)(?:.*version)?[ \/]([\w.]+)/.exec(s)||/(webkit|opera)(?:.*version)?[ \/]([\w.]+)/.exec(s)||/(msie) ([\w.]+)/.exec(s)||/(trident).+rv:(\w.)+/.exec(s)||[];o=s[1];h=parseFloat(s[2]);switch(o){case"msie":case"trident":o="ie";h=y.documentMode||h;break;case"firefox":o="ff";break;case"ipod":case"ipad":case"iphone":o="ios";break;case"webkit":o="safari"}for(u.browser={name:o,version:h},u.browser[o]=!0,l=0,d=i.browsers.length;l<d;l++)for(f in i.browsers[l])if(o===f)for(r(f),g=i.browsers[l][f].min,nt=i.browsers[l][f].max,e=g;e<=nt;e++)h>e?(i.browserCss.gt&&r("gt-"+f+e),i.browserCss.gte&&r("gte-"+f+e)):h<e?(i.browserCss.lt&&r("lt-"+f+e),i.browserCss.lte&&r("lte-"+f+e)):h===e&&(i.browserCss.lte&&r("lte-"+f+e),i.browserCss.eq&&r("eq-"+f+e),i.browserCss.gte&&r("gte-"+f+e));else r("no-"+f);r(o);r(o+parseInt(h,10));i.html5&&o==="ie"&&h<9&&p("abbr|article|aside|audio|canvas|details|figcaption|figure|footer|header|hgroup|main|mark|meter|nav|output|progress|section|summary|time|video".split("|"),function(n){y.createElement(n)});p(ut.pathname.split("/"),function(n,u){if(this.length>2&&this[u+1]!==t)u&&r(this.slice(u,u+1).join("-").toLowerCase()+i.section);else{var f=n||"index",e=f.indexOf(".");e>0&&(f=f.substring(0,e));c.id=f.toLowerCase()+i.page;u||r("root"+i.section)}});u.screen={height:n.screen.height,width:n.screen.width};tt();b=0;n.addEventListener?n.addEventListener("resize",it,!1):n.attachEvent("onresize",it)})(window);
/*! head.css3 - v1.0.0 */
(function(n,t){"use strict";function a(n){for(var r in n)if(i[n[r]]!==t)return!0;return!1}function r(n){var t=n.charAt(0).toUpperCase()+n.substr(1),i=(n+" "+c.join(t+" ")+t).split(" ");return!!a(i)}var h=n.document,o=h.createElement("i"),i=o.style,s=" -o- -moz- -ms- -webkit- -khtml- ".split(" "),c="Webkit Moz O ms Khtml".split(" "),l=n.head_conf&&n.head_conf.head||"head",u=n[l],f={gradient:function(){var n="background-image:";return i.cssText=(n+s.join("gradient(linear,left top,right bottom,from(#9f9),to(#fff));"+n)+s.join("linear-gradient(left top,#eee,#fff);"+n)).slice(0,-n.length),!!i.backgroundImage},rgba:function(){return i.cssText="background-color:rgba(0,0,0,0.5)",!!i.backgroundColor},opacity:function(){return o.style.opacity===""},textshadow:function(){return i.textShadow===""},multiplebgs:function(){i.cssText="background:url(https://),url(https://),red url(https://)";var n=(i.background||"").match(/url/g);return Object.prototype.toString.call(n)==="[object Array]"&&n.length===3},boxshadow:function(){return r("boxShadow")},borderimage:function(){return r("borderImage")},borderradius:function(){return r("borderRadius")},cssreflections:function(){return r("boxReflect")},csstransforms:function(){return r("transform")},csstransitions:function(){return r("transition")},touch:function(){return"ontouchstart"in n},retina:function(){return n.devicePixelRatio>1},fontface:function(){var t=u.browser.name,n=u.browser.version;switch(t){case"ie":return n>=9;case"chrome":return n>=13;case"ff":return n>=6;case"ios":return n>=5;case"android":return!1;case"webkit":return n>=5.1;case"opera":return n>=10;default:return!1}}};for(var e in f)f[e]&&u.feature(e,f[e].call(),!0);u.feature()})(window);
/*! head.load - v1.0.3 */
(function(n,t){"use strict";function w(){}function u(n,t){if(n){typeof n=="object"&&(n=[].slice.call(n));for(var i=0,r=n.length;i<r;i++)t.call(n,n[i],i)}}function it(n,i){var r=Object.prototype.toString.call(i).slice(8,-1);return i!==t&&i!==null&&r===n}function s(n){return it("Function",n)}function a(n){return it("Array",n)}function et(n){var i=n.split("/"),t=i[i.length-1],r=t.indexOf("?");return r!==-1?t.substring(0,r):t}function f(n){(n=n||w,n._done)||(n(),n._done=1)}function ot(n,t,r,u){var f=typeof n=="object"?n:{test:n,success:!t?!1:a(t)?t:[t],failure:!r?!1:a(r)?r:[r],callback:u||w},e=!!f.test;return e&&!!f.success?(f.success.push(f.callback),i.load.apply(null,f.success)):e||!f.failure?u():(f.failure.push(f.callback),i.load.apply(null,f.failure)),i}function v(n){var t={},i,r;if(typeof n=="object")for(i in n)!n[i]||(t={name:i,url:n[i]});else t={name:et(n),url:n};return(r=c[t.name],r&&r.url===t.url)?r:(c[t.name]=t,t)}function y(n){n=n||c;for(var t in n)if(n.hasOwnProperty(t)&&n[t].state!==l)return!1;return!0}function st(n){n.state=ft;u(n.onpreload,function(n){n.call()})}function ht(n){n.state===t&&(n.state=nt,n.onpreload=[],rt({url:n.url,type:"cache"},function(){st(n)}))}function ct(){var n=arguments,t=n[n.length-1],r=[].slice.call(n,1),f=r[0];return(s(t)||(t=null),a(n[0]))?(n[0].push(t),i.load.apply(null,n[0]),i):(f?(u(r,function(n){s(n)||!n||ht(v(n))}),b(v(n[0]),s(f)?f:function(){i.load.apply(null,r)})):b(v(n[0])),i)}function lt(){var n=arguments,t=n[n.length-1],r={};return(s(t)||(t=null),a(n[0]))?(n[0].push(t),i.load.apply(null,n[0]),i):(u(n,function(n){n!==t&&(n=v(n),r[n.name]=n)}),u(n,function(n){n!==t&&(n=v(n),b(n,function(){y(r)&&f(t)}))}),i)}function b(n,t){if(t=t||w,n.state===l){t();return}if(n.state===tt){i.ready(n.name,t);return}if(n.state===nt){n.onpreload.push(function(){b(n,t)});return}n.state=tt;rt(n,function(){n.state=l;t();u(h[n.name],function(n){f(n)});o&&y()&&u(h.ALL,function(n){f(n)})})}function at(n){n=n||"";var t=n.split("?")[0].split(".");return t[t.length-1].toLowerCase()}function rt(t,i){function e(t){t=t||n.event;u.onload=u.onreadystatechange=u.onerror=null;i()}function o(f){f=f||n.event;(f.type==="load"||/loaded|complete/.test(u.readyState)&&(!r.documentMode||r.documentMode<9))&&(n.clearTimeout(t.errorTimeout),n.clearTimeout(t.cssTimeout),u.onload=u.onreadystatechange=u.onerror=null,i())}function s(){if(t.state!==l&&t.cssRetries<=20){for(var i=0,f=r.styleSheets.length;i<f;i++)if(r.styleSheets[i].href===u.href){o({type:"load"});return}t.cssRetries++;t.cssTimeout=n.setTimeout(s,250)}}var u,h,f;i=i||w;h=at(t.url);h==="css"?(u=r.createElement("link"),u.type="text/"+(t.type||"css"),u.rel="stylesheet",u.href=t.url,t.cssRetries=0,t.cssTimeout=n.setTimeout(s,500)):(u=r.createElement("script"),u.type="text/"+(t.type||"javascript"),u.src=t.url);u.onload=u.onreadystatechange=o;u.onerror=e;u.async=!1;u.defer=!1;t.errorTimeout=n.setTimeout(function(){e({type:"timeout"})},7e3);f=r.head||r.getElementsByTagName("head")[0];f.insertBefore(u,f.lastChild)}function vt(){for(var t,u=r.getElementsByTagName("script"),n=0,f=u.length;n<f;n++)if(t=u[n].getAttribute("data-headjs-load"),!!t){i.load(t);return}}function yt(n,t){var v,p,e;return n===r?(o?f(t):d.push(t),i):(s(n)&&(t=n,n="ALL"),a(n))?(v={},u(n,function(n){v[n]=c[n];i.ready(n,function(){y(v)&&f(t)})}),i):typeof n!="string"||!s(t)?i:(p=c[n],p&&p.state===l||n==="ALL"&&y()&&o)?(f(t),i):(e=h[n],e?e.push(t):e=h[n]=[t],i)}function e(){if(!r.body){n.clearTimeout(i.readyTimeout);i.readyTimeout=n.setTimeout(e,50);return}o||(o=!0,vt(),u(d,function(n){f(n)}))}function k(){r.addEventListener?(r.removeEventListener("DOMContentLoaded",k,!1),e()):r.readyState==="complete"&&(r.detachEvent("onreadystatechange",k),e())}var r=n.document,d=[],h={},c={},ut="async"in r.createElement("script")||"MozAppearance"in r.documentElement.style||n.opera,o,g=n.head_conf&&n.head_conf.head||"head",i=n[g]=n[g]||function(){i.ready.apply(null,arguments)},nt=1,ft=2,tt=3,l=4,p;if(r.readyState==="complete")e();else if(r.addEventListener)r.addEventListener("DOMContentLoaded",k,!1),n.addEventListener("load",e,!1);else{r.attachEvent("onreadystatechange",k);n.attachEvent("onload",e);p=!1;try{p=!n.frameElement&&r.documentElement}catch(wt){}p&&p.doScroll&&function pt(){if(!o){try{p.doScroll("left")}catch(t){n.clearTimeout(i.readyTimeout);i.readyTimeout=n.setTimeout(pt,50);return}e()}}()}i.load=i.js=ut?lt:ct;i.test=ot;i.ready=yt;i.ready(r,function(){y()&&u(h.ALL,function(n){f(n)});i.feature&&i.feature("domloaded",!0)})})(window);
/*
//# sourceMappingURL=/accelerator/lib/head.min.js.map
*/
app.register('ag-auto-menu', function () {
    var self;

    return {
        template: '<div class="menu-container"><ul class="menu"></ul></div>',
        current: '',
        fallback: '', // if no menu is built
        publish: {
            hide: false, // Should we initially hide menu?
            placement: ['top', 'bottom'], // top or bottom?
            exclude: '', // Some content that should not be in the menu?
            slideshows: '',
            binding: 77,
            trigger: ''
        },
        events: {
            "tap li": "navigate",
            "tap .menu-container": function (e) {
                if (this.stateIsnt('hidden')) {
                    this.goTo('hidden');
                    app.$.toolbar.hide();
                }
                else
                    this.goTo('open');
            },
            "swipeleft": function (event) {
                event.stopPropagation();
            },
            "swiperight": function (event) {
                event.stopPropagation();
            }
        },
        states: [
            {
                id: 'hidden',
                onEnter: function () {
                    if (this.props.placement === 'bottom') {
                        app.util.transformElement(this.$el, 'translate(0,62px)');
                    }
                    else {
                        app.util.transformElement(this.$el, 'translate(0,-62px)');
                    }
                },
                onExit: function () {
                    app.util.transformElement(this.$el, 'translate(0,0)');
                }
            },
            {
                id: 'open'
            }
        ],
        onRender: function (el) {
            self = this;
            /* antwerpes modification */
            self.appWidth = $(window).width();
            /* end antwerpes modification*/

            if (app.env == 'ag-microsites' || app.env == 'ag-remote') {
                $(el).hide();
            }

            self.pathLength = 2; // Default to menu of structures
            app.$.menu = this;
            if (this.props.hide) {
                this.hide();
                if (this.props.binding) {
                    app.config.update('keyboard', parseInt(this.props.binding, 10), function () {
                        app.$.trigger("toggle:menu");
                    })
                }
            }
            app.$.on('toggle:menu', function () {
                this.toggle('hidden');
            }.bind(this));
            if (this.props.placement === 'bottom') el.classList.add('placement-bottom');
            // Are we using this menu with specific slideshows?
            if (this.props.slideshows) {
                this.props.slideshows.replace(/\s+/g, ''); // "one, two" => "one,two"
                this.props.slideshows = this.props.slideshows.split(',');
            }
            app.listenTo(app.slideshow, 'update:current', this.updateCurrent);
            app.listenTo(app.slideshow, 'load', function (data) {
                self.setup(data.id);
            });
            this.layout({scale: app.getScale()});
            app.on('update:layout', this.layout);
            this.setup();
            this.goTo('hidden');
        },
        hide: function () {
            this.goTo('hidden');
        },
        setup: function (id) {
            id = id || app.slideshow.getId();
            if (!this.props.slideshows || this.props.slideshows.indexOf(id) > -1) {
                this.createLinks(id);
                this.updateCurrent();
            }
            else {
                this.removeLinks();
            }
            this.setTrigger();
        },
        setTrigger: function () {
            if (this.props.trigger) {
                var parts = this.props.trigger.split(' ');
                var e = parts[0];
                var selector = parts[1] || null;
                var el = document;
                if (selector) {
                    el = document.body.querySelector(selector);
                }
                if (el) el.addEventListener(e, function () {
                    self.toggle('hidden');
                });
            }
        },
        createLinks: function (structure) {
            var list = this.$('.menu')[0];
            var structure = structure || app.slideshow.getId();
            var html = '';
            var chapter, links;
            var data = structure === 'storyboard' ? app.model.getStoryboard() : app.model.getStoryboard(structure);
            var pathPrefix = structure + '/';
            var excludedLinks = this.props.exclude.split(' ');

            if (data && data.content) {
                links = data.content;

                // If a single item in menu, let's try to dive down and get more links
                if (links.length === 1) {
                    chapter = data.content[0];
                    data = app.model.getStructure(chapter);
                    if (data && data.content) {
                        links = data.content;
                        pathPrefix += chapter + '/';
                        this.pathLength = 3;
                    }
                }

            }

            if (!list) {
                list = document.createElement('ul');
                list.classList.add('menu');
                this.$el.appendChild(list);
            }
            else {
                list.innerHTML = '';
            }

            if (links) {
                links.forEach(function (item, i) {
                    if (typeof item !== 'string') item = item[0];
                    if (excludedLinks.indexOf(item) === -1) {
                        var name = app.model.getItem(item).name;
                        html += '<li data-goto="' + pathPrefix + item + '">' + name + '</li>';
                    }
                });

                list.appendChild(app.dom.parse(html));
                this.createScroller(list);
            }
        },
        setFallback: function (html) {
            if (html) this.fallback = html;
        },
        removeLinks: function () {
            var list = this.$('.menu')[0];
            list.innerHTML = this.fallback;
        },
        updateCurrent: function () {
            var path = app.getPath();
            var parts = path.split('/');
            if (parts.length > 2 && self.pathLength === 2) path = parts[0] + '/' + parts[1];
            if (self.current) self.current.classList.remove('selected');
            self.current = self.el.querySelector('.menu [data-goto="' + path + '"]');
            if (self.current) self.current.classList.add('selected');


            /*antwerpes modification */
            if (self.scroller) {
                var moveTo = 0;
                var pos, outerWidth;
                if (self.getWidth().menu > self.appWidth) {
                    if (self.current) {
                      pos = $(self.current).position();
                      outerWidth = $(self.current).outerWidth();
                    }

                    if (pos) {
                      if ((pos.left + outerWidth) > self.appWidth) {
                          moveTo = self.scroller.config.limitsX[0];

                      }
                      else if ((pos.left + outerWidth) < self.getWidth().menu) {
                          moveTo = 0;
                      }

                      setTimeout(function () {
                          return self.scroller.moveTo(moveTo, 0)
                      }, 500);
                    }
                }
            }


            /*antwerpes modification end */


        },
        navigate: function (event) {
            var link = event.target;
            var path;

            if (link) {
                path = link.getAttribute('data-goto');
                if (path) {
                    app.goTo(path);
                    self.updateCurrent(); // Immediate update of menu
                }
                if (self.props.hide) app.$.trigger("toggle:menu");
            }
        },
        createScroller: function (menu) {
            // TODO: listen to window resize and update limits
            var widths = this.getWidth();
            var appWidth = app.dom.get('wrapper').getBoundingClientRect().width;
            var scrollLimit = appWidth - widths.menu;
            // No scroller necessary if menu isn't bigger than width of view
            if (scrollLimit < 0) {
                this.scroller = new Draggy(menu, {
                    restrictY: true,
                    limitsX: [scrollLimit, 0]
                });
            }
            else {
                this.scroller = null;
            }
        },
        getWidth: function () {
            var links = this.el.querySelectorAll('.menu li');
            var menuWidth = 0;
            var linkWidths = [];
            Array.prototype.slice.call(links).forEach(function (link) {
                var width = link.getBoundingClientRect().width;
                menuWidth += width;
                linkWidths.push(width);
            });
            return {
                menu: menuWidth,
                links: linkWidths
            }
        },
        layout: function (data) {
            // Only apply if zoom is supported
            if (typeof self.el.style.zoom !== 'undefined' && !navigator.userAgent.match(/(iphone|ipod|ipad|android)/gi)) {
                self.el.style.zoom = data.scale;
            }
            // Apply scale transform as a fallback
            else {
                app.util.transformElement(self.el, 'translate(-50%, -50%) scale(' + data.scale + ') translate(50%, 50%)');
            }
        },

        moveToCurrent: function (data) {

        }
    }
});

app.register("ag-cleanup", function () {

    // Get the unique entries of an array
    function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }

    return {
        publish: {
            purgeOnLoad: false,
            timeout: 40, // seconds to wait since last purge.
            limit: 10
        },
        events: {},
        states: [],
        history: [],
        archived: [],
        lastPurge: 0,
        onRender: function (el) {
            var self = this;
            app.slideshow.on('update:current', function (data) {
                var unique, clean;
                var cleanupLength = parseInt(self.props.limit / 2); // We'll keep half our limit as active
                // Add current slide to beginning of history
                if (data.current) {
                    self.history.unshift(data.current.id);
                }
                // Allow animation to run first
                setTimeout(function () {
                    // Make sure we don't have the new slide in history already
                    self.history = self.history.filter(onlyUnique);
                    // console.log("SLIDE HISTORY:", self.history);
                    // If our history is larger than limit, then send for cleanup
                    if (self.history.length > parseInt(self.props.limit)) {
                        clean = self.history.splice(cleanupLength - 1, self.history.length - cleanupLength);
                        console.log("CLEAN FROM HISTORY:", clean);
                        self.clean(clean);
                    }
                }, 800);
            });
            // Fix for bug in Accelerator when slideshow is loaded and first slide has been archived
            app.slide.on('slide:enter', function (data) {
                var el = app.dom.get(data.id);
                if (el) el.classList.remove('archived');
            });
            app.slideshow.on('load', function (data) {
                var info = app.slideshow.resolve();
                var el = app.dom.get(info.slide);
                if (el) el.classList.remove('archived');
            });
            if (this.props.purgeOnLoad) {
                app.slideshow.on('load', function (data) {
                    console.log("Unloading slideshow");
                    // Remove all slides except the newly loaded one
                    // TODO: implement
                });
            }
        },
        onRemove: function (el) {

        },
        // Archive old slides and remove previously archived ones
        clean: function (slides) {
            var now = new Date().getTime();
            var currentSlide = app.slideshow.get();
            slides.forEach(function (id) {
                app.dom.archive(id);
            });
            // Only remove prevously archived if timeout has expired
            if (this.archived.length && now > (this.lastPurge + parseInt(this.props.timeout * 1000))) {
                this.archived.forEach(function (id) {
                    if (id !== currentSlide) app.slide.remove(id, true);
                });
                this.archived = slides;
                this.lastPurge = now;
            }
            else {
                this.archived = this.archived.concat(slides);
            }
        }
    };

});

app.register('ag-indicators', function() {

	var self;

	return {
		publish: {
			type: ['circle', 'rectangle', 'square', 'line']
		},
		template: false,
		onRender: function(el) {
			self = this;
			app.listenTo(app.slideshow, 'load', this.build.bind(this));
			app.listenTo(app.slideshow, 'update:current', this.indicate.bind(this));
			this.el.classList.add('ag-indicators');
			if (this.props.type) this.el.classList.add('ag-indicators-' + this.props.type);
			this.build();
		},
		indicate: function() {
			var i = app.slideshow.getIndex();
			var previous = this.el.querySelector('.active');
			var current = this.el.querySelector('.indicator_' + i.h + '_' + i.v);
			if (previous) previous.classList.remove('active');
			if (current) current.classList.add('active');
		},
		build: function() {
			var list = app.slideshow.inspect().list; // The raw list
			var isLinear = app.model.isLinear(app.slideshow.getId());
			var html = "";
			if (isLinear) {
				this.el.classList.add('ag-indicators-linear');
			}
			else {
				this.el.classList.remove('ag-indicators-linear');
			}
			list.forEach(function(item, i) {
				html += '<ul class="ag-indicator-chapter">';
				if (typeof item === 'string') {
					html += '<li class="indicator_' + i + '_0"></li>'
				}
				else {
					item.forEach(function(id, j) {
						html += '<li class="indicator_' + i + '_' + j + '"></li>'
					})
				}
				html += '</ul>';
			});
			this.el.innerHTML = html;
			this.indicate();
		}
	}

});
app.register("ag-overlay", function() {

	return {
		publish: {
			width: "80%",
			height: "80%",
			noBackground: false,
			noCloseBtn: false,
			content: "No content available"
		},
		events: {
			"tap .ag-overlay-close": "close"
		},
		states: [
			{
				id: "ag-overlay-open",
				onEnter: function() {
					var slide;
					app.lock();
					if (this.slideId) {
						setTimeout(function() {
							slide = app.slide.get(this.slideId);
							if (slide) {
								app.slide.trigger('slide:enter', {id: this.slideId});
								if(slide.onEnter){
									slide.onEnter();
								}
							}
						}.bind(this),300);
					}
					app.trigger('open:overlay', {id: this.id, slideId: this.slideId});
				},
				onExit: function() {
					var slide;
					app.trigger('close:overlay', {id: this.id, slideId: this.slideId});
					app.unlock();
					if (this.slideId) {
						slide = app.slide.get(this.slideId);
						if (slide) {
							app.slide.trigger('slide:exit', {id: this.slideId});
							if(slide.onExit){
								slide.onExit();
							}
						}
						app.slide.remove(this.slideId, true);
					}
				}
			}
		],
		close: function(event) {
			this.reset();
		},
		// Open provided HTML
		open: function(content) {
			content = content || this.props.content;
			if (content) {
				this.$('.ag-overlay-content')[0].innerHTML = content;
			}
			this.goTo('ag-overlay-open');
		},
		// Load a slide into the overlay
		load: function(slideId) {
			this.slideId = slideId;
			this.$('.ag-overlay-content')[0].innerHTML = "";
			// Need to remove slide if already loaded in presentation
			app.slide.remove(slideId, true);
			app.dom.insert([{id: slideId}], false, this.$('.ag-overlay-content')[0]);
			this.goTo('ag-overlay-open');
			// Fetch the slide but don't render it yet
			// app.slide.load(slideId).then(function(data) {
			//   console.log(data);
			//   this.slideId = slideId;
			//   app.dom.render(slideId, this.$('.ag-overlay-content')[0]);
			//   this.goTo('ag-overlay-open');
			// }.bind(this));
			// app.dom.insert([slideId], false, this.$('.ag-overlay-content')[0]);
			// this.slide = app.slide.get(slideId);
			// this.slideEl = app.dom.get(slideId);
		},
		setDimensions: function(width, height) {
			var contentEl = this.$('.ag-overlay-content')[0];
			var closeBtn = this.$('.ag-overlay-x')[0];
			var wMargin;
			var hMargin;
			var wUnit = "%";
			var hUnit = "%";
			// Assume percentage
			if (width < 101) {
				wMargin = (100 - width) / 2;
			}
			// otherwise treat as pixels
			{
				// Only supports % for now
			}
			// Assume percentage
			if (height < 101) {
				hMargin = (100 - height) / 2;
			}
			// otherwise treat as pixels
			{
				// Only supports % for now
			}

			if (contentEl) {
				// contentEl.style.top = hMargin + hUnit;
				// contentEl.style.bottom = hMargin + hUnit;
				// contentEl.style.left = wMargin + wUnit;
				// contentEl.style.right = wMargin + wUnit;
			}
			if (contentEl) {
				// closeBtn.style.top = (hMargin - 1) + hUnit;
				// closeBtn.style.right = (wMargin - 1) + wUnit;
			}

		},
		onRender: function(el) {
			var content = this.el.innerHTML;
			var html = '';
			var width = parseInt(this.props.width);
			var height = parseInt(this.props.height);
			if (!this.props.noBackground) {
				html = '<div class="ag-overlay-background ag-overlay-close"></div>';
			}
			html += '<div class="ag-overlay-content">';
			html += content;
			html += '</div>';
			if (!this.props.noCloseBtn) {
				html += '<div class="ag-overlay-x ag-overlay-close"></div>';
			}
			// html += '</div>';
			this.el.innerHTML = html;
			this.setDimensions(width, height);
		},
		onRemove: function(el) {

		},
		onEnter: function(el) {

		},
		onExit: function(el) {

		}
	}

});
app.register('ag-slide-analytics', function() {

  /**
   * Agnitio Slide Analytics Module
   *
   * This module will save data about
   * the slides visited.
   *
   * Usage:
   * Include <div data-modules="ag-slide-analytics"></div>
   * in index.html. See docs for more info.
   */

  var self;
  return {
    template: false,
    // The interface to this module
    publish: {
      debug: false,
      map: "", // Provide the namespace for map, e.g. "monitorMap" => window.monitorMap
      offset: 0, // e.g. if 1: default/safety/study => safety/study
      skip: ""
    },
    onRender: function(el) {
      self = this;
      app.listenTo(app.slide, 'slide:enter', this.save.bind(this));
      if (this.props.debug) ag.debug(true);
    },
    /**
     * Assign correct id or name
     * Get correct id and name for chapter, subchapter and slide
     * Lookup order:
     * 1. map[id]
     * 2. app.json[type]['name']
     * 3. id
     * @private
     * @param id STRING Id of structure to find label for
     * @param itemType STRING One of 'slide', 'chapter', or 'slideshow'
     */
    assignValues: function(id, itemType) {
      var val;
      var data;
      var name;

      if (itemType === 'slide') {
        data = app.model.getSlide(id);
      }
      else if (itemType === 'chapter') {
        data = app.model.getStructure(id);
      }
      if (itemType === 'slideshow') {
        data = app.model.getStoryboard(id);
      }

      if (data) {
        // If specific id and name has been specified
        // for monitoring in presentation.json
        if (data.monitoring) {
          id = data.monitoring.id || id;
          name = data.monitoring.name || data.name;
        }
        else if (this.map && this.map[id]) {
          id = this.map[id].id || id;
          name = this.map[id].name || data.name;
        }
        else {
          name = data.name;
        }
        return {id: id, name: name};
      }
      return {id: id, name: null};
    },
    save: function(data) {
      var id = data.id;
      var path = app.getPath();
      var index = app.slideshow.getIndex();
      var slideIndex = index.v ? index.v : index.h;
      var components = app.slideshow.resolve();
      var subChapterId = components.chapter || null;
      var chapterId = components.slideshow || null;
      var chapter = {id: null, name: null};
      var subChapter = {id: null, name: null};
      var slide = this.assignValues(id, 'slide');
      
      if (subChapterId) subChapter = this.assignValues(subChapterId, 'chapter');
      if (chapterId) chapter = this.assignValues(chapterId, 'slideshow');

      // Slide id and name are required
      if (!slide.name) {
        if (console.error) console.error('Slide will not be monitored! Name must be specified for "' + data.id + '"');
        return;
      }

      if (window.ag) {
        ag.submit.slide({
          id: slide.id,
          name: slide.name,
          path: path,
          slideIndex: slideIndex,
          chapter: chapter.name,
          chapterId: chapter.id,
          subChapter: subChapter.name,
          subChapterId: subChapter.id
        })
      }
    }
  }
});
app.register('ag-slide-popup', function() {
  return {
    slideEl: null,
    popupScript: null,
    loaded: false,
    popupEl: null,
    template: false,
    publish: {
      slide: null,
      trigger: null,
      noCloseBtn: false,
      popupClass: null
    },
    events: {
      "tap .close-popup-btn": function(e) { e.stopPropagation(); this.close()}
    },
    states: [
      {
        id: 'open',
        label: 'Open popup',
        onEnter: function() {
          if (!this.loaded) this.renderContent();
          if (this.popupScript && this.popupScript.onEnter) this.popupScript.onEnter(this.slideEl);
          this.popupEl.classList.add('state-open');
          app.trigger('open:popup', {id: this.id});
        },
        onExit: function() {
          if (this.popupScript && this.popupScript.onExit) this.popupScript.onExit(this.slideEl);
          this.popupEl.classList.remove('state-open');
          app.trigger('close:popup', {id: this.id});
        }
      }
    ],
    onRender: function(el) {
      var slideId = this.props.slide;
      var self = this;

      this.setPopupClass();
      this.createContainer(self);
      this.setTrigger();
      this.addCloseBtn();

      if (slideId) {
        // Fetch the slide but don't render it yet
        app.slide.load(slideId, function(data) {
          this.slideEl = app.dom.get(slideId);
        });
      }
      else {
        this.slideEl = el.querySelector('[data-partial]');
        this.loaded = true;
      }
    },
    open: function(e) {
      if (this.stateIsnt('open')) this.goTo('open');
    },
    close: function() {
      if (this.stateIs('open')) this.reset();
    },
    renderContent: function() {
      this.loaded = true;
      app.dom.render(this.props.slide, this.popupEl);
      this.popupScript = app.slide.get(this.props.slide);
    },
    createContainer: function(self) {
      self.popupEl = document.createElement('DIV');
      self.popupEl.classList.add("ag-slide-popup");
      if (self.props.popupClass) self.el.classList.add(self.props.popupClass);
      self.el.appendChild(this.popupEl);
    },
    setTrigger: function() {
      if (this.props.trigger) {
        var parts = this.props.trigger.split(' ');
        var e = parts[0];
        var selector = parts[1] || null;

        if (selector) {
          this.parent.delegate(e, selector, this.open.bind(this));
        }
        else {
          this.delegate(e, this.open.bind(this));
        }
      }
    },
    setPopupClass: function(){
      if (this.props.custom) this.props.popupClass = this.props.custom;
    },
    addCloseBtn: function() {
      if (!this.props.noCloseBtn) {
        var el = app.dom.parse('<div class="close-popup-btn"></div>');
        this.popupEl.appendChild(el);
      }
    }
  }
});
app.register("ag-video", function() {

    // This module makes videos stateful so that they'll work remotely
    // ... and fix some layering issues that default controls cause
    // Usage: <div src="modules/rainmaker_modules/ag-video/assets/myvideo.mp4" data-module="ag-video">

    var self = null;
    return {
        publish: {
            src: "",
            poster: "",
            type: "video/mp4",
            monitor: false
        },
        events: {
            "playing video": function() {this.goTo('play')},
            "pause video": function() {this.goTo('pause')},
            "tap .ag-video-play-toggle": 'toggleVideo',
            "tap .ag-upper-click-layer": 'toggleVideo',
            "tap .ag-video-fullscreen-button": 'toggleFullscreen',
            "swipeleft .ag-video-controls": 'noSwipe',
            "swiperight .ag-video-controls": 'noSwipe',
            "swipeleft": 'checkIfSwipe',
            "swiperight": 'checkIfSwipe',
            "swipeup": 'checkIfSwipe',
            "swipedown": 'checkIfSwipe',
            "onDrop .ag-video-seek-ele": 'handleDrop'
        },
        states: [
            {
                id: 'play',
                onEnter: function() {
                    this.videoEle.play();
                }
            },
            {
                id: 'pause',
                onEnter: function() {
                    this.videoEle.pause();
                    if (this.props.monitor) {
                        ag.submit.data({
                            category: "Video viewed",
                            label: this.props.src,
                            value: this.formatTime(this.currentPlayTime),
                            valueType: "time",
                            path: app.getPath(),
                            unique: true
                        });
                    }
                }
            }
        ],
        onRender: function(el) {
            self = this;
            this.currentPlayTime = 0;
            this.videoEle = this.el.querySelector('.ag-video-element');
            this.progressBar = this.el.querySelector('.ag-video-progress-bar');
            this.currentTimeText = this.el.querySelector('.ag-video-current-time');
            this.endTimeText = this.el.querySelector('.ag-video-total-time');
            this.videoContainer = this.el.querySelector('.ag-video-container');
            this.seekEle = this.el.querySelector('.ag-video-seek-ele');
            this.progressBarContainer = this.el.querySelector('.ag-video-progress-container');

            // this.videoEle.addEventListener('error', this.fallBackSrc.bind(this));
            this.videoEle.src = this.props.src;
            this.videoEle.poster = this.props.poster;
            this.videoEle.type = this.props.type;

            // Make sure play and pause is passed on if handled with default controls
            this.videoEle.addEventListener('playing', function() {
                if (self.stateIsnt('play')) self.goTo('play');
            });
            this.videoEle.addEventListener('pause', function() {
                if (self.stateIsnt('pause')) self.goTo('pause');
            });

            // Wait for for everything to finish loading
            setTimeout(function(){
                self.seekHandle = new Draggy(self.seekEle, {
                  restrictY: true, limitsX: [0, self.progressBarContainer.offsetWidth], onChange: self.moveSeekHandle
                });
            },100);
        },
        // If can't load src as specified, then try to load from "slides/[parentId]/[src specified]"
        // This is to make it work if lazyloaded
        // TODO: use slides path in config
        fallBackSrc: function(event) {
            var el = event.target;
            // el.removeEventListener('error', this.fallBackSrc.bind(this));
            el.src = "slides/" + this.parentId + "/" + this.props.src;
        },
        onEnter: function(el) {
            this.videoEle.addEventListener('timeupdate', this.setTime);
            document.addEventListener('webkitfullscreenchange', this.addFullscreenClass, false);
            document.addEventListener('mozfullscreenchange', this.addFullscreenClass, false);
            document.addEventListener('fullscreenchange', this.addFullscreenClass, false);
            document.addEventListener('MSFullscreenChange', this.addFullscreenClass, false);
            window.addEventListener('resize', this.updateSeekHandle, false);
        },
        onExit: function(el) {
            this.videoEle.removeEventListener('timeupdate', this.setTime);
            document.removeEventListener('webkitfullscreenchange', this.addFullscreenClass, false);
            document.removeEventListener('mozfullscreenchange', this.addFullscreenClass, false);
            document.removeEventListener('fullscreenchange', this.addFullscreenClass, false);
            document.removeEventListener('MSFullscreenChange', this.addFullscreenClass, false);
            window.removeEventListener('resize', this.updateSeekHandle, false);
            if (this.stateIs('play')) this.goTo('pause');
        },
        onRemove: function() {
            this._removeElement();
        },
        noSwipe: function(e){
            e.stopPropagation();
        },
        checkIfSwipe: function(e){
            // prevent swiping in fullscreen mode
            if(self.el.classList.contains('ag-video-fullscreen'))
                self.noSwipe(e);
        },
        toggleVideo: function(e){
            if(this.stateIs('play'))
                this.goTo('pause');
            else
                this.goTo('play');
        },
        setTime: function(e) {
            self.currentPlayTime = self.videoEle.currentTime;
            self.updateProgress();
        },
        updateProgress: function(time) {
            this.currentPlayTime = time || this.currentPlayTime;
            var seekHandlePos = 0;
            var value = 0;
            
            if (this.currentPlayTime > 0) {
                value = Math.floor((100 / this.videoEle.duration) * this.currentPlayTime);
            }

            var videoCurrentTime = this.formatTime(this.currentPlayTime);
            var videoEndTime = this.formatTime(this.videoEle.duration);

            this.currentTimeText.innerHTML = videoCurrentTime;
            this.endTimeText.innerHTML = videoEndTime; 

            this.progressBar.style.width = value + "%";

            seekHandlePos = value * 0.01 * this.progressBarContainer.offsetWidth;
            this.seekHandle.moveTo(seekHandlePos);
        },
        formatTime: function(seconds) {
            var s = Math.floor(seconds % 60),
                m = Math.floor(seconds / 60 % 60),
                h = Math.floor(seconds / 3600);

            if (h > 0) {
                h = ((h < 10) ? "0" + h : h) + ":";
            }
            else {
                h = "00:";
            }

            if (m > 0) {
                m = ((m < 10) ? "0" + m : m) + ":";
            }
            else {
                m = "00:";
            }

            // Check if leading zero is need for seconds
            s = (s < 10) ? "0" + s : s;

            return h + m + s;
        },
        toggleFullscreen: function(e){
            var fullscreenEle = document.fullscreenElement || document.msFullscreenElement || 
                                document.mozFullScreenElement || document.webkitCurrentFullScreenElement;

            var userAgent = navigator.userAgent || navigator.vendor || window.opera;

            if(userAgent.match( /iPad/i ) || userAgent.match( /iPhone/i ) || userAgent.match( /iPod/i )){
                if (this.videoEle.webkitEnterFullscreen){
                    this.videoEle.webkitEnterFullscreen();
                }
            }
            else{
                if (this.videoContainer.requestFullscreen) {
                    if(fullscreenEle)
                        document.exitFullscreen();
                    else
                        this.videoContainer.requestFullscreen();
                } 
                else if (this.videoContainer.msRequestFullscreen) {
                    if(fullscreenEle)
                        document.msExitFullscreen();
                    else
                        this.videoContainer.msRequestFullscreen();
                } 
                else if (this.videoContainer.mozRequestFullScreen) {
                    if(fullscreenEle)
                        document.mozCancelFullScreen();
                    else
                        this.videoContainer.mozRequestFullScreen();
                } 
                else if (this.videoContainer.webkitRequestFullscreen) {
                    if(fullscreenEle)
                        document.webkitCancelFullScreen();
                    else
                        this.videoContainer.webkitRequestFullscreen();
                }
            }
        },
        addFullscreenClass: function(){
            var fullscreenEle = document.fullscreenElement || document.msFullscreenElement || 
                                document.mozFullScreenElement || document.webkitCurrentFullScreenElement;

            if (fullscreenEle) {
                self.el.classList.add('ag-video-fullscreen');
            }
            else{
                self.el.classList.remove('ag-video-fullscreen');
            }
        },
        updateSeekHandle: function(){
            // draggy - changes limits and position on window resize
            self.seekHandle.config.limitsX[1] = self.progressBarContainer.offsetWidth;
            var seekHandlePos = self.videoEle.currentTime / self.videoEle.duration * self.progressBarContainer.offsetWidth;
            self.seekHandle.moveTo(seekHandlePos);
        },
        moveSeekHandle: function(x, y){
            // updates progressbar
            var currentPos = x / self.progressBarContainer.offsetWidth;
            self.updateProgress(self.videoEle.duration * currentPos);
        },
        handleDrop: function(e) {
            // moves handle
            var pos = e.target.position;
            var currentPos = pos[0] / self.progressBarContainer.offsetWidth;
            self.videoEle.currentTime = self.videoEle.duration * currentPos;
        }
    }

});
/* global app */
/* global ag */
app.register("ag-viewer", function () {

	/**
	 * Agnitio Viewer Module
	 *
	 * This module will open URLs or PDF documents
	 * in iFrame on top of presentation.
	 *
	 * Usage:
	 * - Call ag.openPDF in non-agnitio app or on the web
	 * - Call ag.openURL in non-agnitio app or on the web
	 * - Add 'data-viewer="browser"' to a link (<a>)
	 */

	return {
		template: false,
		publish: {},
		events: {
			"tap .close": "closeViewer"
		},
		states: [],

		onRender: function (el) {

			// app.on('ready', this.init.bind(this));

			this.frame = null;
			this.content = [];
			this.inDevice = true;
			var info = ag.platform.info();

			// If non-Engager, let's open PDFs in viewer
			if (!info || (info.localizedModel !== "iPad" && info.platform !== "Windows")) {
				this.inDevice = false;
				ag.on('openPDF', this.openContent.bind(this));
			}
			ag.on('openURL', this.openContent.bind(this));
			ag.on('openSlide', this.openSlide.bind(this));
			ag.on('openSlideshow', this.openSlideshow.bind(this));

			document.addEventListener('click', this.handleClick.bind(this));

		},
		onRemove: function (el) {
			this._removeElement(); // Will undelegate events
		},
		handleClick: function (event) {
			var el = event.target;
			var attr = el.getAttribute('data-viewer') || el.hasAttribute('data-viewer');
			var href = el.getAttribute('href') || attr;
			var slide = el.getAttribute('slide');
			if (attr) {
				event.preventDefault();
				event.stopPropagation();
				if (attr === "slide") {
					if (!href) return;
					ag.publish('openSlide', {slide: href});
				}
				else if (attr === "slideshow") {
					if (!href) return;
					ag.publish('openSlideshow', {slideshow: href, slide: slide});
				}
				else if (href) {
					if (/.pdf/.test(href)) {
						ag.openPDF(href);
					}
					else if (ag.openURL && typeof href === 'string') {
						ag.openURL(href);
					}
				}
			}
		},
		openLink: function (link) {
			var href = link.getAttribute('href');
			if (ag.openURL) ag.openURL(href);
		},
		closeViewer: function () {
			app.unlock();
			var last = this.content.length - 1;
			var view = this.content[last];
			if (view && view.slide) {
				app.slide.remove(view.slide, true);
			}
			if(view && window.viewer.classList.contains('inline-slideshow')){
				if(!window.viewer.classList.contains('state-ag-overlay-open')){
					return;
				}
				window.viewer.classList.remove('state-ag-overlay-open');
				window.viewer.classList.add('state-default');
				var that = this;
				app.trigger('close:before:inlineSlideshow');
				setTimeout(function () {
					app.trigger('close:inlineSlideshow');
					window.viewer.classList.remove('loaded');
					window.viewer.classList.remove('visible');
					that.el.innerHTML = "";
					that.frame = null;
					view.container = null;
					that.content.pop();
				}, 1000);
			}else{
				if(view){
					view.container.classList.remove('loaded');
					view.container.classList.remove('visible');
					this.el.removeChild(view.container);
					this.frame = null;
					view.container = null;
					this.content.pop();
				}
			}
		},
		openContent: function (path) {
			var view = {};
			var markup = [
				'<header>',
				'<a class="close" href="#"><span class="icon"></span></a>',
				'</header>',
				'<div class="spinner"></div>',
				'<div class="viewport">',
				'<iframe src="' + path + '"></iframe>',
				'</div>'
			];
			if (!this.inDevice) markup.splice(2, 0, '<a class="external" href="' + path + '" target="_blank"><span class="icon"></span></a>');
			view.container = document.createElement('div');
			view.container.classList.add('preview-link-overlay');
			this.el.appendChild(view.container);
			view.container.innerHTML = markup.join('');
			this.frame = this.el.querySelector('iframe');
			this.frame.addEventListener('load', this.load.bind(this));
			this.content.push(view);
			setTimeout(function () {
				view.container.classList.add('visible');
			}, 1);
			app.lock();
		},
		openSlide: function (data) {
			var viewer = document.createElement('div');
			var view = {};
			viewer.classList.add('viewport');
			// Need to remove slide if already loaded in presentation
			app.slide.remove(data.slide, true);
			view.slide = data.slide;
			view.container = document.createElement('div');
			view.container.classList.add('preview-link-overlay');
			this.el.appendChild(view.container);
			view.container.innerHTML = [
				'<header>',
				'<a class="close" href="#"><span class="icon"></span></a>',
				'</header>',
				'<div class="spinner"></div>'
			].join('');
			app.dom.insert([{id: data.slide}], false, viewer);
			view.container.appendChild(viewer);
			this.content.push(view);
			setTimeout(function () {
				view.container.classList.add('visible');
				view.container.classList.add('loaded');
			}, 1);
			app.lock();
		},
		openSlideshow: function (data) {
			if(window.viewer.getElementsByClassName('preview-link-overlay')[0]){
				return;
			}
			data.view = {};
			data.view.container = document.createElement('div');
			data.view.container.classList.add('preview-link-overlay');
			data.view.container.innerHTML = [
				'<div class="ag-overlay-background"></div>',
				'<div class="ah-top-inline-sidebar">',
				'<div class="wrapper-inline-indication">',
				'<div class="inline-menu"></div>',
				'</div>',
				'</div>',
				'<div class="ah-bottom-inline-sidebar">',
				'<div class="wrapper-inline-indication">',
				'<div class="inline-indicators"></div>',
				'</div>',
				'</div>',
				'<div class="ag-overlay-x ag-overlay-close close"></div>'
			].join('');
			this.el.appendChild(data.view.container);
			app.trigger('init:inlineSlideshow', {data: data});
			this.content.push(data.view);
			app.trigger('open:inlineSlideshow', {data: data});
			app.lock();
		},
		load: function () {
			var last = this.content.length - 1;
			this.content[last].container.classList.add('loaded');
			this.frame.removeEventListener('load', this.load.bind(this));
		}
	}

});
/**
 * Implements auto reference popups.
 * ---------------------------------
 *
 * Adds tap handlers to every element that has the [data-reference-id] data attribute.
 * On tap a popup with a list of all references in the slide appears.
 *
 * The module is loaded within index.html
 *
 * @module ap-auto-references-popup.js
 * @requires jquery.js, touchy.js
 * @author Andreas Tietz, antwerpes ag
 */
/**
 * Modified ap-auto-references-popup module to read keys instead of numbers.
 * Grouping them when connected references are used.
 * Adding reference-numbers to the sup-elements
 */


app.register("ah-auto-references", function () {

    return {
        publish: {},
        events: {},
        states: [],
        onRender: function (el) {
            app.listenTo(app.slide, 'slide:render', this.autoReferencePopupHandler.bind(this));
            var slide = app.slideshow.get();
            if(slide)
                this.autoReferencePopupHandler({id: slide});
        },
        onRemove: function (el) {

        },
        onEnter: function (el) {

        },
        onExit: function (el) {

        },

        autoReferencePopupHandler: function (data) {
            var $slide = $("#" + data.id);
            var self = this;

            var refindexes = [], point = 0, groups = [];

            $slide.find("[data-reference-id]").each(function () {
                var referenceIds = $(this).attr("data-reference-id"),
                    reflist = referenceIds.split(',');

                $.each(window.mediaRepository.metadata(), function (file, meta) {
                    if (reflist.indexOf("" + meta.referenceKey) > -1) {
                        refindexes.push(meta.referenceId);
                    }
                });

                if(refindexes.length==0)
                    return

                refindexes.reduce(function(prev, next, index){
                    if((next - prev) !== 1){
                        groups.push(refindexes.slice(point, index));
                        point = index;
                    }
                    return next;
                });

                groups.push(refindexes.slice(point));

                $(this)[0].textContent = groups.map(function(group){
                    var result = group;
                    if(group.length > 2){
                        result = [group[0] + "-" + group[group.length - 1]];
                    }

                    return result;
                }).reduce(function(prev, next){
                    return prev.concat(next);
                }).join(",");

                refindexes = [];
                point = 0;
                groups = [];
            });

            $slide.find("[data-reference-id]")
                .off('tap.auto-references-popup')
                .on('tap.auto-references-popup', function () {

                    // Collect unique reference ids:
                    var referenceIds = {};
                    $slide.find("[data-reference-id]").each(function () {

                        var referenceId = $(this).attr("data-reference-id"),
                            reflist = referenceId.split(',');

                        $.each(window.mediaRepository.metadata(), function (file, meta) {
                            if (reflist.indexOf("" + meta.referenceKey) > -1) {
                                refindexes.push(meta.referenceId);
                                referenceIds[meta.referenceId] = true;
                            }
                        });
                    });

                    referenceIds = Object.keys(referenceIds);

                    // Find media resources associated with the collected reference ids:
                    var references = {};
                    $.each(window.mediaRepository.metadata(), function (file, meta) {
                        if (referenceIds.indexOf("" + meta.referenceId) > -1) {
                            references[file] = meta;
                        }
                    });

                    // Render all references into a list:
                    var $scroll = $('<div class="scroll"/>');
                    var $list = $('<ul class="references"/>');
                    $list.append($.map(references, function (meta, file) {
                        return window.mediaRepository.render(file, meta);
                    }));
                    $scroll.append($list);


                    // Put list in popup:
                    var $popup = $('<div class="auto-references-popup" />')
                        .append('<header><div class="x">⊗</div><h1>References</h1></header>')
                        .append($scroll);

                    // Put popup in overlay:
                    $('<div class="auto-references-popup-overlay" />')
                        .append($popup)
                        .on("swipedown swipeup swiperight swipeleft", function (e) {
                            e.stopPropagation();
                        })
                        .on("tap", function (event) {
                            if ($(event.target).is(":not(.auto-references-popup, .auto-references-popup *) .x")) $(this).remove();
                        }).appendTo("#presentation");


                      if(self.scroll) self.scroll.destroy();
                      self.scroll = new IScroll($scroll[0], {scrollbars: true});

                });
        }
    }

});


/*
 * Implements auto reference popups.
 * ---------------------------------
 *
 * Adds tap handlers to every element that has the [data-reference-id] data attribute.
 * On tap a popup with a list of all references in the slide appears.
 *
 * The module is loaded within index.html
 *
 * @module ah-auto-references-popup.js
 * @requires jquery.js, touchy.js
 * @author Andreas Tietz, antwerpes ag

 * Modified by Anthill/Infopulse
 */

(function() {
  app.register('ah-auto-references-popup', function() {
    var groupSeparator, indexSeparator, resources;
    indexSeparator = ',';
    groupSeparator = '-';
    resources = [];
    return {
      publish: {
        isglobalnumeric: false,
        isfiltertrack: false
      },
      events: {},
      states: [],
      getCurrentSlide: function() {
        var ref;
        return (ref = app.slide.get(app.slideshow.get())) != null ? ref.el : void 0;
      },
      onRender: function(el) {
        this.isGlobalNumeric = this.props.isglobalnumeric;
        this.isFilterTrack = this.props.isfiltertrack;
        this.data = window.mediaRepository.metadata();
        app.on('close:overlay', (function(_this) {
          return function() {
            return _this.autoReferences(_this.getCurrentSlide());
          };
        })(this));
        app.on('history:update', this.autoReferences.bind(this));
        return app.listenTo(app.slideshow, 'update:current', (function(_this) {
          return function() {
            var currentSlide;
            currentSlide = _this.getCurrentSlide();
            if (currentSlide) {
              return _this.parseReferences(currentSlide);
            }
          };
        })(this));
      },
      onRemove: function(el) {},
      onEnter: function(el) {},
      onExit: function(el) {},
      removePopup: function() {
        if ($popupOverlay) {
          return $popupOverlay.remove();
        }
      },
      isReferenceId: function(data, media) {
        if (data[media] && data[media].hasOwnProperty("referenceId")) {
          return data[media];
        }
        return false;
      },
      filterTrack: function(references) {
        var currentFlow, currentTrack, filterReferences, key;
        currentTrack = app.slideshow.getId();
        filterReferences = {};
        if (this.isFilterTrack) {
          for (key in references) {
            currentFlow = references[key].flow.split(',');
            if (currentFlow.indexOf(currentTrack) !== -1) {
              filterReferences[key] = references[key];
            }
          }
        }
        if (Object.keys(filterReferences).length !== 0) {
          return filterReferences;
        } else {
          return references;
        }
      },
      getIndex: function(id) {
        if (this.isGlobalNumeric) {
          return this.getGlobalIndex(id);
        } else {
          return this.getLocalIndex(id);
        }
      },
      getLocalIndex: function(id) {
        var filterReference, i, index, range, references;
        filterReference = [];
        references = [];
        for (index in this.refList) {
          if (this.refList[index].indexOf(groupSeparator) > -1) {
            range = this.refList[index].split(groupSeparator);
            references.push(range[0]);
            references.push(range[1]);
          } else {
            if (this.refList[index].indexOf(indexSeparator) > -1) {
              range = this.refList[index].split(indexSeparator);
              for (i in range) {
                references.push(range[i]);
              }
            } else {
              references.push(this.refList[index]);
            }
          }
        }
        for (index in references) {
          if (filterReference.indexOf(references[index]) === -1) {
            filterReference.push(references[index]);
          }
        }
        index = filterReference.indexOf(id) + 1;
        return index;
      },
      getGlobalIndex: function(id) {
        var data, dataRef, dataRefFilter, key, media, mediaIndex;
        dataRef = {};
        data = window.mediaRepository.metadata();
        for (media in data) {
          if (this.isReferenceId(data, media)) {
            dataRef[media] = this.isReferenceId(data, media);
          }
        }
        dataRefFilter = this.filterTrack(dataRef);
        for (media in dataRefFilter) {
          for (key in dataRefFilter[media]) {
            if (key === 'referenceId' && dataRefFilter[media][key] === id) {
              mediaIndex = Object.keys(dataRefFilter).indexOf(media) + 1;
            }
          }
        }
        return mediaIndex;
      },
      getRefByIndex: function(index) {
        var data, i, key, refId;
        data = window.mediaRepository.metadata();
        i = 1;
        for (key in data) {
          if (i === index) {
            refId = data[key]['referenceId'];
          }
          i++;
        }
        return refId;
      },
      getReferencesIds: function(wrapper) {
        var ele, j, len, ref;
        this.refList = [];
        this.elements = wrapper.querySelectorAll('[data-reference-id]');
        ref = this.elements;
        for (j = 0, len = ref.length; j < len; j++) {
          ele = ref[j];
          this.refList.push(ele.getAttribute('data-reference-id'));
        }
        return this.refList;
      },
      parseReferences: function(data, references) {
        var $slide, id, index, j, len;
        this.referenceIds = [];
        if (!references) {
          $slide = app.dom.get(data.id);
          references = this.getReferencesIds($slide);
        }
        for (index = j = 0, len = references.length; j < len; index = ++j) {
          id = references[index];
          this.referenceMap(id, index);
        }
        resources = Object.keys(this.referenceIds);
        return app.trigger('references:update', {
          list: this.getResources()
        });
      },
      referenceMap: function(referenceId, index) {
        var allIndexes;
        allIndexes = [];
        referenceId.split(indexSeparator).forEach((function(_this) {
          return function(referencesList) {
            var from, i, indexes, indexesString, reference, references, to;
            references = referencesList.split(groupSeparator);
            reference = references[0];
            if (references.length > 1) {
              indexes = [];
              from = _this.getIndex(reference);
              to = _this.getIndex(references[1]);
              i = from;
              while (i <= to) {
                _this.referenceIds[_this.getRefByIndex(i)] = true;
                i++;
              }
              indexes.push(from, to);
              indexesString = indexes.join(groupSeparator);
            } else {
              indexesString = _this.getIndex(reference);
              _this.referenceIds[reference] = true;
            }
            if (allIndexes.length === 0) {
              return allIndexes.push(indexesString);
            } else {
              return allIndexes = [allIndexes, indexesString].join(indexSeparator);
            }
          };
        })(this));
        this.render(allIndexes, index);
        return this.referenceIds;
      },
      render: function(indexes, index) {
        var range;
        if (!this.isGlobalNumeric) {
          if (indexes.indexOf(indexSeparator) > -1) {
            range = indexes.split(indexSeparator);
            if (range.length > 2) {
              indexes = range[0] + "-" + range[range.length - 1];
            }
          }
        }
        return this.elements[index].textContent = indexes;
      },
      getResources: function() {
        var references;
        references = {};
        if (this.isGlobalNumeric) {
          $.each(window.mediaRepository.metadata(), function(file, meta) {
            if (resources.indexOf('' + meta.referenceId) > -1) {
              return references[file] = meta;
            }
          });
        } else {
          $.each(resources, function(index) {
            return $.each(window.mediaRepository.metadata(), function(file, meta) {
              if (meta.referenceId === resources[index]) {
                return references[file] = meta;
              }
            });
          });
        }
        return references;
      },
      autoReferences: function(data) {
        return this.parseReferences(data);
      },
      getLocalIndex: function(index) {
        return "[" + index + "]";
      },
      replaceIndex: function($ref, index) {
        var $refNum, ref;
        if (!this.isGlobalNumeric) {
          $refNum = (ref = $ref[0].getElementsByClassName('referenceId')) != null ? ref[0] : void 0;
          return $refNum.innerHTML = this.replaceLocalIndex(++index);
        }
      }
    };
  });

}).call(this);

(function() {
  'use strict';
  var BeforeAfterSlider;

  window.BeforeAfterSlider = BeforeAfterSlider = (function() {
    function BeforeAfterSlider(slider, controlPoints) {
      var sliderOffsetX;
      this.$after = slider.getElementsByClassName("element-after")[0];
      sliderOffsetX = $(slider).offset().left;
      slider.addEventListener("touchmove", (function(_this) {
        return function(event) {
          var offX, touch;
          event.preventDefault();
          touch = event.touches[0];
          offX = touch.pageX - sliderOffsetX;
          return _this.$after.style.width = offX + "px";
        };
      })(this));
      slider.addEventListener("touchend", (function(_this) {
        return function(event) {
          var i, index, len, offX, point, results, touch;
          event.preventDefault();
          touch = event.changedTouches[0];
          offX = touch.pageX - sliderOffsetX;
          if (offX > 0) {
            results = [];
            for (index = i = 0, len = controlPoints.length; i < len; index = ++i) {
              point = controlPoints[index];
              if (offX <= point) {
                _this.$after.style.width = point + "px";
                _this.touchEnd(index + 1);
                break;
              } else {
                results.push(void 0);
              }
            }
            return results;
          } else {
            _this.$after.style.width = "0px";
            return _this.touchEnd(0);
          }
        };
      })(this));
    }

    BeforeAfterSlider.prototype.reset = function() {
      return this.$after.style.width = 0;
    };

    BeforeAfterSlider.prototype.touchEnd = function(index) {};

    return BeforeAfterSlider;

  })();

}).call(this);

/**
 * -----------------------------------------------------------------
 * @module ah-binding-handler.js
 * @requires
 * @author
 */

app.register("ah-binding-handler", function () {
    return {
        publish: {},
        events: {},
        states: [],
        onRender: function (el) {
            this.currentSlide = app.slide.get();
            this.applyBindings(el);
            var self = this;
            app.listenTo(app, 'history:update', function (data) {
                self.currentSlide = app.view.views[data.id];
                self.applyBindings(self.currentSlide.el);
            });
            app.on('open:inlineSlideshow', function (data) {
	            self.applyBindings(viewer);
            });
        },
        applyBindings: function (el) {
            if (!el) el = document;
            var self = this;
            Object.keys(this.handlers()).forEach(function (bindKey) {
                var attribute = 'data-' + bindKey,
                    handler = self.handlers()[bindKey],
                    currentList = el.querySelectorAll('[' + attribute + ']');
                arryaList = Array.prototype.slice.call(currentList);
                if (el.hasAttribute(attribute)) {
                    arryaList.push(el);
                }
                arryaList.forEach(function (el) {
                    if (!el.bindingFlag) {
                        handler(el, attribute);
                        el.bindingFlag = true;
                    }
                });
            })
        },
        handlers: function () {
            var self = this;

            function handlerController(el, attribute, tapHandler) {
                el.addEventListener('tap', function (event) {
                    event.preventDefault();
                    tapHandler(el, el.attributes[attribute].value);
                });
            }

            return {
                'popup': function (element, attributes) {
                    handlerController(element, attributes, function (el, slideId) {
                        self._openOverlay(el, 'overlay-popup-custom', slideId);
                    });
                },
                'goto-state': function (element, attributes) {
                    handlerController(element, attributes, function (el, attribute) {
                        self.currentSlide.goTo(attribute);
                    })
                },
                'goto-slide': function (element, attributes) {
                    handlerController(element, attributes, function (el, attribute) {
                        app.goTo(attribute);
                    })
                },
                'goto-inline-slide': function (element, attributes) {
                    handlerController(element, attributes, function (el, attribute) {
	                    app.module.get('inlineSlideshow').goTo(attribute);
                    })
                },
                'stop-swipe': function (element) {
                    ['swipeup', 'swipedown', 'swipeleft', 'swiperight'].forEach(function (event) {
                        element.addEventListener(event, function (event) {
                            event.stopPropagation()
                        });
                    });
                }
            }
        },
        _openOverlay: function (el, overlayId, slideId) {
            var popup = app.view.get(overlayId),
                attributes = el.attributes,
                customClass;
            if (attributes["data-custom"]) {
                customClass = attributes["data-custom"].value;
                popup.el.classList.add(customClass);
            }
            if (popup && slideId) popup.load(slideId);
        },
        onRemove: function (el) {
        },
        onEnter: function (el) {
            this.applyBindings(el);
        },
        onExit: function (el) {
        }
    }
});

(function() {
  app.register('ah-bottom-logo', function() {
    return {
      events: {
        "tap": "openNotes"
      },
      states: [],
      onRender: function() {
        return this.notesModule = app.module.get('notes');
      },
      openNotes: function() {
        return this.notesModule.show();
      }
    };
  });

}).call(this);

app.register("ah-cleanup", function() {

  // Based on the ag-cleanup module

  // Get the unique entries of an array
  function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }

  return {
    publish: {
        purgeOnLoad: false,
        timeout: 40, // seconds to wait since last purge.
        limit: 10
    },
    events: {

    },
    states: [],
    history: [],
    archived: [],
    lastPurge: 0,
    onRender: function(el) {
        var self = this;
        app.slideshow.on('update:current', function(data) {
            var unique, clean;
            var cleanupLength = parseInt(self.props.limit / 2); // We'll keep half our limit as active
            // Add current slide to beginning of history
            if (data.current) {
                self.history.unshift(data.current.id);
            }
            // Allow animation to run first
            setTimeout(function() {
              // Make sure we don't have the new slide in history already
              self.history = self.history.filter(onlyUnique);
              // console.log("SLIDE HISTORY:", self.history);
              // If our history is larger than limit, then send for cleanup
              if (self.history.length > parseInt(self.props.limit)) {
                  clean = self.history.splice(cleanupLength - 1, self.history.length - cleanupLength);
                  // console.log("CLEAN FROM HISTORY:", clean);
                  self.clean(clean);
              }
            }, 800);
        });
        // Fix for bug in Accelerator when slideshow is loaded and first slide has been archived
        app.slideshow.on('load', function(data) {
          var info = app.slideshow.resolve();
          var el = app.dom.get(info.slide);
          if (el) el.classList.remove('archived');
        });
        if (this.props.purgeOnLoad) {
            app.slideshow.on('load', function(data) {
              // console.log("Unloading slideshow");
                // Remove all slides except the newly loaded one
                // TODO: implement
            });
        }
    },
    onRemove: function(el) {

    },
    // Archive old slides and remove previously archived ones
      clean: function(slides) {
          var now = new Date().getTime();
          var currentSlide = app.slideshow.get();
          slides.forEach(function(id) {
              app.dom.archive(id);
          });
          // Only remove prevously archived if timeout has expired
          if (this.archived.length && now > (this.lastPurge + parseInt(this.props.timeout * 1000))) {
              this.archived.forEach(function(id) {
                  if (id !== currentSlide) app.slide.remove(id, true);
              });
              this.archived = slides;
              this.lastPurge = now;
          }
          else {
              this.archived = this.archived.concat(slides);
          }
      }
  };

});


(function() {
  app.register('ah-correct-events', function() {
    return {
      publish: {},
      states: [],
      onRender: function() {
        this.slides = app.model.get().slides;
        app.listenTo(app.slide, 'slide:render', this.validator.bind(this));
        return app.listenTo(app.slideshow, 'update:current', this.validatorSlideshow.bind(this));
      },
      validator: function(el) {
        if (this.slides[el.id]) {
          return app.slide.trigger('slide:anthill-render', el);
        }
      },
      validatorSlideshow: function(el) {
        if (el.current.id !== el.prev.id) {
          return app.slideshow.trigger('update:anthill-current', el);
        }
      }
    };
  });

}).call(this);

(function() {
  var AhDevelopSitemap;

  AhDevelopSitemap = (function() {
    function AhDevelopSitemap() {
      this.template = '<button class="close">X</button>' + '<header>' + '<p>FILTERS:</p>' + '<div class="rounded" title="Slides">' + '<input type="checkbox" value="None" id="rounded" name="slides" checked />' + '<label for="rounded"></label>' + '</div>' + '<div class="rounded" title="Popups">' + '<input type="checkbox" value="None" id="rounded1" name="popups" checked />' + '<label for="rounded1"></label>' + '</div>' + '<input type="text" placeholder="slideId..." />' + '<output></output>' + '<button class="sort"></button>' + '</header>' + '<div class="wrapper"><ul class="links"></ul></div>';
      this.collections = [];
      this.currentPopup = null;
      this.currentSlide = null;
      this.scroll = null;
      this.canUsePopups = false;
      this.states = [
        {
          id: 'show',
          onEnter: function(e) {
            this.currentSlide = app.slide.get().id;
            this.filter();
            return this.scrollToActive();
          }
        }
      ];
      this.events = {
        'tap .close': 'close',
        'tap .sort': 'changeSort',
        'tap li': 'navigate',
        'change [name="slides"]': 'toggleSlides',
        'keyup [type="text"]': 'filterByName',
        'change [name="popups"]': 'togglePopups',
        "swipeleft": function(event) {
          return event.stopPropagation();
        },
        "swiperight": function(event) {
          return event.stopPropagation();
        }
      };
      this.publish = {
        exclude: '',
        popUps: true
      };
    }

    AhDevelopSitemap.prototype.onRender = function(element) {
      this.params = {
        isShowPopups: true,
        isShowSlides: true,
        searchValue: '',
        sort: 'desc'
      };
      this.$output = element.getElementsByTagName('output')[0];
      this.$sort = element.getElementsByClassName('sort')[0];
      if (!app.view.get("overlay-popup-custom") && this.props.popUps) {
        throw new Error("Can't find overlay-popup or off this.props.popUps!");
      } else {
        this.canUsePopups = true;
      }
      window['presentation'].addEventListener('doubleTap', (function(_this) {
        return function() {
          return _this.goTo('show');
        };
      })(this), false);
      this.scroll = new IScroll($(".wrapper")[0], {
        scrollbars: false,
        bounce: false
      });
      this.getCollections();
      if (this.props.popUps) {
        this.collections = this.collections.concat(this.getPopups());
      }
      return this.filtered = [].concat(this.collections);
    };

    AhDevelopSitemap.prototype.filterByName = function(event) {
      var value;
      value = event.target.value;
      this.params.searchValue = value;
      return this.filter();
    };

    AhDevelopSitemap.prototype.toggleSlides = function(event) {
      this.params.isShowSlides = event.target.checked;
      return this.filter();
    };

    AhDevelopSitemap.prototype.togglePopups = function(event) {
      this.params.isShowPopups = event.target.checked;
      return this.filter();
    };

    AhDevelopSitemap.prototype.changeSort = function() {
      this.params.sort = this.params.sort === '' ? 'desc' : '';
      this.$sort.classList.toggle('active');
      return this.filter();
    };

    AhDevelopSitemap.prototype.filter = function() {
      var compareSort, res;
      compareSort = (function(_this) {
        return function(a, b) {
          if (a.id < b.id) {
            return -1;
          }
          if (a.id > b.id) {
            return 1;
          }
          return 0;
        };
      })(this);
      if (!this.params.isShowPopups || !this.params.isShowSlides) {
        this.filtered = this.collections.filter((function(_this) {
          return function(slide) {
            var compare;
            if (_this.params.isShowPopups) {
              compare = slide.isPopup;
            }
            if (_this.params.isShowSlides) {
              compare = !slide.isPopup;
            }
            return compare;
          };
        })(this));
      } else {
        this.filtered = this.collections;
      }
      res = this.filtered.filter((function(_this) {
        return function(slide) {
          return slide.id.toLowerCase().indexOf(_this.params.searchValue) > -1;
        };
      })(this));
      res.sort(compareSort);
      if (this.params.sort === 'desc') {
        res.reverse();
      }
      this.createLinks(res);
      this.$output.innerText = res.length;
      return this.scrollToActive();
    };

    AhDevelopSitemap.prototype.close = function(e) {
      e.stopPropagation();
      return this.reset();
    };

    AhDevelopSitemap.prototype.scrollToActive = function() {
      this.scroll.refresh();
      return this.scroll.scrollTo(0, 0, 500);
    };

    AhDevelopSitemap.prototype.navigate = function(event) {
      var path, popupId, target;
      event.stopPropagation();
      target = event.delegateTarget;
      path = target.dataset.goto;
      popupId = target.dataset.popup;
      if (this.canUsePopups) {
        app.view.get("overlay-popup-custom").reset();
        if (popupId) {
          app.view.get("overlay-popup-custom").load(popupId);
        }
      }
      if (path) {
        app.goTo(path);
      }
      return this.reset();
    };

    AhDevelopSitemap.prototype.createLinks = function(slides) {
      var className, excludedLinks, html, key, list, slide;
      list = this.el.getElementsByClassName('links')[0];
      html = '';
      while (list.firstChild) {
        list.removeChild(list.firstChild);
      }
      excludedLinks = this.props.exclude.split(' ');
      for (key in slides) {
        slide = slides[key];
        if (excludedLinks.indexOf(slide.id) > -1) {
          continue;
        }
        className = '';
        if (slide.id === this.currentSlide) {
          className = 'active';
        }
        if (slide.isPopup) {
          html += "<li class='" + className + "' data-popup='" + slide.path + "'> <img src='slides/" + slide.id + "/" + slide.id + ".png'> [POPUP]<h2>" + slide.id + "</h2></li>";
        } else {
          html += "<li class='" + className + "' data-goto='" + slide.path + "'> <img src='slides/" + slide.id + "/" + slide.id + ".png'><h2>" + slide.id + "</h2></li>";
        }
      }
      return list.appendChild(app.dom.parse(html));
    };

    AhDevelopSitemap.prototype.getCollections = function() {
      var collectionName, collections, i, j, len, len1, slide, slideShowStructure, slides, slideshow, slideshows;
      collections = app.model.get().storyboards;
      slideshows = app.model.get().structures;
      this.collections.length = 0;
      for (collectionName in collections) {
        slideShowStructure = collections[collectionName].content;
        for (i = 0, len = slideShowStructure.length; i < len; i++) {
          slideshow = slideShowStructure[i];
          slides = slideshows[slideshow].content;
          for (j = 0, len1 = slides.length; j < len1; j++) {
            slide = slides[j];
            this.collections.push({
              id: slide,
              isPopup: false,
              path: collectionName + "/" + slideshow + "/" + slide
            });
          }
        }
      }
      return this.collections;
    };

    AhDevelopSitemap.prototype.getPopups = function() {
      var popups, slide, slides;
      slides = app.model.get().slides;
      popups = [];
      for (slide in slides) {
        if (slide.indexOf('PopupSlide') > -1) {
          popups.push({
            id: slide,
            isPopup: true,
            path: slide
          });
        }
      }
      return popups;
    };

    return AhDevelopSitemap;

  })();

  app.register("ah-develop-sitemap", function() {
    return new AhDevelopSitemap();
  });

}).call(this);

(function() {
  app.register("ah-fix-slide-animation", function() {
    return {
      onRender: function() {
        this.setAnimation(this.getSB());
        return app.slideshow.on('load', (function(_this) {
          return function(data) {
            return _this.setAnimation(_this.getSB());
          };
        })(this));
      },
      getSB: function() {
        return app.model.get().storyboards[app.slideshow.getId()].linear;
      },
      setAnimation: function(isLinear) {
        presentation.classList.remove('linear-animation');
        if (isLinear) {
          return presentation.classList.add('linear-animation');
        }
      }
    };
  });

}).call(this);

(function() {
  app.register("ah-fix-slide-render", function() {
    return {
      onRender: function() {
        document.addEventListener("touchmove", function(event) {
          return event.preventDefault();
        });
        return app.listenTo(app.slideshow, 'update:anthill-current', (function(_this) {
          return function(el) {
            var current;
            current = document.querySelectorAll('.slide.present');
            if (current.length > 1) {
              return app.util.toArray(current).forEach(function(slide) {
                if (slide.id.indexOf('PopupSlide') > -1 || slide.id === el.current.id) {
                  return;
                }
                return slide.classList.remove('present');
              });
            }
          };
        })(this));
      }
    };
  });

}).call(this);

(function() {
  app.register("ah-history", function() {
    return {
      publish: {},
      onRender: function() {
        this.stack = [];
        app.listenTo(app.slide, 'slide:enter', this.add.bind(this));
        app.listenTo(app, 'slideEnter:inlineSlideshow', this.add.bind(this));
        app.listenTo(app.slide, 'slide:exit', this.remove.bind(this));
        return app.listenTo(app, 'slideExit:inlineSlideshow', this.remove.bind(this));
      },
      _getSlideId: function(data) {
        return data.slideId || data.id || data.slide.id;
      },
      add: function(data) {
        var slideId;
        slideId = this._getSlideId(data);
        this.stack.push({
          id: slideId,
          slide: app.slide.get(slideId)
        });
        return this.trigger();
      },
      remove: function(data) {
        var item, slideId;
        slideId = this._getSlideId(data);
        item = this.stack.find(function(item) {
          return slideId === item.id;
        });
        this.stack.splice(this.stack.indexOf(item), 1);
        if (this.stack.length !== 0) {
          return this.trigger();
        }
      },
      getCurrentView: function() {
        return this.stack[this.stack.length - 1];
      },
      getStack: function() {
        return this.stack;
      },
      trigger: function() {
        return app.trigger('history:update', this.getCurrentView());
      }
    };
  });

}).call(this);

(function() {
  app.register('ah-inline-indicators', function() {
    return {
      events: {},
      states: [],
      excludedSlides: [],
      updatedSlide: [],
      onRender: function() {
        app.on('init:inlineSlideshow', this.load.bind(this));
        return app.on('slideEnter:inlineSlideshow', this.setActive.bind(this));
      },
      load: function(props) {
        var indicatorWrapper, inlineSlideshow;
        this.indicatorWrappers = props.data.view.container.getElementsByClassName('inline-indicators')[0];
        inlineSlideshow = app.model.get().structures[props.data.slideshow];
        if (!this.indicatorWrappers || !inlineSlideshow.indicators) {
          return;
        }
        indicatorWrapper = document.createElement('ul');
        this.slides = inlineSlideshow.content;
        this.slides.forEach((function(_this) {
          return function(slide) {
            var indicator;
            indicator = document.createElement('li');
            indicator.classList.add('inline-indicator');
            indicator.setAttribute('indicator', slide);
            return indicatorWrapper.appendChild(indicator);
          };
        })(this));
        return this.indicatorWrappers.appendChild(indicatorWrapper);
      },
      setActive: function(props) {
        var ref, ref1, ref2;
        if ((ref = this.indicatorWrappers.getElementsByClassName('active')) != null) {
          if ((ref1 = ref[0]) != null) {
            ref1.classList.remove('active');
          }
        }
        return (ref2 = this.indicatorWrappers.querySelector('[indicator="' + props.slide.el.id + '"]')) != null ? ref2.classList.add('active') : void 0;
      }
    };
  });

}).call(this);

(function() {
  app.register('ah-inline-menu', function() {
    return {
      events: {},
      states: [],
      excludedSlides: [],
      updatedSlide: [],
      onRender: function() {
        app.on('init:inlineSlideshow', this.load.bind(this));
        return app.on('slideEnter:inlineSlideshow', this.setActive.bind(this));
      },
      hide: function() {
        document.getElementsByClassName('ah-top-inline-sidebar')[0].classList.add('hide');
        return document.getElementsByClassName('inline-slideshow-wrapper')[0].classList.remove('menu-indicator');
      },
      show: function() {
        document.getElementsByClassName('ah-top-inline-sidebar')[0].classList.remove('hide');
        return document.getElementsByClassName('inline-slideshow-wrapper')[0].classList.add('menu-indicator');
      },
      load: function(props) {
        var indicatorWrapper, inlineSlideshow;
        this.indicatorWrappers = props.data.view.container.getElementsByClassName('inline-menu')[0];
        inlineSlideshow = app.model.get().structures[props.data.slideshow];
        this.show();
        if (!this.indicatorWrappers || inlineSlideshow.indicators) {
          this.hide();
          return;
        }
        indicatorWrapper = document.createElement('nav');
        this.slides = inlineSlideshow.content;
        this.slides.forEach((function(_this) {
          return function(slide) {
            var indicator;
            indicator = document.createElement('button');
            indicator.innerHTML = app.model.get().slides[slide].name;
            indicator.classList.add('inline-navigator');
            indicator.setAttribute('indicator', slide);
            indicator.setAttribute('data-goto-inline-slide', slide);
            return indicatorWrapper.appendChild(indicator);
          };
        })(this));
        return this.indicatorWrappers.appendChild(indicatorWrapper);
      },
      setActive: function(props) {
        var ref, ref1, ref2;
        if ((ref = this.indicatorWrappers.getElementsByClassName('active')) != null) {
          if ((ref1 = ref[0]) != null) {
            ref1.classList.remove('active');
          }
        }
        return (ref2 = this.indicatorWrappers.querySelector('[indicator="' + props.slide.el.id + '"]')) != null ? ref2.classList.add('active') : void 0;
      }
    };
  });

}).call(this);

(function() {
  app.register('ah-inline-slideshow', function() {
    return {
      events: {},
      states: [],
      onRender: function() {
        app.on('init:inlineSlideshow', this.init.bind(this));
        app.on('open:inlineSlideshow', this.show.bind(this));
        return app.on('close:inlineSlideshow', this.hide.bind(this));
      },
      getSlideIndex: function(id) {
        if (id == null) {
          id = this.currentSlideDOM.el.id;
        }
        return this.slides.indexOf(id);
      },
      getCurrentSlide: function() {
        return this.currentSlideDOM.el.id;
      },
      presentAnimation: function($slide) {
        return setTimeout(function() {
          $slide.classList.remove("future", "past");
          return $slide.classList.add("present");
        }, 50);
      },
      triggerEvent: function(type) {
        if (type == null) {
          type = 'Enter';
        }
        this.currentSlideDOM['on' + type]();
        app.trigger('slide' + type + ':inlineSlideshow', {
          slide: this.currentSlideDOM
        });
        return console.log('slide' + type + ':inlineSlideshow  --- ' + this.currentSlideDOM.id);
      },
      animate: function(id) {
        var $slide, i, isNext, len, ref, results, slide;
        isNext = false;
        ref = this.slides;
        results = [];
        for (i = 0, len = ref.length; i < len; i++) {
          slide = ref[i];
          $slide = app.slide.get(slide).el;
          if (id === slide) {
            isNext = true;
            results.push(this.presentAnimation($slide));
          } else if (isNext) {
            $slide.classList.remove("past", "present");
            results.push($slide.classList.add("future"));
          } else {
            $slide.classList.remove("future", "present");
            results.push($slide.classList.add("past"));
          }
        }
        return results;
      },
      goTo: function(id) {
        if (id !== this.getCurrentSlide()) {
          this.triggerEvent('Exit');
          this.animate(id);
          this.currentSlideDOM = app.slide.get(id);
          return this.triggerEvent();
        }
      },
      openNextSlide: function(that) {
        var nextSlide;
        if (that == null) {
          that = this;
        }
        app.lock();
        nextSlide = that.slides[that.getSlideIndex() + 1];
        if (nextSlide) {
          return this.goTo(nextSlide);
        }
      },
      openPrevSlide: function(that) {
        var nextSlide;
        if (that == null) {
          that = this;
        }
        app.lock();
        nextSlide = that.slides[that.getSlideIndex() - 1];
        if (nextSlide) {
          return this.goTo(nextSlide);
        }
      },
      _setClasses: function() {
        window.viewer.classList.add('inline-slideshow', 'custom-popup', 'state-default');
        return this.inlineSlideshowWrapper.classList.add('inline-slideshow-wrapper', 'slides');
      },
      _clearDOM: function(slides) {
        return slides.forEach(function(slide) {
          return app.slide.remove(slide, true);
        });
      },
      _setCurrentSlide: function(slide) {
        return this.currentSlide = slide;
      },
      _insertDOMSlides: function(slides) {
        var slidesStructure;
        slidesStructure = slides.map(function(slide) {
          return {
            "id": slide
          };
        });
        return app.dom.insert(slidesStructure, false, this.inlineSlideshowWrapper);
      },
      show: function(props) {
        return setTimeout((function(_this) {
          return function() {
            _this.currentSlideDOM = app.slide.get(_this.currentSlide);
            _this.currentSlideDOM.el.classList.add("present");
            _this.currentSlideDOM.onEnter();
            _this.currentSlideDOM.parent.el.dataset.transitionSpeed = app.config.get("transitionSpeed");
            app.trigger('slideEnter:inlineSlideshow', {
              slide: _this.currentSlideDOM
            });
            props.data.view.container.classList.add('visible');
            props.data.view.container.classList.add('loaded');
            window.viewer.classList.add('state-ag-overlay-open');
            return window.viewer.classList.remove('state-default');
          };
        })(this), 100);
      },
      hide: function() {
        app.unlock();
        return app.trigger('slideExit:inlineSlideshow', {
          slide: this.currentSlideDOM
        });
      },
      addEventsHandler: function() {
        var that;
        that = this;
        this.inlineSlideshowWrapper.addEventListener('swipeleft', (function(_this) {
          return function() {
            return _this.openNextSlide(that);
          };
        })(this));
        return this.inlineSlideshowWrapper.addEventListener('swiperight', (function(_this) {
          return function() {
            return _this.openPrevSlide(that);
          };
        })(this));
      },
      init: function(props) {
        var viewPort;
        viewPort = document.createElement('div');
        this.inlineSlideshowWrapper = document.createElement('div');
        this._setClasses();
        viewPort.appendChild(this.inlineSlideshowWrapper);
        viewPort.classList.add('viewport');
        this.slides = app.model.get().structures[props.data.slideshow].content;
        this._clearDOM(this.slides);
        this._setCurrentSlide(props.data.slide || this.slides[0]);
        this._insertDOMSlides(this.slides);
        props.data.view.container.appendChild(viewPort);
        return this.addEventsHandler();
      }
    };
  });

}).call(this);

(function() {
  app.register("ah-listener", function() {
    return {
      onRender: function() {
        return app.listenTo(app.slideshow, 'update:anthill-current', (function(_this) {
          return function(el) {
            app.view.get("overlay-popup-custom").reset();
            return app.view.get("viewer").closeViewer();
          };
        })(this));
      }
    };
  });

}).call(this);

/**
 * Provides a media library section.
 * ---------------------------------
 * Provides a user interface for searching and viewing arbitrary
 * media meta data and content from the media repository.
 *
 * @module ap-media-library.js
 * @requires jquery.js, iscroll.js, media-repository.js
 * @author Andreas Tietz, antwerpes ag
 */
app.register("ah-media-library", function () {

    /**
     * Implements a media library section.
     * -----------------------------------
     * Implements a user interface for searching and viewing arbitrary
     * media meta data and content from the media repository.
     * Shows all media entries by default.
     * The basic set of listed entries may be optionally limited by some constraint,
     * e.g. "all media entries that are attachable to emails"
     * or "all media entries that have the 'pdf' and 'reference' tags in it".
     *
     * @class ap-media-library
     * @constructor
     * @param {object} options Valid properties are:
     *     - preFilterSearchTerms: limits basic set of entries: what is to be searched (leave undefined to show all)
     *     - preFilterAttributesToBeSearched: limits basic set of entries: where is it to be found
     *     - renderOptions: options to be passed to renderers
     */
    var self;
    return {
        publish: {
            prefiltersearchterms: undefined,
            prefilterattributestobesearched: undefined,
            renderOptions: {},
            attachment: false,
            followupmail: false,
            hide: false
        },
        events: {

        },
        states: [
            {
                id: "hide"
            },
            {
                id: "visible"
            },
            {
                id: "followupmail"
            },
            {
                id: "attachment"
            }
        ],
        onRender: function (el) {

            this.extend = app.registry.get('ap-media-library');
            this.hide = this.extend.hide;
            this.show = this.extend.show;
            this.unload = this.extend.unload;
            this.refreshScroll = this.extend.refreshScroll;
            self = this;
            app.$.mediaLibrary = this;

            this.load(el);

            if (this.props.hide) {
                this.hide();
            }

            if (this.props.attachment) {
                this.goTo('attachment');
            }
            if (this.props.followupmail) {
                this.goTo('followupmail');
            }

            app.$.on('open:ah-media-library', function () {
                if (this.stateIs("hide"))
                    this.show();
            }.bind(this));

            app.$.on('close:ah-media-library', function () {
                if (this.stateIs("visible"))
                    this.hide();
            }.bind(this));

            app.$.on('toolbar:hidden', function () {
                if (this.stateIs("visible"))
                    this.hide();
            }.bind(this));

            app.$.on('media-library:refresh', function () {
                if(this.stateIs("attachment"))
                    this.scroll.refresh();
            }.bind(this));

        },
        onRemove: function (el) {

        },
        onEnter: function (el) {

        },
        onExit: function (el) {

        },

        sortReferences: function(object){
            var tempArray = [], tempArraySort, result = {}, tempMedia = [];
            Object.keys(object).forEach(function (item) {
                if(object.hasOwnProperty(item))
                    if(object[item].hasOwnProperty('referenceId'))
                        tempArray.push(object[item].title.toLowerCase())
                    else
                        tempMedia.push(object[item].title.toLowerCase())
            });
            tempArraySort = tempArray.sort();
            tempMedia.forEach(function (item) {
                tempArraySort.push(item);
            });
            tempArraySort.forEach(function (item, index) {
                Object.keys(object).forEach(function (item) {
                    if(object.hasOwnProperty(item) && tempArraySort[index] == object[item].title.toLowerCase())
                        result[item] = object[item]
                });
            });
            return result
        },
        load: function (el) {
            var $list = $(el).find("ul");
            $list.empty();
            if(self.scroll) self.scroll.destroy();
            // Initialize scrolling:
            self.scroll = new IScroll(self.$(".scroll")[0], {scrollbars: true});


            // Fill the list with a basic set of media entries (e.g. "all media entries that are attachable to emails"):



            /**
             * Implements a media library section.
             * -----------------------------------
             * Implements a user interface for searching and viewing arbitrary
             * media meta data and content from the media repository.
             * Shows all media entries by default.
             * The basic set of listed entries may be optionally limited by some constraint,
             * e.g. "all media entries that are attachable to emails"
             * or "all media entries that have the 'pdf' and 'reference' tags in it".
             *
             * @param {object} options Valid properties are:
             *     - preFilterSearchTerms: limits basic set of entries: what is to be searched (leave undefined to show all)
             *     - preFilterAttributesToBeSearched: limits basic set of entries: where is it to be found
             *     - renderOptions: options to be passed to renderers
             */

            var media = this.sortReferences(window.mediaRepository.find(this.props.prefiltersearchterms, this.props.prefilterattributestobesearched));
            if (media) {
                var index = 1;
                $.each(media, function (file, meta) {
                    var listElement = window.mediaRepository.render(file, meta, self.renderOptions);
                    if(listElement[0].getElementsByClassName('referenceId').length != 0){
                        listElement[0].getElementsByClassName('referenceId')[0].innerHTML = '[' + index + ']';
                        index ++;
                    }
                    if (listElement) {
                        $list.append(listElement);
                    }
                });
                self.scroll.refresh();
            }

            // Set up live-search (on each key stroke):
            var $listElements = $(self.el).find("ul li");
            var $input = $(self.el).find("input");
            $input.keyup(function (e) {
                // Dismiss on enter:
                if (e.keyCode == 13) {
                    $input.blur();
                    return;
                }
                // Search and update list:
                var searchString = $input.val().toLowerCase();
                var searchTerms = searchString.split(/\s+/g);
                $listElements.each(function () {
                    var $listElement = $(this);
                    var found = searchTerms.reduce(function (found, searchTerm) {
                        return found && ($listElement.text().toLowerCase().indexOf(searchTerm) !== -1);
                    }, true);
                    $listElement.toggleClass("hidden", !found);
                });
                self.scroll.refresh();
            });
        }
    }

});
/**
 * Provides a database interface for accessing meta information of media content.
 * ------------------------------------------------------------------------------
 *
 * The module is loaded within index.html
 *
 * @module ap-media-repository.js
 * @requires jquery.js, media.json
 * @author Andreas Tietz, David Buezas, antwerpes ag
 */
app.register("ah-media-repository", function () {


    /**
     * Implements a database interface for accessing meta information of media content.
     * --------------------------------------------------------------------------------
     *
     * The media repository provides search and rendering capability
     * of arbitrary media information and content based on meta data
     * defined in a json file. Within this json file arbitrary meta
     * data can be associated with any type of file or content.
     * The solution is relying heavily the "convention over configuration" principle.
     *
     * Anatomy of a media entry:
     *
     *     // It's key can be any kind of unique string:
     *     // An example convention might be to put in the file path
     *     // of a real file the meta data should be associated with:
     *     "content/pdf/reference_01.pdf": {
	 *         // Meta data is defined as attributes. Those can really
	 *         // be completely arbitrary as long as there is a renderer
	 *         // implemented that is capable of processing these values.
	 *         // Currently string, number and boolean are the types of
	 *         // object values supported by the search/find functionality.
	 *         "title": "Doe J, Lorem Ipsum 1. 2005",
	 *         "referenceId": 3,
	 *         "allowDistribution": true,
	 *         "tags": "document pdf publication reference 3"
	 *     },
     *
     * Apart from searching, each media entry can also be rendered into
     * DOM via "renderers", e.g. in order to be displayed inside a list.
     * For each "type" of media (which is completely up to the developer to be defined),
     * a separate renderer must be implemented and registered at the media repository.
     * When a media entry is about to be rendered, the media repository uses the
     * renderer that matches first the media type of the file or content of the entry
     * (first come first serve at registration time).
     *
     * Anatomy of a media renderer:
     *
     *     MediaRepository.addRenderer({
	 *         // Regular expression used to determine what "type" of
	 *         // media entries are accepted by this renderer:
	 *         regex: <some regular expression>,
	 *         // Function returning a jQuery DOM element representing that
	 *         // media entry based on the filename or content as well as meta data
	 *         render: function (fileOrContent, meta, options) {
	 *             return <some generated jQuery DOM element>;
	 *         }
	 *     });
     *
     *
     * @class ap-media-repository
     * @constructor
     *
     *
     **/


    var _metadata;
    var _renderers;


    return {
        publish: {},
        events: {},
        states: [],
        onRender: function (el) {
            this.extend = app.registry.get('ap-media-repository');

            this.find = this.extend.find;

            window.mediaRepository = this; // export globally
            window.mediaRepository.load("media.json"); // load database file

            var createBasicMediaEntry; //forward declaration

            // Content:
            window.mediaRepository.addRenderer({
                regex: /^content\:\/\//,
                render: function (file, meta, options) {
                    return createBasicMediaEntry(file, meta, $.noop)
                        .addClass('content')
                        .append("<span class='title'>" + file.replace("content://", "") + "</span>")
                        .append("<span class='tags'>" + (meta.tags ? "[" + meta.tags + "]" : "") + "</span>");
                }
            });

            // PDF:
            window.mediaRepository.addRenderer({
                regex: /\.(pdf)$/,
                render: function (file, meta, options) {
                    //debugger;
                    ahUtils = app.module.get('ah-utils')
                    options = $.extend({
                        onTap: function () {
                            console.log('ag.openPDF("' + file + '", "' + ahUtils.stripTags(meta.title) + '")');
                            ag.openPDF(file, ahUtils.stripTags(meta.title));
                        }
                    }, options);
                    return createBasicMediaEntry(file, meta, options.onTap)
                        .addClass('pdf')
                        .append("<span class='title'>" + (meta.title || "") + "</span>")
                        .append("<span class='tags'>" + (meta.tags ? "[" + meta.tags + "]" : "") + "</span>");
                }
            });

            // URL:
            window.mediaRepository.addRenderer({
                /* http://blog.mattheworiordan.com/post/13174566389 */
                regex: /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/,
                render: function (file, meta, options) {
                    options = $.extend({
                        shouldAllowTap: function () {
                            return true;
                        },
                        onTap: function () {
                            console.log('window.open("' + file.replace(/^(https?|ftp)/, "agnitiodefaultbrowser") + '", "' + meta.title + '")');

                            //iOS Fix until Agnitio fixes their ag.openUrl function
                            //window.open(file.replace(/^(https?|ftp)/, "agnitiodefaultbrowser"));

                            ag.openURL(file, meta.title);


                        }
                    }, options);
                    return createBasicMediaEntry(file, meta, options.onTap)
                        .addClass('url')
                        .append("<span class='title'>" + (meta.title || "") + "</span>")
                        .append("<span class='url'>(" + file + ")</span>")
                        .append("<span class='tags'>" + (meta.tags ? "[" + meta.tags + "]" : "") + "</span>");
                }
            });

            // Video (with thumbnail):
            // Expecting a PNG thumbnail image by convention
            // e.g. thumbnail for "content/video/my_movie.mp4" is expected to be "content/video/my_movie.mp4.png"
            window.mediaRepository.addRenderer({
                regex: /\.(mov|mp4|m4v)$/,
                render: function (file, meta, options) {
                    options = $.extend({
                        onTap: function () {
                            $("<div class='videoPopup'><video src='" + file + "' controls/><div class='close'></div></div>")
                                .on("swipedown swipeup swiperight swipeleft", function (e) {
                                    e.stopPropagation();
                                })
                                .on("tap", function (event) {
                                    if ($(event.target).is(":not(video)")) $(this).remove();
                                }).appendTo("#presentation");
                            var v = $("#presentation").find('.videoPopup video').get(0)
                            if (v.load) v.load()
                        }
                    }, options);
                    var entry = createBasicMediaEntry(file, meta, options.onTap)
                        .addClass('video')
                        .append("<span class='title'>" + meta.title + "</span>")
                        .append("<span class='tags'>" + (meta.tags ? "[" + meta.tags + "]" : "") + "</span>");
                    entry.find(".icon").css({
                        "background-image": "url('" + file + ".png')",
                        "background-size": "contain"
                    });
                    return entry;
                }
            });

            createBasicMediaEntry = function (file, meta, onTap) {
                var presentationName = app.config.get('name');
                var storageNamespace = presentationName + ":attachmentStorage";
                var attachmentStorage = JSON.parse(localStorage[storageNamespace] || "{}");
                var index = window.mediaRepository.getIndex(file);

                var $emailAttachmentToggler = null;
                if (meta.allowDistribution) {
                    $emailAttachmentToggler = $("<div class='emailAttachmentToggler' />");
                    $emailAttachmentToggler.on("tap", function (e) {
                        attachmentStorage = JSON.parse(localStorage[storageNamespace] || "{}");

                        var file = $(this).parent().attr("data-file");
                        var isAttached = !attachmentStorage[file];
                        if (isAttached) {
                            attachmentStorage[file] = true;
                        } else {
                            delete(attachmentStorage[file]);
                        }
                        localStorage[storageNamespace] = JSON.stringify(attachmentStorage);
                        $("[data-file='" + file + "']").attr("data-is-attached", isAttached);
                    });
                }

                return $("<li/>")
                    .addClass('mediaEntry')
                    .attr("data-file", file)
                    .attr("data-is-attached", !!attachmentStorage[file])
                    .append($emailAttachmentToggler)
                    .append("<div class='icon' />")
                    .append(meta.referenceId ? "<div class='referenceId'>[" + index + "]</div>" : "")
                    .on("tap", ":not(.emailAttachmentToggler)", function () {
                        onTap.apply(this);
                    });
            };
        },
        onRemove: function (el) {

        },
        onEnter: function (el) {

        },
        onExit: function (el) {

        },
        sortReferences: function(object){
            var tempArray = [], tempArraySort, result = {};
            for(item in object){
                if(object.hasOwnProperty(item))
                    tempArray.push(object[item].title.toLowerCase())
            }
            tempArraySort = tempArray.sort();
            for(index in tempArraySort){
                for(item in object){
                    if(object.hasOwnProperty(item) && tempArraySort[index] == object[item].title.toLowerCase())
                        result[item] = object[item]
                }
            }
            return result
        },
        filterTrack: function(references) {
            var currentTrack = app.slideshow.getId(),
                filterReferences = {}, isFilterTrack = false;
            if(isFilterTrack){
                for(var key in references){
                    var currentFlow = references[key].flow.split(',');
                    if(currentFlow.indexOf(currentTrack) != -1)
                        filterReferences[key] = references[key]
                }
            }

            return Object.keys(filterReferences).length != 0 ? filterReferences : references
        },
        isReferenceId: function(data, media){
            if(data[media] && data[media].hasOwnProperty("referenceId")) {
                return data[media]
            }
            return false
        },
        getIndex: function(item){
            var data, media, mediaIndex, dataRef = {}, dataRefTrack={};
            data = window.mediaRepository.metadata();
            for (media in data) {
                if(this.isReferenceId(data, media))
                    dataRef[media] = this.isReferenceId(data, media)
            }
            dataRefTrack = this.filterTrack(dataRef);
            for (media in dataRefTrack) {
                if(data.hasOwnProperty(media)){
                    mediaIndex = Object.keys(dataRefTrack).indexOf(item) + 1;
                }
            }
            return mediaIndex;
        },
        load: function (file) {
            _metadata = {};
            _metadata = JSON.parse(app.cache.get(file));
            if (!_metadata) {
                $.ajax({
                    url: file,
                    dataType: "json",
                    success: function (json) {
                        _metadata = json;
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        throw "Error loading media repository metadata file '" + file + "': " + textStatus;
                    },
                    async: false
                });
            }
        },
        /**
         * Returns the meta data object representing the meta database at runtime.
         *
         * @method metadata
         * @return {Object} Reference to the meta data object.
         */
        metadata: function () {
            return _metadata;
        },
        /**
         * Adds a renderer to the renderer chain.
         *
         * @method addRenderer
         * @param {Object} Renderer to be added.
         */
        addRenderer: function (renderer) {
            _renderers = _renderers || [];
            _renderers.push(renderer);
        },
        /**
         * Renders a media entry. Uses the first matching renderer in the renderer chain.
         *
         * @method render
         * @param {String} file Media entry key.
         * @param {Object} meta Media entry meta data.
         * @param {Object} options Attributes passed to the designated renderer.
         * @return {jQuery Element} Rendered jQuery Element ready to be inserted into the DOM.
         */
        render: function (file, meta, options) {
            var renderer = _renderers.reduce(function (bestRenderer, currentRenderer) {
                return bestRenderer || (currentRenderer.regex.test(file) && currentRenderer)
            }, undefined);
            if (!renderer) console.log("Warning: no renderer found for media resource ", file);
            else return renderer.render(file, meta, options);
        }
    }

});
(function() {
  app.register('ah-notes', function() {
    return {
      publish: {
        useGlobal: false,
        attr: "data-notes",
        separator: ' ',
        separateValue: 1,
        useLatinNumbers: true,
        startIndexLetter: 64,
        parseNumberLetter: 26
      },
      events: {
        "tap .ag-overlay-x": "hide"
      },
      states: [
        {
          id: "show"
        }
      ],
      loadedData: null,
      _renderedNotes: {},
      onRender: function() {
        this.$root = $("#notes");
        this.$list = this.$root.find('ul');
        this._preventSwipe();
        this._dataSetPropName = this._getDatasetProp();
        this.init();
        if (this.scroll) {
          this.scroll.destroy();
        }
        return this.scroll = new IScroll($(".scroll")[0], {
          scrollbars: false,
          bounce: false
        });
      },
      handleEnter: function(el) {
        var notesForCurrentSlide;
        this.resetList();
        notesForCurrentSlide = this._renderedNotes[el.id];
        if (!notesForCurrentSlide) {
          return;
        }
        console.log(notesForCurrentSlide);
        return app.trigger('notes:update', {
          list: notesForCurrentSlide
        });
      },
      handleRender: function(el) {
        this._$el = $(".slide[id=" + el.id + "]");
        if (this._$el.length) {
          return this.updateNotes(this._$el);
        }
      },
      handleLeave: function() {},
      handleCloseOverlay: function() {
        var el;
        el = {};
        el.id = app.slideshow.get();
        return this.handleEnter(el);
      },
      _preventSwipe: function() {
        return this.$root.on("swipedown swipeup swiperight swipeleft", function(e) {
          return e.stopPropagation();
        });
      },
      show: function() {
        if (this.$list.find('li').length) {
          return this.goTo('show');
        }
      },
      hide: function() {
        return this.reset();
      },
      init: function() {
        this.loadedData = window.ahstrings.notes;
        this.handleRender({
          id: app.slideshow.get()
        });
        app.listenTo(app.slide, 'slide:render', this.handleRender.bind(this));
        app.on('history:update', this.handleEnter.bind(this));
        app.listenTo(app.slide, 'slide:exit', this.handleLeave.bind(this));
        return app.listenTo(app, 'close:overlay', this.handleCloseOverlay.bind(this));
      },
      _fiterUnique: function(keys) {
        return keys.filter(function(el, index, array) {
          return array.indexOf(el) === index;
        });
      },
      _parseDOM: function(el) {
        var bindedData, elId, elements, keys, notesKeys;
        keys = [];
        elements = [];
        elId = el[0].id;
        el.find("[" + this.props.attr + "]").each((function(_this) {
          return function(iterator, element) {
            var noteKey;
            noteKey = _this._getNotesKey(element);
            if (!noteKey.length) {
              _this._throwException("Invalid notes in " + elId);
            }
            keys.push(noteKey.trim());
            return elements.push(element);
          };
        })(this));
        notesKeys = keys.length ? this._fiterUnique(this._devideArrray(keys)) : [];
        bindedData = this._getBindedData(notesKeys, elements);
        this._setCounters(bindedData);
        this._renderedNotes[elId] = notesKeys;
        return notesKeys;
      },
      _throwException: function(message) {
        throw new Error(message);
      },
      _getBindedData: function(notesKeys, elements) {
        var binding;
        return binding = elements.map((function(_this) {
          return function(element) {
            var containedNotes;
            containedNotes = [];
            notesKeys.forEach(function(key, index) {
              if (_this._contains(_this._getNotesKey(element), key)) {
                return containedNotes.push({
                  key: key,
                  index: !_this.props.useGlobal ? index : _this._getAbsoluteCounter(key)
                });
              }
            });
            return {
              element: element,
              containedNotes: containedNotes
            };
          };
        })(this));
      },
      _setCounters: function(bindedData) {
        return bindedData.forEach((function(_this) {
          return function(item) {
            var counters, sequenced;
            counters = _this._getCounters(item.containedNotes);
            sequenced = _this._getSequence(counters);
            return item.element.textContent = sequenced.toString();
          };
        })(this));
      },
      resetList: function() {
        return this.$list.empty();
      },
      updateNotes: function(el) {
        return this._parseDOM(el);
      },
      _getSequence: function(counters) {
        var sequenced;
        sequenced = [];
        counters.reduce((function(_this) {
          return function(accum, curr, index, array) {
            var i, j, ref, ref1;
            if (curr === array[index + 1] - 1) {
              return ++accum;
            } else {
              if (accum > _this.props.separateValue) {
                sequenced.push((_this._generateLetterOrNumber(index - accum)) + "-" + (_this._generateLetterOrNumber(index)));
              } else {
                for (i = j = ref = index - accum, ref1 = index; ref <= ref1 ? j <= ref1 : j >= ref1; i = ref <= ref1 ? ++j : --j) {
                  sequenced.push(_this._generateLetterOrNumber(array[i]));
                }
              }
              return accum = 0;
            }
          };
        })(this), 0);
        return sequenced;
      },
      _generateLetterOrNumber: function(number) {
        if (this.props.useLatinNumbers) {
          return String.fromCharCode(this.props.startIndexLetter + (number % this.props.parseNumberLetter)).toLowerCase();
        } else {
          return number;
        }
      },
      _getCounters: function(containedNotes) {
        return containedNotes.map(function(item) {
          return ++item.index;
        });
      },
      _getNotesKey: function(element) {
        return element.dataset[this._dataSetPropName];
      },
      _devideArrray: function(keys) {
        return keys.join(this.props.separator).split(this.props.separator);
      },
      _contains: function(string, key) {
        return new RegExp("\\b" + key + "\\b", "g").test(string);
      },
      _getAbsoluteCounter: function(key) {
        return Object.keys(this.loadedData).indexOf(key);
      },
      _getDatasetProp: function() {
        var camelCased, propName;
        propName = this.props.attr.slice(5, this.props.attr.length);
        return camelCased = propName.replace(/-([a-z])/g, function(letter) {
          return letter[1].toUpperCase();
        });
      }
    };
  });

}).call(this);

/**
 * Provides a reference library section.
 * -------------------------------------
 * Implements a user interface for viewing media entries that have a "referenceId" attribute.
 *
 * @module ah-reference-library.js
 * @requires module.js, jquery.js, iscroll.js, media-repository.js
 * @author Andreas Tietz antwerpes ag
 */
app.register("ah-reference-library", function() {

  var self, $list;
  return {
    publish: {

    },
    events: {

    },
    states: [
      {
        id: "visible"
      }
    ],
    onRender: function(el) {
      $list = $(el).find("ul");
      self = this;

      app.$.on('open:ah-reference-library', function () {
        if(this.stateIsnt("visible"))
          this.show();
      }.bind(this));

      app.$.on('close:ah-reference-library', function () {
        this.hide();
      }.bind(this));

      app.$.on('toolbar:hidden', function () {
        this.hide();
        this.unload();
      }.bind(this));

      this.load(el);


    },
    onRemove: function(el) {

    },
    onEnter: function(el) {

    },
    onExit: function(el) {

    },

    hide: function () {
      app.unlock();
      this.reset();
    },

    show: function () {
      app.lock();
      this.goTo('visible');
      this.load();
    },

    unload: function() {
      if(this.stateIs("visible"))
        if(self.scroll) self.scroll.destroy();
    },
    sortReferences: function(object){
      var tempArray = [], tempArraySort, result = {};
      for(item in object){
        if(object.hasOwnProperty(item))
          tempArray.push(object[item].title.toLowerCase())
      }
      tempArraySort = tempArray.sort();
      for(index in tempArraySort){
        for(item in object){
          if(object.hasOwnProperty(item) && tempArraySort[index] == object[item].title.toLowerCase())
            result[item] = object[item]
        }
      }
      return result
    },
    filterTrack: function(references) {
      var currentTrack = app.slideshow.getId(),
          filterReferences = {}, isFilterTrack = true;
      if(isFilterTrack){
        for(var key in references){
          var currentFlow = references[key].flow.split(',');
          if(currentFlow.indexOf(currentTrack) != -1)
            filterReferences[key] = references[key]
        }
      }
      
      return Object.keys(filterReferences).length != 0 ? filterReferences : references
    },
    load: function(el){
      $list.empty();
      // Initialize scrolling:
      if(self.scroll) self.scroll.destroy();
      self.scroll = new IScroll(self.$(".scroll")[0], {scrollbars: true});

      // Fill the list with all media entries that have a reference id:
      var references = window.mediaRepository.find("", "referenceId");
      // var references = this.filterTrack(window.mediaRepository.find("", "referenceId"));
      if (references) {
        $.each(references, function (file, meta) {
          $list.append(window.mediaRepository.render(file, meta));
        });
        self.scroll.refresh();
      }
    }
  }

});
(function() {
  app.register('ah-slide-analytics', function() {
    return {
      publish: {},
      events: {},
      states: [],
      onRender: function(element) {
        this.modelData = app.model.get();
        this.analyticsExtend = app.module.get('agAnalytics');
        return app.on('slideEnter:inlineSlideshow', this.submitMonitoring.bind(this));
      },
      isSkippedSlide: function(id) {
        return app.model.getSlide(id).isSkipped;
      },
      submitMonitoring: function(data) {
        var components, slide, slideId;
        slideId = data.slide.id;
        if (this.isSkippedSlide(slideId)) {
          return;
        }
        slide = this.analyticsExtend.assignValues(slideId, 'slide');
        components = app.slideshow.resolve();
        return this.sendMonitoring(slide.id, slide.name, components.slideshow, components.chapter);
      },
      submitCustomSlideMonitoring: function(elementData) {
        var components, elementId, elementName;
        elementId = elementData.id;
        elementName = elementData.name;
        components = app.slideshow.resolve();
        return this.sendMonitoring(elementId, elementName, components.slideshow, components.chapter);
      },
      getChapterName: function(slideshowId) {
        return this.modelData.storyboards[slideshowId].name;
      },
      sendMonitoring: function(id, name, slideshow, chapter) {
        if (!window.ag) {
          return;
        }
        ag.submit.slide({
          id: id,
          name: name,
          path: app.slideshow.getPath(),
          chapter: this.getChapterName(slideshow),
          chapterId: chapter
        });
        return console.log(id, name);
      }
    };
  });

}).call(this);

(function() {
  app.register('ah-refs-notes-popup', function() {
    return {
      publish: {},
      events: {
        "tap .x": "hide",
        "swipeup": "stopSwipe",
        "swipedown": "stopSwipe",
        "swipeleft": "stopSwipe",
        "swiperight": "stopSwipe"
      },
      states: [
        {
          id: "show"
        }
      ],
      onRender: function(element) {
        this.element = $(element);
        this.refModule = app.module.get('ah-auto-references-popup');
        this.loadedNotes = window.ahstrings.notes;
        app.on('history:update', this.autoPopupHandler.bind(this));
        app.on('state:update', this.autoPopupHandler.bind(this));
        app.on('notes:update', this.getNotesList.bind(this));
        app.on('references:update', this.getRefsList.bind(this));
        this.$refWrapper = this.element.find('.refs-wrapper');
        this.$listRefs = this.element.find('.references');
        this.notesWrapper = this.element.find('.notes-wrapper');
        this.$listNotes = this.element.find('.notes');
        this.$scroll = this.element.find('.scroll')[0];
        return this.scroll = new IScroll(this.$scroll, {
          scrollbars: 'custom'
        });
      },
      getNotesList: function(data) {
        return this.notesList = data.list;
      },
      getRefsList: function(data) {
        return this.refsList = data.list;
      },
      refWithReplaceIndex: function($refElement, index) {
        return this.refModule.replaceIndex($refElement, index);
      },
      disable: function(node, list) {
        if (list.length <= 0) {
          return node.addClass('disable');
        }
      },
      enable: function(nodeElem) {
        return nodeElem.removeClass('disable');
      },
      generateRefsList: function(tmpReferences) {
        return Object.keys(tmpReferences).map((function(_this) {
          return function(file, index) {
            var $refElement;
            $refElement = window.mediaRepository.render(file, tmpReferences[file]);
            _this.refWithReplaceIndex($refElement, index);
            return $refElement;
          };
        })(this));
      },
      generateNotesList: function(notesList) {
        return notesList.map((function(_this) {
          return function(item) {
            return $("<li>" + _this.loadedNotes[item].description + "</li>");
          };
        })(this));
      },
      autoPopupHandler: function(data) {
        var $slide;
        $slide = $(data.slide.el);
        return $slide.find('[data-reference-id], [data-notes]').off('tap.auto-references-popup').on('tap.auto-references-popup', (function(_this) {
          return function(event) {
            _this.initRefAndNotesPopup(data, event);
            return _this.show();
          };
        })(this));
      },
      initRefAndNotesPopup: function(data, event) {
        event.stopPropagation();
        this.$listRefs.empty();
        this.$listNotes.empty();
        this.addRefsNodes(this.refsList);
        this.addNotesNodes(this.notesList);
        this.scroll.refresh();
        return this.scroll.scrollTo(0, 0);
      },
      addRefsNodes: function(refsList) {
        this.disable(this.$refWrapper, refsList);
        this.$listRefs.append(this.generateRefsList(refsList));
        return this.$refWrapper.append(this.$listRefs);
      },
      addNotesNodes: function(notesList) {
        this.disable(this.notesWrapper, notesList);
        this.$listNotes.append(this.generateNotesList(notesList));
        return this.notesWrapper.append(this.$listNotes);
      },
      stopSwipe: function() {
        return event.stopPropagation();
      },
      show: function() {
        return this.goTo('show');
      },
      hide: function() {
        this.reset();
        this.enable(this.$refWrapper);
        return this.enable(this.notesWrapper);
      }
    };
  });

}).call(this);

(function() {
  'use strict';
  var LinearSlider;

  window.LinearSlider = LinearSlider = (function() {
    function LinearSlider(element) {
      var dragHandler, dragLine, itemsArray, slider;
      slider = element.querySelector('.linear-slider');
      dragHandler = slider.querySelector('.arrow-btn');
      dragLine = slider.querySelector('.drag-line');
      itemsArray = slider.querySelectorAll('.text-item');
      this.dragLimit = dragLine.offsetWidth - dragHandler.offsetWidth;
      this.curElements = itemsArray.length - 1;
      this.curPosition = 0;
      this.dragElement = new Draggy(dragHandler, {
        restrictY: true,
        limitsX: [0, this.dragLimit],
        onChange: this.onSliderMove.bind(this)
      });
      element.addEventListener("onDrop", (function(_this) {
        return function() {
          dragHandler.classList.remove('active');
          return _this.dragElement.moveTo((_this.dragLimit * _this.curPosition) / _this.curElements, 0);
        };
      })(this));
    }

    LinearSlider.prototype.onSliderMove = function(x, y) {
      console.log(x);
      return this.curPosition = Math.round(x / (this.dragLimit / this.curElements));
    };

    return LinearSlider;

  })();

}).call(this);


/*
   app.module.get("ah-utils").getSlideData( SLIDE_ID, ( data )=>
      console.log( data )
    )
 */

(function() {
  var AhUtils;

  AhUtils = (function() {
    function AhUtils() {
      this.getSlideData = function(slideId, callback) {
        var path;
        path = "slides/" + slideId + "/strings.json";
        this.getData(path, callback);
      };
      this.getData = function(path, callback) {
        var xhr;
        xhr = new XMLHttpRequest;
        xhr.open('GET', path, false);
        xhr.onreadystatechange = function() {
          if (xhr.readyState !== 4) {
            return;
          }
          if (xhr.status !== 0 && xhr.status !== 200) {
            if (xhr.status === 400) {
              console.log("Could not locate " + path);
            } else {
              console.error("getSlideData " + path + " HTTP error: " + xhr.status);
            }
            return;
          }
          return callback(JSON.parse(xhr.responseText));
        };
        xhr.send();
      };
    }

    return AhUtils;

  })();

  app.register("ah-utils", function() {
    return new AhUtils;
  });

}).call(this);

(function() {
  app.register('ah-video', function() {
    return {
      publish: {
        assets: "shared/video/"
      },
      events: {},
      states: [],
      onRender: function(element) {
        var fullscreenButton;
        this.isOpened = false;
        this.agVideo = app.module.get('agVideo');
        fullscreenButton = this.agVideo.el.getElementsByClassName('ag-video-fullscreen-button')[0];
        fullscreenButton.addEventListener('tap', (function(_this) {
          return function() {
            if (_this.agVideo.el.classList.contains('ag-video-fullscreen')) {
              _this.agVideo.el.classList.remove('ag-video-fullscreen');
            } else {
              _this.agVideo.el.classList.add('ag-video-fullscreen');
            }
            event.preventDefault();
            event.stopPropagation();
            return _this.agVideo.updateSeekHandle();
          };
        })(this));
        return app.on('close:overlay', (function(_this) {
          return function() {
            if (_this.isOpened) {
              return _this.closeVideo();
            }
          };
        })(this));
      },
      initVideo: function(videoSet) {
        this.agVideo.videoEle.src = this.props.assets + videoSet.src;
        if (videoSet.monitor) {
          this.agVideo.props.monitor = true;
        }
        this.agVideo.videoEle.poster = videoSet.poster;
        this.agVideo.el.getElementsByClassName('placeholder')[0].innerText = videoSet.placeholder;
        return this.agVideo.videoEle.load();
      },
      openVideo: function() {
        setTimeout((function(_this) {
          return function() {
            return _this.agVideo.videoEle.currentTime = 0;
          };
        })(this), 0);
        return this.agVideo.onEnter(this.agVideo.el);
      },
      closeVideo: function() {
        this.agVideo.onExit(this.agVideo.el);
        setTimeout((function(_this) {
          return function() {
            _this.agVideo.videoEle.currentTime = 0;
            return _this.agVideo.reset();
          };
        })(this), 0);
        return this.hide();
      },
      show: function() {
        this.openVideo();
        this.agVideo.el.classList.add('show');
        app.lock();
        return this.isOpened = true;
      },
      hide: function() {
        this.agVideo.el.classList.remove('show');
        app.unlock();
        return this.isOpened = false;
      }
    };
  });

}).call(this);

app.register("ap-auto-menu-handle", function() {

  return {
    publish: {
        
    },
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    },
    onExit: function(el) {

    }
  }

});
/**
 * Implements auto auto-references.
 * --------------------------------
 * Listens to agnitios "slide:enter" once and to the "load"-event, if the slideshow changes
 * Collects all references made on each loaded slide e.g.
 *
 *     <sup data-reference-id="1">1</sup>
 *
 * and augments the side-clip on the slide by prepending the fully rendered reference elements.
 *
 * @module ap-auto-references.js
 * @requires jquery.js, auto-side-clip.js, media-repository.js, renderers.js
 * @author David Buezas, antwerpes ag
 */

app.register("ap-auto-references", function () {
    /**
     * Implements a reference library section.
     * ---------------------------------------
     * Implements a user interface for viewing media entries that have a "referenceId" attribute.
     *
     * @class ap-reference-library
     * @constructor
     */

    return {
        publish: {},
        events: {},
        states: [],
        onRender: function (el) {
            app.listenToOnce(app.slide, 'slide:enter', this.addAutoReferences.bind(this));
            app.listenTo(app.slideshow, 'load', this.addAutoReferences.bind(this));
        },
        onRemove: function (el) {

        },
        onEnter: function (el) {

        },
        onExit: function (el) {

        },

        addAutoReferences: function (data) {

            // Load all slide html files of the currently loaded collection
            // and gather all unique reference ids (asynchronously):

            $("article.slide").each(function() {
                var $slide = $(this);
                var referenceIds = {}; // unique reference ids
                $slide.find("[data-reference-id]").each(function () {

                    referenceIds[$(this).attr("data-reference-id")] = true;
                });
                referenceIds = Object.keys(referenceIds);

                // Find media resources associated with the collected reference ids:
                var references = {};
                $.each(window.mediaRepository.metadata(), function (file, meta) {

                    if (referenceIds.indexOf("" + meta.referenceId) > -1) {
                        references[file] = meta;
                    }
                });

                var $list = $("<ul class='references'/>");
                // Render all references into the list:
                $list.append($.map(references, function (meta, file) {
                    return window.mediaRepository.render(file, meta);
                }));
                $slide.find(".auto-side-clip").prepend($list);

            });

        }
    }

});
/**
 * Implements auto reference popups.
 * ---------------------------------
 *
 * Adds tap handlers to every element that has the [data-reference-id] data attribute.
 * On tap a popup with a list of all references in the slide appears.
 *
 * The module is loaded within index.html
 *
 * @module ap-auto-references-popup.js
 * @requires jquery.js, touchy.js
 * @author Andreas Tietz, antwerpes ag
 */
app.register("ap-auto-references-popup", function () {

    return {
        publish: {},
        events: {},
        states: [],
        onRender: function (el) {
            app.listenTo(app.slide, 'slide:enter', this.autoReferencePopupHandler.bind(this));
        },
        onRemove: function (el) {

        },
        onEnter: function (el) {

        },
        onExit: function (el) {

        },

        autoReferencePopupHandler: function (data) {
            var $slide = $("#" + data.id);
            var self = this;

            $slide.find("[data-reference-id]")
                .off('tap.auto-references-popup')
                .on('tap.auto-references-popup', function () {

                    // Collect unique reference ids:
                    var referenceIds = {};
                    $slide.find("[data-reference-id]").each(function () {

                        var referenceId = $(this).attr("data-reference-id");

                        if (referenceId.indexOf('-') > -1) {
                            var range = referenceId.split('-');
                            var from = parseInt(range[0]);
                            var to = parseInt(range[1]);

                            for(var i = from; i <= to; i++){
                                referenceIds[i] = true;
                            }
                        }
                        else
                        {
                            var ids = referenceId.split(',');


                            $.each(ids, function (index, value) {
                                referenceIds[value] = true;
                            });
                        }
                    });
                    referenceIds = Object.keys(referenceIds);

                    // Find media resources associated with the collected reference ids:
                    var references = {};
                    $.each(window.mediaRepository.metadata(), function (file, meta) {
                        if (referenceIds.indexOf("" + meta.referenceId) > -1) {
                            references[file] = meta;
                        }
                    });

                    // Render all references into a list:
                    var $scroll = $('<div class="scroll"/>');
                    var $list = $('<ul class="references"/>');
                    $list.append($.map(references, function (meta, file) {
                        return window.mediaRepository.render(file, meta);
                    }));
                    $scroll.append($list);



                    // Put list in popup:
                    var $popup = $('<div class="auto-references-popup" />')
                        .append('<header><div class="x">⊗</div><h1>References</h1></header>')
                        .append($scroll);

                    // Put popup in overlay:
                    $('<div class="auto-references-popup-overlay" />')
                        .append($popup)
                        .on("swipedown swipeup swiperight swipeleft", function (e) {
                            e.stopPropagation();
                        })
                        .on("tap", function (event) {
                            if ($(event.target).is(":not(.auto-references-popup, .auto-references-popup *) .x")) $(this).remove();
                        }).appendTo("#presentation");

                      // self.scroll.refresh();
                      if(self.scroll) self.scroll.destroy();
                      self.scroll = new IScroll($scroll[0], {scrollbars: true});

                });
        }
    }

});

/**
 * Implements auto sideclips.
 * --------------------------
 * Listens to agnitios "slideEnter" event. When this event is triggered, this module looks for DOM elements with the "auto-side-clip" class:
 *
 *      <div class="auto-side-clip"> SIDE CLIP CONTENT </div>
 *
 * and builds a side clip around it.
 * The generated side clips adapt their width and height to their content up to a limit. After that, they start being scrollable.
 *
 * @module ap-auto-side-clip.js
 * @requires jquery.js, touchy.js
 * @author David Buezas, antwerpes ag
 */
app.register("ap-auto-side-clip", function () {

    return {
        publish: {},
        events: {},
        states: [],
        onRender: function (el) {
            app.listenTo(app.slide, 'slide:enter', this.autoSideClipHandler.bind(this));
        },
        onRemove: function (el) {

        },
        onEnter: function (el) {

        },
        onExit: function (el) {

        },

        autoSideClipHandler: function (data) {
            var $slide = $("#" + data.id);

            // search for auto-side-clip elements not already set up
            $slide.find(".auto-side-clip:not(.configured)").each(function () {

                // mark thise auto-side-clip as already set up
                var $content = $(this).addClass("configured");

                var $sideClipHandle = $("<div class='sideClipHandle'/>");

                $content.wrap("<div class='contentContainer'/>"); //fixes webkit scroll render bug
                var $contentContainer = $content.parent();

                $contentContainer.wrap("<div class='sideClipContainer'/>");
                var $sideClipContainer = $contentContainer.parent();

                $sideClipHandle.appendTo($sideClipContainer);
                var $sideClipOverlay = $("<div class='sideClipOverlay'/>");
                $sideClipOverlay.insertBefore($sideClipContainer);

                function toggleSideClip() {
                    $sideClipContainer.toggleClass("active");
                    $sideClipOverlay.toggleClass("active");
                }

                $sideClipHandle.add($sideClipOverlay).on("tap", toggleSideClip);

                $sideClipOverlay.on("swipedown swipeup swiperight swipeleft", function (e) {
                    toggleSideClip();
                    e.stopPropagation();
                });
                $sideClipContainer.on("swipedown swipeup swiperight swipeleft", function (e) {
                    e.stopPropagation();
                });
            });
        }
    }

});
/**
 * Provides a function to navigate backwards
 * ---------------------------------------------------------
 *
 * Navigates backwards through the sequence of app.goTo calls.
 *
 * @module ap-back-navigation.js
 * @requires agnitio.js
 * @author David Buezas, antwerpes ag
 */


app.register("ap-back-navigation", function () {

    var self;
    return {
        publish: {},
        events: {
          "tap": "back"
        },
        states: [],
        history: [],
        active: false,
        path: null,
        onRender: function (el) {
            self = this;


            app.$.BackNavigation = this;
            app.slide.on('slide:enter',this.update.bind(this));
            app.slide.on('slide:exit',this.save.bind(this));

            this.monitorUsage();

        },
        onRemove: function (el) {
          self = null;
        },
        monitorUsage: function (el) {
          if (ag) {
            ag.submit.data({
              label: "Registered Module",
              value: "ap-back-navigation",
              category: "BHC Template Modules",
              isUnique: true,
              labelId: "bhc_registered_module",
              categoryId: "bhc_template_modules"
            });
          }
        },
        // Save the path of the slide we are currently on
        update: function(data) {
          this.path = app.getPath();
        },
        save: function(data) {
          if (!this.active && this.path) {
            this.history.push(this.path);
          }
          else {
            this.active = false;
          }
        },
        storeLastCollection: function(data) {
          self.prevCollection = data.id;
        },

        storePrevSlide: function (data) {
            self.prevSlide = data.prev.id;
        },

        setPrevCollection: function (id) {
          self.prevCollection = id;
        },

        back: function () {
          // When moving in history we will not add the slides we are moving to
          this.active = true;
          var path = this.history.pop();
          app.goTo(path);
        }
    }

});

/**
 * Provides a function to lock and connect slides
 * ---------------------------------------------------------
 * *
 * * This module allows the creation of groups of different slides.
 * These will be connected either horizontally or vertically and stay connected when creating custom collections.
 *
 * Usage:
 *
 * To define content groups, you need to create a `contentGroups.json` next to the `presentation.json` file.
 *
 * Remember to make sure that all slideshows in the `presentation.json` file match the rules defined by the different content groups.
 *
 * If they don't match, the group definition will be ignored and a warning will be given in the javascript console.
 *
 * e.g.:
 *
 * "test-group-1": {
 *       "orientation": "horizontal",
 *       "slides": ["radiology_slide_1", "radiology_slide_2"]
 *   },
 * "test-group-2": {
 *       "orientation": "vertical",
 *       "slides": ["radiology_slide_1", "radiology_slide_2", "radiology_slide_3"]
 *   }
 *
 * The content groups will be checked on each start
 *
 * The module is loaded within index.html
 *
 * @module ap-content-groups.js
 * @requires agnitio.js, contentGroups.json, ap-custom-collections.js
 * @author Marc Lutz, antwerpes ag
 */

app.register("ap-content-groups", function () {
    var self;
    return {
        definition: null,
        json: {},
        publish: {},
        events: {},
        states: [],
        onRender: function (el) {

            self = this;
            app.$.contentGroups = this;


            $.get('contentGroups.json').done(function (groupDefinition) {
                if (typeof groupDefinition === 'string') {
                    groupDefinition = JSON.parse(groupDefinition);
                }

                self.json = groupDefinition;

            });


            this.validateContentgroups();

            this.monitorUsage();

        },
        onRemove: function (el) {
          self = null;
        },
        monitorUsage: function (el) {
          if (ag) {
            ag.submit.data({
              label: "Registered Module",
              value: "ap-content-groups",
              category: "BHC Template Modules",
              isUnique: true,
              labelId: "bhc_registered_module",
              categoryId: "bhc_template_modules"
            });
          }
        },

        collectionContainsSlides: function (collection, groupDefinition) {
            /**
             * Implements a function to check if the collection contains a slide from the contentGroup.json
             * --------------------------------------------------------------------------------------------
             *
             * Tests if the collection needs validation
             * by checking if the collection has any slide from the contentGroup
             *
             * @class validateContentgroups
             * @constructor
             */


            // get all slides which are used in this collection
            var collectionClone = $.extend(true, {}, collection);

            var slidesUsed;

            if (collection.content) {

                if (collectionClone.content.length > 0) {
                    $.each(collectionClone.content, function (i, slideshowName) {
                        collectionClone.content[i] = app.model.getStructure(slideshowName).content;
                    });

                    slidesUsed = collectionClone.content.reduce(function (a, b) {
                        return a.concat(b)
                    });
                }

            } else {
                slidesUsed = collectionClone.slides;
            }

            // check if the collection contains any slide from this contentGroup
            var collectionContainsSlide = false;


            if (slidesUsed) {
                $.each(groupDefinition.slides, function (i, slide) {
                    if (~slidesUsed.indexOf(slide)) {
                        collectionContainsSlide = true;
                    }
                });

            }
            return collectionContainsSlide
        },



        validateContentgroups: function () {
            // validate groups


            /**
             * Implements a function to validate the content groups
             * -----------------------------------------------------------
             *
             * @class validateContentgroups
             * @constructor
             */

            //var customCollections = app.$.customCollectionsStorage.getAll();
            //$.extend(true, app.model.get().storyboards, customCollections);

            $.get('contentGroups.json').done(function (groupDefinition) {
                if (typeof groupDefinition === 'string') {
                    groupDefinition = JSON.parse(groupDefinition);
                }
                for (var property in groupDefinition) {
                    if (groupDefinition.hasOwnProperty(property)) {
                        var groupName = property;
                        var definition = groupDefinition[property];
                        if (definition.orientation === "horizontal") {
                            $.each(app.model.get().storyboards, function (i, collection) {
                                if (collection.type === "collection") {

                                    var collectionContainsSlide = self.collectionContainsSlides(collection, definition);
                                    // if it contains a slide, validate if its been used correctly

                                    if (collectionContainsSlide) {
                                        var firstSlides = [];
                                        if (collection.content) {
                                            // normal Collections
                                            $.each(collection.content, function (i, slideshow) {
                                                if (definition.slides.indexOf(app.model.getStructure(slideshow).content[0]) > -1) {
                                                    if (app.model.getStructure(slideshow).content.length === 1) {
                                                        // okay
                                                    } else {
                                                        console.warn('horizontal contentGroups may not have more than one vertical slides @%s', slideshow)
                                                    }
                                                }
                                                if(typeof app.model.getStructure(slideshow).content === 'string')
                                                    firstSlides.push(app.model.getStructure(slideshow).content);
                                                else
                                                    firstSlides.push(app.model.getStructure(slideshow).content[0]);
                                            });
                                            if (~firstSlides.join(':').indexOf(definition.slides.join(':'))) {

                                                console.log('contentGroup %s valid for presentation %s', groupName, collection.id);
                                                // write the matching contentgroups to the structures for use in the custom-collections


                                                var storyboard = app.model.hasStoryboard(collection.id) ? app.model.getStoryboard(collection.id) : app.model.getStoryboard(collection.name);

                                                if(storyboard){
                                                    if (!storyboard.contentGroups) {
                                                        storyboard.contentGroups = {};

                                                    }
                                                    storyboard.contentGroups[groupName] = definition;
                                                }

                                            } else {
                                                console.warn('contentGroup %s is invalid for presentation %s', groupName, collection.id);
                                                console.warn('the group definition will be ignored!');
                                                delete groupDefinition[groupName];
                                            }
                                        } else {
                                            // custom Collections
                                            $.each(collection.slideshows, function (i, slideshow) {
                                                // if the slideshow contains a slide from the group definition
                                                if (definition.slides.indexOf(slideshow.content[0]) > -1) {
                                                    // and is only 1 slide long vertically
                                                    if (slideshow.content.length === 1) {
                                                        // okay
                                                    } else {
                                                        console.warn('horizontal contentGroups may not have more than one vertical slides @%s', slideshow)
                                                    }
                                                }
                                                if(typeof app.model.getStructure(slideshow).content === 'string')
                                                    firstSlides.push(app.model.getStructure(slideshow).content);
                                                else
                                                    firstSlides.push(app.model.getStructure(slideshow).content[0]);
                                            });

                                            if (~firstSlides.join(':').indexOf(definition.slides.join(':'))) {
                                                console.log('contentGroup %s valid for presentation %s', groupName, collection.id);
                                                // write the matching contentgroups to the structures for use in the custom-collections
                                                var storyboard = app.model.hasStoryboard(collection.id) ? app.model.getStoryboard(collection.id) : app.model.getStoryboard(collection.name);

                                                if(storyboard){
                                                    if (!storyboard.contentGroups) {
                                                        storyboard.contentGroups = {};

                                                    }
                                                    storyboard.contentGroups[groupName] = definition;
                                                }
                                            } else {
                                                console.warn('contentGroup %s is invalid for presentation %s', groupName, collection.id);
                                                console.warn('the group definition will be ignored!');
                                                delete groupDefinition[groupName];
                                            }
                                        }
                                    }
                                }
                            });
                        } else if (definition.orientation === "vertical") {
                            $.each(app.model.get().storyboards, function (i, collection) {

                                if (collection.type === "collection") {
                                    var collectionContainsSlide = self.collectionContainsSlides(collection, definition);
                                    if (collectionContainsSlide) {
                                        var allSlides = [];
                                        if (collection.content) {
                                            $.each(collection.content, function (i, slideshow) {
                                                var slideshowSlides = app.model.getStructure(slideshow).content;
                                                allSlides.push(slideshowSlides);
                                            });

                                        } else {
                                            // custom Collection
                                            $.each(collection.slideshows, function (i, slideshow) {
                                                allSlides.push(slideshow.content);
                                            });
                                        }
                                        if (~allSlides.join(':').indexOf(definition.slides.join(','))) {
                                            console.log('contentGroup %s valid for presentation %s', groupName, collection.id);
                                            // write the matching contentgroups to the structures for use in the custom-collections
                                            var storyboard = app.model.hasStoryboard(collection.id) ? app.model.getStoryboard(collection.id) : app.model.getStoryboard(collection.name);

                                            if(storyboard){
                                                if (!storyboard.contentGroups) {
                                                    storyboard.contentGroups = {};

                                                }
                                                storyboard.contentGroups[groupName] = definition;
                                            }
                                        } else {
                                            console.warn('contentGroup %s is invalid for presentation %s', groupName, collection.id);
                                            console.warn('the group definition will be ignored!');
                                            delete groupDefinition[groupName];
                                        }
                                    }
                                }

                            });
                        } else {
                            console.log("unknown ContentGroup definition type");
                        }
                    }
                }

                self.definition = groupDefinition;
            });
        }
    }

});

/**
 * Implements Custom Collections Editor.
 * -------------------------------------
 *
 * This module allows the edition of custom collections directly on the client (iPad / Browser).
 *
 * @module ap-custom-collections.js
 * @requires jquery.js, iscroll.js, ap-custom-collections-storage.js, ap-overview.js
 * @author David Buezas, antwerpes
 */


app.register("ap-custom-collections", function () {


    var self;
    var eventNamespace = ".custom-collections";
    var _mouseMoveEvent;
    var _mouseUpEvent;
    var _mouseDownEvent;


    return {
        presentationName: null,
        dragData: null,
        publish: {},
        events: {
            "mousedown .o_slide": "startDragEventHandler",
            "touchstart .o_slide": "startDragEventHandler",
            "MSPointerDown .o_slide": "startDragEventHandler",
            "tap .collectionName": function (event) {
                self.markAlreadyUsedSlides();
            }
        },
        states: [
            {
                id: "visible"
            }
        ],
        onRender: function (el) {

            self = this;
            self.dragData = null;
            app.$.customCollections = this;

            app.$.on('open:ap-custom-collections', function (data) {
                this.show();

                this.load(data.presentationName);

            }.bind(this));

            app.$.on('close:ap-custom-collections', function () {
                this.hide();
                this.unload();
            }.bind(this));

            // Fix: above listener is never triggered because it's not a "button"
            // in bottom bar
            // This is called from ap-toolbar module
            app.$.on('close:ap-custom-collections-menu', function(){
                this.hide();
                this.unload();
            }.bind(this));

            app.$.on('toolbar:hidden', function () {
                this.hide();
            }.bind(this));

            var $editZone = $(".editZone");

            $(".cancel").on("tap", function () {
                // user canceled, so go back to the custom collection menu without saving
                self.unload();
                self.hide();
                app.$.overview.hide();
                app.$.customCollectionsMenu.show();

            });
            $(".save").on("tap", function () {

                // save and then go back to the custom collection menu
                var presentation = $.map($editZone.find(".o_slideshow"), function (slideshow) {
                    return [$.map($(slideshow).find(".o_slide"), function (slide) {
                        return $(slide).attr("data-id");
                    })];
                });


                var slides = [];
                var contentArray = [];
                $.each(presentation, function (index, slideshows) {
                    $.each(slideshows, function (index, id) {
                        slides.push(id);

                    });
                });

                var slideshowArray = [];

                var slideshowIdArray = [];
                $.each(presentation, function (i, slideshow) {

                    var slideshowId = "custom-slideshow-" + Math.floor((Math.random() * 10000) + 1);
                    var slideShowName;
                    switch (i) {
                        case 0:
                            slideShowName = "Home";
                            break;
                        case presentation.length - 1:
                            slideShowName = "Summary";
                            break;
                        default:
                            slideShowName = "Chapter " + i
                    }


                    slideshowIdArray.push(slideshowId);

                    var slideshowObj = {id: slideshowId, name: slideShowName, type: "slideshow", content: slideshow};
                    slideshowArray.push(slideshowObj);

                    app.model.addStructure(slideshowId, slideshowObj);

                });


                //var homeSlideName = slideshowArray[0].id;

                var existingCollection = app.$.customCollectionsStorage.get(self.presentationName) || {};
                var collectionId = existingCollection.id || null;
                if (!collectionId) {
                    collectionId = "custom-collection-" + Math.floor((Math.random() * 10000) + 1);
                }

                // If updating current slideshow, let's move back to first slide
                // This will prevent presentation to get stuck on removed slide
                if (collectionId === app.slideshow.getId()) app.slideshow.first();

                var existingStoryboard = app.model.getStoryboard(collectionId);

                if (existingStoryboard) {
                    app.model.updateContent(collectionId, slideshowIdArray);
                }
                else {
                    app.model.addStoryboard(collectionId, {
                        id: collectionId,
                        name: self.presentationName,
                        content: slideshowIdArray,
                        type: "collection"
                    });
                }

                var customCollection = {
                    id: collectionId,
                    name: self.presentationName,
                    type: "collection",
                    slideshows: slideshowArray,
                    slides: slides,
                    presentation: presentation,
                    isCustomPresentation: true
                };

                app.$.customCollectionsStorage.add(self.presentationName, customCollection);
                app.$.contentGroups.validateContentgroups();

                self.unload();
                self.hide();
                app.$.overview.hide();
                app.$.customCollectionsMenu.show();

                // Need to update the menu in case current slideshow was changed
                if (app.slideshow.getId() === collectionId) {
                    app.$.menu.setup(collectionId);
                }


            });

            this.monitorUsage();

        },
        onRemove: function (el) {
          self = null;
        },
        monitorUsage: function (el) {
          if (ag) {
            ag.submit.data({
              label: "Registered Module",
              value: "ap-custom-collections",
              category: "BHC Template Modules",
              isUnique: true,
              labelId: "bhc_registered_module",
              categoryId: "bhc_template_modules"
            });
          }
        },
        onEnter: function (el) {

        },
        onExit: function (el) {

        },

        hide: function () {

            app.unlock();
            this.reset();
            this.unload();


        },

        show: function () {
            app.lock();
            this.goTo('visible');
        },

        load: function (presentationName) {
            self.presentationName = presentationName;

            _mouseMoveEvent = touchy.events.move + eventNamespace;
            _mouseUpEvent = touchy.events.end + eventNamespace;
            _mouseDownEvent = touchy.events.start + eventNamespace;


            var $document = $(document);
            var slideHTML = "<div class='o_slide'>"; // thumbnail template
            var slideshowHTML = "<div class='o_slideshow'>"; // slideshow template
            var collectionHTML = "<div class='o_collection'>"; // collection template


            $(".presentationName").text(presentationName);

            var $editZone = $(".editZone");
            var storage = app.$.customCollectionsStorage.get(presentationName);
            var $overview = $(".overview");

            // add home and summary slides to the new presentation

            if (storage.presentation.length === 0) {
                storage.slideshows = [{
                    "id": "home",
                    "name": "Home",
                    "type": "slideshow",
                    "content": ["HomePageSlide"]
                }, {
                    "id": "discussion",
                    "name": "Discussion",
                    "type": "slideshow",
                    "content": ["Discussion60Slide"]
                }];
                storage.slides = ["home_slide", "discussion"];
                storage.presentation = [
                    ["home_slide"],
                    ["discussion"]
                ];
            }

            function checkImageAddText(src, ele, slide){
                var image = new Image;
                image.onload = function() {
                    if ('naturalHeight' in this) {
                        if (this.naturalHeight + this.naturalWidth === 0) {
                            this.onerror();
                            return false;
                        }
                    } else if (this.width + this.height == 0) {
                        this.onerror();
                        return false;
                    }
                    ele.text("");
                    return true;
                };
                image.onerror = function() {
                    ele.text(app.model.get().slides[slide].name);
                    console.log("thumbnail not loaded");
                    return false;
                };
                image.src = src;
            }

            // create the initial DOM representation from stored presentation
            var $o_collection = $(collectionHTML).append($.map(storage.presentation, function (slideshow) {
                var $slideshow = $(slideshowHTML);
                $slideshow.append($.map(slideshow, function (slideId) {

                    var $slide = $(slideHTML).attr("data-id", slideId);
                    $slide.css({"background-image": "url(slides/" + slideId + "/" + slideId + ".png)"});

                    var $thumbText = $("<p class='o_text' />").appendTo($slide);

                    checkImageAddText("slides/" + slideId + "/" + slideId + ".png", $thumbText, slideId);

                    $.each(app.$.contentGroups.json, function (groupName, group) {
                        if (group.slides.indexOf(slideId) < 0) {
                            return
                        }
                        else if (group.slides.indexOf(slideId) == 0){
                            $slide.addClass('first-in-group');
                        }
                        $slide.data('contentGroup', group);
                        $slide.addClass('grouped');
                        $slide.addClass(groupName);
                        
                        if (group.orientation == 'vertical'){
                            $slide.addClass('vertical-group');
                        }
                        if (group.orientation === 'horizontal') {
                            $slideshow.addClass('grouped');
                            if (group.slides.indexOf(slideId) == 0){
                                $slideshow.addClass('first-in-group');
                            }
                        }
                    });

                    return $slide;
                }));
                return $slideshow;
            }));

            $editZone.append($o_collection);

            self._setupPlaceholders();
            self.markAlreadyUsedSlides();

            var slideWidth = 136;
            var slideHeight = 102;

            app.$.overview.showCustomOverview();


            self.markAlreadyUsedSlides();

            // each time a new collection is selected on the overview, new slide thumbnails are created
            // so we have to re-mark the slides which are already present in the edit zone


            self.scroll = new IScroll($editZone[0], {
                scrollbars: true,
                mouseWheel: true,
                scrollX: true,
                directionLockThreshold: Number.MAX_VALUE
            });


            /*
             Stores information about the dragged slide while it is being dragged.
             At runtime it looks like this:
             {
             startPos: {
             x: number,
             y: number
             },
             $clone: (jQ object) draggable slide clone,
             $original: (jQ object) original slide,
             currentTarget: DOM element
             */


            $document.on(_mouseMoveEvent, function (event) {
                if (event.target.classList.contains('alreadyUsed')) {
                    // slide comes from the overview and is already present in the edit zone
                    // so we forbid it by returning now
                    return;
                }
                if (self.dragData == null || self.dragData == undefined) return; // if drag data is undefined, the user is not dragging anything

                var touch, target;

                self.dragData.didDrag = true;

                if (touchy.isTouch) {
                    touch = event.originalEvent.targetTouches[0];
                    target = document.elementFromPoint(touch.pageX, touch.pageY);
                } else {
                    touch = event;
                    target = event.target;
                }
                /* Drag Move*/
                self.dragData.$clone.css("transform",
                    "translate3d(" +
                    (touch.pageX - self.dragData.startPos.x) + "px," +
                    (touch.pageY - self.dragData.startPos.y) + "px," +
                    "0" +
                    ")"
                );


                self.dragData.$clone.css("z-index", "9999");
                /* Drag over */

                var newTarget = null;
                var contentGroup = self.dragData.$original.data('contentGroup');
                if (target && target.classList.contains("o_slide")) {

                    // if dragging over a slide, decide which placeholder (over, under, left, right) to mark
                    // based on over which quartile the finger (mouse) is
                    var $underlyingSlide = $(target);
                    var overSlidePos = $underlyingSlide.offset();
                    var x = (overSlidePos.left + slideWidth / 2) - touch.pageX;
                    var y = (overSlidePos.top + slideHeight / 2) - touch.pageY;
                    var angle = Math.atan2(y, -x);
                    var PI = Math.PI;
                    var $underlyingSlideshow = $underlyingSlide.parent();
                    if (angle > -PI / 4 && angle < PI / 4) { // right
                        newTarget = $underlyingSlideshow.next(".placeholder")[0];
                    } else if (angle > PI / 4 && angle < PI * 3 / 4) { // over
                        newTarget = $underlyingSlide.prev(".placeholder")[0];
                    } else if (angle > PI * 3 / 4 || angle < -PI * 3 / 4) { // left
                        newTarget = $underlyingSlideshow.prev(".placeholder")[0];
                    } else if (angle > -PI * 3 / 4 && angle < -PI / 4) { // under
                        newTarget = $underlyingSlide.next(".placeholder")[0];
                    }
                    if (!(contentGroup && contentGroup.orientation == 'horizontal' && $(target).parent('.o_collection').length > 0)) {
                        newTarget = null;
                    } else {
                        $editZone.addClass("highlighted");
                    }


                } else if (target && target.classList.contains('placeholder')) {
                    newTarget = target;
                    if (contentGroup && contentGroup.orientation == 'horizontal' && !($(target).parent('.o_collection').length > 0)) {
                        newTarget = null
                    }
                }
                if (self.dragData.currentTarget !== newTarget) {
                    if (self.dragData.currentTarget) self.dragData.currentTarget.classList.remove("target");
                    self.dragData.currentTarget = null;
                    if (newTarget) {
                        self.dragData.currentTarget = newTarget;
                        self.dragData.currentTarget.classList.add("target");
                    }
                }
            });

            $document.on(_mouseUpEvent, function (event) {

                /* Drop */
                if (self.dragData == null || self.dragData == undefined) return;	// if drag data is undefined, the user is not dragging anything
                self.dragData.$original.removeClass("source");

                if (self.dragData.currentTarget) {
                    /* Drop Insert */
                    var $newSlide = self.dragData.$original.clone(true).off().removeClass("highlighted");

                    $(self.dragData.currentTarget).replaceWith($newSlide);

                    var contentGroup = $newSlide.data('contentGroup');
                    if (contentGroup) {
                        $newSlide.addClass('grouped');
                        $newSlide[0].classList.add('first-in-group');
                        if (contentGroup.orientation == 'vertical'){
                            $newSlide.addClass('vertical-group');
                        }
                        if (contentGroup && contentGroup.orientation == 'vertical' && !$newSlide.parent().is(".o_slideshow")) {
                            $newSlide.wrapAll(slideshowHTML);
                        }
                    }

                    if ($newSlide.parent().is(".o_slideshow")) {
                        $newSlide.css({
                            height: 0
                        });
                        $newSlide
                            .animate({
                                height: slideHeight
                            }, 500, "easeOutBounce", function () {
                                self.scroll.refresh();
                            });
                    } else {
                        $newSlide.wrap(slideshowHTML);
                        var $slideshow = $newSlide.parent();
                        $slideshow.css({
                            width: 0
                        });
                        $slideshow
                            .animate({
                                width: slideWidth
                            }, 500, "easeOutBounce", function () {
                                self.scroll.refresh();
                            });
                    }
                    if (contentGroup && contentGroup.orientation === 'horizontal') {
                        $newSlide.parents('.o_slideshow').addClass('grouped');

                        $newSlide[0].parentNode.classList.add('first-in-group');
                    }
                }
                self.dragData.$clone.remove();

                var comesFromCustomPresentation = $editZone.find(self.dragData.$original).length > 0;
                if (comesFromCustomPresentation && self.dragData.didDrag) {
                    /* Delete original */
                    var originalIsAlone = (self.dragData.$original.siblings(".o_slide").length === 0);

                    if (originalIsAlone) {
                        var $slideshow = self.dragData.$original.parent();
                        var placeholder = $slideshow.prev(".placeholder");
                        placeholder.css("transition", "none");
                        $slideshow.add(placeholder)
                            .animate({
                                width: 0
                            }, 500, "easeOutBounce", function () {
                                $(this).remove();
                                self._setupPlaceholders();
                                self.markAlreadyUsedSlides();
                                self.scroll.refresh();
                            });
                    } else {
                        var placeholder = self.dragData.$original.next(".placeholder");
                        placeholder.css("transition", "none");
                        self.dragData.$original.add(placeholder)
                            .animate({
                                height: 0
                            }, 500, "easeOutBounce", function () {
                                $(this).remove();
                                self._setupPlaceholders();
                                self.markAlreadyUsedSlides();
                                self.scroll.refresh();
                            });
                    }
                }
                self._setupPlaceholders();
                self.dragData = undefined;
                self.markAlreadyUsedSlides();
                self.scroll.enable();
            });

        },

        unload: function () {
            $(".editZone").empty();
            app.$.overview.unload();
            self.dragData = null;
            $(document).off(_mouseUpEvent);
            $(document).off(_mouseMoveEvent);
        },

        /**
         * Adds and removes placeholders as needed.
         * Thumbnails are dropped into placeholders.
         * @private
         * @method _setupPlaceholders
         */
        _setupPlaceholders: function () {
            var placeholderHTML = "<div class='placeholder'>"; // placeholder template, slides can be dragged into placeholders
            var $editZone = $(".editZone");
            $editZone.find(".o_slide:first-child," +
            ".o_slide+.o_slide, .o_slideshow+.o_slideshow")
                .not('.o_slideshow:first-child .o_slide:first-child, ' +
                '.o_slideshow:last-child .o_slide:first-child, ' +
                '.o_slide.grouped+.o_slide.grouped, .o_slideshow.grouped + .o_slideshow.grouped')
                .before(placeholderHTML);
            $editZone.find(".first-in-group.vertical-group").before(placeholderHTML);
            $editZone.find(".o_slide:last-child")
                .not('.o_slideshow:first-child .o_slide, .o_slideshow:last-child .o_slide')
                .after(placeholderHTML);
            $editZone.find(".o_collection:empty").append(placeholderHTML);
            $editZone.find(".placeholder+.placeholder").prev().remove();
            $editZone.find(".o_slideshow > .placeholder:only-child, .o_slideshow:empty").remove();
            $editZone.find(".placeholder+.placeholder").prev().remove(); // must be repeated!

            // show the first vertical placeholder fully so that you see immediately where you need to drop the first slide(s)
            if ($editZone.find('.o_slideshow + .placeholder + .o_slideshow').length == 1) {
                $editZone.find('.o_slideshow + .placeholder + .o_slideshow').prev().addClass('expanded');
            } else {
                $editZone.find('.o_slideshow + .placeholder + .o_slideshow').prev().removeClass('expanded');
            }
        },


        /**
         * Adds the class "alreadyUsed" to all the overview slides which were added to the edit zone.
         * This class will forbid marked slides from being added again
         * @private
         * @method markAlreadyUsedSlides
         */
        markAlreadyUsedSlides: function () {
            var scope = "#" + self.id;
            var $editZone = $(".editZone");
            var $overview = $(scope + " .custom-collections .overview .collectionOverview");
            $overview.find(".alreadyUsed").removeClass("alreadyUsed");
            $editZone.find(".o_slide").each(function () {

                $overview.find("[data-id='" + $(this).attr("data-id") + "']").addClass('alreadyUsed');
            });
        },

        startDragEventHandler: function (event) {
            /* Drag Start */
            if (event.target.classList.contains('alreadyUsed')) {
                // slide comes from the overview and is already present in the edit zone
                // so we forbid it by returning now
                return;
            }

            var touch = (touchy.isTouch) ? event.targetTouches[0] : event;

            self.scroll.disable();

            var $originalSlide = $(event.target);
            var $clone = $originalSlide.clone().removeClass("highlighted");

            var contentGroups = $originalSlide.parents('.o_collection').data('contentGroups');
            var originalSlideOffsetCompensation = {
                x: 0,
                y: 0
            };

            if (contentGroups) {
                var originalSlideId = $originalSlide.data('id');
                var scope = "#" + self.id;
                $.each(contentGroups, function (groupName, group) {

                    if (group.slides.indexOf(originalSlideId) < 0) {
                        return
                    }
                    if (group.orientation === 'vertical') {
                        originalSlideOffsetCompensation = {
                            x: 0,
                            y: (group.slides.indexOf(originalSlideId) * $originalSlide.height()) * -1
                        }
                    } else {
                        originalSlideOffsetCompensation = {
                            x: (group.slides.indexOf(originalSlideId) * $originalSlide.width()) * -1,
                            y: 0
                        }
                    }


                    var selector = '';

                    $.each(group.slides, function (i, slide) {
                        if (i != 0) {
                            selector += ','
                        }
                        selector += scope + ' .o_slide[data-id="' + slide + '"]'
                    });

                    $originalSlide = $(selector);
                    $clone = $originalSlide.clone(true).off();
                });

            } else if (contentGroup = $originalSlide.data('contentGroup')) {

                var selector = '';
                $.each(contentGroup.slides, function (i, slide) {
                    if (i != 0) {
                        selector += ','
                    }
                    selector += '.editZone .o_slide[data-id="' + slide + '"]'
                });
                if (contentGroup.orientation === 'vertical') {
                    originalSlideOffsetCompensation = {
                        x: 0,
                        y: (contentGroup.slides.indexOf($originalSlide.data('id')) * $originalSlide.height()) * -1
                    }
                } else {
                    originalSlideOffsetCompensation = {
                        x: (contentGroup.slides.indexOf($originalSlide.data('id')) * $originalSlide.width()) * -1,
                        y: 0
                    }
                }
                $originalSlide = $(selector);
                $clone = $originalSlide.clone(true).off();
            }

            self.dragData = {
                startPos: {
                    x: touch.pageX + originalSlideOffsetCompensation.x,
                    y: touch.pageY + originalSlideOffsetCompensation.y
                },
                $clone: $clone,
                $original: $originalSlide,
                currentTarget: null,
                didDrag: false
            };
            self.dragData.$original.addClass("source");
            var offset = self.dragData.$original.offset();
            self.dragData.$clone
                .css({
                    left: offset.left,
                    top: offset.top
                });

            // the clone must be inserted at the end of the body, so that it can "float" over everything else
            // without having to use z-indexes and overflow visibility
            $("body").append(self.dragData.$clone);

            event.preventDefault(); // prevent text selection on desktop
        }

    }

});

/**
 * Implements Custom Collections Menu.
 * -----------------------------------
 *
 * This module allows the creation / naming / deletion and loading of custom collections.
 *
 * @module ap-custom-collections-menu.js
 * @requires jquery.js, iscroll.js, custom-collections-storage.js
 * @author David Buezas, antwerpes ag
 */
app.register("ap-custom-collections-menu", function () {


    var translation = {
        "ENTER_PRESENTATION_NAME": "Please enter a name for your presentation",
        "PRESENTATION_NAME": "Presentation name",
        "NAME_ALREADY_EXISTS": "Another presentation exists with the same name, please choose a new one",
        "$PRESENTATION_NAME$_WILL_BE_REMOVED": "'$PRESENTATION_NAME$' will be erased",
        "OK": "OK",
        "CANCEL": "Cancel",
        "YES": "Yes",
        "NO": "No",
        "PRESENTATION_IS_EMPTY": "This presentation is empty.",
        "NOT_ALLOWED_TO_DELETE": "Deleting the current presentation is not allowed"
    };

    /**
     * Implements Custom Collections Menu
     * ------------------------------------
     * This module allows the creation / naming / deletion and loading of custom collections.
     *
     * @class ap-custom-collections-menu
     * @constructor
     */

    var self;
    return {
        publish: {},
        events: {
            "tap .newPresentation": "createNewPresentation",
            "tap .trash": "deletePresentation",
            "tap .rename": "renamePresentation",
            "tap .presentation": "openPresentation",
            "tap .edit": "editPresentation",
            "tap .favorite": "favorisePresentation"
        },
        states: [
            {
                id: "visible"
            }
        ],
        onRender: function (el) {

            self = this;

            app.$.customCollectionsMenu = this;

            app.$.on('open:ap-custom-collections-menu', function(){

                this.show();

            }.bind(this));

            app.$.on('close:ap-custom-collections-menu', function(){
                this.hide();
            }.bind(this));

            app.$.on('toolbar:hidden', function(){
                this.hide();
            }.bind(this));



            self.appriseDefaults = {
                textOk: translation["OK"],
                textCancel: translation["CANCEL"],
                textYes: translation["YES"],
                textNo: translation["NO"]
            };

            $(".presentationsContainer").sortable({
                items: ".row:not(.newPresentationButtonContainer)",
                scroll: true,
                update: function (event, ui) {
                    self.updateCustomPresentationOrder();
                }
            });


            $.each(app.$.customCollectionsStorage.getAll(), function (index, presentationObject) {
                self._addPresentationToView(presentationObject.name, presentationObject, /* animated: */false);
            });
        },
        onRemove: function (el) {

        },
        onEnter: function (el) {

        },
        onExit: function (el) {

        },

        hide: function () {
            app.unlock();
            this.reset();
        },

        show: function () {
            app.lock();
            this.goTo('visible');
        },


        /**
         * Adds a new entry to the menu
         * -----------------------------------
         *
         * @param {string} presentationName
         * @param {object} presentationObject
         * @param {boolean} animated
         * @private
         * @method _addPresentationToView
         */
        _addPresentationToView: function (presentationName, presentationObject, animated) {
            var self = this;
            var $row = $(".row.template").clone();


            $row.removeClass("template");
            $row.data("presentationName", presentationName);
            $row.find(".presentation .name").text(presentationName);
            if (presentationObject.isFavorite)
                $row.find(".favorite").addClass('selected');
            $row.insertBefore(self.$(".newPresentationButtonContainer"));


        },

        /**
         * Updates the current order of custom presentations
         * @public
         * @method updateCustomPresentationOrder
         */
        updateCustomPresentationOrder: function () {
            var self = this;

            var rowsArray = [];

            $('.presentationsContainer').children('.row').each(function () {
                var presentationName = $(this).data("presentationName");

                if (presentationName != undefined) {

                    if (app.$.customCollectionsStorage.getFavorites().length < 3) {
                        var isFavorite = $(this).find('.favorite').hasClass('selected');

                        var collection = app.$.customCollectionsStorage.get(presentationName);

                        collection.isFavorite = isFavorite;

                        app.$.customCollectionsStorage.add(presentationName, collection);
                    }

                    rowsArray.push(presentationName);
                }


            });

            if (rowsArray.length > 1)
                app.$.customCollectionsStorage.updateOrder(rowsArray);

            app.$.trigger("update:favorites");

        },

        /**
         * creates a new presentation
         * @public
         * @method createNewPresentation
         */
        createNewPresentation: function(){
            function askName(defaultName) {
                var optn = $.extend({}, self.appriseDefaults, {input: defaultName});
                apprise(translation["ENTER_PRESENTATION_NAME"], optn, function (newName) {
                    if (newName === false || newName === "") {
                        return; //user canceled
                    } else if (app.$.customCollectionsStorage.get(newName) != undefined) {
                        apprise(translation["NAME_ALREADY_EXISTS"], self.appriseDefaults, function () {
                            askName(newName); //ask again
                        });
                    } else {
                        var collectionId = "custom-collection-" + Math.floor((Math.random() * 10000) +1);

                        var presentationObject = {
                            id: collectionId,
                            name: newName,
                            type: "collection",
                            slideshows: [{
                                "id": "home",
                                "name": "Home",
                                "type": "slideshow",
                                "content": ["HomePageSlide"]
                            }, {
                                "id": "discussion",
                                "name": "Discussion",
                                "type": "slideshow",
                                "content": ["Discussion60Slide"]
                            }],
                            slides: ["HomePageSlide", "Discussion60Slide"],
                            presentation: [
                              ["HomePageSlide"],
                              ["Discussion60Slide"]
                            ],
                            isCustomPresentation: true,
                            isFavorite: false
                        };
                        app.$.customCollectionsStorage.add(newName, presentationObject);
                        self._addPresentationToView(newName, presentationObject, true);
                    }
                });
            }

            var standardName = translation["PRESENTATION_NAME"];
            askName(standardName);
        },


        /**
         * deletes the presentation
         * @public
         * @method deletePresentation
         */
        deletePresentation: function(event){

            var $row = $(event.target).parent();
            var presentationName = $row.data("presentationName");
            var confirmationTitle = translation["$PRESENTATION_NAME$_WILL_BE_REMOVED"].replace("$PRESENTATION_NAME$", presentationName);
            var optn = $.extend({}, self.appriseDefaults, {confirm: true});
            var current = app.slideshow.getId();
            var collections = app.$.customCollectionsStorage.getAll();
            var collectionData = collections[presentationName];
            if (collectionData && collectionData.id !== current) {
              apprise(confirmationTitle, optn, function (answer) {
                  if (answer) {
                      app.$.customCollectionsStorage.delete(presentationName);
                      $row.animate({height: 0}, function () {
                          $(this).remove();

                      });
                  }
              });

              app.$.trigger("update:favorites");
            }
            else {
              apprise(translation["NOT_ALLOWED_TO_DELETE"], self.appriseDefaults);
            }
        },


        /**
         * renames the presentation
         * @public
         * @method renamePresentation
         */
        renamePresentation: function(event){
            var $row = $(event.target).parent();

            function askName(defaultName) {
                var optn = $.extend({}, self.appriseDefaults, {input: defaultName});
                apprise(translation["ENTER_PRESENTATION_NAME"], optn, function (newName) {
                    if (newName === false || newName == oldName || newName === "") {
                        return; //user canceled
                    } else if (app.$.customCollectionsStorage.get(newName) != undefined) {
                        apprise(translation["NAME_ALREADY_EXISTS"], self.appriseDefaults, function () {
                            askName(newName); //ask again
                        });
                    } else {
                        app.$.customCollectionsStorage.rename(oldName, newName);
                        $row.data("presentationName", newName);
                        var $name = $row.find(".name");
                        $name.css({opacity: 0});
                        setTimeout(function () {
                            $name.text(newName);
                            $name.css({opacity: 1});
                        }, 500);
                    }
                });
            }

            var oldName = $row.data("presentationName");
            askName(oldName);
        },

        /**
         * open the presentation
         * @public
         * @method openPresentation
         */

        openPresentation: function(event){

            var presentationName = $(event.target).parent().parent().data("presentationName");

            if(presentationName == undefined)
                presentationName = $(event.target).parent().data("presentationName");


            var collection = app.$.customCollectionsStorage.get(presentationName);

            //app.menu.linksConfig[homeSlideName] = {title: "<div class='homeIcon' />", classname: "home"}

            if (collection.slideshows.length > 0) {
                var slideshowIdArray = [];
                $.each(collection.slideshows, function (i, slideshow) {
                    slideshowIdArray.push(slideshow.id);
                    var slidesArrary = [];
                    $.each(slideshow.content, function (i, slide) {
                        slidesArrary.push(slide);
                    });

                    var temp = {
                        id: slideshow.id,
                        name: slideshow.name,
                        content: slidesArrary,
                        type: "slideshow"
                    };

                    if(!app.model.hasStructure(slideshow.id))
                        app.model.addStructure(slideshow.id, temp);

                });

                var storyboardData = {
                    id: collection.id,
                    name: collection.name,
                    content: slideshowIdArray
                };

                if(!app.model.hasStoryboard(collection.id))
                    app.model.addStoryboard(collection.id, storyboardData);

                app.slideshow.init(collection.id);
                app.slideshow.load(collection.id);
                app.$.toolbar.hide();

            } else {
                apprise(translation["PRESENTATION_IS_EMPTY"], self.appriseDefaults);
            }
        },


        /**
         * edit the presentation
         * @public
         * @method editPresentation
         */

        editPresentation: function(event) {
            // load custom collections editor into own container
            /*
            new CustomCollections({
                $container: self.$container,
                presentationName: $(this).parent().data("presentationName")
            });
            */
            self.hide();
            var trigger = "open:ap-custom-collections";
            var presentationName = $(event.target).parent().data("presentationName");

            app.$.trigger(trigger, {presentationName: presentationName});


        },

        /**
         * favorise the presentation
         * @public
         * @method favorisePresentation
         */

        favorisePresentation:function(event) {
            // set favorite
            var $this = $(event.target);

            var presentationName = $this.parent().data("presentationName");
            var collection = app.$.customCollectionsStorage.get(presentationName);

            if (app.$.customCollectionsStorage.getFavorites().length < 3 || collection.isFavorite) {
                $this.toggleClass("selected");

                collection.isFavorite = !collection.isFavorite;

                app.$.customCollectionsStorage.add(presentationName, collection);
            }
            else {
                var $popup = $('#maxFavoritesPopUp');

                $popup.fadeIn();

                $popup.delay(1600).fadeOut();
            }

            app.$.trigger("update:favorites");
        }


    }

});

/**
 * Implements Custom Collections Storage.
 * -------------------------------------
 * This module manages the storage (localStorage) for custom collections.
 *
 * The module is loaded within index.html
 *
 * @module ap-custom-collections-storage.js
 * @requires jquery.js
 * @author David Buezas, antwerpes
 */


app.register("ap-custom-collections-storage", function() {


  /**
   * Implements Custom Collections Storage.
   * -------------------------------------
   * This class manages the storage (localStorage) for custom collections.
   *
   * @class ap-custom-collections-storage
   * @constructor
   */
  var self;
  return {
    publish: {

    },
    events: {

    },
    onRender: function(el) {

      if(app.config.get('name') === "Bayer Health Care Rainmaker Template (replace with the name of your presentation)")
        console.error("### Please update the name of the presentation in the config.json ###");

      self = this;
      app.$.customCollectionsStorage = this;
    },

    /**
     * Deletes the custom collection named collectionName from the localStorage
     *
     * @method delete
     * @param {string} collectionName
     */
    delete : function(collectionName){
      var presentationName = app.config.get('name');
      var storageNamespace = presentationName + ":CustomCollections";
      var customCollections = self.getAll();
      var current = app.slideshow.getId();
      console.log('Delete collection', customCollections[collectionName]);
      if (customCollections[collectionName] !== current) {
        delete(customCollections[collectionName]);
        app.model.deleteStoryboard(collectionName);
        localStorage[storageNamespace] = JSON.stringify(customCollections);
      }
      else {
        alert()
      }
    },
    /**
     * returns all custom collections as an associative array from collection name to collection representation
     *
     * @method getAll
     */
    getAll : function(){
      var presentationName = app.config.get('name');
      var storageNamespace = presentationName + ":CustomCollections";
      try{
        return JSON.parse(localStorage[storageNamespace] || "{}");
      } catch (e){
        console.log("Custom Presentations storage is corrupt or empty and will be reseted: "+JSON.stringify(e));
        delete(localStorage[storageNamespace]);
      }
      return {};
    },
    /**
     * Adds a custom collection to the localStorage
     *
     * @method add
     * @param {string} collectionName name of the collection to store
     * @param {string} collectionObject object representation of the collection
     */
    add : function(collectionName, collectionObject){
      var presentationName = app.config.get('name');
      var storageNamespace = presentationName + ":CustomCollections";
      var customCollections = self.getAll();

      customCollections[collectionName] = collectionObject;

      localStorage[storageNamespace] = JSON.stringify(customCollections);
    },
    /**
     * returns the stored collection with the provided name
     *
     * @method get
     * @param {string} collectionName
     */
    get : function(collectionName){
      var presentationName = app.config.get('name');
      var storageNamespace = presentationName + ":CustomCollections";
      var collection = self.getAll()[collectionName];
        
      if (collection == undefined || Array.isArray(collection.presentation) === false){
        return undefined;
      }

      var invalidSlidesCount = 0;
      collection.presentation = collection.presentation.map(function(slideshow){
        return slideshow.filter(function(slideId){
          if (app.model.get().slides[slideId] === undefined){
            invalidSlidesCount++;
            return false;
          }
          return true;
        });
      }).filter(function(slideshow){
        return slideshow.length > 0;
      });

      if (invalidSlidesCount > 0) {
        var errorMsg = translation["MISSING_PRESSENTATIONS_TITLE"]
            .replace("$INVALID_SLIDES_COUNT$", invalidSlidesCount);
        apprise(errorMsg, {textOk: translation["OK"]});
        self.add(collectionName, collection);
      }

      return collection;

    },
    /**
     * renames a stored collection without changing its position (e.g. collections order returned by getAll)
     *
     * @method rename
     * @param {string} oldName
     * @param {string} newName
     */
    rename : function(oldName, newName){
      //rename presentation keeping the presentation order
      var presentationName = app.config.get('name');
      var storageNamespace = presentationName + ":CustomCollections";
      var customCollections={};
      $.each(self.getAll(),function(name, object){
        if(name==oldName){
          $.each(object.slideshows, function(name, slideshow){
            slideshow.name = slideshow.name.replace(oldName,newName);
          });
          object.name = newName;
        }
        customCollections[(name==oldName) ? newName : name ] = object;

      });
      //var isFavorite = customCollectionsStorage.isFavorite(oldName);
      //customCollectionsStorage.isFavorite(oldName, false);
      //customCollectionsStorage.isFavorite(newName, isFavorite);
      localStorage[storageNamespace] = JSON.stringify(customCollections);
    },

    /**
     * returns all stored favorite collections
     *
     * @method getFavorites
     *
     */
    getFavorites : function(){

      var collections = self.getAll();

      var favorites = [];

      $.each(collections,function(collectionName,collectionObject){
        if(collectionObject.isFavorite)
          favorites.push(collectionObject);
      });

      return favorites;

    },
    /**
     * returns all stored favorite collections
     *
     * @method updateOrder
     *
     */
    updateOrder : function(order){
      var presentationName = app.config.get('name');
      var storageNamespace = presentationName + ":CustomCollections";
      var customCollections = self.getAll();
      var newCustomCollections = {};

      //clear old order
      var backUp =  localStorage[storageNamespace];
      localStorage[storageNamespace] = JSON.stringify({});

      //set new order
      try{
        $.each(order,function(index,name){
          newCustomCollections[name] = customCollections[name];
        });
      }catch(e){
        localStorage[storageNamespace] = backUp;
      }


      localStorage[storageNamespace] = JSON.stringify(newCustomCollections);
    },

    /**
     * store all custom-collections
     *
     * @method saveAll
     *
     */
    saveAll : function(customPresentations){
      var presentationName = app.config.get('name');
      var storageNamespace = presentationName + ":CustomCollections";
      localStorage[storageNamespace] = JSON.stringify(customPresentations);
    }
  }

});

/**
 * Implements favorite presentations buttons
 * ---------------------------------
 *
 * adds up to three buttons to a slide, which reference the favorite custom collections
 *
 * @module ap-favorite-presentations-buttons.js
 * @requires jquery.js
 * @author Alexander Kals, antwerpes ag
 */
app.register("ap-favorite-presentations-buttons", function() {

  var self;
  return {
    publish: {
        
    },
    events: {

    },
    states: [],
    onRender: function(el) {
      self = this;
      app.listenTo(app.slideshow, 'load', this.favoritePresentationHandler.bind(this));

      app.$.on('update:favorites', function () {
        self.favoritePresentationHandler();
      }.bind(this));


    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {
      this.favoritePresentationHandler();
    },
    onExit: function(el) {

    },

    favoritePresentationHandler: function(data){

      var content = app.slideshow.resolve();
      var $slide = $("#" + content.slide);
      var $favoritePresentationsContainer = $slide.find('.favoritePresentationsContainer');

      // remove the favorits block if its already a custom presentation
      if (app.slideshow.resolve().slideshow.indexOf("custom-collection") > -1) {
        $favoritePresentationsContainer.empty();
        return;
      }



      if($favoritePresentationsContainer != undefined) {
        $favoritePresentationsContainer.empty();
        $.each(app.$.customCollectionsStorage.getFavorites(), function (index, orderObject) {
          $favoritePresentationsContainer.append("<div class='button' id='button" + index + "'>" + orderObject.name + "</div>");

          $favoritePresentationsContainer.find(("#button" + index)).on("tap", function () {
            var collectionObject = app.$.customCollectionsStorage.get(orderObject.name);
            if(!app.model.hasStoryboard(collectionObject.id)){

              var slideshowIdArray = [];
              $.each(collectionObject.slideshows, function (i, slideshow) {
                slideshowIdArray.push(slideshow.id);
                var slidesArrary = [];
                $.each(slideshow.content, function (i, slide) {
                  slidesArrary.push(slide);
                });

                var temp = {
                  id: slideshow.id,
                  name: slideshow.name,
                  content: slidesArrary,
                  type: "slideshow"
                };

                app.model.addStructure(slideshow.id, temp);

              });

              var storyboardData = {
                id: collectionObject.id,
                name: collectionObject.name,
                content: slideshowIdArray
              };

              app.model.addStoryboard(collectionObject.id, storyboardData);

              //app.slideshow.init(collectionObject.id);
              app.goTo(collectionObject.id);
              app.$.toolbar.hide();

              // set home icon
              //app.menu.linksConfig[orderObject.slideshows[0].id] = {title: "<div class='homeIcon' />", classname: "home"}
            }
            else{


              app.goTo(collectionObject.id);
            }

          });
        });
      }
    }
  }

});
/**
 * Provides a follow up mail section.
 * ----------------------------------
 * Provides a user interface for composing a follow up email.
 * Focuses on adding and removing media-entries from the media library as attachments.
 *
 * <b>WARNING: Please be responsible and mark only those media entries as attachments whose size
 * can be handled by common mail servers (using the allowDistribution property on entries within media.json).
 * Exceeding the mail servers size limitations may result in attachments not being able to be transmitted.</b>
 *
 * @module ap-follow-up-mail.js
 * @requires jquery.js, media-library.js, agnitio.js, touchy.js
 * @author Alexander Kals, Andreas Tietz, David Buezas, antwerpes ag
 */
app.register("ap-follow-up-mail", function() {


  /**
   * Implements a follow up mail section.
   * ------------------------------------
   * Implements a user interface for composing a follow up email.
   * Focuses on adding and removing media-entries from the media library as attachments.
   *
   * <b>WARNING: Please be responsible and mark only those media entries as attachments whose size
   * can be handled by common mail servers (using the allowDistribution property on entries within media.json).
   * Exceeding the mail servers size limitations may result in attachments not being able to be transmitted.</b>
   *
   * @class ap-follow-up-mail
   * @constructor
   *
   */
  var self;
  var address = "";
  var subject = "Reference to our meeting"; //"Dateien/Referenzen aus unserem Gespräch";

  return {
    publish: {
       },
    events: {
      "tap .sendButton": "sendMailHandler",
      "tap .library .emailAttachmentToggler": "emailAttachmentHandler",
      "tap .clearButton": "clear"

    },
    states: [
      {
        id: "visible",
        onEnter: function(){
          app.$.trigger("media-library:refresh");
          app.$.mediaLibrary.refreshScroll();
        }
      }
    ],
    onRender: function(el) {
      self = this;

      app.$.on('open:ap-follow-up-mail', function () {
        if(this.stateIsnt("visible"))
          this.show();
      }.bind(this));

      app.$.on('close:ap-follow-up-mail', function () {
        this.hide();
      }.bind(this));

      app.$.on('toolbar:hidden', function () {
        this.hide();
      }.bind(this));



      self.bodyText = "Dear Customer,<br /><br />in reference to our meeting, please find attached the following documents.<br /><br />Best regards<br />Your Bayer team <br /><br /><hr />";


      var presentationName = app.config.get('name');

      localStorage.removeItem(presentationName + ":attachmentStorage");

      this.monitorUsage();

    },
    onRemove: function(el) {
      self = null;
    },
    monitorUsage: function (el) {
      if (ag) {
        ag.submit.data({
          label: "Registered Module",
          value: "ap-follow-up-mail",
          category: "BHC Template Modules",
          isUnique: true,
          labelId: "bhc_registered_module",
          categoryId: "bhc_template_modules"
        });
      }
    },

    hide: function () {
      app.unlock();
      this.reset();
    },

    show: function () {
      app.lock();
      this.goTo('visible');

    },

    sendMailHandler: function(){

      //var generalBodyText = "Dear Customer,<br /><br />in reference to our meeting, please find attached the following documents.<br /><br />Best regards<br />Your Bayer team"; //Sehr geehrter Kunde,<br /><br />anbei finden Sie wie gewünscht die folgenden Dateien/Referenzen aus unserem Gespräch.<br /><br />Mit freundlichen Grüßen,<br />Ihr Bayer Team",

      var presentationName = app.config.get('name');
      var storageNamespace = presentationName + ":attachmentStorage";
      var collectedAttachments = Object.keys(JSON.parse(localStorage[storageNamespace] || "{}"));

      var fileAttachments = [];

      self.bodyText = "Dear Customer,<br /><br />in reference to our meeting, please find attached the following documents.<br /><br />Best regards<br />Your Bayer team <br /><br /><hr />";

      // Attachments that don't represent real files are writen into the body text,
      // all others are being attached as files:
      $.each(collectedAttachments, function (index, attachment) {
        var contentRegex = /^content\:\/\//;
        var urlRegex = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/;
        if (contentRegex.test(attachment)) {
          self.bodyText += "<br /><br />";
          self.bodyText += attachment.replace(contentRegex, "");
        } else if (urlRegex.test(attachment)) {
          self.bodyText += "<br /><br />";
          self.bodyText += attachment;
        } else {
          fileAttachments.push(attachment);
        }
      });

      // Hand over to agnitio native mail dialog:
      console.log('ag.sendMail("' + address + '", "' + subject + '", "' + self.bodyText + '",', fileAttachments, ")");
      ag.sendMail(address, subject, self.bodyText, fileAttachments);
    },

    emailAttachmentHandler: function(event){
      var filename = $(event.target).parent("li").attr("data-file");
      var $movedDown = $(".attachments ul li[data-file='"+filename+"']");
      $(".attachments ul").append($movedDown);
    },

    clear: function(event){
      $(".mail [data-file][data-is-attached='true'] .emailAttachmentToggler").each(function() {
        // touchy custom events doesn't support jquery trigger
        var tapEvent = document.createEvent('UIEvents');
        tapEvent.initEvent("tap", true, true);
        this.dispatchEvent(tapEvent);
      });
    }

  }

});

/**
 * Implements a frequently asked questions section.
 * ------------------------------------------------
 * Questions and answeres are maintained directly inside the frequently-asked-questions.html file.
 * Tapping on a question or answer shows/hides the answer.
 *
 *
 * @module ap-frequently-asked-questions.js
 * @requires jquery.js, iscroll.js, module.js
 * @author Andreas Tietz, antwerpes ag
 */

app.register("ap-frequently-asked-questions", function() {

  /**
   * Implements a frequently asked questions section.
   * ------------------------------------------------
   * Questions and answeres are maintained directly inside the frequently-asked-questions.html file.
   * Tapping on a question or answer shows/hides the answer.
   *
   * @class ap-frequently-asked-questions
   * @constructor
   */

  var self;
  return {
    publish: {

    },
    events: {


    },
    states: [
      {
        id: "visible"
      }
    ],
    onRender: function(el) {
      self = this;

      app.$.on('open:ap-frequently-asked-questions', function () {
          this.show();
      }.bind(this));

      app.$.on('close:ap-frequently-asked-questions', function () {
        if (this.stateIs("visible"))
          this.hide();
      }.bind(this));

      app.$.on('toolbar:hidden', function () {
        if (this.stateIs("visible"))
          this.hide();
      }.bind(this));

      // Initialize scrolling:

      if(self.scroll) self.scroll.destroy();
      var $scroll = $(self.el).find(".scroll");
      self.scroll = new IScroll($scroll[0], {scrollbars: true});

      this.monitorUsage();

    },
    onRemove: function(el) {
      self = null;
    },
    monitorUsage: function (el) {
      if (ag) {
        ag.submit.data({
          label: "Registered Module",
          value: "ap-frequently-asked-questions",
          category: "BHC Template Modules",
          isUnique: true,
          labelId: "bhc_registered_module",
          categoryId: "bhc_template_modules"
        });
      }
    },

    hide: function () {
      this.unload();
      app.unlock();
      this.reset();
    },

    show: function () {
      $(".FrequentlyAskedQuestions li").on("tap", function () {
        $(this).toggleClass("maximized");

        self.scroll.refresh();

        if($(this).hasClass("maximized"))
          self.scroll.scrollToElement(this);

      });
      app.lock();

      this.goTo('visible');
    },

    unload: function () {
      // Cleanup:
      $(self.el).find("li").off("tap");

    }
  }

});

/**
 * Provides a media library section.
 * ---------------------------------
 * Provides a user interface for searching and viewing arbitrary
 * media meta data and content from the media repository.
 *
 * @module ap-media-library.js
 * @requires jquery.js, iscroll.js, media-repository.js
 * @author Andreas Tietz, antwerpes ag
 */
app.register("ap-media-library", function () {

    /**
     * Implements a media library section.
     * -----------------------------------
     * Implements a user interface for searching and viewing arbitrary
     * media meta data and content from the media repository.
     * Shows all media entries by default.
     * The basic set of listed entries may be optionally limited by some constraint,
     * e.g. "all media entries that are attachable to emails"
     * or "all media entries that have the 'pdf' and 'reference' tags in it".
     *
     * @class ap-media-library
     * @constructor
     * @param {object} options Valid properties are:
     *     - preFilterSearchTerms: limits basic set of entries: what is to be searched (leave undefined to show all)
     *     - preFilterAttributesToBeSearched: limits basic set of entries: where is it to be found
     *     - renderOptions: options to be passed to renderers
     */
    var self;

    return {
        publish: {
            prefiltersearchterms: undefined,
            prefilterattributestobesearched: undefined,
            renderOptions: {},
            attachment: false,
            followupmail: false,
            hide: false
        },
        events: {

        },
        states: [
            {
                id: "hide"
            },
            {
                id: "visible"
            },
            {
                id: "followupmail"
            },
            {
                id: "attachment"
            }
        ],
        onRender: function (el) {

            self = this;
            app.$.mediaLibrary = this;

            this.load(el);

            if (this.props.hide) {
                this.hide();
            }

            if (this.props.attachment) {
                this.goTo('attachment');
            }
            if (this.props.followupmail) {
                this.goTo('followupmail');
            }

            app.$.on('open:ap-media-library', function () {
                if (this.stateIs("hide")){
                  this.show();
                }
                    
            }.bind(this));

            app.$.on('close:ap-media-library', function () {
                if (this.stateIs("visible"))
                    this.hide();
            }.bind(this));

            app.$.on('toolbar:hidden', function () {
                if (this.stateIs("visible"))
                    this.hide();
                    this.unload();
            }.bind(this));

            app.$.on('media-library:refresh', function () {
                if(this.stateIs("attachment"))
                    this.scroll.refresh();
            }.bind(this));

            this.monitorUsage();

        },
        onRemove: function (el) {
          self = null;
        },
        monitorUsage: function (el) {
          if (ag) {
            ag.submit.data({
              label: "Registered Module",
              value: "ap-media-library",
              category: "BHC Template Modules",
              isUnique: true,
              labelId: "bhc_registered_module",
              categoryId: "bhc_template_modules"
            });
          }
        },

        hide: function () {
            app.unlock();
            this.goTo('hide');
        },

        show: function () {
            app.lock();
            this.goTo('visible');
            this.load();

        },

        unload: function() {
            if(this.stateIs("visible"))
                if(self.scroll) self.scroll.destroy();
        },

        refreshScroll: function(){
          this.scroll.refresh();
        },

        load: function (el) {

            var $list = $(el).find("ul");
            $list.empty();
            if(self.scroll) self.scroll.destroy();
            // Initialize scrolling:
            self.scroll = new IScroll(self.$(".scroll")[0], {scrollbars: true});

            
            // Fill the list with a basic set of media entries (e.g. "all media entries that are attachable to emails"):



            /**
             * Implements a media library section.
             * -----------------------------------
             * Implements a user interface for searching and viewing arbitrary
             * media meta data and content from the media repository.
             * Shows all media entries by default.
             * The basic set of listed entries may be optionally limited by some constraint,
             * e.g. "all media entries that are attachable to emails"
             * or "all media entries that have the 'pdf' and 'reference' tags in it".
             *
             * @param {object} options Valid properties are:
             *     - preFilterSearchTerms: limits basic set of entries: what is to be searched (leave undefined to show all)
             *     - preFilterAttributesToBeSearched: limits basic set of entries: where is it to be found
             *     - renderOptions: options to be passed to renderers
             */

            var media = window.mediaRepository.find(this.props.prefiltersearchterms, this.props.prefilterattributestobesearched);
            if (media) {
                $.each(media, function (file, meta) {
                    var listElement = window.mediaRepository.render(file, meta, self.renderOptions);
                    if (listElement) {
                        $list.append(listElement);
                    }
                });

            }

            // Set up live-search (on each key stroke):
            var $listElements = $(self.el).find("ul li");
            var $input = $(self.el).find("input");
            $input.keyup(function (e) {
                // Dismiss on enter:
                if (e.keyCode == 13) {
                    $input.blur();
                    return;
                }
                // Search and update list:
                var searchString = $input.val().toLowerCase();
                var searchTerms = searchString.split(/\s+/g);
                $listElements.each(function () {
                    var $listElement = $(this);
                    var found = searchTerms.reduce(function (found, searchTerm) {
                        return found && ($listElement.text().toLowerCase().indexOf(searchTerm) !== -1);
                    }, true);
                    $listElement.toggleClass("hidden", !found);
                });
                self.scroll.refresh();
            });

            self.scroll.refresh();


        }
    }

});

/**
 * Provides a database interface for accessing meta information of media content.
 * ------------------------------------------------------------------------------
 *
 * The module is loaded within index.html
 *
 * @module ap-media-repository.js
 * @requires jquery.js, media.json
 * @author Andreas Tietz, David Buezas, antwerpes ag
 */
app.register("ap-media-repository", function () {


    /**
     * Implements a database interface for accessing meta information of media content.
     * --------------------------------------------------------------------------------
     *
     * The media repository provides search and rendering capability
     * of arbitrary media information and content based on meta data
     * defined in a json file. Within this json file arbitrary meta
     * data can be associated with any type of file or content.
     * The solution is relying heavily the "convention over configuration" principle.
     *
     * Anatomy of a media entry:
     *
     *     // It's key can be any kind of unique string:
     *     // An example convention might be to put in the file path
     *     // of a real file the meta data should be associated with:
     *     "content/pdf/reference_01.pdf": {
	 *         // Meta data is defined as attributes. Those can really
	 *         // be completely arbitrary as long as there is a renderer
	 *         // implemented that is capable of processing these values.
	 *         // Currently string, number and boolean are the types of
	 *         // object values supported by the search/find functionality.
	 *         "title": "Doe J, Lorem Ipsum 1. 2005",
	 *         "referenceId": 3,
	 *         "allowDistribution": true,
	 *         "tags": "document pdf publication reference 3"
	 *     },
     *
     * Apart from searching, each media entry can also be rendered into
     * DOM via "renderers", e.g. in order to be displayed inside a list.
     * For each "type" of media (which is completely up to the developer to be defined),
     * a separate renderer must be implemented and registered at the media repository.
     * When a media entry is about to be rendered, the media repository uses the
     * renderer that matches first the media type of the file or content of the entry
     * (first come first serve at registration time).
     *
     * Anatomy of a media renderer:
     *
     *     MediaRepository.addRenderer({
	 *         // Regular expression used to determine what "type" of
	 *         // media entries are accepted by this renderer:
	 *         regex: <some regular expression>,
	 *         // Function returning a jQuery DOM element representing that
	 *         // media entry based on the filename or content as well as meta data
	 *         render: function (fileOrContent, meta, options) {
	 *             return <some generated jQuery DOM element>;
	 *         }
	 *     });
     *
     *
     * @class ap-media-repository
     * @constructor
     *
     *
     **/


    var _metadata;
    var _renderers;


    return {
        publish: {},
        events: {},
        states: [],
        onRender: function (el) {
            window.mediaRepository = this; // export globally
            window.mediaRepository.load("media.json"); // load database file

            var createBasicMediaEntry; //forward declaration

            // Content:
            window.mediaRepository.addRenderer({
                regex: /^content\:\/\//,
                render: function (file, meta, options) {
                    return createBasicMediaEntry(file, meta, $.noop)
                        .addClass('content')
                        .append("<span class='title'>" + file.replace("content://", "") + "</span>")
                        .append("<span class='tags'>" + (meta.tags ? "[" + meta.tags + "]" : "") + "</span>");
                }
            });

            // PDF:
            window.mediaRepository.addRenderer({
                regex: /\.(pdf)$/,
                render: function (file, meta, options) {
                    // Clean title from any html tags
                    var cleanTitle = meta.title.replace(/(<([^>]+)>)/ig,"");
                    options = $.extend({
                        onTap: function () {
                            console.log('ag.openPDF("' + file + '", "' + cleanTitle + '")');
                            ag.openPDF(file, cleanTitle);
                        }
                    }, options);
                    return createBasicMediaEntry(file, meta, options.onTap)
                        .addClass('pdf')
                        .append("<span class='title'>" + (meta.title || "") + "</span>")
                        .append("<span class='tags'>" + (meta.tags ? "[" + meta.tags + "]" : "") + "</span>");
                }
            });

            // URL:
            window.mediaRepository.addRenderer({
                /* http://blog.mattheworiordan.com/post/13174566389 */
                regex: /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/,
                render: function (file, meta, options) {
                    var cleanTitle = meta.title.replace(/(<([^>]+)>)/ig,"");
                    options = $.extend({
                        shouldAllowTap: function () {
                            return true;
                        },
                        onTap: function () {
                            console.log('window.open("' + file.replace(/^(https?|ftp)/, "agnitiodefaultbrowser") + '", "' + cleanTitle + '")');

                            //iOS Fix until Agnitio fixes their ag.openUrl function
                            //window.open(file.replace(/^(https?|ftp)/, "agnitiodefaultbrowser"));

                            ag.openURL(file, cleanTitle);


                        }
                    }, options);
                    return createBasicMediaEntry(file, meta, options.onTap)
                        .addClass('url')
                        .append("<span class='title'>" + (meta.title || "") + "</span>")
                        .append("<span class='url'>(" + file + ")</span>")
                        .append("<span class='tags'>" + (meta.tags ? "[" + meta.tags + "]" : "") + "</span>");
                }
            });

            // Video (with thumbnail):
            // Expecting a PNG thumbnail image by convention
            // e.g. thumbnail for "content/video/my_movie.mp4" is expected to be "content/video/my_movie.mp4.png"
            window.mediaRepository.addRenderer({
                regex: /\.(mov|mp4|m4v)$/,
                render: function (file, meta, options) {
                    options = $.extend({
                        onTap: function () {
                            $("<div class='videoPopup'><video src='" + file + "' controls/><div class='close'></div></div>")
                                .on("swipedown swipeup swiperight swipeleft", function (e) {
                                    e.stopPropagation();
                                })
                                .on("tap", function (event) {
                                    if ($(event.target).is(":not(video)")) $(this).remove();
                                }).appendTo("#presentation");
                            var v = $("#presentation").find('.videoPopup video').get(0)
                            if (v.load) v.load()
                        }
                    }, options);
                    var entry = createBasicMediaEntry(file, meta, options.onTap)
                        .addClass('video')
                        .append("<span class='title'>" + meta.title + "</span>")
                        .append("<span class='tags'>" + (meta.tags ? "[" + meta.tags + "]" : "") + "</span>");
                    entry.find(".icon").css({
                        "background-image": "url('" + file + ".png')",
                        "background-size": "contain"
                    });
                    return entry;
                }
            });

            createBasicMediaEntry = function (file, meta, onTap) {
                var presentationName = app.config.get('name');
                var storageNamespace = presentationName + ":attachmentStorage";
                var attachmentStorage = JSON.parse(localStorage[storageNamespace] || "{}");

                var $emailAttachmentToggler = null;
                if (meta.allowDistribution) {
                    $emailAttachmentToggler = $("<div class='emailAttachmentToggler' />");
                    $emailAttachmentToggler.on("tap", function (e) {
                        attachmentStorage = JSON.parse(localStorage[storageNamespace] || "{}");

                        var file = $(this).parent().attr("data-file");
                        var isAttached = !attachmentStorage[file];
                        if (isAttached) {
                            attachmentStorage[file] = true;
                        } else {
                            delete(attachmentStorage[file]);
                        }
                        localStorage[storageNamespace] = JSON.stringify(attachmentStorage);
                        $("[data-file='" + file + "']").attr("data-is-attached", isAttached);
                        app.$.trigger("media-library:refresh");
                    });
                }

                return $("<li/>")
                    .addClass('mediaEntry')
                    .attr("data-file", file)
                    .attr("data-is-attached", !!attachmentStorage[file])
                    .append($emailAttachmentToggler)
                    .append("<div class='icon' />")
                    .append(meta.referenceId ? "<div class='referenceId'>[" + meta.referenceId + "]</div>" : "")
                    .on("tap", ":not(.emailAttachmentToggler)", function () {
                        onTap.apply(this);
                    });
            };
        },
        onRemove: function (el) {

        },
        onEnter: function (el) {

        },
        onExit: function (el) {

        },

        /**
         * Loads a JSON file containing meta data in an object for later runtime use.
         *
         * @method load
         * @param {String} file Path of file containing meta data.
         */
        load: function (file) {
            _metadata = {};
            _metadata = JSON.parse(app.cache.get(file));
            if (!_metadata) {
                $.ajax({
                    url: file,
                    dataType: "json",
                    success: function (json) {
                        _metadata = json;
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        throw "Error loading media repository metadata file '" + file + "': " + textStatus;
                    },
                    async: false
                });
            }
        },
        /**
         * Returns the meta data object representing the meta database at runtime.
         *
         * @method metadata
         * @return {Object} Reference to the meta data object.
         */
        metadata: function () {
            return _metadata;
        },
        /**
         * Searches through the meta data object for given search terms in given attributes.
         *
         * @method find
         * @param {String} [searchTerms] Search terms separated by whitespaces. If not specified, all entries will be returned.
         * @param {String|Array} [attributesToBeSearched] Meta attribute keys whose values should be searched for searchTerms. If not specified, all attributes are being searched.
         * @return {Object} Hash containing found media entries or null if nothing was found.
         */
        find: function (searchTerms, attributesToBeSearched) {

            var searchTerms = searchTerms || "";
            var searchTerms = searchTerms.toLowerCase().split(/\s+/g);
            if (typeof attributesToBeSearched === "string") {
                attributesToBeSearched = [attributesToBeSearched];
            }
            var results = null;
            $.each(_metadata, function (file, meta) {
                var haystack = "";

                if (attributesToBeSearched) { // search only specific attributes
                    $.each(attributesToBeSearched, function (index, attributeToBeSearched) {
                        var attrVal = meta[attributeToBeSearched];
                        if (attrVal && (typeof attrVal === "string"
                            || typeof attrVal === "number"
                            || typeof attrVal === "boolean")) {
                            haystack += attrVal.toString() + " ";
                        }
                    });
                } else { // search all
                    $.each(meta, function (attrKey, attrVal) {
                        if (typeof attrVal === "string" || typeof attrVal === "number" || typeof attrVal === "boolean") {
                            haystack += attrVal.toString() + " ";
                        }
                    });
                }
                if (haystack.length > 0) {
                    haystack = haystack.toLowerCase();
                    var found = searchTerms.reduce(function (found, searchTerm) {
                        return found && (haystack.indexOf(searchTerm) !== -1);
                    }, true);
                    if (found) {
                        results = results || {};
                        results[file] = meta;
                    }
                }
            });
            return results;
        },
        /**
         * Adds a renderer to the renderer chain.
         *
         * @method addRenderer
         * @param {Object} Renderer to be added.
         */
        addRenderer: function (renderer) {
            _renderers = _renderers || [];
            _renderers.push(renderer);
        },
        /**
         * Renders a media entry. Uses the first matching renderer in the renderer chain.
         *
         * @method render
         * @param {String} file Media entry key.
         * @param {Object} meta Media entry meta data.
         * @param {Object} options Attributes passed to the designated renderer.
         * @return {jQuery Element} Rendered jQuery Element ready to be inserted into the DOM.
         */
        render: function (file, meta, options) {
            var renderer = _renderers.reduce(function (bestRenderer, currentRenderer) {
                return bestRenderer || (currentRenderer.regex.test(file) && currentRenderer)
            }, undefined);
            if (!renderer) console.log("Warning: no renderer found for media resource ", file);
            else return renderer.render(file, meta, options);
        }
    }

});

app.register("ap-notepad", function() {

  /**
   * Implements the Notepad Class.
   * -------------------------------------
   * This class enables the user to draw over other content via touch/mouse events.
   *
   * The module is loaded within index.html
   *
   * @class ap-notepad
   * @constructor
   *
   */

  var BRUSH_RED   = "modules/rainmaker_modules/ap-notepad/assets/brush_red.png";
  var BRUSH_GREEN = "modules/rainmaker_modules/ap-notepad/assets/brush_green.png";
  var brush = new Image();
  var sketcher;


  var self;
  return {
    publish: {

    },
    events: {
        "tap .exit": "toggleNotepad",
        "tap .green": function() { brush.src = BRUSH_GREEN; },
        "tap .red": function() { brush.src = BRUSH_RED; },
        "tap .clear": function() { sketcher.clear(); }
    },
    states: [
        {
            id: 'active',
            onEnter: function() {
                app.lock();
                var pencilPos = $(".bar .button.notepad").offset();
                $(".palette").css({
                    top: pencilPos.top+3,
                    left: pencilPos.left+9
                });
            }
        },
        {
            id: 'draw1',
            onEnter: function(prev, data) {
                this.copyDrawing(data);
            }
        },
        {
            id: 'draw2',
            onEnter: function(prev, data) {
                this.copyDrawing(data);
            }
        }
    ],
    toggleNotepad: function() {
        var state = this.getState();
        if (state) {
            this.reset();
        }
        else {
            this.goTo('active');
        }
        sketcher.clear();
    },
      // Methods for copying drawing on remote screen
      copyDrawing: function(data) {
          sketcher.context.putImageData(data, 0, 0);
      },
      updateDrawing: function() {
          var wrapper = app.dom.get('wrapper');
          var imageData = sketcher.context.getImageData(0,0, wrapper.offsetWidth, wrapper.offsetHeight);
          if (this.stateIs('draw1')) {
              this.goTo('draw2', imageData);
          }
          else {
              this.goTo('draw1', imageData);
          }
      },
      init: function() {
          brush.src = BRUSH_RED;
          brush.onload = function () {
              sketcher = new Sketcher($("#notepad-canvas"), brush);
          };
      },
    onRender: function(el) {
      self = this;
      app.$.notepad = this;

      this.on('reset', app.unlock);

        ag.on('updateNotepad', function(data) {
            self.updateDrawing();
        });

        if (window.Sketcher) {
            this.init();
        }
        else {
            head.load('modules/ap-notepad/cdm.sketcher.js', function() {
                this.init();
            });
        }

      $(el).find(".button:not(.clear,.exit)").on("tap", function (event) {
        $(self.el).find('.button').removeClass('active');
        $(event.target).addClass('active');
      });

      this.monitorUsage();

    },
    onRemove: function(el) {
        self = null;
    },
    monitorUsage: function (el) {
      if (ag) {
        ag.submit.data({
          label: "Registered Module",
          value: "ap-notepad",
          category: "BHC Template Modules",
          isUnique: true,
          labelId: "bhc_registered_module",
          categoryId: "bhc_template_modules"
        });
      }
    }
  };

});

var Trig = {
    distanceBetween2Points: function ( p1, p2 ) {
        return Math.sqrt(Math.pow(p2.x - p1.x, 2 ) + Math.pow( p2.y - p1.y, 2 ) );
    },
    
    angleBetween2Points: function ( point1, point2 ) {
        var dx = point2.x - point1.x;
        var dy = point2.y - point1.y;   
        return Math.atan2( dx, dy );
    }
}

function Sketcher (canvasElement, brushImage, cb) {
    var self = this;
    self.brush = brushImage;
    self.touchSupported = ("ontouchstart" in window);
    self.canvas = canvasElement; // A jQuery element
    self.context = self.canvas.get(0).getContext('2d');
    self.lastMousePoint = {
        x : 0,
        y : 0
    };
    self.angle_old = 0;

    if (self.touchSupported) {
        self.mouseDownEvent = 'touchstart';
        self.mouseMoveEvent = 'touchmove';
        self.mouseUpEvent = 'touchend';
    } else {
        self.mouseDownEvent = 'mousedown';
        self.mouseMoveEvent = 'mousemove';
        self.mouseUpEvent = 'mouseup';
    }

    if (cb) self.callback = cb;

    self.canvas.on(self.mouseDownEvent, self.onCanvasMouseDown());
}

Sketcher.prototype.onCanvasMouseDown = function () {
    var self = this;
    return function (event) {
        self.mouseMoveHandler = self.onCanvasMouseMove();
        self.mouseUpHandler = self.onCanvasMouseUp();

        self.canvas.on(self.mouseMoveEvent, self.mouseMoveHandler);
        self.canvas.on(self.mouseUpEvent, self.mouseUpHandler);

        self.updateMousePosition(event);
        self.updateCanvas(event);
    };
};

Sketcher.prototype.onCanvasMouseMove = function () {
    var self = this;
    return function (event) {
        self.updateCanvas(event);
        if (this.touchSupported) {
            event.originalEvent.targetTouches[0].preventDefault();
            event.originalEvent.targetTouches[0].stopPropagation();
        } else {
            event.preventDefault();
            event.stopPropagation();
        }
        return false;
    };
};

Sketcher.prototype.onCanvasMouseUp = function () {
    var self = this;
    return function (event) {
        ag.publish('updateNotepad');
        self.canvas.off(self.mouseMoveEvent, self.mouseMoveHandler);
        self.canvas.off(self.mouseUpEvent, self.mouseUpHandler);
        self.mouseMoveHandler = null;
        self.mouseUpHandler = null;
    };
};

Sketcher.prototype.updateMousePosition = function (event) {
    var target;
    if (this.touchSupported) {
        target = event.originalEvent.targetTouches[0];
    } else {
        target = event;
    }
    var offset = this.canvas.offset();
    this.lastMousePoint.x = target.pageX - offset.left;
    this.lastMousePoint.y = target.pageY - offset.top;
};

Sketcher.prototype.updateCanvas = function (event) {
    //now with David's angle smoothness!!!
    var halfBrushW = this.brush.width / 2;
    var halfBrushH = this.brush.height / 2;

    var start = {
        x : this.lastMousePoint.x,
        y : this.lastMousePoint.y
    };

    this.updateMousePosition(event);

    var end = {
        x : this.lastMousePoint.x,
        y : this.lastMousePoint.y
    };

    var distance = parseInt(Trig.distanceBetween2Points(start, end));
    var angle = Trig.angleBetween2Points(start, end);
    var x=start.x, y=start.y;
    if (this.angle_old==0)
        this.angle_old=angle;
    var angle_diff=angle-this.angle_old;
    if (angle_diff>Math.PI)
        angle_diff-=2*Math.PI;
    if (angle_diff<-Math.PI)
        angle_diff+=2*Math.PI;

    for (var z = 0; (z <= distance || z == 0); z++) {
        if (distance>0){
            this.angle_old+=(angle_diff)/distance;
            x += (Math.sin(this.angle_old) ) ;
            y += (Math.cos(this.angle_old) ) ;
        }
        //console.log( x, y, angle, z );
        this.context.drawImage(this.brush, x-halfBrushW, y-halfBrushH);
    }
    this.lastMousePoint.x = x;
    this.lastMousePoint.y = y;
    this.angle_old=angle;
};

Sketcher.prototype.clear = function () {
    var c = this.canvas[0];
    this.context.clearRect(0, 0, c.width, c.height);
};

/**
 * Implements the Collections Overview.
 * ------------------------------------
 * This module Presents an overview of the default collections set in presentation.json and allows loading slides by tapping on their thumbnails.
 *
 *
 * @module ap-overview.js
 * @constructor
 * @param {object} options Valid properties are:
 *         - slideTapAction: one of the following strings representing what to do when a thumbnail receives the tap event
 *         - "goto" => goto slide on tap
 *         - "noop" => do nothing
 *         - "showPreview" => (NOT IMPLEMENTED) shows a bigger thumbnail of the slide
 *     - disableScrollOnSlideDrag: {boolean}
 */

app.register("ap-overview", function () {

    /**
     * Implements the Collections Overview.
     * ------------------------------------
     * This module Presents an overview of the default collections set in presentation.json and allows loading slides by tapping on their thumbnails.
     *
     * @class ap-overview
     * @constructor
     * @param {object} options Valid properties are:
     *     - $container: jQuery DOM object inside which to load the module
     *     - slideTapAction: one of the following strings representing what to do when a thumbnail receives the tap event
     *         - "goto" => goto slide on tap
     *         - "noop" => do nothing
     *         - "showPreview" => (NOT IMPLEMENTED) shows a bigger thumbnail of the slide
     *     - disableScrollOnSlideDrag: {boolean}
     */

    var self;
    var eventNamespace = ".overview";
    var $collectionsList;
    var $collectionOverview;
    var $collectionContainer;
    var _mouseDownEvent;
    var _mouseUpEvent;

    return {
        slideTapAction: ["goto", "noop", "showPreview"][0],
        disableScrollOnSlideDrag: false,
        publish: {
            iscustomoverview: false,
            highlightCurrentSlide: true
        },
        events: {
            "tap .overview .collectionName": "collectionsListHandler",
            "tap .overview .o_slide": "slideHandler",
            "longTouch .overview .o_slide": "slidePreview"
        },
        states: [
            {
                id: "visible"
            },
            {
                id: "customOverview"
            }
        ],
        onRender: function (el) {



            self = this;

            app.$.overview = this;

            this.normalizeCollections();

            if (this.props.iscustomoverview){
                this.goTo("customOverview");
            }

            _mouseDownEvent = touchy.events.start + eventNamespace;

            _mouseUpEvent = touchy.events.end + eventNamespace;

            $collectionsList = $(el).find(".collectionsList"); // available collections container
            $collectionOverview = $(el).find(".collectionOverview"); // selected collection overview container
            $collectionContainer = $collectionOverview.find(".o_collection"); // actual collection thumbnail
            var $overview = $(el).find('#overview');

            $collectionsList.empty();

            app.$.on('open:ap-overview', function () {
                if(this.stateIsnt("visible")){
                    this.show();
                }
            }.bind(this));

            app.$.on('close:ap-overview', function () {

                this.hide();
            }.bind(this));

            app.$.on('toolbar:hidden', function () {
                this.hide();
            }.bind(this));

            app.$.on('insert:ap-overview', function () {
                this.goTo("custom-overview");
                this.updateOverview();
            }.bind(this));

            this.monitorUsage();

        },
        onRemove: function (el) {
          self = null;
        },
        monitorUsage: function (el) {
          if (ag) {
            ag.submit.data({
              label: "Registered Module",
              value: "ap-overview",
              category: "BHC Template Modules",
              isUnique: true,
              labelId: "bhc_registered_module",
              categoryId: "bhc_template_modules"
            });
          }
        },

        hide: function () {
            app.unlock();
            this.reset();
        },

        show: function () {
            this.goTo('visible');
            app.lock();
            self.slideTapAction = "goto";
            app.$.customCollections.hide();
            this.updateOverview();
        },

        showCustomOverview: function () {
            this.goTo('customOverview');
            self.slideTapAction = "noop";
            self.disableScrollOnSlideDrag = true;
            this.updateOverview();
        },

        unload: function () {
            // $collectionsList.empty();
        },

        updateOverview: function () {
            var $body = $("body");

            $collectionsList = $(".overview .collectionsList"); // available collections container

            $collectionContainer = $collectionOverview.find(".o_collection"); // actual collection thumbnail
            var $overview = $('.overview');

            $collectionsList.empty();

            var storyboards = app.model.get().storyboards;

            //traverse app.json and get the collections list (ignoring custom collections)
            $.each(storyboards, function (index, collection) {
                if(collection.type !== "collection") return;

                if (collection.id.indexOf('custom') == -1 ) {
                    var $collectionName = $("<div class='collectionName' />");

                    $.extend({id: collection.id}, collection);
                    $collectionName.data("collection", collection);
                    $collectionName.attr("data-collectionId", collection.id);
                    $collectionName.text(collection.name);
                    $collectionsList.append($collectionName);

                }
            });

            // get custom collections
            $.each(app.$.customCollectionsStorage.getAll(), function (name, content) {
                if (content.slideshows.length != 0) {
                    var $collectionName = $("<div class='collectionName' />");
                    $collectionName.data("collection", content);
                    $collectionName.attr("data-collectionId", content.id);
                    $collectionName.text(content.name);
                    $collectionsList.append($collectionName);
                }
            });



            /* very strange fix, which currently works */
            var $currentCollection = $collectionsList.find("[data-collectionid='" + app.getPath().split("/")[0] + "']");
            var tempEvent = {
                target: $currentCollection
            };
            self.collectionsListHandler(tempEvent);


            if (this.props.iscustomoverview) {

                $collectionOverview = $(".custom-collections .overview .collectionOverview"); // selected collection overview container
            }
            else {

                $collectionOverview = $(".overview .collectionOverview"); // selected collection overview container
            }

            if(self.scroll) self.scroll.destroy();

            self.scroll = new IScroll( $(".overview .collectionOverview").get(0), {
                scrollbars: true,
                mouseWheel: true,
                scrollX: true,
                directionLockThreshold: Number.MAX_VALUE
            });

            if(self.scrollCustom) self.scrollCustom.destroy();
            self.scrollCustom = new IScroll( $(".custom-collections .overview .collectionOverview").get(0), {
                scrollbars: true,
                mouseWheel: true,
                scrollX: true,
                directionLockThreshold: Number.MAX_VALUE
            });


            if (self.disableScrollOnSlideDrag) {
                $collectionContainer.on(_mouseDownEvent, ".o_slide", function () {
                    self.scrollCustom.disable();
                    $("body").one(_mouseUpEvent, function () {
                        self.scrollCustom.enable();
                    });
                });
            }

        },

        normalizeCollections: function() {

            var allCollections = app.model.get().storyboards;

            $.each(allCollections, function(i, collection){

                    if(!collection.id && !collection.type){
                        var newContent = [];
                        $.each(collection.content, function(index, content){
                            var slideShowName;
                            switch (index) {
                                case 0:
                                    slideShowName = "Home";
                                    break;
                                case content.length - 1:
                                    slideShowName = "Summary";
                                    break;
                                default:
                                    slideShowName = "Chapter " + index
                            }

                            var slideShowId;
                            if(typeof content === 'string'){
                                slideShowId = content + "-chapter-" + Math.floor((Math.random() * 10000) + 1);

                            }else
                            {
                                slideShowId = slideShowName + "-" + Math.floor((Math.random() * 10000) + 1);

                            }

                            var temp = {
                                "id": slideShowId,
                                "name": slideShowName,
                                "type": "slideshow",
                                "content": content
                            };
                            app.model.addStructure(slideShowId, temp);
                            newContent.push(slideShowId);
                        });

                        app.model.deleteStoryboard(i);
                        var collectionId =  collection.name + "_" + Math.floor((Math.random() * 10000) + 1);
                        var collectionObj = {
                            "id": collectionId,
                            "name": collection.name,
                            "linear": false,
                            "type": "collection",
                            "content": newContent
                        };
                        app.model.addStoryboard(collectionId,collectionObj);
                    }
                }
            );

        },

        collectionsListHandler: function (event) {

            if (self.scroll) {
                setTimeout(function(){
                    self.scroll.refresh();
                    self.scroll.scrollTo(0,0);
                },0);
            }

            if (self.scrollCustom) {
                setTimeout(function(){
                    self.scrollCustom.refresh();
                    self.scrollCustom.scrollTo(0,0);
                },0);
            }

            $collectionsList.find(".selected").removeClass('selected');
            $(event.target).addClass('selected');
            // load the selected collection

            var collection = $(event.target).data("collection");
            var collectionId = $(event.target).attr("data-collectionId");

            function checkImageAddText(src, ele, slide){
                var image = new Image;
                image.onload = function() {
                    if ('naturalHeight' in this) {
                        if (this.naturalHeight + this.naturalWidth === 0) {
                            this.onerror();
                            return false;
                        }
                    } else if (this.width + this.height == 0) {
                        this.onerror();
                        return false;
                    }
                    ele.text("");
                    return true;
                };
                image.onerror = function() {
                    ele.text(app.model.get().slides[slide].name);
                    console.log("thumbnail not loaded");
                    return false;
                };
                image.src = src;
            }

            $collectionContainer
                .empty()
                .attr("data-id", collectionId)
                .data('contentGroups', app.model.getStoryboard(collectionId).contentGroups);
            // TODO add Class and Data
            if (collection.isCustomPresentation) {
                collection.contentGroups = app.model.getStoryboard(app.model.getStoryboard()).contentGroups;
                $.each(collection.slideshows, function (index, slideshow) {
                    var $slideshow = $("<div class='o_slideshow' />")
                        .appendTo($collectionContainer)
                        .attr("data-id", slideshow.id)
                        .append("<div class='o_slideshowName'>" + slideshow.name + "</div>");
                    $.each(slideshow.content, function (index, slideId) {
                        var $slide = $("<div class='o_slide' />")
                            .appendTo($slideshow)
                            .css({"background-image": "url(slides/" + slideId + "/" + slideId + ".png)"})
                            .attr("data-id", slideId);
                        var $thumbText = $("<p class='o_text' />")
                            .appendTo($slide);

                        if (collection.contentGroups) {

                            $.each(collection.contentGroups, function (i, group) {
                                if (~group.slides.indexOf(slideId)) {
                                    $slide.data('contentGroup', group);
                                    $slide.addClass('grouped');
                                    if (group.orientation == 'horizontal') {
                                        $slide.parents('.o_slideshow').addClass('grouped');
                                    }
                                    
                                    if (group.orientation == 'vertical') {
                                        $slide.addClass('vertical-group');
                                    }
                                }
                                if (group.slides.indexOf(slideId) == 0){
                                    $slide.addClass('first-in-group');
                                }

                            });
                        }

                        checkImageAddText("slides/" + slideId + "/" + slideId + ".png", $thumbText, slideId);

                    });
                });


                if (collection.slideshows.length > 0 && app.model.hasStoryboard(collection.id) == false) {
                    var slideshowIdArray = [];
                    $.each(collection.slideshows, function (i, slideshow) {
                        slideshowIdArray.push(slideshow.id);
                        var slidesArrary = [];
                        $.each(slideshow.content, function (i, slide) {
                            slidesArrary.push(slide);
                        });

                        app.model.addStructure(slideshow.id, slideshow);
                    });

                    var collectionObj = {
                        "content": slideshowIdArray,
                        "name": collection.name,
                        "linear": false,
                        "id": collection.id,
                        "type": "collection"
                    };

                    app.model.addStoryboard(collection.id, collectionObj);

                }


            }
            else {

                collection = app.model.getStoryboard(collectionId);

                $.each(collection.content, function (index, slideshowId) {

                    var slideshow = app.model.getStructure(slideshowId);
                    slideshow = $.extend({id: slideshowId}, slideshow);
                    var $slideshow = $("<div class='o_slideshow' />")
                        .appendTo($collectionContainer)
                        .attr("data-id", slideshow.id)
                        .append("<div class='o_slideshowName'>" + slideshow.name + "</div>");



                    if (typeof slideshow.content === 'string'){
                        var slideId = slideshow.content;
                        var $slide = $("<div class='o_slide' />")
                            .appendTo($slideshow)
                            .css({"background-image": "url(slides/" + slideId + "/" + slideId + ".png)"})
                            .attr("data-id", slideId);
                        var $thumbText = $("<p class='o_text' />")
                            .appendTo($slide);
                        if (collection.contentGroups) {
                            $.each(collection.contentGroups, function (i, group) {
                                if (~group.slides.indexOf(slideId)) {
                                    $slide.data('contentGroup', group);
                                    $slide.addClass('grouped');

                                    if (group.orientation == 'horizontal') {
                                        $slide.parents('.o_slideshow').addClass('grouped')
                                    }
                                }
                                if (group.slides.indexOf(slideId) == 0){
                                    $slide.addClass('first-in-group');
                                }

                            });
                        }

                        checkImageAddText("slides/" + slideId + "/" + slideId + ".png", $thumbText, slideId);
                    }else
                    {
                        $.each(slideshow.content, function (index, slideId) {
                            var $slide = $("<div class='o_slide' />")
                                .appendTo($slideshow)
                                .css({"background-image": "url(slides/" + slideId + "/" + slideId + ".png)"})
                                .attr("data-id", slideId);
                            var $thumbText = $("<p class='o_text' />")
                                .appendTo($slide);
                            if (collection.contentGroups) {
                                $.each(collection.contentGroups, function (i, group) {
                                    if (~group.slides.indexOf(slideId)) {
                                        $slide.data('contentGroup', group);
                                        $slide.addClass('grouped');
                                        if (group.orientation == 'horizontal') {
                                            $slide.parents('.o_slideshow').addClass('grouped')
                                        }
                                    }

                                });
                            }

                            checkImageAddText("slides/" + slideId + "/" + slideId + ".png", $thumbText, slideId);
                        });
                    }



                });
            }

            var slideThumbSelector = "";
            $.each(app.getPath().substr().split("/"), function (i, id) {
                slideThumbSelector += "[data-id='" + id + "'] ";
            });
            $collectionOverview.find(".highlighted").removeClass('highlighted');
            $collectionOverview.find(slideThumbSelector).addClass('highlighted');

        },

        slideHandler: function (event) {
            if (self.slideTapAction === "goto") {
                var slideId = $(event.target).attr("data-id");
                var slideshowId = $(event.target).parent().attr("data-id");
                var collectionId = $(event.target).parent().parent().attr("data-id");

                var path = collectionId + "/" + slideshowId + "/" + slideId;

                app.goTo(path);
                app.$.menu.updateCurrent();
                app.$.toolbar.hide();

            } else if (self.slideTapAction === "noop") {
                //do nothing
            }
        },

        slidePreview: function (event) {

            if(self.stateIsnt("customOverview")){

                event.preventDefault();
                var slide = $(event.target).attr("data-id");

                if (slide){
                    var ele = event.target.outerHTML,
                        eleInPopup = undefined;

                    app.view.get("ag-overlay").open(ele);
                    eleInPopup = document.querySelector('#ag-overlay .o_slide');

                    function addClick(ele){
                        var slideId = $(ele).attr("data-id"),
                            slideshowId = $(ele).parent().attr("data-id"),
                            collectionId = $(ele).parent().parent().attr("data-id"),
                            path = collectionId + "/" + slideshowId + "/" + slideId;

                        app.goTo(path);
                        app.$.menu.updateCurrent();
                        app.$.toolbar.hide();
                        eleInPopup.removeEventListener('tap', function(){addClick(event.target)}, false);
                        app.view.get("ag-overlay").reset();
                    }

                    eleInPopup.addEventListener('tap', function(){addClick(event.target)}, false);
                }
            }
        }
    }
});

/**
 * Provides a reference library section.
 * -------------------------------------
 * Implements a user interface for viewing media entries that have a "referenceId" attribute.
 *
 * @module ap-reference-library.js
 * @requires module.js, jquery.js, iscroll.js, media-repository.js
 * @author Andreas Tietz antwerpes ag
 */
app.register("ap-reference-library", function() {

  var self;

  return {
    publish: {

    },
    events: {

    },
    states: [
      {
        id: "visible"
      }
    ],
    onRender: function(el) {
      self = this;

      app.$.on('open:ap-reference-library', function () {
        if(this.stateIsnt("visible")){
          this.show();
        }
      }.bind(this));

      app.$.on('close:ap-reference-library', function () {
        this.hide();
      }.bind(this));

      app.$.on('toolbar:hidden', function () {
        this.hide();
        this.unload();
      }.bind(this));

      this.load(el);

      this.monitorUsage();

    },
    onRemove: function(el) {
      self = null;
    },
    monitorUsage: function (el) {
      if (ag) {
        ag.submit.data({
          label: "Registered Module",
          value: "ap-reference-library",
          category: "BHC Template Modules",
          isUnique: true,
          labelId: "bhc_registered_module",
          categoryId: "bhc_template_modules"
        });
      }
    },

    hide: function () {
      app.unlock();
      this.reset();
    },

    show: function () {
      app.lock();
      this.goTo('visible');
      this.load();
    },

    unload: function() {
      if(this.stateIs("visible"))
        if(self.scroll) self.scroll.destroy();
    },

    load: function(el){
      var $list = $(el).find("ul");

      $list.empty();
      // Initialize scrolling:
      if(self.scroll) self.scroll.destroy();
      self.scroll = new IScroll(self.$(".scroll")[0], {scrollbars: true});

      // Fill the list with all media entries that have a reference id:
      var references = window.mediaRepository.find("", "referenceId");
      if (references) {
        $.each(references, function (file, meta) {
          $list.append(window.mediaRepository.render(file, meta));
        });
        self.scroll.refresh();
      }
    }
  }

});

/**
 * Implements navigation joystick
 * ---------------------------------
 *
 * updates the joystick component
 *
 * The module is loaded within index.html
 *
 * @module ap-slide-indicator.js
 * @requires jquery.js, agnitio.js
 * @author Alexander Kals, antwerpes ag
 */

app.register("ap-slide-indicator", function () {

    return {
        publish: {},
        events: {},
        states: [],
        onRender: function (el) {
            app.listenToOnce(app.slide, 'slide:enter', this.updateIndicators.bind(this));
            app.listenToOnce(app.slideshow, 'load', this.updateIndicators.bind(this));
            app.listenTo(app.slideshow, 'update:current', this.updateIndicators.bind(this));
            this.monitorUsage();
        },
        onRemove: function (el) {

        },
        monitorUsage: function (el) {
          if (ag) {
            ag.submit.data({
              label: "Registered Module",
              value: "ap-slide-indicator",
              category: "BHC Template Modules",
              isUnique: true,
              labelId: "bhc_registered_module",
              categoryId: "bhc_template_modules"
            });
          }
        },

        /**
         * Updates the navigation joystick
         * @method updateIndicators
         */

        updateIndicators: function (data) {


            var slideList = app.slideshow.inspect();
            var $joystick = $('.joystick');

            if(slideList.getLeft()){
                $joystick.find('.left').css('display', 'block');
            }
            else{
                $joystick.find('.left').css('display', 'none');
            }
            if(slideList.getRight()){
                $joystick.find('.right').css('display', 'block');
            }
            else{
                $joystick.find('.right').css('display', 'none');
            }
            if(slideList.getUp()){
                $joystick.find('.up').css('display', 'block');
            }
            else {
                $joystick.find('.up').css('display', 'none');
            }
            if(slideList.getDown()){
                $joystick.find('.down').css('display', 'block');
            }
            else{
                $joystick.find('.down').css('display', 'none');
            }
        }
    }

});

/**
 * Implements a function that will open the specific-product-characteristics.pdf file with the agnitio in-app PDF viewer.
 * -----------------------------------------------------------------
 * @module ap-specific-product-characteristics.js
 * @requires agnitio.js
 * @author Andreas Tietz, antwerpes ag
 */
app.register("ap-specific-product-characteristics", function() {

  /**
   * Implements a function that will open the specific-product-characteristics.pdf file with the agnitio in-app PDF viewer.
   * ----------------------------------------------------------------------------------------------------------------------
   *
   * @class ap-specific-product-characteristics.js
   * @constructor
   */
  return {
    publish: {

    },
    events: {

    },
    states: [],
    onRender: function(el) {
      app.$.specificProductCharacteristics = this;
      this.monitorUsage();
    },
    onRemove: function(el) {

    },
    monitorUsage: function (el) {
      if (ag) {
        ag.submit.data({
          label: "Registered Module",
          value: "ap-specific-product-characteristics",
          category: "BHC Template Modules",
          isUnique: true,
          labelId: "bhc_registered_module",
          categoryId: "bhc_template_modules"
        });
      }
    },

    openPdf: function() {
      console.log('openPDF("shared/pdfs/EyleaSPCAug2016.pdf", "Specific Product Characteristics"');
      ag.openPDF("shared/pdfs/EyleaSPCAug2016.pdf", "Specific Product Characteristics");
      var $joystick = $('.joystick');
      var $addSlideButton = $('[data-module="ap-add-slide-button"]');
      $joystick.fadeIn();
      $addSlideButton.fadeIn();

    }
  }

});

/**
 * Implements Bottom Toolbar.
 * --------------------------
 * @module ap-toolbar.js
 * @requires jquery.js, all other classes referenced in toolbar.html
 * @author Alexander Kals, David Buezas, Andreas Tietz, antwerpes ag
 */
app.register("ap-toolbar", function () {


    /**
     * Implements Bottom Toolbar.
     * --------------------------
     * To add buttons to the toolbar add the following to toolbar.html:
     *
     *		<div class="button" data-toolbar-state="..." data-module-to-load="..."></div>
     *
     * where:
     *		- data-toolbar-state = ["minimized" | "maximized" | "hidden"]
     *			states the end state of the toolbar when the button is tapped
     *		- data-module-to-load = A class name (must inherit from Module)
     *			states the class from which an instance will be created and loaded inside the content container
     *
     * Also add the module to be loaded within the "content"-container
     *  e.g.:
     *    <div data-module="ap-overview" hide></div>
     *
     * If you don't need a module, just remove the button and the entry
     * @class ap-toolbar
     */

    var self;

    return {
        publish: {
            hide: false,
            microsite: false
        },
        events: {
            "tap": "handleEvent",
            "tap .button[data-module-to-load='ap-specific-product-characteristics']": function (event) {
                app.$.specificProductCharacteristics.openPdf();
                self.handleEvent(event);
            },
            "tap .bar .button.notepad": function () {
                app.$.notepad.toggleNotepad();
            },
            "tap .bar .button.jumpToLastSlide": function () {
                self.jumpToLastSlide();
                app.$.menu.updateCurrent();
                app.$.toolbar.hide();
            },
            "swipeleft": function (event) {
                event.stopPropagation();
            },
            "swiperight": function (event) {
                event.stopPropagation();
            },
            "swipeup": function (event) {
                event.stopPropagation();
            },
            "swipedown": function (event) {
                event.stopPropagation();
            }
        },
        states: [
            {
                id: 'minimized',
                onEnter: function () {
                    // app.util.transformElement(this.$el, '-webkit-translate3d(0,100%,0)');

                }
            },
            {
                id: 'hidden',
                onEnter: function () {
                    // app.util.transformElement(this.$el, '-webkit-translate3d(0,667px,0)');
                }
            },
            {
                id: 'maximized',
                onEnter: function () {
                    // app.util.transformElement(this.$el, '-webkit-translate3d(0,0,0)');
                }
            }
        ],
        onRender: function (el) {

            self = this;
            app.$.toolbar = this;

            if (this.props.hide) {
                this.hide();
            }

            $(".ap-toolbar").attr("data-state", "hidden");
            this.goTo("hidden");

        },
        onRemove: function (el) {

        },
        onEnter: function (el) {

        },
        onExit: function (el) {

        },

        setMicrosite: function () {
            self.microsite = true;
            $(this.$el).addClass("microsite");
        },

        hide: function () {
            app.$.trigger("toolbar:hidden");
            var $joystick = $('.joystick');
            $(".ap-toolbar").attr("data-state", "hidden");
            $joystick.fadeIn();
            this.goTo("hidden");
        },


        open: function (e) {


        },

        jumpToLastSlide: function(){
            var collectionLength = app.slideshow.getLength();
            var lastSlide = app.model.getStoryboard(app.slideshow.getId()).content[collectionLength - 2];
            app.$.BackNavigation.setPrevCollection(app.model.getStoryboard(app.slideshow.getId()).id);
            app.slideshow.goTo(lastSlide);

        },

        handleEvent: function (e) {

            if($(e.target).hasClass('active')) return;

            var $allButtons = $(".button[data-module-to-load]");
            var $joystick = $('.joystick');
            var $addSlideButton = $('[data-module="ap-add-slide-button"]');

            var target = e.target;


            if ($(target).hasClass(this.props.dataModule)) {
                var state = $(target).attr("data-state");
                var map = {
                    hidden: "minimized",
                    maximized: "hidden",
                    minimized: "hidden"
                };
                self.goTo(map[state]);
                $allButtons.removeClass("active");

                $("input").blur();
                $joystick.fadeIn();
                if (app.env != 'ag-microsites' && app.env != 'ag-remote') {
                 $addSlideButton.fadeIn();
                }
                $(target).attr("data-state", map[state]);
                app.$.menu.hide();
                app.$.trigger("toolbar:hidden");
            }


            var moduleToLoad = $(target).attr("data-module-to-load");
            var currentModule = "";
            if (moduleToLoad) {

                var state = $(target).attr("data-toolbar-state");
                setTimeout(function () {
                    // avoid same touch event triggering input elements focus

                }, 0);
                if (state) {
                    app.$.toolbar.goTo(state);
                    app.$.menu.hide();
                }

                if(currentModule === moduleToLoad) return;

                var trigger = "open:" + moduleToLoad;
                app.$.trigger(trigger);
                $joystick.fadeOut();
                $addSlideButton.fadeOut();

                $allButtons.not($(target)).removeClass("active");

                $allButtons.each(function (index, item) {

                    var otherModule = $(item).attr("data-module-to-load");
                    if (moduleToLoad != otherModule) {
                        var trigger = "close:" + otherModule;
                        app.$.trigger(trigger);
                    }
                });

                $(target).addClass("active");
            }

        }

    }
});

/**
 * Provides a video library section.
 * ---------------------------------
 * Implements a user interface for viewing media entries that have the "video" tag.
 *
 * @module ap-video-library.js
 * @requires module.js, jquery.js, iscroll.js, media-repository.js
 * @author Andreas Tietz antwerpes ag
 */

app.register("ap-video-library", function() {


  /**
   * Implements a video library section.
   * -----------------------------------
   * Implements a user interface for viewing media entries that have the "video" tag.
   *
   * @class ap-video-library
   * @constructor
   *
   */
  var self;

  return {
    publish: {

    },
    events: {

    },
    states: [

      {
        id: "visible"
      }
    ],
    onRender: function(el) {
      self = this;

      app.$.on('open:ap-video-library', function () {
        if(this.stateIsnt("visible")){
          this.show();
        }
      }.bind(this));

      app.$.on('close:ap-video-library', function () {
        this.hide();
      }.bind(this));

      app.$.on('toolbar:hidden', function () {
        this.hide();
        this.unload();
      }.bind(this));


      var $list = $(el).find("ul");

      // Initialize scrolling:
      self.scroll = new IScroll(self.$(".scroll")[0], {scrollbars: true});

      // Fill the list with all media entries that have the "video" tag:
      var videos = window.mediaRepository.find("video");
      if (videos) {
        $.each(videos, function (file, meta) {
          $list.append(window.mediaRepository.render(file, meta));
        });
        self.scroll.refresh();
      }

      this.monitorUsage();
    },
    onRemove: function(el) {
      self = null;
    },
    monitorUsage: function (el) {
      if (ag) {
        ag.submit.data({
          label: "Registered Module",
          value: "ap-video-library",
          category: "BHC Template Modules",
          isUnique: true,
          labelId: "bhc_registered_module",
          categoryId: "bhc_template_modules"
        });
      }
    },

    hide: function () {
      app.unlock();
      this.reset();
    },

    show: function () {
      app.lock();
      this.goTo('visible');
    },

    unload: function() {
      if(this.stateIs("visible"))
        if(self.scroll) self.scroll.destroy();
    }
  }

});


/**
*
* BHC Template Module: GPC Number
*
* A module that reads the 'gpc' setting in config.json and displays it.
*
* Usage:
* Insert <div data-module="bhc-gpc-number"></div> where number should be displayed
*
*/

app.register("bhc-gpc-number", function() {

  return {
    template: false,
    publish: {

    },
    events: {

    },
    states: [],
    onRender: function(el) {
      var gpc = app.config.get('gpc');
      var existingText = this.el.innerHTML;

      if (!gpc || gpc === "999-999-999") {
        gpc = "Not Applied"
      }

      if (gpc) {
        this.el.innerHTML = existingText + " " + gpc;
      }

      this.monitorUsage();
    },
    onRemove: function(el) {

    },
    monitorUsage: function() {
      if (ag) {
        ag.submit.data({
          label: "Registered Module",
          value: "bhc-gpc-number",
          category: "BHC Template Modules",
          isUnique: true,
          labelId: "bhc_registered_module",
          categoryId: "bhc_template_modules"
        });
      }
    }
  }

});

(function() {
  var ContraindicationsSlide;

  ContraindicationsSlide = (function() {
    function ContraindicationsSlide() {
      this.events = {};
      this.states = [];
    }

    ContraindicationsSlide.prototype.onRender = function(element) {};

    ContraindicationsSlide.prototype.onEnter = function(element) {};

    ContraindicationsSlide.prototype.onExit = function(element) {};

    return ContraindicationsSlide;

  })();

  app.register('ContraindicationsSlide', function() {
    return new ContraindicationsSlide();
  });

}).call(this);

(function() {
  var Discussion60Slide;

  Discussion60Slide = (function() {
    function Discussion60Slide() {
      this.events = {};
      this.states = [];
    }

    Discussion60Slide.prototype.onRender = function(element) {};

    Discussion60Slide.prototype.onEnter = function(element) {};

    Discussion60Slide.prototype.onExit = function(element) {};

    return Discussion60Slide;

  })();

  app.register('Discussion60Slide', function() {
    return new Discussion60Slide();
  });

}).call(this);

(function() {
  var Discussion610Slide;

  Discussion610Slide = (function() {
    function Discussion610Slide() {
      this.events = {};
      this.states = [];
    }

    Discussion610Slide.prototype.onRender = function(element) {};

    Discussion610Slide.prototype.onEnter = function(element) {};

    Discussion610Slide.prototype.onExit = function(element) {};

    return Discussion610Slide;

  })();

  app.register('Discussion610Slide', function() {
    return new Discussion610Slide();
  });

}).call(this);

(function() {
  var Discussion611Slide;

  Discussion611Slide = (function() {
    function Discussion611Slide() {
      this.events = {};
      this.states = [];
    }

    Discussion611Slide.prototype.onRender = function(element) {};

    Discussion611Slide.prototype.onEnter = function(element) {};

    Discussion611Slide.prototype.onExit = function(element) {};

    return Discussion611Slide;

  })();

  app.register('Discussion611Slide', function() {
    return new Discussion611Slide();
  });

}).call(this);

(function() {
  var Discussion612Slide;

  Discussion612Slide = (function() {
    function Discussion612Slide() {
      this.events = {};
      this.states = [];
    }

    Discussion612Slide.prototype.onRender = function(element) {};

    Discussion612Slide.prototype.onEnter = function(element) {};

    Discussion612Slide.prototype.onExit = function(element) {};

    return Discussion612Slide;

  })();

  app.register('Discussion612Slide', function() {
    return new Discussion612Slide();
  });

}).call(this);

(function() {
  var Discussion61Slide;

  Discussion61Slide = (function() {
    function Discussion61Slide() {
      this.events = {};
      this.states = [];
    }

    Discussion61Slide.prototype.onRender = function(element) {
      return this.sliders = new LinearSlider(element);
    };

    Discussion61Slide.prototype.onEnter = function(element) {};

    Discussion61Slide.prototype.onExit = function(element) {};

    return Discussion61Slide;

  })();

  app.register('Discussion61Slide', function() {
    return new Discussion61Slide();
  });

}).call(this);

(function() {
  var Discussion62Slide;

  Discussion62Slide = (function() {
    function Discussion62Slide() {
      this.events = {};
      this.states = [];
    }

    Discussion62Slide.prototype.onRender = function(element) {};

    Discussion62Slide.prototype.onEnter = function(element) {};

    Discussion62Slide.prototype.onExit = function(element) {};

    return Discussion62Slide;

  })();

  app.register('Discussion62Slide', function() {
    return new Discussion62Slide();
  });

}).call(this);

(function() {
  var Discussion63Slide;

  Discussion63Slide = (function() {
    function Discussion63Slide() {
      this.events = {};
      this.states = [];
    }

    Discussion63Slide.prototype.onRender = function(element) {};

    Discussion63Slide.prototype.onEnter = function(element) {};

    Discussion63Slide.prototype.onExit = function(element) {};

    return Discussion63Slide;

  })();

  app.register('Discussion63Slide', function() {
    return new Discussion63Slide();
  });

}).call(this);

(function() {
  var Discussion64Slide;

  Discussion64Slide = (function() {
    function Discussion64Slide() {
      this.events = {};
      this.states = [];
    }

    Discussion64Slide.prototype.onRender = function(element) {};

    Discussion64Slide.prototype.onEnter = function(element) {};

    Discussion64Slide.prototype.onExit = function(element) {};

    return Discussion64Slide;

  })();

  app.register('Discussion64Slide', function() {
    return new Discussion64Slide();
  });

}).call(this);

(function() {
  var Discussion65Slide;

  Discussion65Slide = (function() {
    function Discussion65Slide() {
      this.events = {};
      this.states = [];
    }

    Discussion65Slide.prototype.onRender = function(element) {};

    Discussion65Slide.prototype.onEnter = function(element) {};

    Discussion65Slide.prototype.onExit = function(element) {};

    return Discussion65Slide;

  })();

  app.register('Discussion65Slide', function() {
    return new Discussion65Slide();
  });

}).call(this);

(function() {
  var Discussion66Slide;

  Discussion66Slide = (function() {
    function Discussion66Slide() {
      this.events = {};
      this.states = [];
    }

    Discussion66Slide.prototype.onRender = function(element) {};

    Discussion66Slide.prototype.onEnter = function(element) {};

    Discussion66Slide.prototype.onExit = function(element) {};

    return Discussion66Slide;

  })();

  app.register('Discussion66Slide', function() {
    return new Discussion66Slide();
  });

}).call(this);

(function() {
  var Discussion67Slide;

  Discussion67Slide = (function() {
    function Discussion67Slide() {
      this.events = {};
      this.states = [];
    }

    Discussion67Slide.prototype.onRender = function(element) {};

    Discussion67Slide.prototype.onEnter = function(element) {};

    Discussion67Slide.prototype.onExit = function(element) {};

    return Discussion67Slide;

  })();

  app.register('Discussion67Slide', function() {
    return new Discussion67Slide();
  });

}).call(this);

(function() {
  var Discussion68Slide;

  Discussion68Slide = (function() {
    function Discussion68Slide() {
      this.events = {};
      this.states = [];
    }

    Discussion68Slide.prototype.onRender = function(element) {};

    Discussion68Slide.prototype.onEnter = function(element) {};

    Discussion68Slide.prototype.onExit = function(element) {};

    return Discussion68Slide;

  })();

  app.register('Discussion68Slide', function() {
    return new Discussion68Slide();
  });

}).call(this);

(function() {
  var Discussion69Slide;

  Discussion69Slide = (function() {
    function Discussion69Slide() {
      this.events = {};
      this.states = [];
    }

    Discussion69Slide.prototype.onRender = function(element) {};

    Discussion69Slide.prototype.onEnter = function(element) {};

    Discussion69Slide.prototype.onExit = function(element) {};

    return Discussion69Slide;

  })();

  app.register('Discussion69Slide', function() {
    return new Discussion69Slide();
  });

}).call(this);

(function() {
  var Eylea20Slide;

  Eylea20Slide = (function() {
    function Eylea20Slide() {
      this.events = {};
      this.states = [];
    }

    Eylea20Slide.prototype.onRender = function(element) {
      var button, i, index, len, ref, results;
      this.element = element;
      this.buttonsList = this.element.getElementsByClassName('show-description');
      this.descriptionsList = this.element.getElementsByClassName('description');
      this.analitycsModule = app.module.get('ahAnalitycs');
      ref = this.buttonsList;
      results = [];
      for (index = i = 0, len = ref.length; i < len; index = ++i) {
        button = ref[index];
        results.push((function(_this) {
          return function(button, index) {
            return button.addEventListener('tap', function() {
              var monitoringName, ref1;
              button.classList.add("hidden");
              if ((ref1 = _this.descriptionsList[index]) != null) {
                ref1.classList.add("active");
              }
              monitoringName = button.getAttribute('data-name');
              return _this.submitMonitoring(monitoringName);
            });
          };
        })(this)(button, index));
      }
      return results;
    };

    Eylea20Slide.prototype.onEnter = function(element) {};

    Eylea20Slide.prototype.onExit = function(element) {
      return this.resetActiveDescriptions();
    };

    Eylea20Slide.prototype.resetActiveDescriptions = function() {
      var button, i, index, len, ref, results;
      ref = this.buttonsList;
      results = [];
      for (index = i = 0, len = ref.length; i < len; index = ++i) {
        button = ref[index];
        results.push((function(_this) {
          return function(button, index) {
            var ref1;
            button.classList.remove("hidden");
            return (ref1 = _this.descriptionsList[index]) != null ? ref1.classList.remove("active") : void 0;
          };
        })(this)(button, index));
      }
      return results;
    };

    Eylea20Slide.prototype.submitMonitoring = function(monitoringName) {
      return this.analitycsModule.submitCustomSlideMonitoring({
        id: this.element.id,
        name: monitoringName
      });
    };

    return Eylea20Slide;

  })();

  app.register('Eylea20Slide', function() {
    return new Eylea20Slide();
  });

}).call(this);

(function() {
  var Eylea50Slide;

  Eylea50Slide = (function() {
    function Eylea50Slide() {
      this.events = {};
      this.states = [];
    }

    Eylea50Slide.prototype.onRender = function(element) {};

    Eylea50Slide.prototype.onEnter = function(element) {};

    Eylea50Slide.prototype.onExit = function(element) {};

    return Eylea50Slide;

  })();

  app.register('Eylea50Slide', function() {
    return new Eylea50Slide();
  });

}).call(this);

(function() {
  var HomePageSlide;

  HomePageSlide = (function() {
    function HomePageSlide() {
      this.events = {};
      this.states = [];
    }

    HomePageSlide.prototype.onRender = function(element) {
      return ['swipeup', 'swipedown', 'swiperight', 'tap'].forEach(function(event) {
        return element.addEventListener(event, function(ev) {
          ev.stopPropagation();
          return app.slideshow.next();
        });
      });
    };

    HomePageSlide.prototype.onEnter = function(element) {};

    HomePageSlide.prototype.onExit = function(element) {};

    return HomePageSlide;

  })();

  app.register('HomePageSlide', function() {
    return new HomePageSlide();
  });

}).call(this);

(function() {
  var HomePageSlide;

  HomePageSlide = (function() {
    function HomePageSlide() {
      this.events = {};
      this.states = [];
    }

    HomePageSlide.prototype.onRender = function(element) {
      return ['swipeup', 'swipedown', 'swiperight', 'tap'].forEach(function(event) {
        return element.addEventListener(event, function(ev) {
          ev.stopPropagation();
          return app.slideshow.next();
        });
      });
    };

    HomePageSlide.prototype.onEnter = function(element) {};

    HomePageSlide.prototype.onExit = function(element) {};

    return HomePageSlide;

  })();

  app.register('HomePageSlide', function() {
    return new HomePageSlide();
  });

}).call(this);

(function() {
  var ProactiveManagement30Slide;

  ProactiveManagement30Slide = (function() {
    function ProactiveManagement30Slide() {
      this.events = {};
      this.states = [];
    }

    ProactiveManagement30Slide.prototype.onRender = function(element) {};

    ProactiveManagement30Slide.prototype.onEnter = function(element) {};

    ProactiveManagement30Slide.prototype.onExit = function(element) {};

    return ProactiveManagement30Slide;

  })();

  app.register('ProactiveManagement30Slide', function() {
    return new ProactiveManagement30Slide();
  });

}).call(this);

(function() {
  var MultiTargetApproachSlide;

  MultiTargetApproachSlide = (function() {
    function MultiTargetApproachSlide() {
      this.events = {};
      this.states = [
        {
          "id": "first"
        }, {
          "id": "second"
        }, {
          "id": "third"
        }
      ];
    }

    MultiTargetApproachSlide.prototype.onRender = function(element) {
      return this.next();
    };

    MultiTargetApproachSlide.prototype.onEnter = function(element) {};

    MultiTargetApproachSlide.prototype.onExit = function(element) {};

    return MultiTargetApproachSlide;

  })();

  app.register('MultiTargetApproachSlide', function() {
    return new MultiTargetApproachSlide();
  });

}).call(this);

(function() {
  var RecentInsights10Slide;

  RecentInsights10Slide = (function() {
    function RecentInsights10Slide() {
      this.events = {};
      this.states = [];
    }

    RecentInsights10Slide.prototype.onRender = function(element) {};

    RecentInsights10Slide.prototype.onEnter = function(element) {};

    RecentInsights10Slide.prototype.onExit = function(element) {};

    return RecentInsights10Slide;

  })();

  app.register('RecentInsights10Slide', function() {
    return new RecentInsights10Slide();
  });

}).call(this);

(function() {
  var RecentInsights110Slide;

  RecentInsights110Slide = (function() {
    function RecentInsights110Slide() {
      this.events = {};
      this.states = [];
    }

    RecentInsights110Slide.prototype.onRender = function(element) {};

    RecentInsights110Slide.prototype.onEnter = function(element) {};

    RecentInsights110Slide.prototype.onExit = function(element) {};

    return RecentInsights110Slide;

  })();

  app.register('RecentInsights110Slide', function() {
    return new RecentInsights110Slide();
  });

}).call(this);

(function() {
  var RecentInsights111Slide;

  RecentInsights111Slide = (function() {
    function RecentInsights111Slide() {
      this.events = {};
      this.states = [];
    }

    RecentInsights111Slide.prototype.onRender = function(element) {};

    RecentInsights111Slide.prototype.onEnter = function(element) {};

    RecentInsights111Slide.prototype.onExit = function(element) {};

    return RecentInsights111Slide;

  })();

  app.register('RecentInsights111Slide', function() {
    return new RecentInsights111Slide();
  });

}).call(this);

(function() {
  var RecentInsights112Slide;

  RecentInsights112Slide = (function() {
    function RecentInsights112Slide() {
      this.events = {};
      this.states = [];
    }

    RecentInsights112Slide.prototype.onRender = function(element) {};

    RecentInsights112Slide.prototype.onEnter = function(element) {};

    RecentInsights112Slide.prototype.onExit = function(element) {};

    return RecentInsights112Slide;

  })();

  app.register('RecentInsights112Slide', function() {
    return new RecentInsights112Slide();
  });

}).call(this);

(function() {
  var RecentInsights11Slide;

  RecentInsights11Slide = (function() {
    function RecentInsights11Slide() {
      this.events = {};
      this.states = [];
    }

    RecentInsights11Slide.prototype.onRender = function(element) {
      return this.sliders = new LinearSlider(element);
    };

    RecentInsights11Slide.prototype.onEnter = function(element) {};

    RecentInsights11Slide.prototype.onExit = function(element) {};

    return RecentInsights11Slide;

  })();

  app.register('RecentInsights11Slide', function() {
    return new RecentInsights11Slide();
  });

}).call(this);

(function() {
  var RecentInsights12Slide;

  RecentInsights12Slide = (function() {
    function RecentInsights12Slide() {
      this.events = {};
      this.states = [];
    }

    RecentInsights12Slide.prototype.onRender = function(element) {};

    RecentInsights12Slide.prototype.onEnter = function(element) {};

    RecentInsights12Slide.prototype.onExit = function(element) {};

    return RecentInsights12Slide;

  })();

  app.register('RecentInsights12Slide', function() {
    return new RecentInsights12Slide();
  });

}).call(this);

(function() {
  var RecentInsights13Slide;

  RecentInsights13Slide = (function() {
    function RecentInsights13Slide() {
      this.events = {};
      this.states = [];
    }

    RecentInsights13Slide.prototype.onRender = function(element) {};

    RecentInsights13Slide.prototype.onEnter = function(element) {};

    RecentInsights13Slide.prototype.onExit = function(element) {};

    return RecentInsights13Slide;

  })();

  app.register('RecentInsights13Slide', function() {
    return new RecentInsights13Slide();
  });

}).call(this);

(function() {
  var RecentInsights14Slide;

  RecentInsights14Slide = (function() {
    function RecentInsights14Slide() {
      this.events = {};
      this.states = [];
    }

    RecentInsights14Slide.prototype.onRender = function(element) {
      return this.sliders = new LinearSlider(element);
    };

    RecentInsights14Slide.prototype.onEnter = function(element) {};

    RecentInsights14Slide.prototype.onExit = function(element) {};

    return RecentInsights14Slide;

  })();

  app.register('RecentInsights14Slide', function() {
    return new RecentInsights14Slide();
  });

}).call(this);

(function() {
  var RecentInsights15Slide;

  RecentInsights15Slide = (function() {
    function RecentInsights15Slide() {
      this.events = {};
      this.states = [];
    }

    RecentInsights15Slide.prototype.onRender = function(element) {};

    RecentInsights15Slide.prototype.onEnter = function(element) {};

    RecentInsights15Slide.prototype.onExit = function(element) {};

    return RecentInsights15Slide;

  })();

  app.register('RecentInsights15Slide', function() {
    return new RecentInsights15Slide();
  });

}).call(this);

(function() {
  var RecentInsights16Slide;

  RecentInsights16Slide = (function() {
    function RecentInsights16Slide() {
      this.events = {};
      this.states = [];
    }

    RecentInsights16Slide.prototype.onRender = function(element) {};

    RecentInsights16Slide.prototype.onEnter = function(element) {};

    RecentInsights16Slide.prototype.onExit = function(element) {};

    return RecentInsights16Slide;

  })();

  app.register('RecentInsights16Slide', function() {
    return new RecentInsights16Slide();
  });

}).call(this);

(function() {
  var RecentInsights17Slide;

  RecentInsights17Slide = (function() {
    function RecentInsights17Slide() {
      this.events = {};
      this.states = [];
    }

    RecentInsights17Slide.prototype.onRender = function(element) {};

    RecentInsights17Slide.prototype.onEnter = function(element) {};

    RecentInsights17Slide.prototype.onExit = function(element) {};

    return RecentInsights17Slide;

  })();

  app.register('RecentInsights17Slide', function() {
    return new RecentInsights17Slide();
  });

}).call(this);

(function() {
  var RecentInsights18Slide;

  RecentInsights18Slide = (function() {
    function RecentInsights18Slide() {
      this.events = {};
      this.states = [];
    }

    RecentInsights18Slide.prototype.onRender = function(element) {};

    RecentInsights18Slide.prototype.onEnter = function(element) {};

    RecentInsights18Slide.prototype.onExit = function(element) {};

    return RecentInsights18Slide;

  })();

  app.register('RecentInsights18Slide', function() {
    return new RecentInsights18Slide();
  });

}).call(this);

(function() {
  var RecentInsights19Slide;

  RecentInsights19Slide = (function() {
    function RecentInsights19Slide() {
      this.events = {};
      this.states = [];
    }

    RecentInsights19Slide.prototype.onRender = function(element) {
      return this.sliders = new LinearSlider(element);
    };

    RecentInsights19Slide.prototype.onEnter = function(element) {};

    RecentInsights19Slide.prototype.onExit = function(element) {};

    return RecentInsights19Slide;

  })();

  app.register('RecentInsights19Slide', function() {
    return new RecentInsights19Slide();
  });

}).call(this);

(function() {
  var RI_Quiz_1;

  RI_Quiz_1 = (function() {
    function RI_Quiz_1() {
      this.events = {};
      this.states = [];
    }

    RI_Quiz_1.prototype.onRender = function(element) {};

    RI_Quiz_1.prototype.onEnter = function(element) {};

    RI_Quiz_1.prototype.onExit = function(element) {};

    return RI_Quiz_1;

  })();

  app.register('RI_Quiz_1', function() {
    return new RI_Quiz_1();
  });

}).call(this);

(function() {
  var RI_Quiz_10;

  RI_Quiz_10 = (function() {
    function RI_Quiz_10() {
      this.events = {};
      this.states = [];
    }

    RI_Quiz_10.prototype.onRender = function(element) {
      return this.sliders = new LinearSlider(element);
    };

    RI_Quiz_10.prototype.onEnter = function(element) {};

    RI_Quiz_10.prototype.onExit = function(element) {};

    return RI_Quiz_10;

  })();

  app.register('RI_Quiz_10', function() {
    return new RI_Quiz_10();
  });

}).call(this);

(function() {
  var RI_Quiz_11;

  RI_Quiz_11 = (function() {
    function RI_Quiz_11() {
      this.events = {};
      this.states = [];
    }

    RI_Quiz_11.prototype.onRender = function(element) {};

    RI_Quiz_11.prototype.onEnter = function(element) {};

    RI_Quiz_11.prototype.onExit = function(element) {};

    return RI_Quiz_11;

  })();

  app.register('RI_Quiz_11', function() {
    return new RI_Quiz_11();
  });

}).call(this);

(function() {
  var RI_Quiz_12;

  RI_Quiz_12 = (function() {
    function RI_Quiz_12() {
      this.events = {};
      this.states = [];
    }

    RI_Quiz_12.prototype.onRender = function(element) {};

    RI_Quiz_12.prototype.onEnter = function(element) {};

    RI_Quiz_12.prototype.onExit = function(element) {};

    return RI_Quiz_12;

  })();

  app.register('RI_Quiz_12', function() {
    return new RI_Quiz_12();
  });

}).call(this);

(function() {
  var RI_Quiz_13;

  RI_Quiz_13 = (function() {
    function RI_Quiz_13() {
      this.events = {};
      this.states = [];
    }

    RI_Quiz_13.prototype.onRender = function(element) {};

    RI_Quiz_13.prototype.onEnter = function(element) {};

    RI_Quiz_13.prototype.onExit = function(element) {};

    return RI_Quiz_13;

  })();

  app.register('RI_Quiz_13', function() {
    return new RI_Quiz_13();
  });

}).call(this);

(function() {
  var RI_Quiz_14;

  RI_Quiz_14 = (function() {
    function RI_Quiz_14() {
      this.events = {};
      this.states = [];
    }

    RI_Quiz_14.prototype.onRender = function(element) {
      return this.sliders = new LinearSlider(element);
    };

    RI_Quiz_14.prototype.onEnter = function(element) {};

    RI_Quiz_14.prototype.onExit = function(element) {};

    return RI_Quiz_14;

  })();

  app.register('RI_Quiz_14', function() {
    return new RI_Quiz_14();
  });

}).call(this);

(function() {
  var RI_Quiz_15;

  RI_Quiz_15 = (function() {
    function RI_Quiz_15() {
      this.events = {};
      this.states = [];
    }

    RI_Quiz_15.prototype.onRender = function(element) {};

    RI_Quiz_15.prototype.onEnter = function(element) {};

    RI_Quiz_15.prototype.onExit = function(element) {};

    return RI_Quiz_15;

  })();

  app.register('RI_Quiz_15', function() {
    return new RI_Quiz_15();
  });

}).call(this);

(function() {
  var RI_Quiz_16;

  RI_Quiz_16 = (function() {
    function RI_Quiz_16() {
      this.events = {};
      this.states = [];
    }

    RI_Quiz_16.prototype.onRender = function(element) {};

    RI_Quiz_16.prototype.onEnter = function(element) {};

    RI_Quiz_16.prototype.onExit = function(element) {};

    return RI_Quiz_16;

  })();

  app.register('RI_Quiz_16', function() {
    return new RI_Quiz_16();
  });

}).call(this);

(function() {
  var RI_Quiz_17;

  RI_Quiz_17 = (function() {
    function RI_Quiz_17() {
      this.events = {};
      this.states = [];
    }

    RI_Quiz_17.prototype.onRender = function(element) {};

    RI_Quiz_17.prototype.onEnter = function(element) {};

    RI_Quiz_17.prototype.onExit = function(element) {};

    return RI_Quiz_17;

  })();

  app.register('RI_Quiz_17', function() {
    return new RI_Quiz_17();
  });

}).call(this);

(function() {
  var RI_Quiz_18;

  RI_Quiz_18 = (function() {
    function RI_Quiz_18() {
      this.events = {};
      this.states = [];
    }

    RI_Quiz_18.prototype.onRender = function(element) {};

    RI_Quiz_18.prototype.onEnter = function(element) {};

    RI_Quiz_18.prototype.onExit = function(element) {};

    return RI_Quiz_18;

  })();

  app.register('RI_Quiz_18', function() {
    return new RI_Quiz_18();
  });

}).call(this);

(function() {
  var RI_Quiz_19;

  RI_Quiz_19 = (function() {
    function RI_Quiz_19() {
      this.events = {};
      this.states = [];
    }

    RI_Quiz_19.prototype.onRender = function(element) {};

    RI_Quiz_19.prototype.onEnter = function(element) {};

    RI_Quiz_19.prototype.onExit = function(element) {};

    return RI_Quiz_19;

  })();

  app.register('RI_Quiz_19', function() {
    return new RI_Quiz_19();
  });

}).call(this);

(function() {
  var RI_Quiz_2;

  RI_Quiz_2 = (function() {
    function RI_Quiz_2() {
      this.events = {};
      this.states = [];
    }

    RI_Quiz_2.prototype.onRender = function(element) {
      return this.sliders = new LinearSlider(element);
    };

    RI_Quiz_2.prototype.onEnter = function(element) {};

    RI_Quiz_2.prototype.onExit = function(element) {};

    return RI_Quiz_2;

  })();

  app.register('RI_Quiz_2', function() {
    return new RI_Quiz_2();
  });

}).call(this);

(function() {
  var RI_Quiz_20;

  RI_Quiz_20 = (function() {
    function RI_Quiz_20() {
      this.events = {};
      this.states = [];
    }

    RI_Quiz_20.prototype.onRender = function(element) {};

    RI_Quiz_20.prototype.onEnter = function(element) {};

    RI_Quiz_20.prototype.onExit = function(element) {};

    return RI_Quiz_20;

  })();

  app.register('RI_Quiz_20', function() {
    return new RI_Quiz_20();
  });

}).call(this);

(function() {
  var RI_Quiz_21;

  RI_Quiz_21 = (function() {
    function RI_Quiz_21() {
      this.events = {};
      this.states = [];
    }

    RI_Quiz_21.prototype.onRender = function(element) {};

    RI_Quiz_21.prototype.onEnter = function(element) {};

    RI_Quiz_21.prototype.onExit = function(element) {};

    return RI_Quiz_21;

  })();

  app.register('RI_Quiz_21', function() {
    return new RI_Quiz_21();
  });

}).call(this);

(function() {
  var RI_Quiz_22;

  RI_Quiz_22 = (function() {
    function RI_Quiz_22() {
      this.events = {};
      this.states = [];
    }

    RI_Quiz_22.prototype.onRender = function(element) {};

    RI_Quiz_22.prototype.onEnter = function(element) {};

    RI_Quiz_22.prototype.onExit = function(element) {};

    return RI_Quiz_22;

  })();

  app.register('RI_Quiz_22', function() {
    return new RI_Quiz_22();
  });

}).call(this);

(function() {
  var RI_Quiz_23;

  RI_Quiz_23 = (function() {
    function RI_Quiz_23() {
      this.events = {};
      this.states = [];
    }

    RI_Quiz_23.prototype.onRender = function(element) {};

    RI_Quiz_23.prototype.onEnter = function(element) {};

    RI_Quiz_23.prototype.onExit = function(element) {};

    return RI_Quiz_23;

  })();

  app.register('RI_Quiz_23', function() {
    return new RI_Quiz_23();
  });

}).call(this);

(function() {
  var RI_Quiz_24;

  RI_Quiz_24 = (function() {
    function RI_Quiz_24() {
      this.events = {};
      this.states = [];
    }

    RI_Quiz_24.prototype.onRender = function(element) {};

    RI_Quiz_24.prototype.onEnter = function(element) {};

    RI_Quiz_24.prototype.onExit = function(element) {};

    return RI_Quiz_24;

  })();

  app.register('RI_Quiz_24', function() {
    return new RI_Quiz_24();
  });

}).call(this);

(function() {
  var RI_Quiz_25;

  RI_Quiz_25 = (function() {
    function RI_Quiz_25() {
      this.events = {};
      this.states = [];
    }

    RI_Quiz_25.prototype.onRender = function(element) {};

    RI_Quiz_25.prototype.onEnter = function(element) {};

    RI_Quiz_25.prototype.onExit = function(element) {};

    return RI_Quiz_25;

  })();

  app.register('RI_Quiz_25', function() {
    return new RI_Quiz_25();
  });

}).call(this);

(function() {
  var RI_Quiz_3;

  RI_Quiz_3 = (function() {
    function RI_Quiz_3() {
      this.events = {};
      this.states = [];
    }

    RI_Quiz_3.prototype.onRender = function(element) {};

    RI_Quiz_3.prototype.onEnter = function(element) {};

    RI_Quiz_3.prototype.onExit = function(element) {};

    return RI_Quiz_3;

  })();

  app.register('RI_Quiz_3', function() {
    return new RI_Quiz_3();
  });

}).call(this);

(function() {
  var RI_Quiz_4;

  RI_Quiz_4 = (function() {
    function RI_Quiz_4() {
      this.events = {};
      this.states = [];
    }

    RI_Quiz_4.prototype.onRender = function(element) {};

    RI_Quiz_4.prototype.onEnter = function(element) {};

    RI_Quiz_4.prototype.onExit = function(element) {};

    return RI_Quiz_4;

  })();

  app.register('RI_Quiz_4', function() {
    return new RI_Quiz_4();
  });

}).call(this);

(function() {
  var RI_Quiz_5;

  RI_Quiz_5 = (function() {
    function RI_Quiz_5() {
      this.events = {};
      this.states = [];
    }

    RI_Quiz_5.prototype.onRender = function(element) {
      return this.sliders = new LinearSlider(element);
    };

    RI_Quiz_5.prototype.onEnter = function(element) {};

    RI_Quiz_5.prototype.onExit = function(element) {};

    return RI_Quiz_5;

  })();

  app.register('RI_Quiz_5', function() {
    return new RI_Quiz_5();
  });

}).call(this);

(function() {
  var RI_Quiz_6;

  RI_Quiz_6 = (function() {
    function RI_Quiz_6() {
      this.events = {};
      this.states = [];
    }

    RI_Quiz_6.prototype.onRender = function(element) {};

    RI_Quiz_6.prototype.onEnter = function(element) {};

    RI_Quiz_6.prototype.onExit = function(element) {};

    return RI_Quiz_6;

  })();

  app.register('RI_Quiz_6', function() {
    return new RI_Quiz_6();
  });

}).call(this);

(function() {
  var RI_Quiz_7;

  RI_Quiz_7 = (function() {
    function RI_Quiz_7() {
      this.events = {};
      this.states = [];
    }

    RI_Quiz_7.prototype.onRender = function(element) {};

    RI_Quiz_7.prototype.onEnter = function(element) {};

    RI_Quiz_7.prototype.onExit = function(element) {};

    return RI_Quiz_7;

  })();

  app.register('RI_Quiz_7', function() {
    return new RI_Quiz_7();
  });

}).call(this);

(function() {
  var RI_Quiz_8;

  RI_Quiz_8 = (function() {
    function RI_Quiz_8() {
      this.events = {};
      this.states = [];
    }

    RI_Quiz_8.prototype.onRender = function(element) {};

    RI_Quiz_8.prototype.onEnter = function(element) {};

    RI_Quiz_8.prototype.onExit = function(element) {};

    return RI_Quiz_8;

  })();

  app.register('RI_Quiz_8', function() {
    return new RI_Quiz_8();
  });

}).call(this);

(function() {
  var RI_Quiz_9;

  RI_Quiz_9 = (function() {
    function RI_Quiz_9() {
      this.events = {};
      this.states = [];
    }

    RI_Quiz_9.prototype.onRender = function(element) {};

    RI_Quiz_9.prototype.onEnter = function(element) {};

    RI_Quiz_9.prototype.onExit = function(element) {};

    return RI_Quiz_9;

  })();

  app.register('RI_Quiz_9', function() {
    return new RI_Quiz_9();
  });

}).call(this);

(function() {
  var StartStrong30A1PopupSlide;

  StartStrong30A1PopupSlide = (function() {
    function StartStrong30A1PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StartStrong30A1PopupSlide.prototype.onRender = function(element) {};

    StartStrong30A1PopupSlide.prototype.onEnter = function(element) {};

    StartStrong30A1PopupSlide.prototype.onExit = function(element) {};

    return StartStrong30A1PopupSlide;

  })();

  app.register('StartStrong30A1PopupSlide', function() {
    return new StartStrong30A1PopupSlide();
  });

}).call(this);

(function() {
  var StartStrong30B1PopupSlide;

  StartStrong30B1PopupSlide = (function() {
    function StartStrong30B1PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StartStrong30B1PopupSlide.prototype.onRender = function(element) {};

    StartStrong30B1PopupSlide.prototype.onEnter = function(element) {};

    StartStrong30B1PopupSlide.prototype.onExit = function(element) {};

    return StartStrong30B1PopupSlide;

  })();

  app.register('StartStrong30B1PopupSlide', function() {
    return new StartStrong30B1PopupSlide();
  });

}).call(this);

(function() {
  var StartStrong30B2PopupSlide;

  StartStrong30B2PopupSlide = (function() {
    function StartStrong30B2PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StartStrong30B2PopupSlide.prototype.onRender = function(element) {};

    StartStrong30B2PopupSlide.prototype.onEnter = function(element) {};

    StartStrong30B2PopupSlide.prototype.onExit = function(element) {};

    return StartStrong30B2PopupSlide;

  })();

  app.register('StartStrong30B2PopupSlide', function() {
    return new StartStrong30B2PopupSlide();
  });

}).call(this);

(function() {
  var StartStrong30B3PopupSlide;

  StartStrong30B3PopupSlide = (function() {
    function StartStrong30B3PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StartStrong30B3PopupSlide.prototype.onRender = function(element) {};

    StartStrong30B3PopupSlide.prototype.onEnter = function(element) {};

    StartStrong30B3PopupSlide.prototype.onExit = function(element) {};

    return StartStrong30B3PopupSlide;

  })();

  app.register('StartStrong30B3PopupSlide', function() {
    return new StartStrong30B3PopupSlide();
  });

}).call(this);

(function() {
  var StartStrong30B4PopupSlide;

  StartStrong30B4PopupSlide = (function() {
    function StartStrong30B4PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StartStrong30B4PopupSlide.prototype.onRender = function(element) {};

    StartStrong30B4PopupSlide.prototype.onEnter = function(element) {};

    StartStrong30B4PopupSlide.prototype.onExit = function(element) {};

    return StartStrong30B4PopupSlide;

  })();

  app.register('StartStrong30B4PopupSlide', function() {
    return new StartStrong30B4PopupSlide();
  });

}).call(this);

(function() {
  var StartStrong30B5PopupSlide;

  StartStrong30B5PopupSlide = (function() {
    function StartStrong30B5PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StartStrong30B5PopupSlide.prototype.onRender = function(element) {};

    StartStrong30B5PopupSlide.prototype.onEnter = function(element) {};

    StartStrong30B5PopupSlide.prototype.onExit = function(element) {};

    return StartStrong30B5PopupSlide;

  })();

  app.register('StartStrong30B5PopupSlide', function() {
    return new StartStrong30B5PopupSlide();
  });

}).call(this);

(function() {
  var StartStrong30C1PopupSlide;

  StartStrong30C1PopupSlide = (function() {
    function StartStrong30C1PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StartStrong30C1PopupSlide.prototype.onRender = function(element) {};

    StartStrong30C1PopupSlide.prototype.onEnter = function(element) {};

    StartStrong30C1PopupSlide.prototype.onExit = function(element) {};

    return StartStrong30C1PopupSlide;

  })();

  app.register('StartStrong30C1PopupSlide', function() {
    return new StartStrong30C1PopupSlide();
  });

}).call(this);

(function() {
  var StartStrong30C2PopupSlide;

  StartStrong30C2PopupSlide = (function() {
    function StartStrong30C2PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StartStrong30C2PopupSlide.prototype.onRender = function(element) {};

    StartStrong30C2PopupSlide.prototype.onEnter = function(element) {};

    StartStrong30C2PopupSlide.prototype.onExit = function(element) {};

    return StartStrong30C2PopupSlide;

  })();

  app.register('StartStrong30C2PopupSlide', function() {
    return new StartStrong30C2PopupSlide();
  });

}).call(this);

(function() {
  var StartStrong30C3PopupSlide;

  StartStrong30C3PopupSlide = (function() {
    function StartStrong30C3PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StartStrong30C3PopupSlide.prototype.onRender = function(element) {};

    StartStrong30C3PopupSlide.prototype.onEnter = function(element) {};

    StartStrong30C3PopupSlide.prototype.onExit = function(element) {};

    return StartStrong30C3PopupSlide;

  })();

  app.register('StartStrong30C3PopupSlide', function() {
    return new StartStrong30C3PopupSlide();
  });

}).call(this);

(function() {
  var StartStrong30Slide;

  StartStrong30Slide = (function() {
    function StartStrong30Slide() {
      this.events = {
        'tap button.left': function() {
          return this.goToState1();
        },
        'tap button.state2': function() {
          return this.goToState2();
        },
        'tap button.state3': function() {
          return this.goToState3();
        }
      };
      this.states = [
        {
          id: '0'
        }, {
          id: '1'
        }, {
          id: '2'
        }, {
          id: '3'
        }
      ];
      this.config = {
        "figcaption": ['state1', 'state2', 'state3', 'state3'],
        "headline": ['main', 'main', 'second', 'second'],
        "holder": [false, true, true, true],
        "button2": [true, true, false, false],
        "button3": [true, true, true, false],
        "monitoringNames": ["DME_Eff_StartStrongVividDME_State1_Slide", "DME_Eff_StartStrongVividDME_State2_Slide", "DME_Eff_StartStrongVividDME_State3_Slide", "DME_Eff_StartStrongVividDME_State4_Slide"]
      };
    }

    StartStrong30Slide.prototype.updateGraph = function(element) {
      var tag;
      tag = document.createElement('div');
      tag.classList.add('white-holder');
      element.getElementsByClassName('g-line')[1].appendChild(tag);
      return tag;
    };

    StartStrong30Slide.prototype.onRender = function(element) {
      this.element = element;
      app.module.get('ahUtils').getSlideData(this.el.id, (function(_this) {
        return function(data) {
          return _this.texts = data;
        };
      })(this));
      this.refs = app.module.get('ah-auto-references-popup');
      this.refsPopup = app.module.get('ahRefsNotesPopup');
      this.analitycsModule = app.module.get('ahAnalitycs');
      this.tag = this.updateGraph(element);
      this.figcaption = element.getElementsByTagName('figcaption')[0];
      this.h2 = element.getElementsByTagName('h2')[0];
      this.h3 = element.getElementsByTagName('h3')[0];
      this.graphText3 = element.getElementsByClassName('graph-text-data')[0];
      this.button_state2 = element.getElementsByClassName('state2')[0];
      return this.button_state3 = element.getElementsByClassName('state3')[0];
    };

    StartStrong30Slide.prototype.toggleHolder = function(param) {
      if (param) {
        return this.tag.classList.add('hidden');
      } else {
        return this.tag.classList.remove('hidden');
      }
    };

    StartStrong30Slide.prototype.changeText = function(element, text) {
      return element.innerHTML = text;
    };

    StartStrong30Slide.prototype.setButtonState = function(button, param) {
      if (param) {
        button.classList.remove('minus');
      } else {
        button.classList.add('minus');
      }
      if (param) {
        return button.classList.add('plus');
      } else {
        return button.classList.remove('plus');
      }
    };

    StartStrong30Slide.prototype.stateCommon = function(state) {
      this.goTo(state);
      this.changeText(this.figcaption, this.texts.graph_captions[this.config.figcaption[state]]);
      this.changeText(this.h2, this.texts.lead_paragraphs[this.config.headline[state]]);
      if (this.stateIs('3')) {
        this.changeText(this.graphText3, this.texts.graph.graph_block_text);
      }
      this.refs.autoReferences(this.el);
      app.trigger('state:update', {
        slide: {
          el: this.el
        }
      });
      this.toggleHolder(this.config.holder[state]);
      this.setButtonState(this.button_state2, this.config.button2[state]);
      this.setButtonState(this.button_state3, this.config.button3[state]);
      return this.submitMonitoring(this.config.monitoringNames[state]);
    };

    StartStrong30Slide.prototype.goToState1 = function() {
      return this.stateCommon((+(this.getState().id === '0')).toString());
    };

    StartStrong30Slide.prototype.goToState2 = function() {
      return this.stateCommon(['0', '1'].indexOf(this.getState().id) > -1 ? '2' : '1');
    };

    StartStrong30Slide.prototype.goToState3 = function() {
      return this.stateCommon(this.getState().id === '2' ? '3' : '2');
    };

    StartStrong30Slide.prototype.onEnter = function(element) {
      return this._setState(1);
    };

    StartStrong30Slide.prototype.onExit = function(element) {
      this.changeText(this.graphText3, "");
      return this._setState(1);
    };

    StartStrong30Slide.prototype.submitMonitoring = function(name) {
      return this.analitycsModule.submitCustomSlideMonitoring({
        id: this.element.id,
        name: name
      });
    };

    return StartStrong30Slide;

  })();

  app.register('StartStrong30Slide', function() {
    return new StartStrong30Slide();
  });

}).call(this);

(function() {
  var StartStrong31Slide;

  StartStrong31Slide = (function() {
    function StartStrong31Slide() {
      this.events = {};
      this.states = [];
    }

    StartStrong31Slide.prototype.onRender = function(element) {
      var $slider, baseFont, controlPoints, controlPoints2;
      $slider = element.getElementsByClassName("before-after-slider")[0];
      baseFont = parseFloat($("body").css("font-size"));
      controlPoints = [72, 127, 189, 243, 316, 387, 445, 511, 571, 641];
      controlPoints2 = [93, 165, 244, 318, 413, 505, 586, 669, 745, 842];
      this.slider = new BeforeAfterSlider($slider, baseFont < 21 ? controlPoints : controlPoints2);
      app.BeforeAfterSlider = {};
      return this.slider.touchEnd = function(index) {
        return app.BeforeAfterSlider.value = index;
      };
    };

    StartStrong31Slide.prototype.onEnter = function(element) {};

    StartStrong31Slide.prototype.onExit = function(element) {
      return this.slider.reset();
    };

    return StartStrong31Slide;

  })();

  app.register('StartStrong31Slide', function() {
    return new StartStrong31Slide();
  });

}).call(this);

(function() {
  var StartStrong32APopupSlide;

  StartStrong32APopupSlide = (function() {
    function StartStrong32APopupSlide() {
      this.events = {};
      this.states = [];
    }

    StartStrong32APopupSlide.prototype.onRender = function(element) {};

    StartStrong32APopupSlide.prototype.onEnter = function(element) {};

    StartStrong32APopupSlide.prototype.onExit = function(element) {};

    return StartStrong32APopupSlide;

  })();

  app.register('StartStrong32APopupSlide', function() {
    return new StartStrong32APopupSlide();
  });

}).call(this);

(function() {
  var StartStrong32Slide;

  StartStrong32Slide = (function() {
    function StartStrong32Slide() {
      this.events = {
        "tap .next-slide": "goToNextSlide"
      };
      this.states = [];
    }

    StartStrong32Slide.prototype.goToNextSlide = function() {
      return app.slideshow.next();
    };

    StartStrong32Slide.prototype.onRender = function(element) {};

    StartStrong32Slide.prototype.onEnter = function(element) {};

    StartStrong32Slide.prototype.onExit = function(element) {};

    return StartStrong32Slide;

  })();

  app.register('StartStrong32Slide', function() {
    return new StartStrong32Slide();
  });

}).call(this);

(function() {
  var StartStrong33Slide;

  StartStrong33Slide = (function() {
    function StartStrong33Slide() {
      this.events = {
        "tap .previous-slide": "goToPreviousSlide"
      };
      this.states = [];
    }

    StartStrong33Slide.prototype.goToPreviousSlide = function() {
      return app.slideshow.prev();
    };

    StartStrong33Slide.prototype.onRender = function(element) {};

    StartStrong33Slide.prototype.onEnter = function(element) {};

    StartStrong33Slide.prototype.onExit = function(element) {};

    return StartStrong33Slide;

  })();

  app.register('StartStrong33Slide', function() {
    return new StartStrong33Slide();
  });

}).call(this);

(function() {
  var StartStrong34A1PopupSlide;

  StartStrong34A1PopupSlide = (function() {
    function StartStrong34A1PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StartStrong34A1PopupSlide.prototype.onRender = function(element) {};

    StartStrong34A1PopupSlide.prototype.onEnter = function(element) {};

    StartStrong34A1PopupSlide.prototype.onExit = function(element) {};

    return StartStrong34A1PopupSlide;

  })();

  app.register('StartStrong34A1PopupSlide', function() {
    return new StartStrong34A1PopupSlide();
  });

}).call(this);

(function() {
  var StartStrong34A2PopupSlide;

  StartStrong34A2PopupSlide = (function() {
    function StartStrong34A2PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StartStrong34A2PopupSlide.prototype.onRender = function(element) {};

    StartStrong34A2PopupSlide.prototype.onEnter = function(element) {};

    StartStrong34A2PopupSlide.prototype.onExit = function(element) {};

    return StartStrong34A2PopupSlide;

  })();

  app.register('StartStrong34A2PopupSlide', function() {
    return new StartStrong34A2PopupSlide();
  });

}).call(this);

(function() {
  var StartStrong34A3PopupSlide;

  StartStrong34A3PopupSlide = (function() {
    function StartStrong34A3PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StartStrong34A3PopupSlide.prototype.onRender = function(element) {};

    StartStrong34A3PopupSlide.prototype.onEnter = function(element) {};

    StartStrong34A3PopupSlide.prototype.onExit = function(element) {};

    return StartStrong34A3PopupSlide;

  })();

  app.register('StartStrong34A3PopupSlide', function() {
    return new StartStrong34A3PopupSlide();
  });

}).call(this);

(function() {
  var StartStrong34A4PopupSlide;

  StartStrong34A4PopupSlide = (function() {
    function StartStrong34A4PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StartStrong34A4PopupSlide.prototype.onRender = function(element) {};

    StartStrong34A4PopupSlide.prototype.onEnter = function(element) {};

    StartStrong34A4PopupSlide.prototype.onExit = function(element) {};

    return StartStrong34A4PopupSlide;

  })();

  app.register('StartStrong34A4PopupSlide', function() {
    return new StartStrong34A4PopupSlide();
  });

}).call(this);

(function() {
  var StartStrong34A5PopupSlide;

  StartStrong34A5PopupSlide = (function() {
    function StartStrong34A5PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StartStrong34A5PopupSlide.prototype.onRender = function(element) {};

    StartStrong34A5PopupSlide.prototype.onEnter = function(element) {};

    StartStrong34A5PopupSlide.prototype.onExit = function(element) {};

    return StartStrong34A5PopupSlide;

  })();

  app.register('StartStrong34A5PopupSlide', function() {
    return new StartStrong34A5PopupSlide();
  });

}).call(this);

(function() {
  var StartStrong34B1PopupSlide;

  StartStrong34B1PopupSlide = (function() {
    function StartStrong34B1PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StartStrong34B1PopupSlide.prototype.onRender = function(element) {};

    StartStrong34B1PopupSlide.prototype.onEnter = function(element) {};

    StartStrong34B1PopupSlide.prototype.onExit = function(element) {};

    return StartStrong34B1PopupSlide;

  })();

  app.register('StartStrong34B1PopupSlide', function() {
    return new StartStrong34B1PopupSlide();
  });

}).call(this);

(function() {
  var StartStrong34Slide;

  StartStrong34Slide = (function() {
    function StartStrong34Slide() {
      this.events = {};
      this.states = [];
    }

    StartStrong34Slide.prototype.onRender = function(element) {};

    StartStrong34Slide.prototype.onEnter = function(element) {};

    StartStrong34Slide.prototype.onExit = function(element) {};

    return StartStrong34Slide;

  })();

  app.register('StartStrong34Slide', function() {
    return new StartStrong34Slide();
  });

}).call(this);

(function() {
  var StartStrong35A1PopupSlide;

  StartStrong35A1PopupSlide = (function() {
    function StartStrong35A1PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StartStrong35A1PopupSlide.prototype.onRender = function(element) {};

    StartStrong35A1PopupSlide.prototype.onEnter = function(element) {};

    StartStrong35A1PopupSlide.prototype.onExit = function(element) {};

    return StartStrong35A1PopupSlide;

  })();

  app.register('StartStrong35A1PopupSlide', function() {
    return new StartStrong35A1PopupSlide();
  });

}).call(this);

(function() {
  var StartStrong35A2PopupSlide;

  StartStrong35A2PopupSlide = (function() {
    function StartStrong35A2PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StartStrong35A2PopupSlide.prototype.onRender = function(element) {};

    StartStrong35A2PopupSlide.prototype.onEnter = function(element) {};

    StartStrong35A2PopupSlide.prototype.onExit = function(element) {};

    return StartStrong35A2PopupSlide;

  })();

  app.register('StartStrong35A2PopupSlide', function() {
    return new StartStrong35A2PopupSlide();
  });

}).call(this);

(function() {
  var StartStrong35A3PopupSlide;

  StartStrong35A3PopupSlide = (function() {
    function StartStrong35A3PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StartStrong35A3PopupSlide.prototype.onRender = function(element) {};

    StartStrong35A3PopupSlide.prototype.onEnter = function(element) {};

    StartStrong35A3PopupSlide.prototype.onExit = function(element) {};

    return StartStrong35A3PopupSlide;

  })();

  app.register('StartStrong35A3PopupSlide', function() {
    return new StartStrong35A3PopupSlide();
  });

}).call(this);

(function() {
  var StartStrong35A4PopupSlide;

  StartStrong35A4PopupSlide = (function() {
    function StartStrong35A4PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StartStrong35A4PopupSlide.prototype.onRender = function(element) {};

    StartStrong35A4PopupSlide.prototype.onEnter = function(element) {};

    StartStrong35A4PopupSlide.prototype.onExit = function(element) {};

    return StartStrong35A4PopupSlide;

  })();

  app.register('StartStrong35A4PopupSlide', function() {
    return new StartStrong35A4PopupSlide();
  });

}).call(this);

(function() {
  var StartStrong35B1PopupSlide;

  StartStrong35B1PopupSlide = (function() {
    function StartStrong35B1PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StartStrong35B1PopupSlide.prototype.onRender = function(element) {};

    StartStrong35B1PopupSlide.prototype.onEnter = function(element) {};

    StartStrong35B1PopupSlide.prototype.onExit = function(element) {};

    return StartStrong35B1PopupSlide;

  })();

  app.register('StartStrong35B1PopupSlide', function() {
    return new StartStrong35B1PopupSlide();
  });

}).call(this);

(function() {
  var StartStrong35Slide;

  StartStrong35Slide = (function() {
    function StartStrong35Slide() {
      this.events = {};
      this.states = [
        {
          id: "default",
          name: "DME_Eff_StartStrongProtocolT_State1_Slide",
          onEnter: function() {
            return this.submitMonitoring();
          }
        }, {
          id: "detail-1",
          name: "DME_Eff_StartStrongProtocolT_State2_Slide",
          onEnter: function() {
            return this.submitMonitoring();
          }
        }, {
          id: "detail-2",
          name: "DME_Eff_StartStrongProtocolT_State3_Slide",
          onEnter: function() {
            return this.submitMonitoring();
          }
        }
      ];
    }

    StartStrong35Slide.prototype.onRender = function(element) {
      this.element = element;
      return this.analitycsModule = app.module.get('ahAnalitycs');
    };

    StartStrong35Slide.prototype.onEnter = function(element) {};

    StartStrong35Slide.prototype.onExit = function(element) {
      return this.reset();
    };

    StartStrong35Slide.prototype.submitMonitoring = function() {
      return this.analitycsModule.submitCustomSlideMonitoring({
        id: this.element.id,
        name: this.getState().name
      });
    };

    return StartStrong35Slide;

  })();

  app.register('StartStrong35Slide', function() {
    return new StartStrong35Slide();
  });

}).call(this);

(function() {
  var StayStrong40A1PopupSlide;

  StayStrong40A1PopupSlide = (function() {
    function StayStrong40A1PopupSlide() {
      this.events = {
        "tap .arrow-btn": function() {
          return this.changeState();
        }
      };
      this.states = [
        {
          id: "default",
          name: "DME_Eff_BCVALongTermDosingGains_State1_PopUp",
          onEnter: function() {
            return this.submitMonitoring();
          }
        }, {
          id: "first",
          name: "DME_Eff_BCVALongTermDosingGains_State2_PopUp",
          onEnter: function() {
            return this.submitMonitoring();
          }
        }, {
          id: "second",
          name: "DME_Eff_BCVALongTermDosingGains_State3_PopUp",
          onEnter: function() {
            return this.submitMonitoring();
          }
        }, {
          id: "third",
          name: "DME_Eff_BCVALongTermDosingGains_State4_PopUp",
          onEnter: function() {
            return this.submitMonitoring();
          }
        }
      ];
      this.$buttons = null;
    }

    StayStrong40A1PopupSlide.prototype.onRender = function(element) {
      this.element = element;
      this.$buttons = element.getElementsByClassName("arrow-btn");
      return this.analitycsModule = app.module.get('ahAnalitycs');
    };

    StayStrong40A1PopupSlide.prototype.changeState = function() {
      var button;
      button = event.target;
      if (button.classList.contains("plus")) {
        this.resetButtons();
        button.classList.remove("plus");
        return button.classList.add("minus");
      } else {
        this.resetButtons();
        this.reset();
        button.classList.remove("minus");
        return button.classList.add("plus");
      }
    };

    StayStrong40A1PopupSlide.prototype.resetButtons = function() {
      var button, i, len, ref, results;
      ref = this.$buttons;
      results = [];
      for (i = 0, len = ref.length; i < len; i++) {
        button = ref[i];
        button.classList.remove("minus");
        results.push(button.classList.add("plus"));
      }
      return results;
    };

    StayStrong40A1PopupSlide.prototype.onEnter = function(element) {
      return this._setState(1);
    };

    StayStrong40A1PopupSlide.prototype.onExit = function(element) {
      this.reset();
      return this.resetButtons();
    };

    StayStrong40A1PopupSlide.prototype.submitMonitoring = function() {
      return this.analitycsModule.submitCustomSlideMonitoring({
        id: this.element.id,
        name: this.getState().name
      });
    };

    return StayStrong40A1PopupSlide;

  })();

  app.register('StayStrong40A1PopupSlide', function() {
    return new StayStrong40A1PopupSlide();
  });

}).call(this);

(function() {
  var StayStrong40A2PopupSlide;

  StayStrong40A2PopupSlide = (function() {
    function StayStrong40A2PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StayStrong40A2PopupSlide.prototype.onRender = function(element) {};

    StayStrong40A2PopupSlide.prototype.onEnter = function(element) {};

    StayStrong40A2PopupSlide.prototype.onExit = function(element) {};

    return StayStrong40A2PopupSlide;

  })();

  app.register('StayStrong40A2PopupSlide', function() {
    return new StayStrong40A2PopupSlide();
  });

}).call(this);

(function() {
  var StayStrong40A3PopupSlide;

  StayStrong40A3PopupSlide = (function() {
    function StayStrong40A3PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StayStrong40A3PopupSlide.prototype.onRender = function(element) {};

    StayStrong40A3PopupSlide.prototype.onEnter = function(element) {};

    StayStrong40A3PopupSlide.prototype.onExit = function(element) {};

    return StayStrong40A3PopupSlide;

  })();

  app.register('StayStrong40A3PopupSlide', function() {
    return new StayStrong40A3PopupSlide();
  });

}).call(this);

(function() {
  var StayStrong40B1PopupSlide;

  StayStrong40B1PopupSlide = (function() {
    function StayStrong40B1PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StayStrong40B1PopupSlide.prototype.onRender = function(element) {};

    StayStrong40B1PopupSlide.prototype.onEnter = function(element) {};

    StayStrong40B1PopupSlide.prototype.onExit = function(element) {};

    return StayStrong40B1PopupSlide;

  })();

  app.register('StayStrong40B1PopupSlide', function() {
    return new StayStrong40B1PopupSlide();
  });

}).call(this);

(function() {
  var StayStrong40C1PopupSlide;

  StayStrong40C1PopupSlide = (function() {
    function StayStrong40C1PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StayStrong40C1PopupSlide.prototype.onRender = function(element) {};

    StayStrong40C1PopupSlide.prototype.onEnter = function(element) {};

    StayStrong40C1PopupSlide.prototype.onExit = function(element) {};

    return StayStrong40C1PopupSlide;

  })();

  app.register('StayStrong40C1PopupSlide', function() {
    return new StayStrong40C1PopupSlide();
  });

}).call(this);

(function() {
  var StayStrong40C2PopupSlide;

  StayStrong40C2PopupSlide = (function() {
    function StayStrong40C2PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StayStrong40C2PopupSlide.prototype.onRender = function(element) {};

    StayStrong40C2PopupSlide.prototype.onEnter = function(element) {};

    StayStrong40C2PopupSlide.prototype.onExit = function(element) {};

    return StayStrong40C2PopupSlide;

  })();

  app.register('StayStrong40C2PopupSlide', function() {
    return new StayStrong40C2PopupSlide();
  });

}).call(this);

(function() {
  var StayStrong40Slide;

  StayStrong40Slide = (function() {
    function StayStrong40Slide() {
      this.events = {
        "tap .arrow-btn": function() {
          return this.changeState();
        }
      };
      this.states = [
        {
          id: "default",
          name: "DME_Eff_StayStrongMaintainGains_State1_Slide",
          onEnter: function() {
            return this.submitMonitoring();
          }
        }, {
          id: "first",
          name: "DME_Eff_StayStrongMaintainGains_State2_Slide",
          onEnter: function() {
            return this.submitMonitoring();
          }
        }, {
          id: "second",
          name: "DME_Eff_StayStrongMaintainGains_State3_Slide",
          onEnter: function() {
            return this.submitMonitoring();
          }
        }, {
          id: "third",
          name: "DME_Eff_StayStrongMaintainGains_State4_Slide",
          onEnter: function() {
            return this.submitMonitoring();
          }
        }
      ];
      this.$buttons = null;
    }

    StayStrong40Slide.prototype.onRender = function(element) {
      this.element = element;
      this.analitycsModule = app.module.get('ahAnalitycs');
      return this.$buttons = element.getElementsByClassName("state-btn");
    };

    StayStrong40Slide.prototype.changeState = function() {
      var button;
      button = event.target;
      if (button.classList.contains("plus")) {
        this.resetButtons();
        button.classList.remove("plus");
        return button.classList.add("minus");
      } else {
        this.resetButtons();
        return this.reset();
      }
    };

    StayStrong40Slide.prototype.resetButtons = function() {
      var button, i, len, ref, results;
      ref = this.$buttons;
      results = [];
      for (i = 0, len = ref.length; i < len; i++) {
        button = ref[i];
        button.classList.remove("minus");
        results.push(button.classList.add("plus"));
      }
      return results;
    };

    StayStrong40Slide.prototype.onEnter = function(element) {};

    StayStrong40Slide.prototype.onExit = function(element) {
      this.resetButtons();
      return this.reset();
    };

    StayStrong40Slide.prototype.submitMonitoring = function() {
      return this.analitycsModule.submitCustomSlideMonitoring({
        id: this.element.id,
        name: this.getState().name
      });
    };

    return StayStrong40Slide;

  })();

  app.register('StayStrong40Slide', function() {
    return new StayStrong40Slide();
  });

}).call(this);

(function() {
  var StayStrong41A1PopupSlide;

  StayStrong41A1PopupSlide = (function() {
    function StayStrong41A1PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StayStrong41A1PopupSlide.prototype.onRender = function(element) {};

    StayStrong41A1PopupSlide.prototype.onEnter = function(element) {};

    StayStrong41A1PopupSlide.prototype.onExit = function(element) {};

    return StayStrong41A1PopupSlide;

  })();

  app.register('StayStrong41A1PopupSlide', function() {
    return new StayStrong41A1PopupSlide();
  });

}).call(this);

(function() {
  var StayStrong41A2PopupSlide;

  StayStrong41A2PopupSlide = (function() {
    function StayStrong41A2PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StayStrong41A2PopupSlide.prototype.onRender = function(element) {};

    StayStrong41A2PopupSlide.prototype.onEnter = function(element) {};

    StayStrong41A2PopupSlide.prototype.onExit = function(element) {};

    return StayStrong41A2PopupSlide;

  })();

  app.register('StayStrong41A2PopupSlide', function() {
    return new StayStrong41A2PopupSlide();
  });

}).call(this);

(function() {
  var StayStrong41A3PopupSlide;

  StayStrong41A3PopupSlide = (function() {
    function StayStrong41A3PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StayStrong41A3PopupSlide.prototype.onRender = function(element) {};

    StayStrong41A3PopupSlide.prototype.onEnter = function(element) {};

    StayStrong41A3PopupSlide.prototype.onExit = function(element) {};

    return StayStrong41A3PopupSlide;

  })();

  app.register('StayStrong41A3PopupSlide', function() {
    return new StayStrong41A3PopupSlide();
  });

}).call(this);

(function() {
  var StayStrong41A4PopupSlide;

  StayStrong41A4PopupSlide = (function() {
    function StayStrong41A4PopupSlide() {
      this.events = {};
      this.states = [];
    }

    StayStrong41A4PopupSlide.prototype.onRender = function(element) {};

    StayStrong41A4PopupSlide.prototype.onEnter = function(element) {};

    StayStrong41A4PopupSlide.prototype.onExit = function(element) {};

    return StayStrong41A4PopupSlide;

  })();

  app.register('StayStrong41A4PopupSlide', function() {
    return new StayStrong41A4PopupSlide();
  });

}).call(this);

(function() {
  var StayStrong41Slide;

  StayStrong41Slide = (function() {
    function StayStrong41Slide() {
      this.events = {};
      this.states = [];
    }

    StayStrong41Slide.prototype.onRender = function(element) {};

    StayStrong41Slide.prototype.onEnter = function(element) {};

    StayStrong41Slide.prototype.onExit = function(element) {};

    return StayStrong41Slide;

  })();

  app.register('StayStrong41Slide', function() {
    return new StayStrong41Slide();
  });

}).call(this);

(function() {
  var UniqueMoA50A1PopupSlide;

  UniqueMoA50A1PopupSlide = (function() {
    function UniqueMoA50A1PopupSlide() {
      this.events = {};
      this.states = [];
    }

    UniqueMoA50A1PopupSlide.prototype.onRender = function(element) {};

    UniqueMoA50A1PopupSlide.prototype.onEnter = function(element) {};

    UniqueMoA50A1PopupSlide.prototype.onExit = function(element) {};

    return UniqueMoA50A1PopupSlide;

  })();

  app.register('UniqueMoA50A1PopupSlide', function() {
    return new UniqueMoA50A1PopupSlide();
  });

}).call(this);

(function() {
  var UniqueMoA50A2PopupSlide;

  UniqueMoA50A2PopupSlide = (function() {
    function UniqueMoA50A2PopupSlide() {
      this.events = {};
      this.states = [];
    }

    UniqueMoA50A2PopupSlide.prototype.onRender = function(element) {};

    UniqueMoA50A2PopupSlide.prototype.onEnter = function(element) {};

    UniqueMoA50A2PopupSlide.prototype.onExit = function(element) {};

    return UniqueMoA50A2PopupSlide;

  })();

  app.register('UniqueMoA50A2PopupSlide', function() {
    return new UniqueMoA50A2PopupSlide();
  });

}).call(this);

(function() {
  var UniqueMoA50A3PopupSlide;

  UniqueMoA50A3PopupSlide = (function() {
    function UniqueMoA50A3PopupSlide() {
      this.events = {};
      this.states = [];
    }

    UniqueMoA50A3PopupSlide.prototype.onRender = function(element) {};

    UniqueMoA50A3PopupSlide.prototype.onEnter = function(element) {};

    UniqueMoA50A3PopupSlide.prototype.onExit = function(element) {};

    return UniqueMoA50A3PopupSlide;

  })();

  app.register('UniqueMoA50A3PopupSlide', function() {
    return new UniqueMoA50A3PopupSlide();
  });

}).call(this);

(function() {
  var UniqueMoA50A4PopupSlide;

  UniqueMoA50A4PopupSlide = (function() {
    function UniqueMoA50A4PopupSlide() {
      this.events = {};
      this.states = [];
    }

    UniqueMoA50A4PopupSlide.prototype.onRender = function(element) {};

    UniqueMoA50A4PopupSlide.prototype.onEnter = function(element) {};

    UniqueMoA50A4PopupSlide.prototype.onExit = function(element) {};

    return UniqueMoA50A4PopupSlide;

  })();

  app.register('UniqueMoA50A4PopupSlide', function() {
    return new UniqueMoA50A4PopupSlide();
  });

}).call(this);

(function() {
  var UniqueMoA50A5PopupSlide;

  UniqueMoA50A5PopupSlide = (function() {
    function UniqueMoA50A5PopupSlide() {
      this.events = {};
      this.states = [];
    }

    UniqueMoA50A5PopupSlide.prototype.onRender = function(element) {};

    UniqueMoA50A5PopupSlide.prototype.onEnter = function(element) {};

    UniqueMoA50A5PopupSlide.prototype.onExit = function(element) {};

    return UniqueMoA50A5PopupSlide;

  })();

  app.register('UniqueMoA50A5PopupSlide', function() {
    return new UniqueMoA50A5PopupSlide();
  });

}).call(this);

(function() {
  var UniqueMoA50A6PopupSlide;

  UniqueMoA50A6PopupSlide = (function() {
    function UniqueMoA50A6PopupSlide() {
      this.events = {};
      this.states = [];
    }

    UniqueMoA50A6PopupSlide.prototype.onRender = function(element) {};

    UniqueMoA50A6PopupSlide.prototype.onEnter = function(element) {};

    UniqueMoA50A6PopupSlide.prototype.onExit = function(element) {};

    return UniqueMoA50A6PopupSlide;

  })();

  app.register('UniqueMoA50A6PopupSlide', function() {
    return new UniqueMoA50A6PopupSlide();
  });

}).call(this);

(function() {
  var UniqueMoA50B1PopupSlide;

  UniqueMoA50B1PopupSlide = (function() {
    function UniqueMoA50B1PopupSlide() {
      this.events = {};
      this.states = [];
    }

    UniqueMoA50B1PopupSlide.prototype.onRender = function(element) {};

    UniqueMoA50B1PopupSlide.prototype.onEnter = function(element) {};

    UniqueMoA50B1PopupSlide.prototype.onExit = function(element) {};

    return UniqueMoA50B1PopupSlide;

  })();

  app.register('UniqueMoA50B1PopupSlide', function() {
    return new UniqueMoA50B1PopupSlide();
  });

}).call(this);

(function() {
  var UniqueMoA50B2PopupSlide;

  UniqueMoA50B2PopupSlide = (function() {
    function UniqueMoA50B2PopupSlide() {
      this.events = {};
      this.states = [];
    }

    UniqueMoA50B2PopupSlide.prototype.onRender = function(element) {};

    UniqueMoA50B2PopupSlide.prototype.onEnter = function(element) {};

    UniqueMoA50B2PopupSlide.prototype.onExit = function(element) {};

    return UniqueMoA50B2PopupSlide;

  })();

  app.register('UniqueMoA50B2PopupSlide', function() {
    return new UniqueMoA50B2PopupSlide();
  });

}).call(this);

(function() {
  var UniqueMoA50B3PopupSlide;

  UniqueMoA50B3PopupSlide = (function() {
    function UniqueMoA50B3PopupSlide() {
      this.events = {};
      this.states = [
        {
          id: "mead",
          name: "DME_Eff_DEXDurability_State1_PopUp",
          onEnter: function() {
            return this.onChangeState();
          }
        }, {
          id: "bevordex",
          name: "DME_Eff_DEXDurability_State2_PopUp",
          onEnter: function() {
            return this.onChangeState();
          }
        }, {
          id: "callanan",
          name: "DME_Eff_DEXDurability_State3_PopUp",
          onEnter: function() {
            return this.onChangeState();
          }
        }
      ];
    }

    UniqueMoA50B3PopupSlide.prototype.onRender = function(element) {
      this.el = element;
      this.analitycsModule = app.module.get('ahAnalitycs');
      this.refs = app.module.get('ah-auto-references-popup');
      this.extedNotes = app.module.get('notes');
      app.module.get('ahUtils').getSlideData(this.el.id, (function(_this) {
        return function(data) {
          return _this.texts = data;
        };
      })(this));
      return this.headline = element.getElementsByTagName('h1')[0];
    };

    UniqueMoA50B3PopupSlide.prototype.changeText = function(element, text) {
      return element.innerHTML = text;
    };

    UniqueMoA50B3PopupSlide.prototype.onEnter = function(element) {
      return this.goTo('mead');
    };

    UniqueMoA50B3PopupSlide.prototype.updateNotes = function(module, element) {
      module.updateNotes($(element));
      return module.handleEnter(element);
    };

    UniqueMoA50B3PopupSlide.prototype.onChangeState = function() {
      var textHeadline;
      textHeadline = this.stateIsnt('mead') ? this.texts.headline_1 : this.texts.headline;
      this.changeText(this.headline, textHeadline);
      this.refs.autoReferences(this.el);
      this.updateNotes(this.extedNotes, this.el);
      app.trigger('state:update', {
        slide: {
          el: this.el
        }
      });
      return this.submitMonitoring();
    };

    UniqueMoA50B3PopupSlide.prototype.onExit = function(element) {};

    UniqueMoA50B3PopupSlide.prototype.submitMonitoring = function() {
      return this.analitycsModule.submitCustomSlideMonitoring({
        id: this.el.id,
        name: this.getState().name
      });
    };

    return UniqueMoA50B3PopupSlide;

  })();

  app.register('UniqueMoA50B3PopupSlide', function() {
    return new UniqueMoA50B3PopupSlide();
  });

}).call(this);

(function() {
  var UniqueMoA50B4PopupSlide;

  UniqueMoA50B4PopupSlide = (function() {
    function UniqueMoA50B4PopupSlide() {
      this.events = {};
      this.states = [];
    }

    UniqueMoA50B4PopupSlide.prototype.onRender = function(element) {};

    UniqueMoA50B4PopupSlide.prototype.onEnter = function(element) {};

    UniqueMoA50B4PopupSlide.prototype.onExit = function(element) {};

    return UniqueMoA50B4PopupSlide;

  })();

  app.register('UniqueMoA50B4PopupSlide', function() {
    return new UniqueMoA50B4PopupSlide();
  });

}).call(this);

(function() {
  var UniqueMoA50B5PopupSlide;

  UniqueMoA50B5PopupSlide = (function() {
    function UniqueMoA50B5PopupSlide() {
      this.events = {};
      this.states = [];
    }

    UniqueMoA50B5PopupSlide.prototype.onRender = function(element) {};

    UniqueMoA50B5PopupSlide.prototype.onEnter = function(element) {};

    UniqueMoA50B5PopupSlide.prototype.onExit = function(element) {};

    return UniqueMoA50B5PopupSlide;

  })();

  app.register('UniqueMoA50B5PopupSlide', function() {
    return new UniqueMoA50B5PopupSlide();
  });

}).call(this);

(function() {
  var UniqueMoA50Slide;

  UniqueMoA50Slide = (function() {
    function UniqueMoA50Slide() {
      this.events = {};
      this.states = [];
    }

    UniqueMoA50Slide.prototype.onRender = function(element) {};

    UniqueMoA50Slide.prototype.onEnter = function(element) {};

    UniqueMoA50Slide.prototype.onExit = function(element) {};

    return UniqueMoA50Slide;

  })();

  app.register('UniqueMoA50Slide', function() {
    return new UniqueMoA50Slide();
  });

}).call(this);

app.cache.put("modules/rainmaker_modules/ag-overlay/overlay.html","<!doctype html>\n<html>\n  <head>\n    <title>Overlay</title>\n  </head>\n  <body>\n    <article id=\"overlay\" class=\"slide\">\n      <div class=\"splash-container\">\n        <div class=\"module-content splash-container\">\n          <div class=\"pure-g\">\n              <div class=\"pure-u-1 pure-u-lg-2-3\">\n                <div class=\"l-box\"><h1>ag-overlay <span class=\"mversion\" id=\"overlay-version\">v0.5.0</span></h1>\n\n<h2>CLI Import</h2>\n\n<pre>\n<code class=\"bash hljs state-default\" data-module=\"ag-highlight-js\" id=\"overlay-import\">agnitio use ag-overlay</code></pre>\n\n<h2>Usage</h2>\n\n<p>In index.html or inside slide html, add:</p>\n\n<pre>\n<code class=\"html hljs xml state-default\" data-module=\"ag-highlight-js\" id=\"overlay-use\"><span class=\"hljs-tag\">&lt;<span class=\"hljs-title\">div</span> <span class=\"hljs-attribute\">id</span>=<span class=\"hljs-value\">&quot;overlay-demo&quot;</span> <span class=\"hljs-attribute\">data-module</span>=<span class=\"hljs-value\">&quot;ag-overlay&quot;</span>&gt;</span><span class=\"hljs-tag\">&lt;/<span class=\"hljs-title\">div</span>&gt;</span></code></pre>\n\n<p>Then from any JavaScript you can call:</p>\n\n<pre>\n<code class=\"javascript hljs state-default\" data-module=\"ag-highlight-js\" id=\"overlay-use2\"><span class=\"hljs-keyword\">var</span> overlay = app.view.get(<span class=\"hljs-string\">&apos;overlay-demo&apos;</span>);\n<span class=\"hljs-keyword\">if</span> (overlay) {\n    overlay.open(<span class=\"hljs-string\">&apos;&lt;h1&gt;Overlay Demo&lt;/h1&gt;&apos;</span>);\n}</code></pre>\n\n<p>Or to load a slide into the overlay:</p>\n\n<pre>\n<code class=\"javascript hljs state-default\" data-module=\"ag-highlight-js\" id=\"overlay-use3\"><span class=\"hljs-keyword\">var</span> overlay = app.view.get(<span class=\"hljs-string\">&apos;overlay-demo&apos;</span>);\n<span class=\"hljs-keyword\">if</span> (overlay) {\n    overlay.load(\'some_slide_id\');\n}</code></pre>\n\n<h2>Options</h2>\n\n<p>The following attributes can be set on the element.</p>\n\n<table class=\"pure-table\">\n	<thead>\n		<tr>\n			<th>Attribute</th>\n			<th>Type</th>\n			<th>Default</th>\n			<th>Notes</th>\n		</tr>\n	</thead>\n	<tbody>\n		<tr>\n			<td>width</td>\n			<td>STRING</td>\n			<td>&quot;80%&quot;</td>\n			<td>Width of the overlay content area. Only supports percent at the moment.</td>\n		</tr>\n		<tr>\n			<td>height</td>\n			<td>STRING</td>\n			<td>&quot;80%&quot;</td>\n			<td>Height of the overlay content area. Only supports percent at the moment.</td>\n		</tr>\n		<tr>\n			<td>no-background</td>\n			<td>BOOLEAN</td>\n			<td>false</td>\n			<td>If the darker background behind the content should not be there.</td>\n		</tr>\n		<tr>\n			<td>content</td>\n			<td>STRING</td>\n			<td>&quot;No content available&quot;</td>\n			<td>Default content to be used if nothing is provided in call to &quot;open&quot;.</td>\n		</tr>\n	</tbody>\n</table>\n\n<h2>API</h2>\n\n<p>The following JavaScript API can be used with a module instance:</p>\n\n<table class=\"pure-table\">\n	<thead>\n		<tr>\n			<th>API</th>\n			<th>Parameters</th>\n			<th>Notes</th>\n		</tr>\n	</thead>\n	<tbody>\n		<tr>\n			<td>open</td>\n			<td>content [STRING]</td>\n			<td>Opens the overlay with the provided content.</td>\n		</tr>\n    <tr>\n      <td>load</td>\n      <td>slideId [STRING]</td>\n      <td>Loads a slide into the overlay and opens it.</td>\n    </tr>\n		<tr>\n			<td>close</td>\n			<td></td>\n			<td>Closes the overlay.</td>\n		</tr>\n		<tr>\n			<td>setDimensions</td>\n			<td>width [INTEGER], height [INTEGER]</td>\n			<td>Update the width and height of the content container. Should be between 0 and 100.</td>\n		</tr>\n	</tbody>\n</table>\n\n<h2>Styling</h2>\n\n<p>The overlay can be styled as desired. The following classes are set on the elements:</p>\n\n<table class=\"pure-table\">\n	<thead>\n		<tr>\n			<th>Class name</th>\n			<th>Notes</th>\n		</tr>\n	</thead>\n	<tbody>\n		<tr>\n			<td>ag-overlay-background</td>\n			<td>Use this to style the semi-transparent background.</td>\n		</tr>\n		<tr>\n			<td>ag-overlay-content</td>\n			<td>Use this to style the container holding the content of the overlay.</td>\n		</tr>\n		<tr>\n			<td>ag-overlay-x</td>\n			<td>This class can be used to style the default close button.</td>\n		</tr>\n	</tbody>\n</table>\n\n<h2>Custom Close Element</h2>\n\n<p>Adding an element anywhere within the module with the class &quot;ag-overlay-close&quot; will close the overlay when tapped. You can include this within the content you provide for the overlay or as a separate element inside the module itself, e.g.:</p>\n\n<pre>\n<code class=\"html hljs xml state-default\" data-module=\"ag-highlight-js\" id=\"overlay-custom-close\"><span class=\"hljs-tag\">&lt;<span class=\"hljs-title\">div</span> <span class=\"hljs-attribute\">id</span>=<span class=\"hljs-value\">&quot;overlay-demo&quot;</span> <span class=\"hljs-attribute\">data-module</span>=<span class=\"hljs-value\">&quot;ag-overlay&quot;</span> <span class=\"hljs-attribute\">no-close-btn</span>&gt;</span>\n    <span class=\"hljs-tag\">&lt;<span class=\"hljs-title\">div</span> <span class=\"hljs-attribute\">class</span>=<span class=\"hljs-value\">&quot;ag-overlay-close overlay-custom-close-btn&quot;</span>&gt;</span>Close<span class=\"hljs-tag\">&lt;/<span class=\"hljs-title\">div</span>&gt;</span>\n<span class=\"hljs-tag\">&lt;/<span class=\"hljs-title\">div</span>&gt;</span></code></pre>\n</div>\n              </div>\n              <div class=\"pure-u-1 pure-u-lg-1-3\">\n                  <h2>Demo</h2>\n                  <button id=\"overlay_test\" class=\"pure-button\">Click to open basic overlay</button>\n                  <button id=\"overlay_slide\" class=\"pure-button\">Click to open overlay with slide</button>\n                  <div id=\"overlay-demo\" data-module=\"ag-overlay\" width=\"80%\" height=\"40%\"></div>\n                  <h2>Description</h2>\n                  <p>This module will display provided content in an overlay. The overlay will be displayed in the center of the screen. Clicking outside the content will close the overlay.<br>\n<br>\n<span style=\"color:#FF0000;\"><strong>NOTE:</strong></span> If loading a slide that already have been loaded, the previous version will first be removed from DOM in order to load it in the overlay. This mean that any DOM states will be reset.</p>\n              </div>\n          </div>\n      </div>\n      </div>\n    </article>\n  </body>\n</html>");
app.cache.put("modules/rainmaker_modules/ag-slide-popup/ag-slide-popup.html","<!doctype html>\n<html>\n  <head>\n    <title>Agnitio Slide Popup Module</title>\n    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../themes/agnitio-default/master.css\">\n    <link rel=\"stylesheet\" type=\"text/css\" href=\"ag_slide_popup.css\">\n  </head>\n  <body>\n    <div id=\"ag-slide-popup\">\n    </div>\n    <script src=\"ag-slide-popup.js\"></script>\n  </body>\n</html>");
app.cache.put("modules/rainmaker_modules/ag-video/ag-video.html","<div class=\"ag-video-container\">\n	<video src=\"\" type=\"\" poster=\"\" class=\"ag-video-element\"></video>\n	<div class=\"ag-video-overlay\">\n		<div class=\"ag-video-big-play-button\"><span></span></div>\n		<div class=\"ag-upper-click-layer\"></div>\n		<div class=\"ag-video-controls\">\n			<div class=\"ag-video-play-toggle\"><div></div></div>\n            <div class=\"ag-video-progress-container ag-video-progress-controller\">\n                <div class=\"ag-video-timer ag-video-current-time\">0:00</div>\n                <div class=\"ag-video-timer ag-video-total-time\"></div>\n                <div class=\"ag-video-progress-bar\"></div>\n                <div class=\"ag-video-seek-ele\"></div>\n            </div>\n            <div class=\"ag-video-fullscreen-button\"><div></div></div>\n		</div>\n	</div>\n</div>");
app.cache.put("modules/rainmaker_modules/ah-media-library/ah-media-library.html","<div class=\"MediaLibrary\">\n<h1>Media Library</h1>\n<input type=\"text\" placeholder=\"Search (e.g. pdf reference 2009)\"/>\n<div class=\"scroll\">\n    <ul></ul>\n</div>\n</div>");
app.cache.put("modules/rainmaker_modules/ah-reference-library/ah-reference-library.html","<div class=\"ReferenceLibrary\">\n  <h1>Reference Library</h1>\n  <div class=\"scroll\">\n    <ul></ul>\n  </div>\n</div>\n");
app.cache.put("modules/rainmaker_modules/ah-refs-notes-popup/ah-refs-notes-popup.html","\n<div class=\"auto-references-popup\">\n  <div class=\"scroll\">\n    <div class=\"scroll-element\">\n      <div class=\"refs-wrapper\">\n        <h1>References</h1>\n        <ul class=\"references\"></ul>\n      </div>\n      <div class=\"notes-wrapper\">\n        <h1>Footnotes</h1>\n        <ul class=\"notes\"></ul>\n      </div>\n    </div>\n  </div>\n  <div class=\"x\">&#8855;</div>\n</div>");
app.cache.put("modules/rainmaker_modules/ap-custom-collections/ap-custom-collections.html","<div class=\"custom-collections\">\n  <div data-module=\"ap-overview\" iscustomoverview>\n  </div>\n  <div class=\"editZone\">\n  </div>\n  <div class=\"divisor\">\n    <div class=\"presentationName\">e.g. name</div>\n    <div class=\"arrow\" ></div>\n    <div class=\"instructions\">Build your presentation by dragging slides</div>\n    <div class=\"arrow\"></div>\n    <div class=\"save\">Save</div>\n    <div class=\"cancel\">Cancel</div>\n  </div>\n</div>\n");
app.cache.put("modules/rainmaker_modules/ap-custom-collections-menu/ap-custom-collections-menu.html","<div class=\"custom-collections-menu\">\n  <div class=\"presentationsContainer\">\n    <div class=\"row template\">\n      <div class=\"button favorite\"> </div>\n      <div class=\"button presentation\">\n        <div class=\"name\">\n          to be replaced by javascript\n        </div>\n      </div>\n      <div class=\"button edit\"> </div>\n      <div class=\"button rename\"> </div>\n      <div class=\"button trash\"> </div>\n    </div>\n    <div class=\"row newPresentationButtonContainer\">\n      <div class=\"button newPresentation\">\n        ...\n      </div>\n    </div>\n  </div>\n  <div id=\"maxFavoritesPopUp\"><span>You can only tag 3 presentations simultaneously as favorite!</span></div>\n</div>\n");
app.cache.put("modules/rainmaker_modules/ap-favorite-presentations-buttons/ap-favorite-presentations-buttons.html","<div class=\"favoritePresentationsContainer\">\n\n</div>");
app.cache.put("modules/rainmaker_modules/ap-follow-up-mail/ap-follow-up-mail.html","<div class=\"FollowUpMail\">\n    <div class=\"mail\">\n        <h1>Follow-Up Mail Attachments</h1>\n\n        <div class=\"attachments\">\n            <div data-module=\"ap-media-library\" attachment prefilterattributestobesearched=\"allowDistribution\"\n                 prefiltersearchterms=\"true\" id=\"ap-media-library_471147114711\"></div>\n        </div>\n        <div class=\"sendButton\">New Follow-Up Mail</div>\n        <div class=\"clearButton\">Clear</div>\n    </div>\n    <div class=\"library\">\n        <div data-module=\"ap-media-library\" followupmail prefilterattributestobesearched=\"allowDistribution\"\n             prefiltersearchterms=\"true\"></div>\n    </div>\n</div>\n");
app.cache.put("modules/rainmaker_modules/ap-frequently-asked-questions/ap-frequently-asked-questions.html","<div class=\"FrequentlyAskedQuestions\">\n  <h1>FAQ</h1>\n  <div class=\"scroll\">\n    <ol>\n      <li>\n        <h2 class=\"q\">This is the first question?</h2>\n        <p class=\"a\">Here is the answer to the first question.</p>\n      </li>\n      <li>\n        <h2 class=\"q\">This is the second question?</h2>\n        <p class=\"a\">Here is the answer to the second question.</p>\n      </li>\n      <li>\n        <h2 class=\"q\">This is the third question?</h2>\n        <p class=\"a\">Here is the answer to the third question.</p>\n      </li>\n      <li>\n        <h2 class=\"q\">This is the fourth question?</h2>\n        <p class=\"a\">Here is the answer to the fourth question.</p>\n      </li>\n      <li>\n        <h2 class=\"q\">This is the fifth question?</h2>\n        <p class=\"a\">Here is the answer to the fifth question.</p>\n      </li>\n      <li>\n        <h2 class=\"q\">This is the sixth question?</h2>\n        <p class=\"a\">Here is the answer to the sixth question.</p>\n      </li>\n      <li>\n        <h2 class=\"q\">This is the seventh question?</h2>\n        <p class=\"a\">Here is the answer to the seventh question.</p>\n      </li>\n      <li>\n        <h2 class=\"q\">This is the eighth question?</h2>\n        <p class=\"a\">Here is the answer to the eighth question.</p>\n      </li>\n      <li>\n        <h2 class=\"q\">This is the ninth question?</h2>\n        <p class=\"a\">Here is the answer to the ninth question.</p>\n      </li>\n      <li>\n        <h2 class=\"q\">This is the tenth question?</h2>\n        <p class=\"a\">Here is the answer to the tenth question.</p>\n      </li>\n      <li>\n        <h2 class=\"q\">This is the eleventh question?</h2>\n        <p class=\"a\">Here is the answer to the eleventh question.</p>\n      </li>\n      <li>\n        <h2 class=\"q\">This is the twelfth question?</h2>\n        <p class=\"a\">Here is the answer to the twelfth question.</p>\n      </li>\n      <li>\n        <h2 class=\"q\">This is the thirteenth question?</h2>\n        <p class=\"a\">Here is the answer to the thirteenth question.</p>\n      </li>\n      <li>\n        <h2 class=\"q\">This is the fourteenth question?</h2>\n        <p class=\"a\">Here is the answer to the fourteenth question.</p>\n      </li>\n    </ol>\n  </div>\n</div>\n");
app.cache.put("modules/rainmaker_modules/ap-media-library/ap-media-library.html","<div class=\"MediaLibrary\">\n  <h1>Media Library</h1>\n  <input type=\"text\" placeholder=\"Search (e.g. pdf reference 2009)\"/>\n  <div class=\"scroll\">\n      <ul></ul>\n  </div>\n</div>\n");
app.cache.put("modules/rainmaker_modules/ap-notepad/ap-notepad.html","<div class=\"Notepad\">\n  <canvas id=\"notepad-canvas\" width=\"1920\" height=\"1080\"></canvas>\n  <div class=\"palette\">\n    <div class=\"button exit\"></div>\n    <div class=\"button clear\"></div>\n    <div class=\"button red\"></div>\n    <div class=\"button green\"></div>\n  </div>\n</div>\n");
app.cache.put("modules/rainmaker_modules/ap-overview/ap-overview.html","<div class=\"overview\">\n  <div class=\"collectionsList\">\n  </div>\n  <div class=\"collectionOverview\">\n    <div class=\"o_collection\" />\n  </div>\n</div>\n");
app.cache.put("modules/rainmaker_modules/ap-reference-library/ap-reference-library.html","<div class=\"ReferenceLibrary\">\n  <h1>Reference Library</h1>\n  <div class=\"scroll\">\n    <ul></ul>\n  </div>\n</div>\n");
app.cache.put("modules/rainmaker_modules/ap-slide-indicator/ap-slide-indicator.html","<div class=\"joystick\">\n    <div class=\"up\" ></div>\n    <div class=\"down\" ></div>\n    <div class=\"left\" ></div>\n    <div class=\"right\"></div>\n</div>");
app.cache.put("modules/rainmaker_modules/ap-toolbar/ap-toolbar.html","<div class=\"ap-toolbar\">\n  <div class=\"bar\">\n    <div class=\"button\" data-toolbar-state=\"maximized\" data-module-to-load=\"ap-overview\"></div>\n    <div class=\"button\" data-toolbar-state=\"maximized\" data-module-to-load=\"ap-custom-collections-menu\"></div>\n    <div class=\"button\" data-toolbar-state=\"maximized\" data-module-to-load=\"ap-follow-up-mail\"></div>\n    <div class=\"button\" data-toolbar-state=\"maximized\" data-module-to-load=\"ah-media-library\"></div>\n    <div class=\"button\" data-toolbar-state=\"maximized\" data-module-to-load=\"ap-frequently-asked-questions\"></div>\n    <div class=\"button\" data-toolbar-state=\"minimized\" data-module-to-load=\"ap-specific-product-characteristics\"></div>\n    <div class=\"button\" data-toolbar-state=\"maximized\" data-module-to-load=\"ah-reference-library\"></div>\n    <div class=\"button notepad\"></div>\n    <div class=\"button jumpToLastSlide\"></div>\n    <div class=\"button back\" data-module=\"ap-back-navigation\"></div>\n  </div>\n  <div class=\"content\" data-module-container>\n    <!-- occupied by currently loaded module -->\n    <div data-module=\"ap-overview\" hide></div>\n    <div data-module=\"ap-custom-collections-menu\" hide></div>\n    <div data-module=\"ap-custom-collections\" hide ></div>\n    <div data-module=\"ah-media-library\" hide> </div>\n    <div data-module=\"ah-reference-library\" hide> </div>\n    <div data-module=\"ap-follow-up-mail\" hide> </div>\n    <div data-module=\"ap-frequently-asked-questions\" hide> </div>\n    <div data-module=\"ap-specific-product-characteristics\"> </div>\n  </div>\n</div>\n");
app.cache.put("modules/rainmaker_modules/ap-video-library/ap-video-library.html","<div class=\"VideoLibrary\">\n  <h1>Video Library</h1>\n  <div class=\"scroll\">\n    <ul></ul>\n  </div>\n</div>\n");
app.cache.put("slides/ContraindicationsSlide/ContraindicationsSlide.html","\n<article class=\"slide\" id=\"ContraindicationsSlide\">\n  <h1 class=\"oblique-title\">EYLEA<sup>®</sup>: CONTRAINDICATIONS<sup data-reference-id=\'korobelnik\'></sup></h1>\n  <div class=\"content-wrapper\">\n    <p>Hypersensitivity to the active substance aflibercept<br />or to any of the excipients</p>\n    <p>Active or suspected ocular or periocular infection</p>\n    <p>Active severe intraocular inflammation</p>\n    <div class=\"small_text\"> \n      <p><span>*</span>Polysorbate 20 Sodium dihydrogen phosphate. monohydrate (for pH adjustment). Disodium hydrogen<br />phosphate. heptahydrate (for pH adjustment). Sodium chloride. Sucrose . Water for injection.	</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/Discussion60Slide/Discussion60Slide.html","\n<article class=\"slide\" id=\"Discussion60Slide\">\n  <div class=\"sections\">\n    <section>\n      <div class=\"blending\"></div>\n      <div class=\"main-info\">\n        <div class=\"content\">\n          <h1>START <br>STRONG</h1>\n        </div>\n      </div>\n      <div class=\"description\">\n        <div class=\"content\">\n          <p><b>After 5 initial monthly doses</b>, on average,</p>\n          <p>patients in VIVID DME achieved <span>87</span>% of mean vision gained at Year 1<sup data-reference-id=\'korobelnik,vivid\'></sup></p>\n          <p><span>80</span>% of patients achieved at least one line improvement<sup data-reference-id=\'vividEast\'></sup></p>\n        </div>\n      </div>\n    </section>\n    <section>\n      <div class=\"blending\"></div>\n      <div class=\"main-info\">\n        <div class=\"content\">\n          <h1>STAY <br>STRONG</h1>\n        </div>\n      </div>\n      <div class=\"description\">\n        <div class=\"content\">\n          <p>In pivotal trials up to Week 148</p>\n          <p>Patients maintained Year 1 mean vision gains of <b>&gt;</b><span>10</span> <b>letters</b><sup data-reference-id=\'korobelnik,heier\'></sup></p>\n          <p>Achieving up to <span>11.7</span> <b>letters</b><sup data-reference-id=\'heier\'></sup></p>\n        </div>\n      </div>\n    </section>\n    <section>\n      <div class=\"blending\"></div>\n      <div class=\"main-info\">\n        <div class=\"content\">\n          <h1>UNIQUE <br>MOA</h1>\n        </div>\n      </div>\n      <div class=\"description\">\n        <div class=\"content\">\n          <p><b>Inhibition of both VEGF and PGF<sup data-reference-id=\'eylea\'></sup></b></p>\n          <p><b>Greater binding affinity</b> than natural receptors<sup data-reference-id=\'eylea\'></sup></p>\n        </div>\n      </div>\n    </section>\n  </div>\n</article>");
app.cache.put("slides/Discussion610Slide/Discussion610Slide.html","\n<article class=\"slide discussion\" id=\"Discussion610Slide\">\n  <h2 class=\"content-caption\">ARE YOU READY TO <mark>START STRONG</mark> AND <mark>STAY STRONG</mark>?</h2>\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>TO WHAT EXTENT DO YOU AGREE THAT<br> THE OBJECTIVE OF TREATMENT FOR YOUR<br> WORKING-AGE DME PATIENTS SHOULD BE TO<br> <mark>IMPROVE VISUAL ACUITY AND MAINTAIN IT</mark>?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/Discussion611Slide/Discussion611Slide.html","\n<article class=\"slide discussion\" id=\"Discussion611Slide\">\n  <h2 class=\"content-caption\">ARE YOU READY TO <mark>START STRONG</mark> AND <mark>STAY STRONG</mark>?</h2>\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>DO YOU THINK THAT MORE <mark>FLEXIBLE DOSING<br> SCHEDULES</mark> FOR SECOND YEAR AND BEYOND,<br> SUCH AS TREAT-AND-EXTEND, WOULD PROVIDE<br> BENEFITS FOR YOUR DME PATIENTS?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/Discussion612Slide/Discussion612Slide.html","\n<article class=\"slide discussion\" id=\"Discussion612Slide\">\n  <h2 class=\"content-caption\">ARE YOU READY TO <mark>START STRONG</mark> AND <mark>STAY STRONG</mark>?</h2>\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>WHAT BENEFITS DO YOU SEE FOR YOUR<br> DME PATIENTS IN <mark>DOSING SCHEDULES</mark>,<br> SUCH AS TREAT-AND-EXTEND, FOR THE<br> SECOND YEAR AND BEYOND?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/Discussion61Slide/Discussion61Slide.html","\n<article class=\"slide discussion\" id=\"Discussion61Slide\">\n  <h2 class=\"content-caption\">ARE YOU READY TO <mark>START STRONG</mark> AND <mark>STAY STRONG</mark>?</h2>\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>WHEN <mark>INITIATING ANTI-VEGF THERAPY</mark><br> IN A PATIENT WITH DME, <mark>HOW MANY<br> INITIAL MONTHLY DOSES</mark> WOULD<br> YOU USUALLY ADMINISTER?</p>\n    </div>\n    <div class=\"linear-slider\">\n      <div class=\"drag-line\" data-stop-swipe=\"data-stop-swipe\">\n        <div class=\"text-wrapper\">\n          <div class=\"text-item\">\n            <p>1</p>\n          </div>\n          <div class=\"text-item\">\n            <p>2</p>\n          </div>\n          <div class=\"text-item\">\n            <p>3</p>\n          </div>\n          <div class=\"text-item\">\n            <p>4</p>\n          </div>\n          <div class=\"text-item\">\n            <p>5</p>\n          </div>\n          <div class=\"text-item\">\n            <p>6 or more</p>\n          </div>\n        </div>\n        <button class=\"arrow-btn double\" data-stop-swipe=\"data-stop-swipe\"></button>\n      </div>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/Discussion62Slide/Discussion62Slide.html","\n<article class=\"slide discussion\" id=\"Discussion62Slide\">\n  <h2 class=\"content-caption\">ARE YOU READY TO <mark>START STRONG</mark> AND <mark>STAY STRONG</mark>?</h2>\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>DO YOU CONSIDER <mark>INTENSIVE INITIAL<br> MONTHLY DOSING WITH EYLEA<sup>&reg;</sup></mark><br> TO BE BENEFICIAL IN DME?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/Discussion63Slide/Discussion63Slide.html","\n<article class=\"slide discussion\" id=\"Discussion63Slide\">\n  <h2 class=\"content-caption\">ARE YOU READY TO <mark>START STRONG</mark> AND <mark>STAY STRONG</mark>?</h2>\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>HOW WOULD <mark>5 INITIAL MONTHLY<br> DOSES</mark> BENEFIT YOUR DME PATIENTS,<br> WITH RESPECT TO <mark>VISION GAIN</mark>?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/Discussion64Slide/Discussion64Slide.html","\n<article class=\"slide discussion\" id=\"Discussion64Slide\">\n  <h2 class=\"content-caption\">ARE YOU READY TO <mark>START STRONG</mark> AND <mark>STAY STRONG</mark>?</h2>\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>DO YOU AGREE THAT <mark>ANTI-VEGF TREATMENT</mark><br> PROVIDES <mark>SIGNIFICANTLY IMPROVED<br> OUTCOMES</mark> FOR YOUR DME PATIENTS<br> COMPARED TO LASER TREATMENT?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/Discussion65Slide/Discussion65Slide.html","\n<article class=\"slide discussion\" id=\"Discussion65Slide\">\n  <h2 class=\"content-caption\">ARE YOU READY TO <mark>START STRONG</mark> AND <mark>STAY STRONG</mark>?</h2>\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>WOULD YOU <mark>START STRONG WITH EYLEA<sup>&reg;</sup></mark><br> USING <mark>5 INITIAL MONTHLY DOSES</mark>?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/Discussion66Slide/Discussion66Slide.html","\n<article class=\"slide discussion\" id=\"Discussion66Slide\">\n  <h2 class=\"content-caption\">ARE YOU READY TO <mark>START STRONG</mark> AND <mark>STAY STRONG</mark>?</h2>\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>WOULD YOU CONSIDER EYLEA<sup>&reg;</sup> AS YOUR<br> <mark>TREATMENT OF CHOICE FOR RAPID<br> VISION GAINS</mark> THAT ARE MAINTAINED<br> IN YOUR DME PATIENTS?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/Discussion67Slide/Discussion67Slide.html","\n<article class=\"slide discussion\" id=\"Discussion67Slide\">\n  <h2 class=\"content-caption\">ARE YOU READY TO <mark>START STRONG</mark> AND <mark>STAY STRONG</mark>?</h2>\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>HOW WOULD YOU POSITION EYLEA<sup>&reg;</sup> IN YOUR<br> CLINICAL PRACTICE WHEN CONSIDERING<br> <mark>RAPID VISION GAINS THAT ARE MAINTAINED</mark><br> FOR YOUR WORKING-AGE DME PATIENTS?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/Discussion68Slide/Discussion68Slide.html","\n<article class=\"slide discussion\" id=\"Discussion68Slide\">\n  <h2 class=\"content-caption\">ARE YOU READY TO <mark>START STRONG</mark> AND <mark>STAY STRONG</mark>?</h2>\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>DO YOU CONSIDER <mark>INTENSIVE INITIAL<br> MONTHLY DOSING</mark> WITH EYLEA<sup>&reg;</sup><br> TO BE BENEFICIAL IN DME?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/Discussion69Slide/Discussion69Slide.html","\n<article class=\"slide discussion\" id=\"Discussion69Slide\">\n  <h2 class=\"content-caption\">ARE YOU READY TO <mark>START STRONG</mark> AND <mark>STAY STRONG</mark>?</h2>\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>DO YOU AGREE THAT THE OBJECTIVE<br> OF TREATMENT FOR YOUR WORKING-AGE<br> DME PATIENTS SHOULD BE TO <mark>MAXIMIZE<br> AND MAINTAIN VISUAL ACUITY</mark>?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/Eylea20Slide/Eylea20Slide.html","\n<article class=\"slide\" id=\"Eylea20Slide\">\n  <div class=\"wrapper\">\n    <div class=\"sections\">\n      <section>\n        <div class=\"blending\"></div>\n        <div class=\"main-info\">\n          <div class=\"content\">\n            <h1>START STRONG</h1>\n            <div class=\"sub-texts\">\n              <p>FOR GAINS FROM THE <span>FIRST</span></p>\n              <p>THROUGH THE <span>FIFTH DOSE</span></p>\n            </div>\n            <button class=\"show-description arrow-btn right\" data-name=\"DRSS_EyleaStartStayUnique_State2_Slide\"></button>\n          </div>\n        </div>\n        <div class=\"description\">\n          <div class=\"content\">\n            <div class=\"shape\"></div>\n            <p><b>EYLEA</b><sup>&reg;</sup> provides <b>increasing vision</b><br> gains from baseline,over the course<br> of initial monthly doses.<sup data-reference-id=\'heier\'></sup></p>\n          </div>\n        </div>\n      </section>\n      <section>\n        <div class=\"blending\"></div>\n        <div class=\"main-info\">\n          <div class=\"content\">\n            <h1>MAKE A<br>DIFFERENCE</h1>\n            <div class=\"sub-texts\">\n              <p>WITH <span>MAINTAINED GAINS</span></p>\n            </div>\n            <button class=\"show-description arrow-btn right\" data-name=\"DRSS_EyleaStartStayUnique_State3_Slide\"></button>\n          </div>\n        </div>\n        <div class=\"description\">\n          <div class=\"content\">\n            <div class=\"shape\"></div>\n            <p>In pivotal trials, patients maintained <br>Year 1 mean gains of <b>&gt;10 letters</b> <br>through 148 weeks<sup data-reference-id=\'heier,eylea\'></sup></p>\n          </div>\n        </div>\n      </section>\n      <section>\n        <div class=\"blending\"></div>\n        <div class=\"main-info\">\n          <div class=\"content\">\n            <h1>MULTI-TARGET MOA</h1>\n            <div class=\"sub-texts\">\n            </div>\n            <button class=\"show-description arrow-btn right\" data-name=\"DRSS_EyleaStartStayUnique_State4_Slide\"></button>\n          </div>\n        </div>\n        <div class=\"description\">\n          <div class=\"content\">\n            <div class=\"shape\"></div>\n            <p><b>EYLEA</b><sup>&reg;</sup> was designed to <b>inhibit <br>both VEGF</b> and <b>PGF</b> by binding <br>with <b>greater affinity</b> than each <br>of their <b>natural receptors</b><sup data-reference-id=\'korobelnik\'></sup></p>\n          </div>\n        </div>\n      </section>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/HomePageSlide/HomePageSlide.html","\n<article class=\"slide\" id=\"HomePageSlide\"><img src=\"slides/HomePageSlide/assets/images/bayer_logo.png\" alt=\"\"/>\n  <h1><span class=\"inner-headline\"><span>WHAT YOU</span></span><br/><span class=\"inner-headline\">START TODAY</span><br/><span class=\"inner-headline\"><span>MAKES A</span> DIFFERENCE</span><br/><span class=\"inner-headline\">TOMORROW</span><br/>\n  </h1>\n  <p class=\"note\"><sup>a</sup>EYLEA<sup>&reg;</sup> is indicated for adults for the treatment of neovascular (wet)<br> age-related macular degeneration (AMD), visual impairment due to macular edema <br> secondary to retinal vein occlusion (branch RVO or central RVO), visual impairment due to diabetic<br> macular edema (DME), and visual impairment due to myopic choroidal neovascularization (myopic CNV).<sup data-reference-id=\'eylea\'></sup><br></p>\n</article>");
app.cache.put("slides/MostFrequentlyObservedSlide/MostFrequentlyObservedSlide.html","\n<article class=\"slide\" id=\"MostFrequentlyObservedSlide\">\n  <h1>MOST FREQUENTLY OBSERVED<br />ADVERSE REACTIONS<sup data-reference-id=\'korobelnik\'></sup></h1>\n  <h2 class=\"lead-text\">(in ≥5% of patients treated with EYLEA<sup>®</sup>)</h2>\n  <div class=\"content-wrapper\">\n    <div class=\"graph-wrapper\">\n      <table class=\"table-global\">\n        <colgroup>\n          <col/>\n          <col/>\n        </colgroup>\n        <thead>\n          <tr>\n            <th>ADVERSE EVENT</th>\n            <th>FREQUENCY</th>\n          </tr>\n        </thead>\n        <tbody>\n          <tr>\n            <td>CONJUNCTIVAL HAEMORRHAGE</td>\n            <td>25%</td>\n          </tr>\n          <tr>\n            <td>REDUCED VISUALACUITY</td>\n            <td>11%</td>\n          </tr>\n          <tr>\n            <td>EYE PAIN</td>\n            <td>10%</td>\n          </tr>\n          <tr>\n            <td>CATARACT</td>\n            <td>8%</td>\n          </tr>\n          <tr>\n            <td>INCREASED INTRAOCULAR PRESSURE</td>\n            <td>8%</td>\n          </tr>\n          <tr>\n            <td>VITREOUS DETACHMENT</td>\n            <td>7%</td>\n          </tr>\n          <tr>\n            <td>VITREOUS FLOATERS</td>\n            <td>7%</td>\n          </tr>\n        </tbody>\n      </table>\n      <div class=\"oblique_item\">\n        <div class=\"oblique-wrapper\">\n          <p class=\"oblique-tile\"><span>3102 PATIENTS</span> FROM <span>EIGHT PHASE III</span> STUDIES ARE INCLUDED IN THE EYLEA<sup>®</sup><br />SAFETY POPULATION<sup data-reference-id=\'korobelnik\'></sup></p>\n        </div>\n      </div>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/MultiTargetApproachSlide/MultiTargetApproachSlide.html","\n<article class=\"slide\" id=\"MultiTargetApproachSlide\">\n  <h1>A MULTI-TARGET MoA</h1>\n  <div class=\"content-wrapper\">\n    <h2 class=\"lead-text\">EYLEA<sup>®</sup> is a fully human recombinant fusion protein that contains portions of native VEGFR-1 and VEGFR-2<sup data-reference-id=\'papadopoulos-semeraro \'></sup></h2>\n    <div class=\"graph-wrapper\">\n      <div class=\"left-side\">\n        <div class=\"image-wrapper\">\n          <div class=\"button-wrapper\">\n            <p>TIGHT BINDING</p>\n            <button class=\"icon\" data-goto-state=\"first\"></button>\n          </div>\n          <div class=\"button-wrapper\">\n            <p>MULTI-ACTION</p>\n            <button class=\"icon\" data-goto-state=\"second\"></button>\n          </div>\n          <div class=\"button-wrapper\">\n            <p>EFFICACY DURATION</p>\n            <button class=\"icon\" data-goto-state=\"third\"></button>\n          </div>\n        </div>\n      </div>\n      <div class=\"right-side\">\n        <div class=\"text-wrapper\">\n          <h2>TIGHT BINDING AFFINITY<br /> VS NATIVE RECEPTORS</h2>\n          <p>EYLEA<sup>®</sup> acts as a decoy receptor for<br /> different VEGF molecules,<sup data-reference-id=\'papadopoulos,assessment\'></sup> being<br /> specifically engineered to have very<br /> high affinity for VEGF-A and PGF.<sup data-reference-id=\'amadio,rudge\'></sup> <br />The binding affinity for these molecules<br /> is higher than that of native receptors.<sup data-reference-id=\'eylea\'></sup></p>\n        </div>\n        <div class=\"text-wrapper\">\n          <h2>MULTI-ACTION</h2>\n          <p>EYLEA<sup>®</sup> blocks all isoforms of<br />VEGF-A, VEGF-B and PIGF<sup data-reference-id=\'aflibercept\'></sup></p>\n        </div>\n        <div class=\"text-wrapper\">\n          <h2>EFFICACY DURATION</h2>\n          <p>The predicted, time-dependent<br /> intravitreal biological activity of<br /> EYLEA<sup>®</sup> is 83 days.<sup data-reference-id=\'aflibercept\'></sup></p>\n        </div>\n      </div>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RecentInsights10Slide/RecentInsights10Slide.html","\n<article class=\"slide recent-insights\" id=\"RecentInsights10Slide\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>WHAT ARE YOUR <mark>MAIN TREATMENT<br> GOALS</mark> FOR PATIENTS WITH DME?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RecentInsights110Slide/RecentInsights110Slide.html","\n<article class=\"slide recent-insights\" id=\"RecentInsights110Slide\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>WHAT DO YOU ALREADY KNOW ABOUT<br> THE <mark>CLINICAL EFFICACY OF EYLEA<sup>&reg;</sup></mark> IN DME?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RecentInsights111Slide/RecentInsights111Slide.html","\n<article class=\"slide recent-insights\" id=\"RecentInsights111Slide\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>CAN YOU TELL ME ABOUT THE <mark>LAST TREATMENT-NAÏVE DME PATIENT</mark> FOR WHOM YOU PRESCRIBED<br> <mark>ANTI-VEGF THERAPY</mark>?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RecentInsights112Slide/RecentInsights112Slide.html","\n<article class=\"slide recent-insights\" id=\"RecentInsights112Slide\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p><mark>HOW MANY DOSES</mark> WOULD YOU EXPECT<br> TO GIVE IN YEAR 3? AND IN YEAR 5?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RecentInsights11Slide/RecentInsights11Slide.html","\n<article class=\"slide recent-insights\" id=\"RecentInsights11Slide\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p><span>BEYOND IMPROVING VISUAL ACUITY,</span> <br><span>WHICH OF THE FOLLOWING IS YOUR</span> <br><mark>MAIN CONCERN WHEN TREATING DME</mark>?</p>\n    </div>\n    <div class=\"linear-slider\">\n      <div class=\"drag-line\" data-stop-swipe=\"data-stop-swipe\">\n        <div class=\"text-wrapper\">\n          <div class=\"text-item\">\n            <p>Reduce <br>edema/CRT</p>\n          </div>\n          <div class=\"text-item\">\n            <p>Reduce <br>treatment burden</p>\n          </div>\n          <div class=\"text-item\">\n            <p>Ensure <br>patient compliance</p>\n          </div>\n          <div class=\"text-item\">\n            <p>Act on <br>underlying disease</p>\n          </div>\n        </div>\n        <button class=\"arrow-btn double\" data-stop-swipe=\"data-stop-swipe\"></button>\n      </div>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RecentInsights12Slide/RecentInsights12Slide.html","\n<article class=\"slide recent-insights\" id=\"RecentInsights12Slide\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>WHAT DO YOU ALREADY KNOW ABOUT<br><mark>THE EFFICACY OF ANTI-VEGFs DOSED<br>INTENSIVELY</mark> IN THE FIRST YEAR OF<br>TREATMENT, IN PATIENTS WITH DME?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RecentInsights13Slide/RecentInsights13Slide.html","\n<article class=\"slide recent-insights\" id=\"RecentInsights13Slide\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>HOW IMPORTANT DO YOU CONSIDER<br><mark>INTENSIVE ANTI-VEGF THERAPY</mark><br>IN BEGINNING DME TREATMENT?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RecentInsights14Slide/RecentInsights14Slide.html","\n<article class=\"slide recent-insights\" id=\"RecentInsights14Slide\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p><span>WHEN INITIATING ANTI-VEGF THERAPY,</span> <br><span>IN A PATIENT WITH DME, HOW MANY </span> <br><span><mark>INITIAL MONTHLY DOSES</mark> DO YOU</span> <br><span>USUALLY ADMINISTER?</span></p>\n    </div>\n    <div class=\"linear-slider\">\n      <div class=\"drag-line\" data-stop-swipe=\"data-stop-swipe\">\n        <div class=\"text-wrapper\">\n          <div class=\"text-item\">\n            <p>1</p>\n          </div>\n          <div class=\"text-item\">\n            <p>2</p>\n          </div>\n          <div class=\"text-item\">\n            <p>3</p>\n          </div>\n          <div class=\"text-item\">\n            <p>4</p>\n          </div>\n          <div class=\"text-item\">\n            <p>5</p>\n          </div>\n          <div class=\"text-item\">\n            <p>6 or more</p>\n          </div>\n        </div>\n        <button class=\"arrow-btn double\" data-stop-swipe=\"data-stop-swipe\"></button>\n      </div>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RecentInsights15Slide/RecentInsights15Slide.html","\n<article class=\"slide recent-insights\" id=\"RecentInsights15Slide\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>WHAT IMPORTANCE DO YOU PLACE ON<br> <mark>RAPID INITIAL VISION GAINS</mark> IN YOUR<br> WORKING-AGE PATIENTS WITH DME?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RecentInsights16Slide/RecentInsights16Slide.html","\n<article class=\"slide recent-insights\" id=\"RecentInsights16Slide\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>WHAT DO YOU BELIEVE TO BE THE MAIN<br> <mark>DIFFERENCES IN THE EFFICACY</mark> OF ANTI-VEGF TREATMENTS AND LASER FOR DME?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RecentInsights17Slide/RecentInsights17Slide.html","\n<article class=\"slide recent-insights\" id=\"RecentInsights17Slide\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>WHAT DO YOU BELIEVE TO BE THE MAIN<br> <mark>DIFFERENCES IN PATHOLOGY</mark> BETWEEN<br> DME AND nAMD?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RecentInsights18Slide/RecentInsights18Slide.html","\n<article class=\"slide recent-insights\" id=\"RecentInsights18Slide\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>HOW DO YOU SEE THE <mark>DIFFERENCES<br> IN MECHANISMS OF ACTION</mark> FOR<br> DIFFERENT ANTI-VEGF TREATMENTS?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RecentInsights19Slide/RecentInsights19Slide.html","\n<article class=\"slide recent-insights\" id=\"RecentInsights19Slide\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>HOW DO YOU SEE THE <mark>DIFFERENCES<br> IN MECHANISMS OF ACTION</mark> FOR<br> DIFFERENT ANTI-VEGF TREATMENTS?</p>\n    </div>\n    <div class=\"linear-slider\">\n      <div class=\"drag-line\" data-stop-swipe=\"data-stop-swipe\">\n        <div class=\"text-wrapper\">\n          <div class=\"text-item\">\n            <p>No relevant differences</p>\n          </div>\n          <div class=\"text-item\">\n            <p>Unsure</p>\n          </div>\n          <div class=\"text-item\">\n            <p>Substantially different</p>\n          </div>\n        </div>\n        <button class=\"arrow-btn double\" data-stop-swipe=\"data-stop-swipe\"></button>\n      </div>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RI_Quiz_1/RI_Quiz_1.html","\n<article class=\"slide country_road_bg\" id=\"RI_Quiz_1\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <p>WHAT ARE YOUR <span>MAIN TREATMENT</span><br /><span>GOALS</span> FOR PATIENTS WITH DME?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RI_Quiz_10/RI_Quiz_10.html","\n<article class=\"slide country_road_bg\" id=\"RI_Quiz_10\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>HOW DO YOU SEE THE <span>DIFFERENCES</span><br /><span>IN MECHANISMS OF ACTION</span> FOR<br />DIFFERENT ANTI-VEGF TREATMENTS?</p>\n    </div>\n    <div class=\"linear-slider\">\n      <div class=\"drag-line\" data-stop-swipe=\"data-stop-swipe\">\n        <div class=\"text-wrapper\">\n          <div class=\"text-item\">\n            <p>No relevant differences</p>\n          </div>\n          <div class=\"text-item\">\n            <p>Unsure</p>\n          </div>\n          <div class=\"text-item\">\n            <p>Substantially different</p>\n          </div>\n        </div>\n        <button class=\"arrow-btn double\" data-stop-swipe=\"data-stop-swipe\"></button>\n      </div>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RI_Quiz_11/RI_Quiz_11.html","\n<article class=\"slide country_road_bg\" id=\"RI_Quiz_11\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <p>WHAT DO YOU ALREADY KNOW ABOUT<br />THE <span>CLINICAL EFFICACY OF EYLEA<sup>®</sup></span><br />IN DME?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RI_Quiz_12/RI_Quiz_12.html","\n<article class=\"slide country_road_bg\" id=\"RI_Quiz_12\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <p>CAN YOU TELL ME ABOUT THE <span>LAST</span><br /><span>TREATMENT- NAÏVE DME PATIENT</span> FOR<br />WHOM YOU PRESCRIBED <span>ANTI-VEGF</span><br /><span>THERAPY</span>?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RI_Quiz_13/RI_Quiz_13.html","\n<article class=\"slide country_road_bg\" id=\"RI_Quiz_13\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <p><span>HOW MANY DOSES</span> WOULD YOU EXPECT<br />TO GIVE IN YEAR 3? AND IN YEAR 5?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RI_Quiz_14/RI_Quiz_14.html","\n<article class=\"slide country_road_bg\" id=\"RI_Quiz_14\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>WHEN <span>INITIATING ANTI-VEGF THERAPY</span><br />IN A PATIENT WITH DME, <span>HOW MANY</span><br /><span>INITIAL MONTHLY DOSES</span> WOULD<br />YOU USUALLY ADMINISTER?</p>\n    </div>\n    <div class=\"linear-slider\">\n      <div class=\"drag-line\" data-stop-swipe=\"data-stop-swipe\">\n        <div class=\"text-wrapper\">\n          <div class=\"text-item\">\n            <p>1</p>\n          </div>\n          <div class=\"text-item\">\n            <p>2</p>\n          </div>\n          <div class=\"text-item\">\n            <p>3</p>\n          </div>\n          <div class=\"text-item\">\n            <p>4</p>\n          </div>\n          <div class=\"text-item\">\n            <p>5</p>\n          </div>\n          <div class=\"text-item\">\n            <p>6 or more</p>\n          </div>\n        </div>\n        <button class=\"arrow-btn double\" data-stop-swipe=\"data-stop-swipe\"></button>\n      </div>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RI_Quiz_15/RI_Quiz_15.html","\n<article class=\"slide country_road_bg\" id=\"RI_Quiz_15\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <p>DO YOU CONSIDER <span>INTENSIVE INITIAL</span><br /><span>MONTHLY DOSING WITH EYLEA<sup>®</sup></span><br />TO BE BENEFICIAL IN DME?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RI_Quiz_16/RI_Quiz_16.html","\n<article class=\"slide country_road_bg\" id=\"RI_Quiz_16\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <p>HOW WOULD <span>5 INITIAL MONTHLY</span><br /><span>DOSES</span> BENEFIT YOUR DME PATIENTS,<br />WITH RESPECT TO <span>VISION GAIN</span>?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RI_Quiz_17/RI_Quiz_17.html","\n<article class=\"slide country_road_bg\" id=\"RI_Quiz_17\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <p>DO YOU AGREE THAT <span>ANTI-VEGF</span><br /><span>TREATMENT</span> PROVIDES <span>SIGNIFICANTLY</span><br /><span>IMPROVED OUTCOMES</span> FOR YOUR<br />DME PATIENTS COMPARED TO<br /> LASER TREATMENT?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RI_Quiz_18/RI_Quiz_18.html","\n<article class=\"slide country_road_bg\" id=\"RI_Quiz_18\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <p>WOULD YOU <span>START STRONG WITH</span><br /><span>EYLEA<sup>®</sup></span> USING <span>5 INITIAL MONTHLY</span><br /><span>DOSES</span>?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RI_Quiz_19/RI_Quiz_19.html","\n<article class=\"slide country_road_bg\" id=\"RI_Quiz_19\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <p>WOULD YOU CONSIDER EYLEA<sup>®</sup> AS YOUR<br /><span>TREATMENT OF CHOICE FOR RAPID</span><br /><span>VISION GAINS</span> THAT ARE MAINTAINED<br />IN YOUR DME PATIENTS?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RI_Quiz_2/RI_Quiz_2.html","\n<article class=\"slide country_road_bg\" id=\"RI_Quiz_2\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>BEYOND IMPROVING VISUAL ACUITY,<br>WHICH OF THE FOLLOWING IS YOUR<br><span>MAIN CONCERN WHEN TREATING DME</span>?</p>\n    </div>\n    <div class=\"linear-slider\">\n      <div class=\"drag-line\" data-stop-swipe=\"data-stop-swipe\">\n        <div class=\"text-wrapper\">\n          <div class=\"text-item\">\n            <p>Reduce <br>edema/CRT</p>\n          </div>\n          <div class=\"text-item\">\n            <p>Reduce treatment <br> burden</p>\n          </div>\n          <div class=\"text-item\">\n            <p>Ensure patient<br> compliance</p>\n          </div>\n          <div class=\"text-item\">\n            <p>Act on underlying<br> disease</p>\n          </div>\n        </div>\n        <button class=\"arrow-btn double\" data-stop-swipe=\"data-stop-swipe\"></button>\n      </div>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RI_Quiz_20/RI_Quiz_20.html","\n<article class=\"slide country_road_bg\" id=\"RI_Quiz_20\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <p>HOW WOULD YOU POSITION EYLEA<sup>®</sup><br />IN YOUR CLINICAL PRACTICE WHEN<br />CONSIDERING <span>RAPID VISION GAINS</span><br /><span>THAT ARE MAINTAINED</span> FOR YOUR<br />WORKING-AGE DME PATIENTS?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RI_Quiz_21/RI_Quiz_21.html","\n<article class=\"slide country_road_bg\" id=\"RI_Quiz_21\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <p>DO YOU CONSIDER <span>INTENSIVE INITIAL</span><br /><span>MONTHLY DOSING</span> WITH EYLEA<sup>®</sup><br />TO BE BENEFICIAL IN DME?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RI_Quiz_22/RI_Quiz_22.html","\n<article class=\"slide country_road_bg\" id=\"RI_Quiz_22\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <p>DO YOU AGREE THAT THE OBJECTIVE<br />OF TREATMENT FOR YOUR WORKING-AGE<br /> DME PATIENTS SHOULD BE TO <span>MAXIMIZE</span><br /><span>AND MAINTAIN VISUAL ACUITY</span>?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RI_Quiz_23/RI_Quiz_23.html","\n<article class=\"slide country_road_bg\" id=\"RI_Quiz_23\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <p>TO WHAT EXTEND DO YOU AGREE THAT<br />THE OBJECTIVE OF TREATMENT FOR <br />YOUR WORKING-AGE DME PATIENTS<br />SHOULD BE TO <span>IMPROVE VISUAL</span><br /><span>ACUITY AND MAINTAIN IT</span>?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RI_Quiz_24/RI_Quiz_24.html","\n<article class=\"slide country_road_bg\" id=\"RI_Quiz_24\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <p>DO YOU THINK THAT MORE <span>FLEXIBLE</span><br /><span>DOSING SCHEDULES</span> FOR SECOND<br />YEAR AND BEYOND, SUCH AS <br />TREAT-AND-EXTEND, WOULD PROVIDE<br />BENEFITS FOR YOUR DME PATIENTS?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RI_Quiz_25/RI_Quiz_25.html","\n<article class=\"slide country_road_bg\" id=\"RI_Quiz_25\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <p>WHAT BENEFITS DO YOU SEE FOR YOUR<br />DME PATIENTS IN <span>DOSING SCHEDULES</span>,<br />SUCH AS TREAT-AND-EXTEND, FOR THE<br />SECOND YEAR AND BEYOND?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RI_Quiz_3/RI_Quiz_3.html","\n<article class=\"slide country_road_bg\" id=\"RI_Quiz_3\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <p>WHAT DO YOU ALREADY KNOW ABOUT<br /><span>THE EFFICACY OF ANTI-VEGFs DOSED</span><br /><span>INTENSIVELY</span> IN THE FIRST YEAR OF<br />TREATMENT, IN PATIENTS WITH DME?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RI_Quiz_4/RI_Quiz_4.html","\n<article class=\"slide country_road_bg\" id=\"RI_Quiz_4\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <p>HOW IMPORTANT DO YOU CONSIDER<br /><span>INTENSIVE ANTI-VEGF THERAPY</span><br />IN BEGINNING DME TREATMENT?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RI_Quiz_5/RI_Quiz_5.html","\n<article class=\"slide country_road_bg\" id=\"RI_Quiz_5\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <div class=\"shaped\"></div>\n      <p>WHEN INITIATING ANTI-VEGF THERAPY<br /> IN A PATIENT WITH DME, HOW MANY<br /><span>INITIAL MONTHLY DOSES</span> DO YOU <br />USUALLY ADMINISTER?</p>\n    </div>\n    <div class=\"linear-slider\">\n      <div class=\"drag-line\" data-stop-swipe=\"data-stop-swipe\">\n        <div class=\"text-wrapper\">\n          <div class=\"text-item\">\n            <p>1</p>\n          </div>\n          <div class=\"text-item\">\n            <p>2</p>\n          </div>\n          <div class=\"text-item\">\n            <p>3</p>\n          </div>\n          <div class=\"text-item\">\n            <p>4</p>\n          </div>\n          <div class=\"text-item\">\n            <p>5</p>\n          </div>\n          <div class=\"text-item\">\n            <p>6 or more</p>\n          </div>\n        </div>\n        <button class=\"arrow-btn double\" data-stop-swipe=\"data-stop-swipe\"></button>\n      </div>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RI_Quiz_6/RI_Quiz_6.html","\n<article class=\"slide country_road_bg\" id=\"RI_Quiz_6\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <p>WHAT IMPORTANCE DO YOU PLACE ON<br /><span>RAPID INITIAL VISION GAINS</span> IN YOUR<br />WORKING-AGE PATIENTS WITH DME?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RI_Quiz_7/RI_Quiz_7.html","\n<article class=\"slide country_road_bg\" id=\"RI_Quiz_7\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <p>WHAT DO YOU BELIEVE TO BE THE MAIN<br /><span>DIFFERENCES IN THE EFFICACY</span> OF ANTI-<br />VEGF TREATMENTS AND LASER FOR DME?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RI_Quiz_8/RI_Quiz_8.html","\n<article class=\"slide country_road_bg\" id=\"RI_Quiz_8\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <p>WHAT DO YOU BELIEVE TO BE THE MAIN<br /><span>DIFFERENCES IN PATHOLOGY</span> BETWEEN<br />DME AND wet AMD?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/RI_Quiz_9/RI_Quiz_9.html","\n<article class=\"slide country_road_bg\" id=\"RI_Quiz_9\">\n  <div class=\"paradigma-wrapper\">\n    <div class=\"paradigma\">\n      <p>HOW DO YOU SEE THE <span>DIFFERENCES</span><br /><span>IN MECHANISMS OF ACTION</span> FOR<br />DIFFERENT ANTI-VEGF TREATMENTS?</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/StartStrong30A1PopupSlide/StartStrong30A1PopupSlide.html","\n<article class=\"slide\" id=\"StartStrong30A1PopupSlide\">\n  <!--p.sub-title !{sub_title}-->\n  <p class=\"lead-paragraph\"></p>\n  <h1><mark>EYLEA<sup>&reg;</sup></mark>: MEAN CHANGE IN CST THROUGH WEEK 52<sup data-reference-id=\'korobelnik\'></sup></h1>\n  <figure class=\"figure-fragment graph-fragment\">\n    <figcaption>Mean change in CRT through Week 52<sup data-reference-id=\'korobelnik\'></sup><span class=\"pointer-label\"></span></figcaption>\n    <div class=\"content-wrapper\">\n      <figure class=\"graph\">\n        <div class=\"g-scale g-scale-v\">\n          <ul>\n            <li><span>0</span></li>\n            <li><span>&minus;50</span></li>\n            <li><span>&minus;100</span></li>\n            <li><span>&minus;150</span></li>\n            <li><span>&minus;200</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>μm</span><br/>\n          </p>\n        </div>\n        <div class=\"g-scale g-scale-h\">\n          <ul>\n            <li><span>0</span></li>\n            <li><span>4</span></li>\n            <li><span>8</span></li>\n            <li><span>12</span></li>\n            <li><span>16</span></li>\n            <li><span>20</span></li>\n            <li><span>24</span></li>\n            <li><span>28</span></li>\n            <li><span>32</span></li>\n            <li><span>36</span></li>\n            <li><span>40</span></li>\n            <li><span>44</span></li>\n            <li><span>48</span></li>\n            <li><span>52</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>Week</span><br/>\n          </p>\n        </div>\n        <div class=\"g-content\">\n          <div class=\"graph-blocks\">\n          </div>\n          <div class=\"g-lines\">\n            <div class=\"g-line\">\n              <div class=\"g-line-info\">\n                <div class=\"skew element-blue g-line-value\">\n                  <p>&minus;192.4</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"g-line\">\n              <div class=\"g-line-info\">\n                <div class=\"skew element-gray small-value g-line-value\">\n                  <p>&minus;66.2</p>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </figure>\n    </div>\n    <ul class=\"g-legend\">\n      <li class=\"legend-blue\"><strong>EYLEA<sup>&reg;</sup> 2 mg</strong> administered every month (4 weeks) for the first 5 months, followed by once every 2 months (8 weeks); <strong>n=135.</strong><sup data-reference-id=\'korobelnik\'></sup></li>\n      <li class=\"legend-gray\"><strong>Laser</strong> photocoagulation administered as needed, no more often than every 12 weeks; <strong>n=132.</strong><sup data-reference-id=\'korobelnik\'></sup></li>\n    </ul>\n  </figure>\n</article>");
app.cache.put("slides/StartStrong30B1PopupSlide/StartStrong30B1PopupSlide.html","\n<article class=\"slide\" id=\"StartStrong30B1PopupSlide\">\n  <h1>STUDY DESIGN<sup data-reference-id=\'korobelnik\'></sup></h1>\n  <div class=\"schema-wrapper\">\n    <div class=\"diagram-header\">\n      <div class=\"pointer-label\"></div>\n      <p class=\"text\">RANDOMIZED, MULTICENTER, DOUBLE-MASKED TRIALS IN PATIENTS WITH CLINICALLY<br>SIGNIFICANT DME WITH CENTRAL INVOLVEMENT AND BCVA 20/40 TO 20/320</p>\n    </div>\n    <div class=\"schema\">\n      <div class=\"column\">\n        <div class=\"diagram-billet-block\">\n          <div class=\"diagram-billet\">\n          </div>\n        </div>\n        <p class=\"diagram-billet-text\">(n=406)</p>\n      </div>\n      <div class=\"column\">\n        <div class=\"row\">\n          <div class=\"row-item\">\n            <p class=\"row-item-title\">Patients randomized<br><b>1:1:1</b></p>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"row-item\">\n            <p class=\"row-item-title\">EYLEA<sup>&reg;</sup><br>2 mg q8 wks<sup data-notes=\'monthly_doses\'></sup></p>\n          </div>\n          <div class=\"row-item\">\n            <p class=\"row-item-title\">EYLEA<sup>&reg;</sup><br>2 mg q4 wks<sup data-notes=\'monthly_doses\'></sup></p>\n          </div>\n          <div class=\"row-item\">\n            <p class=\"row-item-title\">Laser<br>photocoagulation<sup data-notes=\'grid_laser\'></sup></p>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"row-item\">\n            <p class=\"row-item-title\"><b>Primary outcome:</b></p>\n            <p class=\"row-item-content\">Mean change in BCVA</p>\n          </div>\n          <div class=\"row-item\">\n            <p class=\"row-item-title\">Primary endpoint:</p>\n            <p class=\"row-item-content\"><b>Week 52<sup data-notes=\'criteria_treatment\'></sup></b></p>\n          </div>\n          <div class=\"row-item\">\n            <p class=\"row-item-title\"><b>Key secondary outcomes:</b></p>\n            <ul class=\"row-item-list\">\n              <li>Percentage of patients gaining 3 lines</li>\n              <li>Change in Diabetic Retinopathy Severity Scale</li>\n            </ul>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"row-item\">\n            <p class=\"row-item-title\"><b>Week 100<sup data-notes=\'eylea_laser_group\'></sup></b></p>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"row-item\">\n            <p class=\"row-item-title\"><b>Week 148</b></p>\n          </div>\n        </div>\n      </div>\n      <div class=\"column\">\n        <div class=\"diagram-billet-block\">\n          <div class=\"diagram-billet\">\n          </div>\n        </div>\n        <p class=\"diagram-billet-text\">(n=466)</p>\n      </div>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/StartStrong30B2PopupSlide/StartStrong30B2PopupSlide.html","\n<article class=\"slide\" id=\"StartStrong30B2PopupSlide\">\n  <h1>DOSING AND MONITORING SCHEDULE<sup data-reference-id=\'korobelnik\'></sup></h1>\n  <figure class=\"figure-fragment\">\n    <figcaption>Dosing and monitoring in VIVID DME<span class=\"pointer-label\"></span></figcaption>\n    <div class=\"content-wrapper\">\n      <table class=\"schedule\">\n        <colgroup>\n          <col/>\n          <col/>\n          <col/>\n          <col/>\n          <col/>\n          <col/>\n          <col/>\n          <col/>\n          <col/>\n          <col/>\n          <col/>\n          <col/>\n          <col/>\n          <col/>\n          <col/>\n          <col/>\n          <col/>\n          <col/>\n        </colgroup>\n        <thead>\n          <tr>\n            <th></th>\n            <th colspan=\"9\">Year 1</th>\n            <th colspan=\"5\">Year 2</th>\n            <th colspan=\"4\">Year 3</th>\n          </tr>\n        </thead>\n        <tbody>\n          <tr>\n            <td>Week</td>\n            <td>0</td>\n            <td>4</td>\n            <td>8</td>\n            <td>12</td>\n            <td>16</td>\n            <td>20</td>\n            <td>24</td>\n            <td>28–48</td>\n            <td>52</td>\n            <td>56</td>\n            <td>60</td>\n            <td>64–92</td>\n            <td>96</td>\n            <td>100</td>\n            <td>104</td>\n            <td>108–144</td>\n            <td>148</td>\n          </tr>\n          <tr>\n            <td>Aflibercept<sup>&reg;</sup>&ensp;2q4</td>\n            <td class=\"circle1\"></td>\n            <td class=\"circle1\"></td>\n            <td class=\"circle1\"></td>\n            <td class=\"circle1\"></td>\n            <td class=\"circle1\"></td>\n            <td class=\"circle1\"></td>\n            <td class=\"circle1\"></td>\n            <td class=\"circle1\"></td>\n            <td class=\"circle1\"></td>\n            <td class=\"circle1\"></td>\n            <td class=\"circle1\"></td>\n            <td class=\"circle1\"></td>\n            <td class=\"circle1\"></td>\n            <td class=\"circle1\"></td>\n            <td class=\"circle1\"></td>\n            <td class=\"circle1\"></td>\n            <td></td>\n          </tr>\n          <tr>\n            <td>EYLEA<sup>&reg;</sup>&ensp;2q8</td>\n            <td class=\"circle2\"></td>\n            <td class=\"circle2\"></td>\n            <td class=\"circle2\"></td>\n            <td class=\"circle2\"></td>\n            <td class=\"circle2\"></td>\n            <td class=\"circle3\"></td>\n            <td class=\"circle2\"></td>\n            <td class=\"circle3\"></td>\n            <td class=\"circle3\"></td>\n            <td class=\"circle2\"></td>\n            <td class=\"circle3\"></td>\n            <td class=\"circle2\"></td>\n            <td class=\"circle2\"></td>\n            <td class=\"circle3\"></td>\n            <td class=\"circle2\"></td>\n            <td class=\"circle3\"></td>\n            <td></td>\n          </tr>\n          <tr>\n            <td>Laser<sup data-notes=\'patients_laser_group_1\'></sup></td>\n            <td class=\"circle4\"></td>\n            <td></td>\n            <td></td>\n            <td class=\"circle4\"></td>\n            <td class=\"circle4\"></td>\n            <td class=\"circle4\"></td>\n            <td class=\"circle4\"></td>\n            <td class=\"circle4\"></td>\n            <td class=\"circle4\"></td>\n            <td class=\"circle4\"></td>\n            <td class=\"circle4\"></td>\n            <td class=\"circle4\"></td>\n            <td class=\"circle4\"></td>\n            <td class=\"circle4\"></td>\n            <td class=\"circle4\"></td>\n            <td class=\"circle4\"></td>\n            <td></td>\n          </tr>\n        </tbody>\n        <tfoot>\n          <tr>\n            <td></td>\n            <td></td>\n            <td></td>\n            <td></td>\n            <td></td>\n            <td></td>\n            <td></td>\n            <td></td>\n            <td></td>\n            <td class=\"arrow\">Primary endpoint</td>\n            <td></td>\n            <td></td>\n            <td></td>\n            <td></td>\n            <td class=\"arrow\">Year 2<br> analysis</td>\n            <td></td>\n            <td></td>\n            <td class=\"arrow\">Year 3<br> analysis</td>\n          </tr>\n        </tfoot>\n      </table>\n      <div class=\"schedule-legend\">\n        <p class=\"circle1\"><b>Aflibercept 2 mg every 4 weeks (2q4)</b></p>\n        <p class=\"circle2\"><b>EYLEA<sup>&reg;</sup> 2 mg every 2 months (2q8)</b> following 5 initial monthly doses</p>\n        <p class=\"circle3\"><b>Sham injection</b></p>\n        <p class=\"circle4\"><b>Laser</b> (no more often than every 12 weeks)</p>\n        <p class=\"circle5\"><b>Laser</b> (as needed based on prespecified criteria)</p>\n      </div>\n    </div>\n  </figure>\n</article>");
app.cache.put("slides/StartStrong30B3PopupSlide/StartStrong30B3PopupSlide.html","\n<article class=\"slide\" id=\"StartStrong30B3PopupSlide\">\n  <h1>BASELINE CHARACTERISTICS<sup data-reference-id=\'korobelnik\'></sup></h1>\n  <table class=\"table-global\">\n    <colgroup>\n      <col/>\n      <col/>\n      <col/>\n      <col/>\n      <col/>\n      <col/>\n      <col/>\n    </colgroup>\n    <thead>\n      <tr>\n        <th></th>\n        <th class=\"vivid\" colspan=\"3\"></th>\n        <th class=\"vista\" colspan=\"3\"></th>\n      </tr>\n      <tr>\n        <th></th>\n        <th class=\"blue\">EYLEA<sup>&reg;</sup><br> 2q8</th>\n        <th class=\"white\">Aflibercept<sup>&reg;</sup> 2q4</th>\n        <th class=\"gray\">Laser</th>\n        <th class=\"blue\">EYLEA<sup>&reg;</sup><br> 2q8</th>\n        <th class=\"white\">Aflibercept<sup>&reg;</sup> 2q4</th>\n        <th class=\"gray\">Laser</th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr>\n        <th>n</th>\n        <td>135</td>\n        <td>136</td>\n        <td>132</td>\n        <td>151</td>\n        <td>154</td>\n        <td>154</td>\n      </tr>\n      <tr>\n        <th>Age, years (SD)</th>\n        <td>64.2 (7.8)</td>\n        <td>62.6 (8.6)</td>\n        <td>63.9 (8.6)</td>\n        <td>63.1 (9.4)</td>\n        <td>62.0 (11.2)</td>\n        <td>61.7 (8.7)</td>\n      </tr>\n      <tr>\n        <th>Female, n (%)</th>\n        <td>47 (34.8)</td>\n        <td>53 (39.0)</td>\n        <td>54 (40.9)</td>\n        <td>73 (48.3)</td>\n        <td>67 (43.5)</td>\n        <td>69 (44.8)</td>\n      </tr>\n      <tr>\n        <th>Race, n (%)</th>\n        <td></td>\n        <td></td>\n        <td></td>\n        <td></td>\n        <td></td>\n        <td></td>\n      </tr>\n      <tr>\n        <th>White</th>\n        <td>106 (78.5)</td>\n        <td>109 (80.1)</td>\n        <td>106 (80.3)</td>\n        <td>125 (82.8)</td>\n        <td>128 (83.1)</td>\n        <td>131 (85.1)</td>\n      </tr>\n      <tr>\n        <th>Black</th>\n        <td>1 (0.7)</td>\n        <td>0</td>\n        <td>1 (0.8)</td>\n        <td>19 (12.6)</td>\n        <td>16 (10.4)</td>\n        <td>16 (10.4)</td>\n      </tr>\n      <tr>\n        <th>Asian</th>\n        <td>27 (20.0)</td>\n        <td>27 (19.9)</td>\n        <td>25 (18.9)</td>\n        <td>2 (1.3)</td>\n        <td>5 (3.2)</td>\n        <td>3 (1.9)</td>\n      </tr>\n      <tr>\n        <th>Other</th>\n        <td>1 (0.7)</td>\n        <td>0</td>\n        <td>0</td>\n        <td>5 (3.3)</td>\n        <td>5 (3.2)</td>\n        <td>4 (2.6)</td>\n      </tr>\n      <tr>\n        <th>Mean HbA<sub>1c</sub>, % (SD)</th>\n        <td>7.7 (1.4)</td>\n        <td>7.8 (1.5)</td>\n        <td>7.7 (1.3)</td>\n        <td>7.9 (1.6)</td>\n        <td>7.9 (1.6)</td>\n        <td>7.6 (1.7)</td>\n      </tr>\n      <tr>\n        <th>Patients with HbA<sub>1c</sub> &gt;8%, n (%)</th>\n        <td>44 (32.6)</td>\n        <td>55 (40.4)</td>\n        <td>42 (31.8)</td>\n        <td>57 (37.7)</td>\n        <td>57 (37.0)</td>\n        <td>45 (29.2)</td>\n      </tr>\n      <tr>\n        <th>Duration of diabetes, years (SD)</th>\n        <td>14.1 (8.9)</td>\n        <td>14.3 (9.2)</td>\n        <td>14.5 (9.8)</td>\n        <td>17.6 (11.5)</td>\n        <td>16.5 (9.9)</td>\n        <td>17.2 (9.5)</td>\n      </tr>\n    </tbody>\n  </table>\n</article>");
app.cache.put("slides/StartStrong30B4PopupSlide/StartStrong30B4PopupSlide.html","\n<article class=\"slide\" id=\"StartStrong30B4PopupSlide\">\n  <h1>BASELINE DISEASE CHARACTERISTICS<sup data-reference-id=\'korobelnik\'></sup></h1>\n  <table class=\"table-global\">\n    <colgroup>\n      <col/>\n      <col/>\n      <col/>\n      <col/>\n      <col/>\n      <col/>\n      <col/>\n    </colgroup>\n    <thead>\n      <tr>\n        <th></th>\n        <th class=\"vivid\" colspan=\"3\"></th>\n        <th class=\"vista\" colspan=\"3\"></th>\n      </tr>\n      <tr>\n        <th></th>\n        <th class=\"blue\">EYLEA<sup>&reg;</sup><br> 2q8</th>\n        <th class=\"white\">Aflibercept<sup>&reg;</sup> 2q4</th>\n        <th class=\"light-gray\">Laser</th>\n        <th class=\"blue\">EYLEA<sup>&reg;</sup><br> 2q8</th>\n        <th class=\"white\">Aflibercept<sup>&reg;</sup> 2q4</th>\n        <th class=\"light-gray\">Laser</th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr>\n        <th>n</th>\n        <td>135</td>\n        <td>136</td>\n        <td>132</td>\n        <td>151</td>\n        <td>154</td>\n        <td>v</td>\n      </tr>\n      <tr>\n        <th>BCVA, ETDRS letters (SD)</th>\n        <td>58.8 (11.2)</td>\n        <td>60.8 (10.7)</td>\n        <td>60.8 (10.6)</td>\n        <td>59.4 (10.9)</td>\n        <td>58.9 (10.8)</td>\n        <td>59.7 (10.9)</td>\n      </tr>\n      <tr>\n        <th>CRT, &micro;m (SD)</th>\n        <td>518 (147)</td>\n        <td>502 (144)</td>\n        <td>540 (152)</td>\n        <td>479 (154)</td>\n        <td>485 (157)</td>\n        <td>483 (153)</td>\n      </tr>\n      <tr>\n        <th>NEI-VFQ score</th>\n        <td></td>\n        <td></td>\n        <td></td>\n        <td></td>\n        <td></td>\n        <td></td>\n      </tr>\n      <tr>\n        <th>Total, points (SD)</th>\n        <td>71.2 (17.8)</td>\n        <td>77.3 (16.2)</td>\n        <td>77.5 (15.2)</td>\n        <td>70.5 (17.1)</td>\n        <td>69.5 (19.9)</td>\n        <td>68.7 (18.1)</td>\n      </tr>\n      <tr>\n        <th>Distance, points (SD)</th>\n        <td>67.8 (22.9)</td>\n        <td>76.7 (21.8)</td>\n        <td>77.0 (20.9)</td>\n        <td>66.8 (22.5)</td>\n        <td>65.3 (23.5)</td>\n        <td>63.7 (23.3)</td>\n      </tr>\n      <tr>\n        <th>Near, points (SD)</th>\n        <td>60.8 (23.5)</td>\n        <td>68.0 (22.9)</td>\n        <td>67.4 (22.2)</td>\n        <td>58.1 (22.9)</td>\n        <td>60.1 (23.9)</td>\n        <td>56.6 (23.1)</td>\n      </tr>\n      <tr>\n        <th>Prior anti-VEGF therapy, n (%)</th>\n        <td>15 (11.1)</td>\n        <td>8 (5.9)</td>\n        <td>13 (9.8)</td>\n        <td>68 (45.0)</td>\n        <td>66 (42.9)</td>\n        <td>63 (40.9)</td>\n      </tr>\n    </tbody>\n  </table>\n</article>");
app.cache.put("slides/StartStrong30B5PopupSlide/StartStrong30B5PopupSlide.html","\n<article class=\"slide\" id=\"StartStrong30B5PopupSlide\">\n  <h1>Key Eligibility criteria<sup data-reference-id=\'korobelnik\'></sup></h1>\n  <dl class=\"chapter-criteria-list\">\n    <dt><span class=\"term-label\"></span>Inclusion criteria</dt>\n    <dd>Adults &ge;18 years of age with type 1 or type 2 diabetes mellitus</dd>\n    <dd>Decrease in vision determined to be primarily the result of DME in the study eye</dd>\n    <dd>BCVA ETDRS letter score 73 to 24 (20/40 to 20/320) in the study eye</dd>\n    <dt><span class=\"term-label\"></span>Exclusion criteria</dt>\n    <dd>Laser photocoagulation (panretinal or macular) in the study eye within 90 days of study day 1</dd>\n    <dd>Previous use of intraocular or periocular corticosteroids in the study eye within 120 days of study day 1</dd>\n    <dd>Previous treatment with anti-VEGF drugs in the study eye (pegaptanib sodium, bevacizumab, ranibizumab, etc.) within 90 days of study day 1</dd>\n    <dd>Active proliferative diabetic retinopathy in the study eye</dd>\n    <dd>Uncontrolled diabetes mellitus</dd>\n    <dd>Only 1 functional eye, even if that eye is otherwise eligible for the study</dd>\n  </dl>\n</article>");
app.cache.put("slides/StartStrong30C1PopupSlide/StartStrong30C1PopupSlide.html","\n<article class=\"slide\" id=\"StartStrong30C1PopupSlide\">\n  <p class=\"lead-paragraph\">IN VISTA DME…</p>\n  <h1><mark>EYLEA<sup>&reg;</sup> 2 mg</mark> PROVIDES STRONG, INCREASING VISION GAINS OVER<br> THE COURSE OF 5 INITIAL MONTHLY DOSES<sup data-reference-id=\'korobelnik\'></sup></h1>\n  <figure class=\"figure-fragment graph-fragment\">\n    <figcaption>Mean change in BCVA though Week 52<sup data-reference-id=\'korobelnik\'></sup><span class=\"pointer-label\"></span></figcaption>\n    <div class=\"content-wrapper\">\n      <figure class=\"graph\">\n        <div class=\"g-scale g-scale-v\">\n          <ul>\n            <li><span>0</span></li>\n            <li><span>2</span></li>\n            <li><span>4</span></li>\n            <li><span>6</span></li>\n            <li><span>8</span></li>\n            <li><span>10</span></li>\n            <li><span>12</span></li>\n            <li><span>14</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>Mean change in BCVA</span><br/><span>ETDRS letters</span><br/>\n          </p>\n        </div>\n        <div class=\"g-scale g-scale-h\">\n          <ul>\n            <li><span>0</span></li>\n            <li><span>4</span></li>\n            <li><span>8</span></li>\n            <li><span>12</span></li>\n            <li><span>16</span></li>\n            <li><span>20</span></li>\n            <li><span>24</span></li>\n            <li><span>28</span></li>\n            <li><span>32</span></li>\n            <li><span>36</span></li>\n            <li><span>40</span></li>\n            <li><span>44</span></li>\n            <li><span>48</span></li>\n            <li><span>52</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>Week</span><br/>\n          </p>\n        </div>\n        <div class=\"g-content\">\n          <div class=\"graph-blocks\">\n            <div class=\"block\">\n              <p>5 initial monthly doses</p>\n            </div>\n            <div class=\"block\">\n              <p>2q8 extension</p>\n            </div>\n          </div>\n          <div class=\"g-lines\">\n            <div class=\"g-line\">\n              <div class=\"g-line-info\">\n                <div class=\"skew element-blue g-line-value\">\n                  <p>+10.7</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"g-line\">\n              <div class=\"g-line-info\">\n                <div class=\"skew element-gray small-value g-line-value\">\n                  <p>+0.2</p>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </figure>\n    </div>\n    <ul class=\"g-legend\">\n      <li class=\"legend-blue\"><strong>EYLEA<sup>&reg;</sup> 2 mg</strong> administered every month (4 weeks) for the first 5 months, followed by once every 2 months (8 weeks); <strong>n=152.</strong><sup data-reference-id=\'korobelnik\'></sup></li>\n      <li class=\"legend-gray\"><strong>Laser</strong> photocoagulation administered as needed, no more often than every 12 weeks; <strong>n=154.</strong><sup data-reference-id=\'korobelnik\'></sup></li>\n    </ul>\n  </figure>\n</article>");
app.cache.put("slides/StartStrong30C2PopupSlide/StartStrong30C2PopupSlide.html","\n<articlee class=\"slide\" id=\"StartStrong30C2PopupSlide\">\n  <p class=\"lead-paragraph\">IN VISTA DME…</p>\n  <h1>AFTER 5 INITIAL MONTHLY DOSES OF <mark>EYLEA<sup>&reg;</sup> 2 mg</mark>, ON AVERAGE,<br> PATIENTS HAD ACHIEVED 93% OF MEAN VISION GAINED AT YEAR 1<sup data-reference-id=\'korobelnik,vivid\'></sup></h1>\n  <figure class=\"figure-fragment graph-fragment\">\n    <figcaption>Mean change in BCVA though Week 52<sup data-reference-id=\'korobelnik,vivid\'></sup><span class=\"pointer-label\"></span></figcaption>\n    <div class=\"content-wrapper\">\n      <figure class=\"graph\">\n        <div class=\"g-scale g-scale-v\">\n          <ul>\n            <li><span>0</span></li>\n            <li><span>2</span></li>\n            <li><span>4</span></li>\n            <li><span>6</span></li>\n            <li><span>8</span></li>\n            <li><span>10</span></li>\n            <li><span>12</span></li>\n            <li><span>14</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>Mean change in BCVA</span><br/><span>ETDRS letters</span><br/>\n          </p>\n        </div>\n        <div class=\"g-scale g-scale-h\">\n          <ul>\n            <li><span>0</span></li>\n            <li><span>4</span></li>\n            <li><span>8</span></li>\n            <li><span>12</span></li>\n            <li><span>16</span></li>\n            <li><span>20</span></li>\n            <li><span>24</span></li>\n            <li><span>28</span></li>\n            <li><span>32</span></li>\n            <li><span>36</span></li>\n            <li><span>40</span></li>\n            <li><span>44</span></li>\n            <li><span>48</span></li>\n            <li><span>52</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>Week</span><br/>\n          </p>\n        </div>\n        <div class=\"g-content\">\n          <div class=\"graph-blocks\">\n            <div class=\"block\">\n              <p>5 initial monthly doses</p>\n            </div>\n            <div class=\"block\">\n              <p>2q8 extension</p>\n            </div>\n          </div>\n          <div class=\"g-lines\">\n            <div class=\"g-line\">\n              <div class=\"g-line-info\">\n                <div class=\"skew element-blue g-line-value\">\n                  <p>+10.7</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"g-line\">\n              <div class=\"g-line-info\">\n                <div class=\"skew element-gray small-value g-line-value\">\n                  <p>+0.2</p>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class=\"point-values\">\n            <p>62<span>%</span></p>\n            <p>70<span>%</span></p>\n            <p>80<span>%</span></p>\n            <p>84<span>%</span></p>\n            <p>93<span>%</span></p>\n            <div class=\"skew point-description\">\n              <p>+9.9</p>\n              <div class=\"skew point-additional\">\n                <p>FIFTH DOSE EFFICACY</p>\n              </div>\n            </div>\n          </div>\n        </div>\n      </figure>\n    </div>\n    <ul class=\"g-legend\">\n      <li class=\"legend-blue\"><strong>EYLEA<sup>&reg;</sup> 2 mg</strong> administered every month (4 weeks) for the first 5 months, followed by once every 2 months (8 weeks); <strong>n=152.</strong><sup data-reference-id=\'korobelnik\'></sup></li>\n      <li class=\"legend-gray\"><strong>Laser</strong> photocoagulation administered as needed, no more often than every 12 weeks; <strong>n=154.</strong><sup data-reference-id=\'korobelnik\'></sup></li>\n    </ul>\n  </figure>\n</articlee>");
app.cache.put("slides/StartStrong30C3PopupSlide/StartStrong30C3PopupSlide.html","\n<article class=\"slide\" id=\"StartStrong30C3PopupSlide\">\n  <p class=\"lead-paragraph\">IN VISTA DME…</p>\n  <h1><mark>EYLEA<sup>&reg;</sup></mark>: RAPID AND ROBUST MEAN REDUCTIONS IN CRT VS LASER<sup data-reference-id=\'korobelnik\'></sup></h1>\n  <figure class=\"figure-fragment graph-fragment top-scale\">\n    <figcaption> <span class=\"pointer-label\"></span></figcaption>\n    <div class=\"content-wrapper\">\n      <figure class=\"graph\">\n        <div class=\"g-scale g-scale-v\">\n          <ul>\n            <li><span>-200</span></li>\n            <li><span>-150</span></li>\n            <li><span>-100</span></li>\n            <li><span>-50</span></li>\n            <li><span>0</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>μm</span><br/>\n          </p>\n        </div>\n        <div class=\"g-scale g-scale-h\">\n          <ul>\n            <li><span>0</span></li>\n            <li><span>4</span></li>\n            <li><span>8</span></li>\n            <li><span>12</span></li>\n            <li><span>16</span></li>\n            <li><span>20</span></li>\n            <li><span>24</span></li>\n            <li><span>28</span></li>\n            <li><span>32</span></li>\n            <li><span>36</span></li>\n            <li><span>40</span></li>\n            <li><span>44</span></li>\n            <li><span>48</span></li>\n            <li><span>52</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>Week</span><br/>\n          </p>\n        </div>\n        <div class=\"g-content\">\n          <div class=\"graph-blocks\">\n          </div>\n          <div class=\"g-lines\">\n            <div class=\"g-line\">\n              <div class=\"g-line-info\">\n                <div class=\"skew element-gray small-value g-line-value\">\n                  <p>−73.3</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"g-line\">\n              <div class=\"g-line-info\">\n                <div class=\"skew element-blue g-line-value\">\n                  <p>−183.1</p>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </figure>\n    </div>\n    <ul class=\"g-legend\">\n      <li class=\"legend-blue\"><strong>EYLEA<sup>&reg;</sup> 2 mg</strong> administered every month (4 weeks) for the first 5 months, followed by once every 2 months (8 weeks); <strong>n=152.</strong><sup data-reference-id=\'korobelnik\'></sup></li>\n      <li class=\"legend-gray\"><strong>Laser</strong> photocoagulation administered as needed, no more often than every 12 weeks; <strong>n=154.</strong><sup data-reference-id=\'korobelnik\'></sup></li>\n    </ul>\n  </figure>\n</article>");
app.cache.put("slides/StartStrong30Slide/StartStrong30Slide.html","\n<article class=\"slide\" id=\"StartStrong30Slide\">\n  <h1>EYLEA<sup>&reg;</sup> PROVIDES INCREASING VISION GAINS<br> OVER THE COURSE OF 5 INITIAL MONTHLY DOSES</h1>\n  <h2></h2>\n  <div class=\"content-wrapper\">\n    <!--h3 !{headline3.main}-->\n    <figure class=\"figure-fragment graph-fragment\">\n      <figcaption>Mean change in BCVA through Week 52<sup data-reference-id=\'korobelnik\'></sup><span class=\"pointer-label\"></span></figcaption>\n      <div class=\"content-wrapper\">\n        <figure class=\"graph\">\n          <div class=\"g-scale g-scale-v\">\n            <ul>\n              <li><span>0</span></li>\n              <li><span>2</span></li>\n              <li><span>4</span></li>\n              <li><span>6</span></li>\n              <li><span>8</span></li>\n              <li><span>10</span></li>\n              <li><span>12</span></li>\n              <li><span>14</span></li>\n            </ul>\n            <p class=\"g-scale-label\"><span>Mean change in BCVA</span><br/><span>ETDRS letters</span><br/>\n            </p>\n          </div>\n          <div class=\"g-scale g-scale-h\">\n            <ul>\n              <li><span>0</span></li>\n              <li><span>4</span></li>\n              <li><span>8</span></li>\n              <li><span>12</span></li>\n              <li><span>16</span></li>\n              <li><span>20</span></li>\n              <li><span>52</span></li>\n            </ul>\n            <p class=\"g-scale-label\"><span>Week</span><br/>\n            </p>\n          </div>\n          <div class=\"g-content\">\n            <div class=\"graph-blocks\">\n              <div class=\"block\">\n                <p>5 initial monthly doses</p>\n              </div>\n              <div class=\"block\">\n                <p>2q8 extension</p>\n              </div>\n            </div>\n            <div class=\"g-lines\">\n              <div class=\"g-line\">\n                <div class=\"g-line-info\">\n                  <div class=\"skew element-blue g-line-value\">\n                    <p>+9.3</p>\n                    <div class=\"skew g-line-additional\">\n                      <p>FIFTH DOSE<br> EFFICACY</p>\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"g-line\">\n                <div class=\"g-line-info\">\n                  <div class=\"skew element-gray g-line-value\">\n                    <p>+1.8</p>\n                  </div>\n                </div>\n              </div>\n              <div class=\"g-line\">\n                <div class=\"g-line-info\">\n                  <div class=\"skew element-blue g-line-value\">\n                    <p>+10.7</p>\n                  </div>\n                </div>\n              </div>\n              <div class=\"g-line\">\n                <div class=\"g-line-info\">\n                  <div class=\"skew element-gray g-line-value\">\n                    <p>+1.2</p>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"wrap-interactive-buttons\">\n              <button class=\"arrow-btn plus state2\"></button>\n              <button class=\"arrow-btn plus state3\"></button>\n              <button class=\"arrow-btn left gold\"></button>\n              <div class=\"graph-text-block\">\n                <p class=\"graph-text-data\"></p>\n              </div>\n            </div>\n            <div class=\"point-values\">\n              <p>50<span>%</span></p>\n              <p>64<span>%</span></p>\n              <p>74<span>%</span></p>\n              <p>78<span>%</span></p>\n              <p>87<span>%</span></p>\n              <div class=\"skew point-description\">\n                <p>+9.3</p>\n                <div class=\"skew point-additional\">\n                  <p>FIFTH DOSE EFFICACY</p>\n                </div>\n              </div>\n            </div>\n          </div>\n        </figure>\n      </div>\n      <ul class=\"g-legend\">\n        <li class=\"legend-blue\"><strong>EYLEA<sup>&reg;</sup> 2 mg</strong> administered every month (4 weeks) for the first 5 months, followed by once every 2 months (8 weeks); <strong>n=135</strong>.<sup data-reference-id=\'korobelnik\'></sup>&emps;BCVA= Best Corrective Visual Acuity</li>\n        <li class=\"legend-gray\"><strong>Laser</strong> photocoagulation administered as needed, no more often than every 12 weeks; <strong>n=132</strong>.<sup data-reference-id=\'korobelnik\'></sup></li>\n        <li>After 5 initial monthly doses, on average, patients in VIVID achieved 87% of mean vision gained at year 1. (9.3/10.7=86.91%)</li>\n      </ul>\n    </figure>\n    <div class=\"wrap-buttons\">\n      <div class=\"wrap-open-popup\">\n        <p>Study design</p>\n        <button class=\"arrow-btn open-popup\" data-viewer=\"slideshow\" slide=\"StartStrong30B1PopupSlide\" href=\"start_strong_30b_inline\"></button>\n      </div>\n      <div class=\"wrap-open-popup\">\n        <p>Additional endpoints</p>\n        <button class=\"arrow-btn open-popup\" data-popup=\"StartStrong30A1PopupSlide\"></button>\n      </div>\n      <div class=\"wrap-open-popup\">\n        <p>VISTA DME</p>\n        <button class=\"arrow-btn open-popup\" data-viewer=\"slideshow\" slide=\"StartStrong30C1PopupSlide\" href=\"start_strong_30c_inline\"></button>\n      </div>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/StartStrong31Slide/StartStrong31Slide.html","\n<article class=\"slide\" id=\"StartStrong31Slide\">\n  <h1>EYLEA<sup>&reg;</sup>:EARLY VISION GAINS<br> IN PATIENTS WITH DME</h1>\n  <!--h2 !{lead_paragraph}-->\n  <div class=\"content-wrapper\">\n    <div class=\"question-wrapper\">\n      <p class=\"patients\">HOW MANY OF THESE <br>DME PATIENTS WOULD <br>YOU EXPECT TO<br>ACHIEVE <mark>AT LEAST <br>A ONE-LINE VISUAL <br>GAIN</mark> AFTER</p>\n      <div class=\"question\">\n        <p class=\"number big-number gold\">5</p>\n        <p class=\"text\"><mark>INITIAL MONTHLY DOSES OF EYLEA<sup>&reg;</sup>?</mark></p>\n      </div>\n    </div>\n    <div class=\"slider-wrapper\">\n      <div class=\"before-after-slider\" data-stop-swipe=\"data-stop-swipe\">\n        <div class=\"element-before\"></div>\n        <div class=\"element-after\"></div>\n        <ul class=\"patients-numbers\">\n          <li>1</li>\n          <li>2</li>\n          <li>3</li>\n          <li>4</li>\n          <li>5</li>\n          <li>6</li>\n          <li>7</li>\n          <li>8</li>\n          <li>9</li>\n          <li>10</li>\n        </ul>\n      </div>\n    </div>\n    <div class=\"act\">\n      <p>Press and hold to highlight answer</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/StartStrong32APopupSlide/StartStrong32APopupSlide.html","\n<article class=\"slide\" id=\"StartStrong32APopupSlide\">\n  <h1>WHAT DO LINES OF VISION MEAN TO YOUR PATIENT?</h1>\n  <h2 class=\"sub-header\">BINOCULAR VISUAL ACUITY<sup data-reference-id=\'davidson,globe\'></sup><sup>,</sup><sup data-notes=\'correlation_lines\'></sup></h2>\n  <div class=\"content\">\n    <div class=\"left-side\">\n      <div class=\"row row-0\">\n        <div class=\"count\"><span>1</span></div>\n        <div class=\"description\">\n          <p>LINE</p>\n          <p>LOST</p>\n        </div>\n      </div>\n      <div class=\"seperator-line-0\"></div>\n      <div class=\"row row-1\">\n        <div class=\"count\"><span>2</span></div>\n        <div class=\"description\">\n          <p>LINES</p>\n          <p>LOST</p>\n        </div>\n      </div>\n      <div class=\"seperator-line-1\"></div>\n      <div class=\"row row-2\">\n        <div class=\"count\"><span>3</span></div>\n        <div class=\"description\">\n          <p>LINES</p>\n          <p>LOST</p>\n        </div>\n      </div>\n      <div class=\"seperator-line-2\"></div>\n    </div>\n    <div class=\"right-side\">\n      <div class=\"row row-0\">\n        <ul>\n          <li><b>Reduced or lost ability to drive</b></li>\n          <li>Dependency on others</li>\n          <li>Difficulty with reading street signs, going down stairs,<br> and watching television</li>\n        </ul>\n      </div>\n      <div class=\"seperator-line-0\"></div>\n      <div class=\"row row-1\">\n        <ul>\n          <li>Limited ability to work or perform role-specific functions</li>\n          <li><b>Difficulty with reading</b> books or newspapers, cooking,<br> sewing, or using hand tools</li>\n        </ul>\n      </div>\n      <div class=\"seperator-line-1\"></div>\n      <div class=\"row row-2\">\n        <ul>\n          <li>Difficulty distinguishing between colors</li>\n        </ul>\n      </div>\n      <div class=\"seperator-line-2\"></div>\n    </div>\n  </div>\n  <div class=\"oblique-tile\">\n    <p>GAINING ONE LINE CAN MAKE A SUBSTANTIAL IMPACT ON PATIENTS’ LIVES</p>\n  </div>\n</article>");
app.cache.put("slides/StartStrong32Slide/StartStrong32Slide.html","\n<article class=\"slide\" id=\"StartStrong32Slide\">\n  <h1>EYLEA<sup>&reg;</sup> EARLY VISION GAINS<br> IN PATIENTS WITH DME</h1>\n  <!--p.lead-paragraph !{lead_text}-->\n  <div class=\"content-wrapper\">\n    <div class=\"graph-wrapper\">\n      <figure>\n        <div class=\"people-illustration\">\n          <p class=\"big-number gold\"><strong>80</strong><span>%</span></p><img src=\"slides/StartStrong32Slide/assets/images/people.png\"/>\n        </div>\n        <figcaption>OF PATIENTS IN VIVID DME AND VISTA DME TREATED WITH <mark>EYLEA<sup>&reg;</sup></mark> ACHIEVED<br><mark>A CLINICALLY MEANINGFUL<br>VISION GAIN</mark><br><b>(AT LEAST ONE LINE IMPROVEMENT) AFTER</b></figcaption>\n      </figure>\n      <p class=\"dose-description\"><mark>5 INITIAL MONTHLY DOSES<sup data-reference-id=\'ziemssen\'></sup><sup>,</sup><sup data-notes=\'data_dme\'></sup></mark></p>\n    </div>\n    <button class=\"arrow-btn right next-slide\"></button>\n  </div>\n  <div class=\"wrap-open-popup\">\n    <p>Patient value of gains</p>\n    <button class=\"arrow-btn plus-white\" data-popup=\"StartStrong32APopupSlide\"></button>\n  </div>\n</article>");
app.cache.put("slides/StartStrong33Slide/StartStrong33Slide.html","\n<article class=\"slide\" id=\"StartStrong33Slide\">\n  <h1>START STRONG WITH EYLEA<sup>&reg;</sup></h1>\n  <h2>FOR EARLY VISION GAINS IN PATIENTS WITH DME<sup data-reference-id=\'korobelnik,vivid,ziemssen\'></sup></h2>\n  <button class=\"arrow-btn left previous-slide\"></button>\n  <div class=\"content-wrapper\">\n    <div class=\"left-side\">\n      <div class=\"graph-wrapper\">\n        <figure>\n          <figcaption>AFTER RECEIVING</figcaption>\n          <div class=\"wrap-number\">\n            <p class=\"big-number gold\">5</p>\n            <mark>initial monthly<br> doses of<br> <span>EYLEA<sup>&reg;</sup> 2 mg</span></mark>\n          </div>\n        </figure>\n      </div>\n    </div>\n    <div class=\"right-side\">\n      <div class=\"graph-wrapper\">\n        <figure>\n          <figcaption>ON AVERAGE,<br> PATIENTS ACHIEVED</figcaption>\n          <p class=\"big-number gold\">87<span>%</span></p>\n          <p class=\"fig-text\">OF MEAN VISION<br> GAINED AT YEAR 1<sup data-reference-id=\'korobelnik,vivid\'></sup></p>\n        </figure>\n      </div>\n      <div class=\"graph-wrapper\">\n        <figure>\n          <p class=\"big-number gold\">80<span>%</span></p>\n          <p class=\"fig-text\">OF PATIENTS GAINED<br> <mark>AT LEAST 1 LINE<br> OF VISION<sup data-reference-id=\'ziemssen\'></sup><sup>,</sup><sup data-notes=\'data_dme\'></sup><mark></p>\n        </figure>\n      </div>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/StartStrong34A1PopupSlide/StartStrong34A1PopupSlide.html","\n<article class=\"slide\" id=\"StartStrong34A1PopupSlide\">\n  <h1>STUDY DESIGN<sup data-reference-id=\'vividEast\'></sup></h1>\n  <div class=\"schema-wrapper\">\n    <div class=\"diagram-header\">\n      <div class=\"pointer-label\"></div>\n      <p class=\"text\">RANDOMIZED, MULTICENTER, DOUBLE-MASKED TRIAL IN PATIENTS WITH CLINICALLY<br>SIGNIFICANT DME WITH CENTRAL INVOLVEMENT AND BCVA 20/40 TO 20/320</p>\n    </div>\n    <div class=\"schema\">\n      <div class=\"column\">\n        <div class=\"diagram-billet-block\">\n          <div class=\"diagram-billet\">\n          </div>\n        </div>\n        <p class=\"diagram-billet-text\">(n=381)</p>\n      </div>\n      <div class=\"column\">\n        <div class=\"row\">\n          <div class=\"row-item\">\n            <p class=\"row-item-title\"><b>Patients randomized<br>1:1:1</b></p>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"row-item\">\n            <p class=\"row-item-title\">EYLEA<sup>&reg;</sup><br>2 mg q8 wks<sup data-notes=\'monthly_doses_8\'></sup></p>\n          </div>\n          <div class=\"row-item\">\n            <p class=\"row-item-title\">EYLEA<sup>&reg;</sup><br>2 mg q4 wks</p>\n          </div>\n          <div class=\"row-item\">\n            <p class=\"row-item-title\">Laser<br>photocoagulation<sup data-notes=\'grid_laser_8\'></sup></p>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"row-item\">\n            <p class=\"row-item-title\"><b>Rescue protocol after 24 weeks:</b></p>\n            <p class=\"row-item-content\">When criteria for additional treatment were met,<sup data-notes=\'study_eyes\'></sup> study eyes in the 2q4 and 2q8 groups received active laser<br>(rather than sham) from Week 24 onward, while those in the laser group received 5 doses of EYLEA<sup>&reg;</sup> 2 mg every<br>4 weeks followed by dosing every 8 weeks.</p>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"row-item\">\n            <p class=\"row-item-title\"><b>Primary outcome:</b></p>\n            <p class=\"row-item-content\">Mean change in BCVA</p>\n          </div>\n          <div class=\"row-item\">\n            <p class=\"row-item-title\"><b>Primary endpoint:</b></p>\n            <p class=\"row-item-content\"><b>Week 52</b></p>\n          </div>\n          <div class=\"row-item\">\n            <p class=\"row-item-title\"><b>Key secondary outcomes:</b></p>\n            <ul class=\"row-item-list\">\n              <li>Percentage of patients gaining 3 lines</li>\n              <li>Change in Diabetic Retinopathy Severity Scale</li>\n            </ul>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/StartStrong34A2PopupSlide/StartStrong34A2PopupSlide.html","\n<article class=\"slide\" id=\"StartStrong34A2PopupSlide\">\n  <h1>DOSING AND MONITORING SCHEDULE<sup data-reference-id=\'vividEast\'></sup></h1>\n  <figure class=\"figure-fragment\">\n    <figcaption>Dosing and monitoring in VIVID EAST<span class=\"pointer-label\"></span></figcaption>\n    <div class=\"content-wrapper\">\n      <table class=\"schedule\">\n        <colgroup>\n          <col/>\n          <col/>\n          <col/>\n          <col/>\n          <col/>\n          <col/>\n          <col/>\n          <col/>\n          <col/>\n          <col/>\n        </colgroup>\n        <thead>\n          <tr>\n            <th></th>\n            <th colspan=\"9\">Year 1</th>\n          </tr>\n        </thead>\n        <tbody>\n          <tr>\n            <td>Week</td>\n            <td>0</td>\n            <td>4</td>\n            <td>8</td>\n            <td>12</td>\n            <td>16</td>\n            <td>20</td>\n            <td>24</td>\n            <td>28–48</td>\n            <td>52</td>\n          </tr>\n          <tr>\n            <td>EYLEA<sup>&reg; </sup>&nbsp;2q4</td>\n            <td class=\"circle1\"></td>\n            <td class=\"circle1\"></td>\n            <td class=\"circle1\"></td>\n            <td class=\"circle1\"></td>\n            <td class=\"circle1\"></td>\n            <td class=\"circle1\"></td>\n            <td class=\"circle1\"></td>\n            <td class=\"circle1\"></td>\n            <td></td>\n          </tr>\n          <tr>\n            <td>EYLEA<sup>&reg; </sup>&nbsp;2q8</td>\n            <td class=\"circle2\"></td>\n            <td class=\"circle2\"></td>\n            <td class=\"circle2\"></td>\n            <td class=\"circle2\"></td>\n            <td class=\"circle2\"></td>\n            <td class=\"circle3\"></td>\n            <td class=\"circle2\"></td>\n            <td class=\"circle3\"></td>\n            <td></td>\n          </tr>\n          <tr>\n            <td>Laser<sup data-notes=\'patients_laser_group_8\'></sup></td>\n            <td class=\"circle4\"></td>\n            <td></td>\n            <td></td>\n            <td class=\"circle4\"></td>\n            <td class=\"circle4\"></td>\n            <td class=\"circle4\"></td>\n            <td class=\"circle4\"></td>\n            <td class=\"circle4\"></td>\n            <td></td>\n          </tr>\n        </tbody>\n        <tfoot>\n          <tr>\n            <td></td>\n            <td></td>\n            <td></td>\n            <td></td>\n            <td></td>\n            <td></td>\n            <td></td>\n            <td></td>\n            <td></td>\n            <td class=\"arrow\">Primary<br> endpoint</td>\n          </tr>\n        </tfoot>\n      </table>\n      <div class=\"schedule-legend\">\n        <p class=\"circle1\"><b>EYLEA<sup>&reg;</sup> 2 mg every 4 weeks (2q4)</b></p>\n        <p class=\"circle2\"><b>EYLEA<sup>&reg;</sup> 2 mg every 2 months (2q8)</b> following 5 initial monthly doses</p>\n        <p class=\"circle3\"><b>Sham injection</b></p>\n        <p class=\"circle4\"><b>Laser</b> (no more often than every 12 weeks)</p>\n        <p class=\"circle5\"><b>Laser</b> (as needed based on prespecified criteria)</p>\n      </div>\n    </div>\n  </figure>\n</article>");
app.cache.put("slides/StartStrong34A3PopupSlide/StartStrong34A3PopupSlide.html","\n<article class=\"slide\" id=\"StartStrong34A3PopupSlide\">\n  <h1>Baseline characteristics<sup data-reference-id=\'vividEast\'></sup></h1>\n  <table class=\"table-global\">\n    <colgroup>\n      <col/>\n      <col/>\n      <col/>\n      <col/>\n    </colgroup>\n    <thead>\n      <tr>\n        <th colspan=\"1\"></th>\n        <th class=\"vivid-east\" colspan=\"3\"></th>\n      </tr>\n      <tr>\n        <th colspan=\"1\"></th>\n        <th class=\"blue\">EYLEA<sup>&reg;</sup> 2q8</th>\n        <th class=\"white\">EYLEA<sup>&reg;</sup> 2q4</th>\n        <th class=\"light-gray\">Laser</th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr>\n        <th>n</th>\n        <td>127</td>\n        <td>127</td>\n        <td>124</td>\n      </tr>\n      <tr>\n        <th>Age, years (SD)</th>\n        <td>57.6 (10.1)</td>\n        <td>59.3 (10.3)</td>\n        <td>58.8 (10.5)</td>\n      </tr>\n      <tr>\n        <th>Female, n (%)</th>\n        <td>60 (47.2)</td>\n        <td>68 (53.5)</td>\n        <td>60 (48.4)</td>\n      </tr>\n      <tr>\n        <th>Race, n (%)</th>\n        <td></td>\n        <td></td>\n        <td></td>\n      </tr>\n      <tr>\n        <th>White</th>\n        <td>10 (7.9)</td>\n        <td>10 (7.9)</td>\n        <td>9 (7.3)</td>\n      </tr>\n      <tr>\n        <th>Black</th>\n        <td>0</td>\n        <td>0</td>\n        <td>0</td>\n      </tr>\n      <tr>\n        <th>Asian</th>\n        <td>117 (92.1)</td>\n        <td>117 (92.1)</td>\n        <td>115 (92.7)</td>\n      </tr>\n      <tr>\n        <th>Other</th>\n        <td>0</td>\n        <td>0</td>\n        <td>0</td>\n      </tr>\n      <tr>\n        <th>Mean HbA<sub>1c</sub>, % (SD)</th>\n        <td>7.4 (1.36)</td>\n        <td>7.6 (1.41)</td>\n        <td>7.3 (1.36)</td>\n      </tr>\n      <tr>\n        <th>Patients with HbA<sub>1c</sub> &gt;8%, n (%)</th>\n        <td>34 (26.8)</td>\n        <td>41 (32.3%)</td>\n        <td>33 (26.6)</td>\n      </tr>\n      <tr>\n        <th>Duration of diabetes, years (SD)</th>\n        <td>11.5 (7.9)</td>\n        <td>12.9 (7.7)</td>\n        <td>12.6 (7.8)</td>\n      </tr>\n    </tbody>\n  </table>\n</article>");
app.cache.put("slides/StartStrong34A4PopupSlide/StartStrong34A4PopupSlide.html","\n<article class=\"slide\" id=\"StartStrong34A4PopupSlide\">\n  <h1>BASELINE DISEASE CHARACTERISTICS<sup data-reference-id=\'vividEast\'></sup></h1>\n  <table class=\"table-global\">\n    <colgroup>\n      <col/>\n      <col/>\n      <col/>\n      <col/>\n    </colgroup>\n    <thead>\n      <tr>\n        <th></th>\n        <th class=\"vivid\" colspan=\"3\"></th>\n      </tr>\n      <tr>\n        <th></th>\n        <th class=\"blue\">EYLEA<sup>&reg;</sup> 2q8</th>\n        <th class=\"white\">EYLEA<sup>&reg;</sup> 2q4</th>\n        <th class=\"light-gray\">Laser</th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr>\n        <th>n</th>\n        <td>127</td>\n        <td>127</td>\n        <td>124</td>\n      </tr>\n      <tr>\n        <th>BCVA, ETDRS letters (SD)</th>\n        <td>57.1 (12.5)</td>\n        <td>55.6 (12.1)</td>\n        <td>55.1 (14.2)</td>\n      </tr>\n      <tr>\n        <th>CRT, &micro;m (SD)</th>\n        <td>520.3 (154.8)</td>\n        <td>526.3 (164.4)</td>\n        <td>527.7 (170.5)</td>\n      </tr>\n      <tr>\n        <th>NEI-VFQ score</th>\n        <td></td>\n        <td></td>\n        <td></td>\n      </tr>\n      <tr>\n        <th>Total, points (SD)</th>\n        <td>68.9 (16.8)</td>\n        <td>67.9 (17.1)</td>\n        <td>69.7 (17.1)</td>\n      </tr>\n      <tr>\n        <th>Distance, points (SD)</th>\n        <td>69.8 (22.9)</td>\n        <td>72.4 (23.2)</td>\n        <td>74.1 (21.8)</td>\n      </tr>\n      <tr>\n        <th>Near, points (SD)</th>\n        <td>64.6 (23.8)</td>\n        <td>63.9 (22.7)</td>\n        <td>65.3 (25.0)</td>\n      </tr>\n    </tbody>\n  </table>\n</article>");
app.cache.put("slides/StartStrong34A5PopupSlide/StartStrong34A5PopupSlide.html","\n<article class=\"slide\" id=\"StartStrong34A5PopupSlide\">\n  <h1>Eligibility criteria<sup data-reference-id=\'vividEast\'></sup></h1>\n  <dl class=\"chapter-criteria-list\">\n    <dt><span class=\"term-label\"></span>Inclusion criteria</dt>\n    <dd>Adults &ge;18 years of age with type 1 or type 2 diabetes mellitus</dd>\n    <dd>Decrease in vision determined to be primarily the result of DME in the study eye</dd>\n    <dd>BCVA ETDRS letter score 73 to 24 (20/40 to 20/320) in the study eye</dd>\n    <dt><span class=\"term-label\"></span>Exclusion criteria</dt>\n    <dd>Laser photocoagulation (panretinal or macular) in the study eye within 90 days of study day 1</dd>\n    <dd>Previous use of intraocular or periocular corticosteroids in the study eye within 120 days of study day 1</dd>\n    <dd>Previous treatment with anti-VEGF drugs in the study eye (pegaptanib sodium, bevacizumab, ranibizumab, etc.) within 90 days of study day 1</dd>\n    <dd>Active proliferative diabetic retinopathy in the study eye</dd>\n    <dd>Uncontrolled diabetes mellitus</dd>\n    <dd>Only 1 functional eye, even if that eye is otherwise eligible for the study</dd>\n  </dl>\n</article>");
app.cache.put("slides/StartStrong34B1PopupSlide/StartStrong34B1PopupSlide.html","\n<article class=\"slide\" id=\"StartStrong34B1PopupSlide\">\n  <div class=\"fake-menu\">\n    <button>CRT</button>\n  </div>\n  <p class=\"sub-text\">IN VIVID EAST…</p>\n  <h1><mark>EYLEA<sup>&reg;</sup></mark>: RAPID AND ROBUST MEAN REDUCTIONS IN CRT VS LASER<sup data-reference-id=\'vividEast\'></sup></h1>\n  <figure class=\"figure-fragment graph-fragment top-scale\">\n    <figcaption>Mean change in CRT though Week 52<sup data-reference-id=\'vividEast\'></sup><span class=\"pointer-label\"></span></figcaption>\n    <div class=\"content-wrapper\">\n      <figure class=\"graph\">\n        <div class=\"g-scale g-scale-v\">\n          <ul>\n            <li><span>–250</span></li>\n            <li><span>–200</span></li>\n            <li><span>–150</span></li>\n            <li><span>–100</span></li>\n            <li><span>–50</span></li>\n            <li><span>0</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>&micro;m</span><br/>\n          </p>\n        </div>\n        <div class=\"g-scale g-scale-h\">\n          <ul>\n            <li><span>0</span></li>\n            <li><span>4</span></li>\n            <li><span>8</span></li>\n            <li><span>12</span></li>\n            <li><span>16</span></li>\n            <li><span>20</span></li>\n            <li><span>24</span></li>\n            <li><span>28</span></li>\n            <li><span>32</span></li>\n            <li><span>36</span></li>\n            <li><span>40</span></li>\n            <li><span>44</span></li>\n            <li><span>48</span></li>\n            <li><span>52</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>Week</span><br/>\n          </p>\n        </div>\n        <div class=\"g-content\">\n          <div class=\"graph-blocks\">\n            <div class=\"block\">\n              <p></p>\n            </div>\n            <div class=\"block\">\n              <p></p>\n            </div>\n          </div>\n          <div class=\"g-lines\">\n            <div class=\"g-line\">\n              <div class=\"g-line-info\">\n                <div class=\"skew element-gray g-line-value\">\n                  <p>–109.3</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"g-line\">\n              <div class=\"g-line-info\">\n                <div class=\"skew element-light-blue g-line-value\">\n                  <p>–234.7</p>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </figure>\n    </div>\n    <ul class=\"g-legend\">\n      <li class=\"legend-light-blue\"><strong>EYLEA<sup>&reg;</sup> 2 mg</strong> administered every month (4 weeks) for the first 5 months, followed by once every 2 months (8 weeks); <strong>n=127</strong>.<sup data-reference-id=\'davidson\'></sup></li>\n      <li class=\"legend-gray\"><strong>Laser</strong> photocoagulation administered as needed, no more often than every 12 weeks; <strong>n=117</strong>.<sup data-reference-id=\'davidson\'></sup></li>\n    </ul>\n  </figure>\n</article>");
app.cache.put("slides/StartStrong34Slide/StartStrong34Slide.html","\n<article class=\"slide\" id=\"StartStrong34Slide\">\n  <h1>START STRONG: VIVID EAST</h1>\n  <h2>STRONG GAINS SEEN IN ASIAN POPULATIONS<sup data-reference-id=\'vividEast\'></sup></h2>\n  <div class=\"content-wrapper\">\n    <h3><strong>EYLEA<sup>&reg;</sup> 2 mg</strong> achieved a mean gain of &gt;2 lines at 1 year in VIVID EAST<sup data-reference-id=\'vividEast\'></sup></h3>\n    <figure class=\"figure-fragment graph-fragment\">\n      <figcaption>Mean change in BCVA though Week 52<sup data-reference-id=\'korobelnik,vividEast\'></sup><span class=\"pointer-label\"></span></figcaption>\n      <div class=\"content-wrapper\">\n        <figure class=\"graph\">\n          <div class=\"g-scale g-scale-v\">\n            <ul>\n              <li><span>0</span></li>\n              <li><span>2</span></li>\n              <li><span>4</span></li>\n              <li><span>6</span></li>\n              <li><span>8</span></li>\n              <li><span>10</span></li>\n              <li><span>12</span></li>\n              <li><span>14</span></li>\n            </ul>\n            <p class=\"g-scale-label\"><span>Mean change in BCVA</span><br/><span>ETDRS letters</span><br/>\n            </p>\n          </div>\n          <div class=\"g-scale g-scale-h\">\n            <ul>\n              <li><span>0</span></li>\n              <li><span>4</span></li>\n              <li><span>8</span></li>\n              <li><span>12</span></li>\n              <li><span>16</span></li>\n              <li><span>20</span></li>\n              <li><span>52</span></li>\n            </ul>\n            <p class=\"g-scale-label\"><span>Week</span><br/>\n            </p>\n          </div>\n          <div class=\"g-content\">\n            <div class=\"graph-blocks\">\n              <div class=\"block\">\n                <p>Initial monthly dosing</p>\n              </div>\n              <div class=\"block\">\n                <p>Extension</p>\n              </div>\n            </div>\n            <div class=\"g-lines\">\n              <div class=\"g-line\">\n                <div class=\"g-line-info\">\n                  <div class=\"skew element-light-blue g-line-value\">\n                    <p>+12.8</p>\n                  </div>\n                </div>\n              </div>\n              <div class=\"g-line\">\n                <div class=\"g-line-info\">\n                  <div class=\"skew element-gray g-line-value\">\n                    <p>+10.7</p>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </figure>\n      </div>\n      <ul class=\"g-legend\">\n        <li class=\"legend-gray\">EYLEA<sup>&reg;</sup> 2 mg administered every month (4 weeks) for the first 5 months, followed by once every 2 months (8 weeks); VIVID DME n=135.<sup data-reference-id=\'korobelnik\'></sup></li>\n        <li class=\"legend-light-blue\">EYLEA<sup>&reg;</sup> 2 mg administered every month (4 weeks) for the first 5 months, followed by once every 2 months (8 weeks); VIVID EAST n=116.<sup data-reference-id=\'vividEast\'></sup></li>\n      </ul>\n    </figure>\n    <div class=\"wrap-buttons\">\n      <div class=\"wrap-open-popup\">\n        <p>Study design</p>\n        <button class=\"arrow-btn open-popup\" data-viewer=\"slideshow\" href=\"start_strong_34a_inline\" slide=\"StartStrong34A1PopupSlide\"></button>\n      </div>\n      <div class=\"wrap-open-popup\">\n        <p>Additional endpoints</p>\n        <button class=\"arrow-btn open-popup\" data-popup=\"StartStrong34B1PopupSlide\"></button>\n      </div>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/StartStrong35A1PopupSlide/StartStrong35A1PopupSlide.html","\n<article class=\"slide\" id=\"StartStrong35A1PopupSlide\">\n  <h1>STUDY DESIGN<sup data-reference-id=\'diabetic\'></sup></h1>\n  <div class=\"schema-wrapper\">\n    <div class=\"diagram-header\">\n      <div class=\"pointer-label\"></div>\n      <p class=\"text\">RANDOMIZED, MULTICENTER, SINGLE-MASKED TRIAL IN PATIENTS WITH CLINICALLY<br>SIGNIFICANT DME WITH CENTRAL INVOLVEMENT AND BCVA 20/32 TO 20/320</p>\n    </div>\n    <div class=\"schema\">\n      <div class=\"column\">\n        <div class=\"diagram-billet-block\">\n          <div class=\"diagram-billet\">\n            <p><b>Protocol T</b></p>\n          </div>\n        </div>\n        <p class=\"diagram-billet-text\">(n=660)</p>\n      </div>\n      <div class=\"column\">\n        <div class=\"row\">\n          <div class=\"row-item\">\n            <p class=\"row-item-title\"><b>Patients randomized<br>1:1:1</b></p>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"row-item\">\n            <p class=\"row-item-title\">Ranibizumab<br>0.3 mg<sup data-notes=\'treatment_decision dose_ranibizumab\'></sup></p>\n          </div>\n          <div class=\"row-item\">\n            <p class=\"row-item-title\">EYLEA<sup>&reg;</sup><br>2 mg<sup data-notes=\'treatment_decision dose_ranibizumab\'></sup></p>\n          </div>\n          <div class=\"row-item\">\n            <p class=\"row-item-title\">Bevacizumab<br>1.25 mg<sup data-notes=\'treatment_decision\'></sup></p>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"row-item\">\n            <p class=\"row-item-title\"><b>Protocol at each visit after baseline:</b></p>\n            <ul class=\"row-item-list\">\n              <li>If not improved or worsened<sup data-notes=\'improvement_worsening\'></sup> from last injection/visit <mark class=\'arrow\'>INJECT</mark></li>\n              <li>If not improved or worsened<sup data-notes=\'improvement_worsening\'></sup> after 2 consecutive injections<br>with 20/20 vision and Stratus equivalent<sup data-notes=\'threshold\'></sup> &lt;250 μm CRT <mark class=\'arrow\'>DEFER</mark></li>\n              <li>If not improved or worsened<sup data-notes=\'improvement_worsening\'></sup> after 2 consecutive injections<br>without 20/20 vision and Stratus equivalent<sup data-notes=\'threshold\'></sup> &lt;250 μm CRT <mark class=\'arrow\'>INJECT</mark><br>in first 6 months, <mark>DEFER</mark> after 6 months</li>\n            </ul>\n          </div>\n          <div class=\"row-item\">\n            <p class=\"row-item-title\"><b>Rescue protocol after 24 weeks:</b></p>\n            <ul class=\"row-item-list\">\n              <li>Focal/grid laser when OCT CRT &ge;250 µm<sup data-notes=\'threshold\'></sup><br>or if there is edema threatening and if the eye<br>has not improved on OCT or VA compared<br>with either of the last two consecutive injections</li>\n            </ul>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"row-item\">\n            <p class=\"row-item-title\">Primary endpoint:</p>\n            <p class=\"row-item-content\">Mean change in BCVA at Week 52</p>\n          </div>\n          <div class=\"row-item\">\n            <p class=\"row-item-title\">Patients unmasked to treatment group<br>following the publication of the Week 52<br>primary results: though discouraged,<br>decision could be made at that time<br>to switch to a non-study anti-VEGF treatment</p>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"row-item\">\n            <p class=\"row-item-title\"><b>Year 2<sup data-notes=\'visits_occurred\'></sup></b></p>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/StartStrong35A2PopupSlide/StartStrong35A2PopupSlide.html","\n<article class=\"slide\" id=\"StartStrong35A2PopupSlide\">\n  <h1>Baseline characteristics<sup data-reference-id=\'diabetic\'></sup><sup>,</sup><sup data-notes=\'treatment_regimen\'>a</sup></h1>\n  <div class=\"content\">\n    <table class=\"table-global\">\n      <colgroup>\n        <col/>\n        <col/>\n        <col/>\n        <col/>\n      </colgroup>\n      <thead>\n        <tr>\n          <th colspan=\"1\"></th>\n          <th class=\"yellow\" colspan=\"3\">Protocol T</th>\n        </tr>\n        <tr>\n          <th colspan=\"1\"></th>\n          <th class=\"blue\">EYLEA<sup>&reg;</sup></th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr>\n          <th>n</th>\n          <td>224</td>\n        </tr>\n        <tr>\n          <th>Age, years, median (25<sup>th</sup>, 75<sup>th</sup> percentile)</th>\n          <td>61 (54, 66)</td>\n        </tr>\n        <tr>\n          <th>Female, n (%)</th>\n          <td>110 (49)</td>\n        </tr>\n        <tr>\n          <th>Race, n (%)</th>\n          <td>&nbsp;</td>\n        </tr>\n        <tr>\n          <th>White</th>\n          <td>145 (65)</td>\n        </tr>\n        <tr>\n          <th>Black</th>\n          <td>32 (14)</td>\n        </tr>\n        <tr>\n          <th>Hispanic</th>\n          <td>37 (17)</td>\n        </tr>\n        <tr>\n          <th>Asian</th>\n          <td>2 (&lt;1)</td>\n        </tr>\n        <tr>\n          <th>Other</th>\n          <td>8 (4)</td>\n        </tr>\n        <tr>\n          <th>HbA<sub>1c</sub>, %, median (25<sup>th</sup>, 75<sup>th</sup> percentile)<sup data-notes=\'missing_hba\'></sup></th>\n          <td>7.6 (6.8, 9.1)</td>\n        </tr>\n        <tr>\n          <th>Duration of diabetes, years (25<sup>th</sup>, 75<sup>th</sup> percentile)</th>\n          <td>15 (8, 21)</td>\n        </tr>\n      </tbody>\n    </table>\n  </div>\n</article>");
app.cache.put("slides/StartStrong35A3PopupSlide/StartStrong35A3PopupSlide.html","\n<article class=\"slide\" id=\"StartStrong35A3PopupSlide\">\n  <h1>Baseline disease characteristics<sup data-reference-id=\'diabetic\'></sup><sup>,</sup><sup data-notes=\'dose_ranibizumab\'></sup></h1>\n  <div class=\"content\">\n    <table class=\"table-global\">\n      <colgroup>\n        <col/>\n        <col/>\n      </colgroup>\n      <thead>\n        <tr>\n          <th colspan=\"1\"></th>\n          <th class=\"yellow\" colspan=\"3\">&#32&#32Protocol T&#32&#32</th>\n        </tr>\n        <tr>\n          <th colspan=\"1\"></th>\n          <th class=\"blue\">&#32&#32EYLEA<sup>&reg;</sup>&#32&#32</th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr>\n          <th>n</th>\n          <td>224</td>\n        </tr>\n        <tr>\n          <th>BCVA, ETDRS letters, median (25<sup>th</sup>, 75<sup>th</sup> percentile)</th>\n          <td>69 (74, 59)</td>\n        </tr>\n        <tr>\n          <th>CST, µm, median (25<sup>th</sup>, 75<sup>th</sup> percentile)</th>\n          <td>387 (310, 483)</td>\n        </tr>\n        <tr>\n          <th>Prior anti-VEGF therapy, n (%)</th>\n          <td>24 (11)</td>\n        </tr>\n      </tbody>\n    </table>\n  </div>\n</article>");
app.cache.put("slides/StartStrong35A4PopupSlide/StartStrong35A4PopupSlide.html","\n<article class=\"slide\" id=\"StartStrong35A4PopupSlide\">\n  <h1>Eligibility criteria<sup data-reference-id=\'diabetic\'></sup></h1>\n  <dl class=\"chapter-criteria-list\">\n    <dt><span class=\"term-label\"></span>Inclusion criteria</dt>\n    <dd>Adults &ge;18 years of age with type 1 or type 2 diabetes mellitus</dd>\n    <dd>Best corrected E-ETDRS visual acuity letter score &le;78 (20/32 or worse) and &le;24 (20/320 or better)</dd>\n    <dd>Definite retinal thickening on clinical exam due to DME involving the center of the macula</dd>\n    <dd>Media clarity, pupillary dilation, and individual cooperation sufficient for adequate fundus photographs</dd>\n    <dd>Central subfield thickness on OCT ≥250 µm on Zeiss Stratus; &ge;320 if male or ≥305 if female <br>on Heidelberg Spectralis; &ge;305 if male or &ge;290 if female on Zeiss Cirrus</dd>\n    <dt><span class=\"term-label\"></span>Exclusion criteria</dt>\n    <dd>Significant renal disease, defined as a history of chronic renal failure requiring dialysis or kidney transplant</dd>\n    <dd>A condition that, in the opinion of the investigator, would preclude participation (e.g., unstable medical status including blood pressure, cardiovascular disease, and glycemic control)</dd>\n    <dd>Participation in an investigational trial within 30 days of randomization that involved treatment with any drug that has not received regulatory approval for the indication being studied at the time of study entry</dd>\n    <dd>Known allergy to any component of the study drug</dd>\n    <dd>Blood pressure &gt;180/110 (systolic above 180 OR diastolic above 110)</dd>\n    <dd>Myocardial infarction, other acute cardiac event requiring hospitalization, stroke, transient ischemic attack, <br>or treatment for acute congestive heart failure within 4 months prior to randomization</dd>\n    <dd>Systemic anti-VEGF or pro-VEGF treatment within four months prior to randomization or anticipated use during the study</dd>\n    <dd>For women of child-bearing potential: pregnant or lactating or intending to become pregnant <br>within the next 24 months</dd>\n  </dl>\n</article>");
app.cache.put("slides/StartStrong35B1PopupSlide/StartStrong35B1PopupSlide.html","\n<article class=\"slide\" id=\"StartStrong35B1PopupSlide\">\n  <!--h2 !{lead_paragraph}-->\n  <h1><mark>EYLEA<sup>&reg;</sup></mark>:MEAN CHANGE IN CST THROUGH WEEK 52<sup data-reference-id=\'diabetic\'></sup><sup>,</sup><sup data-notes=\'treatment_regimen\'></sup></h1>\n  <figure class=\"figure-fragment graph-fragment top-scale\">\n    <figcaption>    <span class=\"pointer-label\"></span></figcaption>\n    <div class=\"content-wrapper\">\n      <figure class=\"graph\">\n        <div class=\"g-scale g-scale-v\">\n          <ul>\n            <li><span>–180</span></li>\n            <li><span>–160</span></li>\n            <li><span>–140</span></li>\n            <li><span>–120</span></li>\n            <li><span>–100</span></li>\n            <li><span>–80</span></li>\n            <li><span>–60</span></li>\n            <li><span>–40</span></li>\n            <li><span>–20</span></li>\n            <li><span>0</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>µm</span><br/>\n          </p>\n        </div>\n        <div class=\"g-scale g-scale-h\">\n          <ul>\n            <li><span>0</span></li>\n            <li><span>4</span></li>\n            <li><span>8</span></li>\n            <li><span>12</span></li>\n            <li><span>16</span></li>\n            <li><span>20</span></li>\n            <li><span>24</span></li>\n            <li><span>28</span></li>\n            <li><span>32</span></li>\n            <li><span>36</span></li>\n            <li><span>40</span></li>\n            <li><span>44</span></li>\n            <li><span>48</span></li>\n            <li><span>52</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>Week</span><br/>\n          </p>\n        </div>\n        <div class=\"g-content\">\n          <div class=\"g-lines\">\n            <div class=\"g-line\">\n              <div class=\"g-line-info\">\n                <div class=\"skew element-yellow g-line-value\">\n                  <p>–169</p>\n                </div>\n                <p class=\"g-line-name\">Protocol T</p>\n              </div>\n            </div>\n          </div>\n        </div>\n      </figure>\n    </div>\n    <ul class=\"g-legend\">\n      <li class=\"legend-yellow\"><strong>EYLEA<sup>&reg;</sup> 2 mg</strong> administered every month (4 weeks) unless vision &ge;20/20 with &lt;250 μm on Zeiss Stratus equivalent and no improvement or worsening <br>in response to the previous 2 injections, for the first 20 weeks; thereafter injection was withheld if stable. <strong>Protocol T n=224</strong>.<sup data-reference-id=\'diabetic\'></sup></li>\n    </ul>\n  </figure>\n</article>");
app.cache.put("slides/StartStrong35Slide/StartStrong35Slide.html","\n<article class=\"slide\" id=\"StartStrong35Slide\">\n  <h1>EYLEA<sup>®</sup>ACHIEVED A MEAN GAIN<br>OF > 2 LINES AT 1 YEAR WITH 5–6<sup data-notes=\'year_injections\'></sup><br>INITIAL MONTHLY DOSES<sup data-reference-id=\'korobelnik,diabetic\'></sup><sup>,</sup><sup data-notes=\'treatment_regimen\'></sup></h1>\n  <!--p.lead-paragraph !{lead_paragraph}-->\n  <!--P.sub-lead-paragraph !{sub_paragraph}-->\n  <div class=\"content-wrapper\">\n    <figure class=\"figure-fragment graph-fragment\">\n      <figcaption>Mean change in BCVA through Week 52<sup data-reference-id=\'korobelnik,diabetic\'></sup><span class=\"pointer-label\"></span></figcaption>\n      <div class=\"content-wrapper\">\n        <figure class=\"graph\">\n          <div class=\"g-scale g-scale-v\">\n            <ul>\n              <li><span>0</span></li>\n              <li><span>2</span></li>\n              <li><span>4</span></li>\n              <li><span>6</span></li>\n              <li><span>8</span></li>\n              <li><span>10</span></li>\n              <li><span>12</span></li>\n              <li><span>14</span></li>\n            </ul>\n            <p class=\"g-scale-label\"><span>Mean change in BCVA</span><br/><span>ETDRS letters</span><br/>\n            </p>\n          </div>\n          <div class=\"g-scale g-scale-h\">\n            <ul>\n              <li><span>0</span></li>\n              <li><span>4</span></li>\n              <li><span>8</span></li>\n              <li><span>12</span></li>\n              <li><span>16</span></li>\n              <li><span>20</span></li>\n              <li><span> </span></li>\n              <li><span> </span></li>\n              <li><span> </span></li>\n              <li><span> </span></li>\n              <li><span> </span></li>\n              <li><span> </span></li>\n              <li><span> </span></li>\n              <li><span>52</span></li>\n            </ul>\n            <p class=\"g-scale-label\"><span>Week</span><br/>\n            </p>\n          </div>\n          <div class=\"g-content\">\n            <div class=\"graph-blocks\">\n              <div class=\"block\">\n                <p>Initial monthly dosing</p>\n              </div>\n              <div class=\"block\">\n                <p>Extension</p>\n              </div>\n            </div>\n            <div class=\"g-lines\">\n              <div class=\"g-line\">\n                <div class=\"g-line-info\">\n                  <div class=\"skew element-blue g-line-value\">\n                    <p>+10.7</p>\n                  </div>\n                </div>\n              </div>\n              <div class=\"g-line\">\n                <div class=\"g-line-info\">\n                  <div class=\"skew element-yellow g-line-value\">\n                    <p>+13.3</p>\n                  </div>\n                  <p class=\"g-line-name\">Protocol T</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"g-legend-additional initial\">\n              <div class=\"skew caption\">\n                <p><b>Initial monthly dosing</b></p>\n              </div>\n              <div class=\"legend-content\">\n                <li class=\"element-yellow\">\n                  <div class=\"skew\">\n                    <p><b>6</b> Protocol T<sup data-notes=\'year_injections\'></sup></p>\n                  </div>\n                </li>\n                <li class=\"element-blue\">\n                  <div class=\"skew\">\n                    <p><b>5</b> VIVID DME</p>\n                  </div>\n                </li>\n              </div>\n            </div>\n            <div class=\"g-legend-additional extension\">\n              <div class=\"skew caption\">\n                <p><b>Mean injections in Year 1</b><br>(initial monthly dosing plus extension period)</p>\n              </div>\n              <div class=\"legend-content\">\n                <li class=\"element-yellow\">\n                  <div class=\"skew\">\n                    <p><b>9.2</b> Protocol T</p>\n                  </div>\n                </li>\n                <li class=\"element-blue\">\n                  <div class=\"skew\">\n                    <p><b>8.7</b> VIVID DME</p>\n                  </div>\n                </li>\n              </div>\n            </div>\n            <button class=\"arrow-btn plus\" data-goto-state=\"detail-1\"></button>\n            <button class=\"arrow-btn plus\" data-goto-state=\"detail-2\"></button>\n            <div class=\"popups-buttons\">\n              <div class=\"wrap-open-popup\">\n                <p>Study design</p>\n                <button class=\"arrow-btn open-popup\" data-viewer=\"slideshow\" href=\"start_strong_35a_inline\" slide=\"StartStrong35A2PopupSlide\"></button>\n              </div>\n              <div class=\"wrap-open-popup\">\n                <p>Additional endpoints</p>\n                <button class=\"arrow-btn open-popup\" data-viewer=\"slideshow\" href=\"start_strong_35b_inline\" slide=\"StartStrong35B1PopupSlide\"></button>\n              </div>\n            </div>\n          </div>\n        </figure>\n      </div>\n      <ul class=\"g-legend\">\n        <li class=\"legend-blue\"><b>EYLEA<sup>&reg;</sup> 2 mg</b> administered every month (4 weeks) for the first 5 months, followed by once every 2 months (8 weeks); <b>VIVID DME n=135.<sup data-reference-id=\'korobelnik\'></sup></b></li>\n        <li class=\"legend-yellow\"><b>EYLEA<sup>&reg;</sup> 2 mg</b> administered every month (4 weeks) unless vision ≥20/20 with <250 μm on Zeiss Stratus equivalent and no improvement<br>or worsening in response to the previous 2 injections, for the first 20 weeks; thereafter injection was withheld if stable<br>(see study design page for definitions); <b>Protocol T n=224.<sup data-reference-id=\'diabetic\'></sup></b></li>\n      </ul>\n    </figure>\n  </div>\n</article>");
app.cache.put("slides/StayStrong40A1PopupSlide/StayStrong40A1PopupSlide.html","\n<article class=\"slide\" id=\"StayStrong40A1PopupSlide\">\n  <h1>PROTOCOL T DEMONSTRATES MAINTAINED GAINS <br>WITH EXTENDED DOSING OUT TO 2 YEARS<sup data-reference-id=\'wells\'></sup><sup>,</sup><sup data-notes=\'treatment_regimen\'></sup></h1>\n  <h1>PROTOCOL T DEMONSTRATES EFFICACY OF MAINTAINED GAINS <br>WITH EXTENDED DOSING<sup data-reference-id=\'wells\'></sup><sup>,</sup><sup data-notes=\'treatment_regimen\'></sup></h1>\n  <figure class=\"figure-fragment graph-fragment\">\n    <figcaption>Mean change in BCVA with long-term dosing<sup data-reference-id=\'heier,wells\'></sup><span class=\"pointer-label\"></span></figcaption>\n    <div class=\"content-wrapper\">\n      <figure class=\"graph\">\n        <div class=\"g-scale g-scale-v\">\n          <ul>\n            <li><span>0</span></li>\n            <li><span>2</span></li>\n            <li><span>4</span></li>\n            <li><span>6</span></li>\n            <li><span>8</span></li>\n            <li><span>10</span></li>\n            <li><span>12</span></li>\n            <li><span>14</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>Mean change in BCVA</span><br/><span>ETDRS letters</span><br/>\n          </p>\n        </div>\n        <div class=\"g-scale g-scale-h\">\n          <ul>\n            <li><span>0</span></li>\n            <li><span>4</span></li>\n            <li><span>12</span></li>\n            <li><span>20</span></li>\n            <li><span>52</span></li>\n            <li><span>100</span></li>\n            <li><span>148</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>Week</span><br/>\n          </p>\n        </div>\n        <div class=\"g-content\">\n          <div class=\"graph-blocks\">\n            <div class=\"block\">\n              <p>Initial <br>monthly <br>dosing</p>\n            </div>\n            <div class=\"block\">\n              <p>Extension</p>\n            </div>\n          </div>\n          <div class=\"g-lines\">\n            <div class=\"g-line\">\n              <div class=\"g-line-info\">\n                <div class=\"skew element-blue g-line-value\">\n                  <p>+11.7</p>\n                  <div class=\"skew g-line-additional\">\n                    <p>Week 148</p>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"g-line\">\n              <div class=\"g-line-info\">\n                <div class=\"skew element-yellow g-line-value\">\n                  <p>+12.8</p>\n                  <div class=\"skew g-line-additional\">\n                    <p>Week 104</p>\n                  </div>\n                </div>\n                <p class=\"g-line-name\">Protocol T</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"g-legends\">\n            <div class=\"g-legend-extended first\">\n              <div class=\"skew caption\">\n                <p><b>Initial monthly dosing</b></p>\n              </div>\n              <div class=\"legend-content\">\n                <li>\n                  <div class=\"skew\">\n                    <p><b>6</b> Protocol T<sup data-notes=\'year_injections\'></sup></p>\n                  </div>\n                </li>\n                <li>\n                  <div class=\"skew\">\n                    <p><b>5</b> VIVID</p>\n                  </div>\n                </li>\n              </div>\n            </div>\n            <div class=\"g-legend-extended second\">\n              <div class=\"skew caption\">\n                <p><b>Mean doses received in Year 1</b> <br>(initial monthly dosing plus extension period)</p>\n              </div>\n              <div class=\"legend-content\">\n                <li>\n                  <div class=\"skew\">\n                    <p><b>9.2</b> Protocol T<sup data-reference-id=\'wells\'></sup></p>\n                  </div>\n                </li>\n                <li>\n                  <div class=\"skew\">\n                    <p><b>8.7</b> VIVID<sup data-reference-id=\'heier\'></sup></p>\n                  </div>\n                </li>\n              </div>\n            </div>\n            <div class=\"g-legend-extended third\">\n              <div class=\"skew caption\">\n                <p><b>Initial monthly dosing</b></p>\n              </div>\n              <div class=\"legend-content\">\n                <li>\n                  <div class=\"skew\">\n                    <p><b>6</b> Protocol T<sup data-notes=\'year_injections\'></sup></p>\n                  </div>\n                </li>\n                <li>\n                  <div class=\"skew\">\n                    <p><b>5</b> VIVID</p>\n                  </div>\n                </li>\n              </div>\n            </div>\n            <div class=\"g-legend-extended fourth\">\n              <div class=\"skew caption\">\n                <p><b>Doses received in Year 2</b></p>\n              </div>\n              <div class=\"legend-content\">\n                <li>\n                  <div class=\"skew\">\n                    <p><b>5.0<sup data-notes=\'median\'></sup></b> Protocol T<sup data-reference-id=\'wells\'></sup></p>\n                  </div>\n                </li>\n                <li>\n                  <div class=\"skew\">\n                    <p><b>~4.9<sup data-notes=\'number_injections_weeks\'></sup></b> VIVID<sup data-reference-id=\'korobelnik,brown\'></sup></p>\n                  </div>\n                </li>\n              </div>\n            </div>\n          </div>\n          <div class=\"control-buttons\">\n            <button class=\"arrow-btn plus\" data-goto-state=\"first\"></button>\n            <button class=\"arrow-btn plus\" data-goto-state=\"second\"></button>\n            <button class=\"arrow-btn plus\" data-goto-state=\"third\"></button>\n          </div>\n        </div>\n      </figure>\n    </div>\n    <ul class=\"g-legend\">\n      <li class=\"legend-yellow\"><strong>EYLEA<sup>&reg;</sup> 2 mg</strong> administered every month (4 weeks) unless vision &ge;20/20 with &lt;250 μm on Zeiss Stratus equivalent and no improvement or worsening <br>in response to the previous 2 injections, for the first 20 weeks; thereafter injection was withheld if stable (see study design page for definitions); <br><strong>Protocol T n=224</strong>.<sup data-reference-id=\'diabetic\'></sup></li>\n      <li class=\"legend-blue\"><strong>EYLEA<sup>&reg;</sup> 2 mg</strong> administered every month (4 weeks) for the first 5 months, followed by once every 2 months (8 weeks); <strong>VIVID DME n=135</strong>.<sup data-reference-id=\'heier\'></sup></li>\n    </ul>\n  </figure>\n</article>");
app.cache.put("slides/StayStrong40A2PopupSlide/StayStrong40A2PopupSlide.html","\n<article class=\"slide\" id=\"StayStrong40A2PopupSlide\">\n  <!--h2 !{lead_paragraph}-->\n  <h1><mark>EYLEA<sup>&reg;</sup></mark>: MEAN CHANGE IN CST THROUGH WEEK 104<sup data-reference-id=\'wells\'></sup><sup>,</sup><sup data-notes=\'treatment_regimen\'></sup></h1>\n  <figure class=\"figure-fragment graph-fragment top-scale\">\n    <figcaption>Mean change in CST through Week 104<sup data-reference-id=\'wells\'></sup><span class=\"pointer-label\"></span></figcaption>\n    <div class=\"content-wrapper\">\n      <figure class=\"graph\">\n        <div class=\"g-scale g-scale-v\">\n          <ul>\n            <li><span>–250</span></li>\n            <li><span>–200</span></li>\n            <li><span>–150</span></li>\n            <li><span>–100</span></li>\n            <li><span>–50</span></li>\n            <li><span>0</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>µm</span><br/>\n          </p>\n        </div>\n        <div class=\"g-scale g-scale-h\">\n          <ul>\n            <li><span>0</span></li>\n            <li><span>4</span></li>\n            <li><span>8</span></li>\n            <li><span>12</span></li>\n            <li><span>16</span></li>\n            <li><span>20</span></li>\n            <li><span>24</span></li>\n            <li><span>28</span></li>\n            <li><span>32</span></li>\n            <li><span>36</span></li>\n            <li><span>40</span></li>\n            <li><span>44</span></li>\n            <li><span>48</span></li>\n            <li><span>52</span></li>\n            <li><span>68</span></li>\n            <li><span>84</span></li>\n            <li><span>104</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>Week</span><br/>\n          </p>\n        </div>\n        <div class=\"g-content\">\n          <div class=\"g-lines\">\n            <div class=\"g-line\">\n              <div class=\"g-line-info\">\n                <div class=\"skew element-yellow g-line-value\">\n                  <p>–171</p>\n                </div>\n                <p class=\"g-line-name\">Protocol T</p>\n              </div>\n            </div>\n          </div>\n        </div>\n      </figure>\n    </div>\n    <ul class=\"g-legend\">\n      <li class=\"legend-yellow\"><strong>EYLEA<sup>&reg;</sup> 2 mg</strong> administered every month (4 weeks) unless vision &ge;20/20 with &lt;250 μm on Zeiss Stratus equivalent and no improvement or worsening <br>in response to the previous 2 injections, for the first 20 weeks; thereafter injection was withheld if stable (see study design page for definitions); <br><strong>Protocol T n=224</strong>.<sup data-reference-id=\'diabetic\'></sup></li>\n    </ul>\n  </figure>\n</article>");
app.cache.put("slides/StayStrong40A3PopupSlide/StayStrong40A3PopupSlide.html","\n<article class=\"slide\" id=\"StayStrong40A3PopupSlide\">\n  <h1>IN PATIENTS WITH VISUAL ACUITY OF &lt;20/40 (&lt;69 ETDRS letters) <br>AT BASELINE IN PROTOCOL T…</h1>\n  <div class=\"content\">\n    <p class=\"lead-paragraph\">GREATER IMPROVEMENTS IN BCVA WITH EYLEA<sup>&reg;</sup> VS RANIBIZUMAB AND BEVACIZUMAB <br>(area under curve; <i>post-hoc</i> analysis)<sup data-reference-id=\'jampol\'></sup><sup>,</sup><sup data-notes=\'dose_ranibizumab\'></sup></p>\n    <figure class=\"figure-fragment graph-fragment\">\n      <figcaption>Mean change in BCVA though Year 2<sup data-reference-id=\'wells,jampol\'></sup><span class=\"pointer-label\"></span></figcaption>\n      <div class=\"content-wrapper\">\n        <figure class=\"graph\">\n          <div class=\"g-scale g-scale-v\">\n            <ul>\n              <li><span>0</span></li>\n              <li><span>5</span></li>\n              <li><span>10</span></li>\n              <li><span>15</span></li>\n              <li><span>20</span></li>\n            </ul>\n            <p class=\"g-scale-label\"><span>Mean change in BCVA</span><br/><span>ETDRS letters</span><br/>\n            </p>\n          </div>\n          <div class=\"g-scale g-scale-h\">\n            <ul>\n              <li><span>0</span></li>\n              <li><span>4</span></li>\n              <li><span>8</span></li>\n              <li><span>12</span></li>\n              <li><span>16</span></li>\n              <li><span>20</span></li>\n              <li><span>24</span></li>\n              <li><span>28</span></li>\n              <li><span>32</span></li>\n              <li><span>36</span></li>\n              <li><span>40</span></li>\n              <li><span>44</span></li>\n              <li><span>48</span></li>\n              <li><span>52</span></li>\n              <li><span>68</span></li>\n              <li><span>84</span></li>\n              <li><span>104</span></li>\n            </ul>\n            <p class=\"g-scale-label\"><span>Week</span><br/>\n            </p>\n          </div>\n          <div class=\"g-content\">\n            <div class=\"g-lines\">\n              <div class=\"g-line\">\n                <div class=\"g-line-info\">\n                  <p class=\"g-line-name\">+18.1<sup data-notes=\'bevacizumab_02 ranibizumab_18\'></sup></p>\n                </div>\n              </div>\n              <div class=\"g-line\">\n                <div class=\"g-line-info\">\n                  <p class=\"g-line-name\">+16.1<sup data-notes=\'bevacizumab_18\'></sup></p>\n                </div>\n              </div>\n              <div class=\"g-line\">\n                <div class=\"g-line-info\">\n                  <p class=\"g-line-name\">+13.3</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"g-legend-additional\">\n              <div class=\"skew caption\">\n                <p><b>Mean change in BCVA averaged</b> <br><span>through Year 2 (AUC)<sup data-reference-id=\'jampol\'></sup></span></p>\n              </div>\n              <div class=\"legend-content\">\n                <li class=\"element-blue\">\n                  <div class=\"skew\">\n                    <p><b>+17.1<sup data-notes=\'bevacizumab_001 ranibizumab_009\'></sup></b> EYLEA<sup>&reg;</sup></p>\n                  </div>\n                </li>\n                <li class=\"element-gray\">\n                  <div class=\"skew\">\n                    <p><b>+13.6</b> Ranibizumab</p>\n                  </div>\n                </li>\n                <li class=\"element-light-gray\">\n                  <div class=\"skew\">\n                    <p><b>+12.1</b> Bevacizumab</p>\n                  </div>\n                </li>\n              </div>\n            </div>\n          </div>\n        </figure>\n      </div>\n      <ul class=\"g-legend\">\n        <li class=\"legend-blue\"><strong>EYLEA<sup>&reg;</sup> 2 mg; n=98.</strong><sup data-reference-id=\'wells\'></sup></li>\n        <li class=\"legend-gray\">Ranibizumab 0.3 mg; <strong>n=94.</strong><sup data-reference-id=\'wells\'></sup></li>\n        <li class=\"legend-light-gray\">Bevacizumab 1.25 mg; <strong>n=92.</strong><sup data-reference-id=\'wells\'></sup></li>\n      </ul>\n    </figure>\n    <p class=\"footnote\">Treatments administered every month (4 weeks) unless vision &ge;20/20 with &lt;250 μm on Zeiss Stratus equivalent and no improvement or worsening in response <br>to the previous 2 injections, for the first 20 weeks; thereafter injection was withheld if stable (see study design page for definitions); <strong>Protocol T n=224.</strong><sup data-reference-id=\'diabetic\'></sup></p>\n  </div>\n</article>");
app.cache.put("slides/StayStrong40B1PopupSlide/StayStrong40B1PopupSlide.html","\n<article class=\"slide\" id=\"StayStrong40B1PopupSlide\">\n  <!--p.lead-paragraph !{slideshow_headline}-->\n  <h1><mark>EYLEA<sup>&reg;</sup></mark>: MEAN CHANGE IN CST THROUGH WEEK 148<sup data-reference-id=\'heier\'></sup></h1>\n  <figure class=\"figure-fragment graph-fragment top-scale\">\n    <figcaption><span class=\"pointer-label\"></span></figcaption>\n    <div class=\"content-wrapper\">\n      <figure class=\"graph\">\n        <div class=\"g-scale g-scale-v\">\n          <ul>\n            <li><span>-250</span></li>\n            <li><span>-200</span></li>\n            <li><span>-150</span></li>\n            <li><span>-100</span></li>\n            <li><span>-50</span></li>\n            <li><span>0</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>&micro;m</span><br/>\n          </p>\n        </div>\n        <div class=\"g-scale g-scale-h\">\n          <ul>\n            <li><span>0</span></li>\n            <li><span>4</span></li>\n            <li><span>12</span></li>\n            <li><span>20</span></li>\n            <li><span>52</span></li>\n            <li><span>100</span></li>\n            <li><span>148</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>Week</span><br/>\n          </p>\n        </div>\n        <div class=\"g-content\">\n          <div class=\"graph-blocks\">\n          </div>\n          <div class=\"g-lines\">\n            <div class=\"g-line\">\n              <div class=\"g-line-info\">\n                <div class=\"skew element-gray small-value g-line-value\">\n                  <p>–122.6</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"g-line\">\n              <div class=\"g-line-info\">\n                <div class=\"skew element-blue g-line-value\">\n                  <p>–202.8</p>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </figure>\n    </div>\n    <ul class=\"g-legend\">\n      <li class=\"legend-blue\"><strong>EYLEA<sup>&reg;</sup> 2 mg</strong> administered every month (4 weeks) for the first 5 months, followed by once every 2 months (8 weeks); <strong>n=135.</strong><sup data-reference-id=\'heier\'></sup></li>\n      <li class=\"legend-gray\"><strong>Laser</strong> photocoagulation administered as needed, no more often than every 12 weeks; <strong>n=132.</strong><sup data-reference-id=\'heier\'></sup></li>\n    </ul>\n  </figure>\n</article>");
app.cache.put("slides/StayStrong40C1PopupSlide/StayStrong40C1PopupSlide.html","\n<article class=\"slide\" id=\"StayStrong40C1PopupSlide\">\n  <!--p.lead-paragraph !{lead_text}-->\n  <h1>TREATMENT EXTENSION WITH <mark>EYLEA<sup>&reg;</sup></mark> CAN MAINTAIN VISION<br> AFTER THE INITIAL MONTHLY DOSING PHASE<sup data-reference-id=\'heier\'></sup></h1>\n  <figure class=\"figure-fragment graph-fragment\">\n    <figcaption>Mean change in BCVA though Week 148<sup data-reference-id=\'heier\'></sup><span class=\"pointer-label\"></span></figcaption>\n    <div class=\"content-wrapper\">\n      <figure class=\"graph\">\n        <div class=\"g-scale g-scale-v\">\n          <ul>\n            <li><span>0</span></li>\n            <li><span>2</span></li>\n            <li><span>4</span></li>\n            <li><span>6</span></li>\n            <li><span>8</span></li>\n            <li><span>10</span></li>\n            <li><span>12</span></li>\n            <li><span>14</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>Mean change in BCVA</span><br/><span>ETDRS letters</span><br/>\n          </p>\n        </div>\n        <div class=\"g-scale g-scale-h\">\n          <ul>\n            <li><span>0</span></li>\n            <li><span>4</span></li>\n            <li><span>12</span></li>\n            <li><span>20</span></li>\n            <li><span>52</span></li>\n            <li><span>100</span></li>\n            <li><span>148</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>Week</span><br/>\n          </p>\n        </div>\n        <div class=\"g-content\">\n          <div class=\"graph-blocks\">\n            <div class=\"block\">\n              <p>2q8 extension</p>\n            </div>\n          </div>\n          <div class=\"g-lines\">\n            <div class=\"g-line\">\n              <div class=\"g-line-info\">\n                <div class=\"skew element-blue g-line-value\">\n                  <p>+10.5</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"g-line\">\n              <div class=\"g-line-info\">\n                <div class=\"skew element-gray small-value g-line-value\">\n                  <p>+1.4</p>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </figure>\n    </div>\n    <ul class=\"g-legend\">\n      <li class=\"legend-blue\"><strong>EYLEA<sup>&reg;</sup> 2 mg</strong> administered every month (4 weeks) for the first 5 months, followed by once every 2 months (8 weeks); <strong>n=152.</strong><sup data-reference-id=\'heier\'></sup></li>\n      <li class=\"legend-gray\"><sup data-reference-id=\'heier\'></sup><strong>Laser</strong> photocoagulation administered as needed, no more often than every 12 weeks; <strong>n=154.</strong><sup data-reference-id=\'heier\'></sup></li>\n    </ul>\n  </figure>\n</article>");
app.cache.put("slides/StayStrong40C2PopupSlide/StayStrong40C2PopupSlide.html","\n<article class=\"slide\" id=\"StayStrong40C2PopupSlide\">\n  <!--p.lead-paragraph !{lead_text}-->\n  <h1><mark>EYLEA<sup>&reg;</sup></mark>: MEAN CHANGE IN CST THROUGH WEEK 148<sup data-reference-id=\'heier\'></sup></h1>\n  <figure class=\"figure-fragment graph-fragment top-scale\">\n    <figcaption><span class=\"pointer-label\"></span></figcaption>\n    <div class=\"content-wrapper\">\n      <figure class=\"graph\">\n        <div class=\"g-scale g-scale-v\">\n          <ul>\n            <li><span>-250</span></li>\n            <li><span>-200</span></li>\n            <li><span>-150</span></li>\n            <li><span>-100</span></li>\n            <li><span>-50</span></li>\n            <li><span>0</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>μm</span><br/>\n          </p>\n        </div>\n        <div class=\"g-scale g-scale-h\">\n          <ul>\n            <li><span>0</span></li>\n            <li><span>4</span></li>\n            <li><span>12</span></li>\n            <li><span>20</span></li>\n            <li><span>52</span></li>\n            <li><span>100</span></li>\n            <li><span>148</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>Week</span><br/>\n          </p>\n        </div>\n        <div class=\"g-content\">\n          <div class=\"g-lines\">\n            <div class=\"g-line\">\n              <div class=\"g-line-info\">\n                <div class=\"skew element-gray small-value g-line-value\">\n                  <p>–109.8</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"g-line\">\n              <div class=\"g-line-info\">\n                <div class=\"skew element-blue g-line-value\">\n                  <p>–190.1</p>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </figure>\n    </div>\n    <ul class=\"g-legend\">\n      <li class=\"legend-blue\"><strong>EYLEA<sup>&reg;</sup> 2 mg</strong> administered every month (4 weeks) for the first 5 months, followed by once every 2 months (8 weeks); <strong>n=152.</strong><sup data-reference-id=\'heier\'></sup></li>\n      <li class=\"legend-gray\"><sup data-reference-id=\'heier\'></sup><strong>Laser</strong> photocoagulation administered as needed, no more often than every 12 weeks; <strong>n=154.</strong><sup data-reference-id=\'heier\'></sup></li>\n    </ul>\n  </figure>\n</article>");
app.cache.put("slides/StayStrong40Slide/StayStrong40Slide.html","\n<article class=\"slide\" id=\"StayStrong40Slide\">\n  <h1>TREATMENT EXTENSION WITH EYLEA<sup>&reg;</sup><br> CAN MAINTAIN VISION AFTER THE INITIAL<br>MONTHLY DOSING PHASE<sup data-reference-id=\'heier\'></sup></h1>\n  <!--h2 !{sub_headline}-->\n  <!--h3 !{lead_paragraph}-->\n  <figure class=\"figure-fragment graph-fragment\">\n    <figcaption>Mean change in BCVA through Week 148<sup data-reference-id=\'heier\'></sup><span class=\"pointer-label\"></span></figcaption>\n    <div class=\"content-wrapper\">\n      <figure class=\"graph\">\n        <div class=\"g-scale g-scale-v\">\n          <ul>\n            <li><span>0</span></li>\n            <li><span>2</span></li>\n            <li><span>4</span></li>\n            <li><span>6</span></li>\n            <li><span>8</span></li>\n            <li><span>10</span></li>\n            <li><span>12</span></li>\n            <li><span>14</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>Mean change in BCVA</span><br/><span>ETDRS letters</span><br/>\n          </p>\n        </div>\n        <div class=\"g-scale g-scale-h\">\n          <ul>\n            <li><span>0</span></li>\n            <li><span>4</span></li>\n            <li><span>12</span></li>\n            <li><span>20</span></li>\n            <li><span>52</span></li>\n            <li><span>100</span></li>\n            <li><span>148</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>Week</span><br/>\n          </p>\n        </div>\n        <div class=\"g-content\">\n          <div class=\"graph-blocks\">\n            <div class=\"block\">\n              <p>Initial monthly <br>dosing</p>\n            </div>\n            <div class=\"block\">\n              <p>Extension</p>\n            </div>\n          </div>\n          <div class=\"g-lines\">\n            <div class=\"g-line\">\n              <div class=\"g-line-info\">\n                <div class=\"skew element-blue g-line-value\">\n                  <p>+11.7</p>\n                  <div class=\"skew g-line-additional\">\n                    <p>EYLEA<sup>&reg;</sup> 2q8</p>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"g-line\">\n              <div class=\"g-line-info\">\n                <div class=\"skew element-light-gray g-line-value\">\n                  <p>+1.6</p>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class=\"g-legends\">\n            <div class=\"g-legend-extended first\">\n              <div class=\"skew caption\">\n                <p class=\"left\"><b>33<sub>%</sub></b></p>\n                <p class=\"right\">Patients<br> achieving<br><b>&ge;3-line gains</b><sup data-reference-id=\'korobelnik\'></sup></p>\n              </div>\n              <div class=\"legend-content\">\n                <div class=\"top\">\n                  <div class=\"skew\">\n                    <p><b>Week 52</b></p>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"g-legend-extended second\">\n              <div class=\"skew caption\">\n                <p class=\"left\"><b>31<sub>%</sub></b></p>\n                <p class=\"right\">Patients<br> achieving<br><b>&ge;3-line gains</b><sup data-reference-id=\'korobelnik,brown\'></sup></p>\n              </div>\n              <div class=\"legend-content\">\n                <div class=\"top\">\n                  <div class=\"skew\">\n                    <p><b>Week 100</b></p>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"g-legend-extended third\">\n              <div class=\"skew caption\">\n                <p class=\"left\"><b>42<sub>%</sub></b></p>\n                <p class=\"right\">Patients<br> achieving<br><b>&ge;3-line gains</b><sup data-reference-id=\'heier,brown\'></sup></p>\n              </div>\n              <div class=\"legend-content\">\n                <div class=\"top\">\n                  <div class=\"skew\">\n                    <p><b>Week 148</b></p>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class=\"control-buttons\">\n            <button class=\"arrow-btn state-btn plus\" data-goto-state=\"first\"></button>\n            <button class=\"arrow-btn state-btn plus\" data-goto-state=\"second\"></button>\n            <button class=\"arrow-btn state-btn plus\" data-goto-state=\"third\"></button>\n          </div>\n        </div>\n      </figure>\n    </div>\n    <ul class=\"g-legend\">\n      <li class=\"legend-blue\"><strong>EYLEA<sup>&reg;</sup> 2 mg</strong> administered every month (4 weeks) for the first 5 months, followed by once every 2 months (8 weeks); n=135.<sup data-reference-id=\'heier\'></sup></li>\n      <li class=\"legend-light-gray\"><strong>Laser</strong> photocoagulation administered as needed, no more often than every 12 weeks; n=132.<sup data-reference-id=\'heier\'></sup></li>\n    </ul>\n  </figure>\n  <div class=\"wrap-open-popup\">\n    <p>Additional endpoints</p>\n    <button class=\"arrow-btn open-popup\" data-viewer=\"slideshow\" href=\"stay_strong_40b_inline\" slide=\"StayStrong40B1PopupSlide\"></button>\n    <p>VISTA DME</p>\n    <button class=\"arrow-btn open-popup\" data-viewer=\"slideshow\" href=\"stay_strong_40c_inline\" slide=\"StayStrong40C1PopupSlide\"></button>\n    <p>Protocol T</p>\n    <button class=\"arrow-btn open-popup\" data-viewer=\"slideshow\" href=\"stay_strong_40a_inline\" slide=\"StayStrong40A1PopupSlide\"></button>\n  </div>\n</article>");
app.cache.put("slides/StayStrong41A1PopupSlide/StayStrong41A1PopupSlide.html","\n<article class=\"slide\" id=\"StayStrong41A1PopupSlide\">\n  <h1 class=\"obliqueTile\"><mark>EYLEA<sup>&reg;</sup></mark>: ADMINISTRATION SCHEDULE IN FIRST YEAR</h1>\n  <div class=\"content\">\n    <h2 class=\"sub-header\">The recommended dose of EYLEA<sup>&reg;</sup> for DME is 2 mg, equivalent to 50 &micro;l,<br> administered with 5 initial monthly doses followed by one injection<br> every two months<sup data-reference-id=\'eylea\'></sup><sup>,</sup><sup data-notes=\'no_requirement\'></sup></h2>\n    <figure>\n      <p class=\"product-name\">EYLEA<sup>&reg;</sup> 2 mg</p>\n      <div class=\"graph\">\n        <div class=\"items\">\n          <div class=\"item-image\">\n            <p>1</p>\n          </div>\n          <div class=\"item-image\">\n            <p>2</p>\n          </div>\n          <div class=\"item-image\">\n            <p>3</p>\n          </div>\n          <div class=\"item-image\">\n            <p>4</p>\n          </div>\n          <div class=\"item-image\">\n            <p>5</p>\n          </div>\n          <div class=\"item-text\">\n            <div class=\"arrow-wrap\">\n              <div class=\"arrow-box\">\n                <p>EXTEND</p>\n              </div>\n              <div class=\"tr\"></div>\n            </div>\n            <p>6</p>\n          </div>\n          <div class=\"item-image\">\n            <p>7</p>\n          </div>\n          <div class=\"item-image\">\n            <p>8</p>\n          </div>\n          <div class=\"item-image\">\n            <p>9</p>\n          </div>\n          <div class=\"item-image\">\n            <p>10</p>\n          </div>\n          <div class=\"item-image\">\n            <p>11</p>\n          </div>\n          <div class=\"item-image\">\n            <p>12</p>\n          </div>\n        </div>\n      </div>\n      <div class=\"bottom-scale\">\n        <p class=\"caption\">Months</p>\n        <div class=\"handler\"></div>\n      </div>\n      <div class=\"addition-items\">\n        <p class=\"bottom-item\">INITIAL MONTHLY DOSING</p>\n        <p class=\"bottom-item\">EXTEND TO ONCE EVERY 2 MONTHS DOSING<sup data-notes=\'no_requirement\'></sup></p>\n      </div>\n    </figure>\n  </div>\n</article>");
app.cache.put("slides/StayStrong41A2PopupSlide/StayStrong41A2PopupSlide.html","\n<article class=\"slide\" id=\"StayStrong41A2PopupSlide\">\n  <h1>Treat-and-extend LABELING with <strong>EYLEA<sup>&reg;</sup></strong></h1>\n  <div class=\"content\">\n    <p class=\"lead-paragraph\">The recommended dose of EYLEA<sup>&reg;</sup> for DME is 2 <span>mg</span>, equivalent to 50 <span>&mu;l</span>, administered with 5 initial monthly doses followed by one injection every two months<sup data-reference-id=\'eylea\'></sup></p>\n    <div class=\"label-wrapper\">\n      <div class=\"label\">\n        <p class=\"top-caption\">LABEL UPDATE</p>\n        <p class=\"main-caption\">Treat-and-extend dosing now approved for EYLEA<sup>&reg;</sup> in DME</p>\n        <p class=\"bottom-caption\">by the European Medicines Agency<sup data-reference-id=\'eylea\'></sup></p>\n      </div>\n    </div>\n    <p class=\"summary\">\"After the first 12 months of treatment with EYLEA<sup>&reg;</sup>, and based on visual and/or anatomic outcomes, the treatment interval may be extended, such as with a treat-and-extend dosing regimen, where the treatment intervals are gradually increased to maintain stable visual and/or anatomic outcomes\"</p>\n    <p class=\"note\">EYLEA<sup>&reg;</sup> Summary of Product Characteristics<sup data-reference-id=\'eylea\'></sup></p>\n  </div>\n</article>");
app.cache.put("slides/StayStrong41A3PopupSlide/StayStrong41A3PopupSlide.html","\n<article class=\"products-line slide\" id=\"StayStrong41A3PopupSlide\">\n  <h1><mark>EYLEA<sup>&reg;</sup></mark>: PROACTIVE DOSING AVAILABLE AFTER YEAR 1,<br> SUCH AS TREAT-AND-EXTEND<sup data-reference-id=\'eylea\'></sup></h1>\n  <div class=\"content\">\n    <h2 class=\"sub-header\">After the first 12 months of treatment with EYLEA<sup>&reg;</sup> the treatment interval may be extended, such<br> as with a treat-and-extend dosing regimen, where the treatment intervals are gradually increased<br> to maintain stable visual and/or anatomic outcomes.<sup data-reference-id=\'eylea\'></sup></h2>\n    <figure>\n      <div class=\"description\">\n        <p class=\"product-name\"><mark>EXAMPLE 1</mark><br> Dosing interval<br> extended<sup data-notes=\'schedule_monitoring\'></sup></p>\n        <p class=\"caption\">BEYOND 52 WEEKS</p>\n      </div>\n      <div class=\"graph\">\n        <div class=\"items\">\n          <div class=\"item-text\">\n            <div class=\"arrow-wrap\">\n              <div class=\"arrow-box\">\n                <p>EXTEND TO<br> <strong>10 WEEKS</strong></p>\n              </div>\n              <div class=\"tr\"></div>\n            </div>\n          </div>\n          <div class=\"item-image\">\n            <p class=\"top-text\">If visual and anatomic<br> response maintained, proactively<br> extend by 2-week increments</p>\n            <p class=\"bottom-text\">INJECT<br> <mark>AND EXTEND</mark></p>\n          </div>\n          <div class=\"item-text\">\n            <div class=\"arrow-wrap\">\n              <div class=\"arrow-box\">\n                <p>EXTEND TO<br> <strong>12 WEEKS</strong></p>\n              </div>\n              <div class=\"tr\"></div>\n            </div>\n          </div>\n          <div class=\"item-image\">\n            <p class=\"top-text\">Visual and anatomic<br> response maintained</p>\n            <p class=\"bottom-text\">INJECT</p>\n          </div>\n          <div class=\"item-text\">\n            <div class=\"arrow-wrap\">\n              <div class=\"arrow-box\">\n                <p>Further treatment is<br> based on visual and/or<br> anatomic outcomes<sup data-notes=\'schedule_monitoring\'></sup></p>\n              </div>\n              <div class=\"tr\"></div>\n            </div>\n          </div>\n        </div>\n        <ul class=\"dashed-lines\">\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n        </ul>\n      </div>\n    </figure>\n  </div>\n</article>");
app.cache.put("slides/StayStrong41A4PopupSlide/StayStrong41A4PopupSlide.html","\n<article class=\"slide products-line\" id=\"StayStrong41A4PopupSlide\">\n  <h1><mark>EYLEA<sup>&reg;</sup></mark>: PROACTIVE DOSING AVAILABLE AFTER YEAR 1,<br> SUCH AS TREAT-AND-EXTEND<sup data-reference-id=\'eylea\'></sup></h1>\n  <div class=\"content\">\n    <h2 class=\"sub-header\">After the first 12 months of treatment with Eylea<sup>&reg;</sup>, the treatment interval may be extended, such<br> as with a treat-and-extend dosing regimen, where the treatment intervals are gradually increased<br> to maintain stable visual and/or anatomic outcomes<sup data-reference-id=\'eylea\'></sup></h2>\n    <figure>\n      <div class=\"description\">\n        <p class=\"product-name\"><mark>EXAMPLE 2</mark><br> Dosing interval<br> extended then<br> reduced<sup data-notes=\'schedule_monitoring\'></sup></p>\n        <p class=\"caption\">BEYOND 52 WEEKS</p>\n      </div>\n      <div class=\"graph\">\n        <div class=\"items\">\n          <div class=\"item-text\">\n            <div class=\"arrow-wrap\">\n              <div class=\"arrow-box\">\n                <p>EXTEND TO<br> <strong>10 WEEKS</strong></p>\n              </div>\n              <div class=\"tr\"></div>\n            </div>\n          </div>\n          <div class=\"item-image\">\n            <p class=\"top-text\">If visual and anatomic <br>response maintained, proactively<br> extend by 2-week increments</p>\n            <p class=\"bottom-text\">INJECT<br> <mark>AND EXTEND</mark></p>\n          </div>\n          <div class=\"item-text\">\n            <div class=\"arrow-wrap\">\n              <div class=\"arrow-box\">\n                <p>EXTEND TO<br> <strong>12 WEEKS</strong></p>\n              </div>\n              <div class=\"tr\"></div>\n            </div>\n          </div>\n          <div class=\"item-image\">\n            <p class=\"top-text\">If disease activity is<br> identified, decrease by<br> 2-week increment</p>\n            <p class=\"bottom-text\">INJECT</p>\n          </div>\n          <div class=\"item-text\">\n            <div class=\"arrow-wrap\">\n              <div class=\"arrow-box\">\n                <p>REDUCE TO<br> <strong>10 WEEKS</strong></p>\n              </div>\n              <div class=\"tr\"></div>\n            </div>\n          </div>\n          <div class=\"item-image\">\n            <p class=\"top-text\">Disease stable</p>\n            <p class=\"bottom-text\">INJECT</p>\n          </div>\n          <div class=\"item-text\">\n            <div class=\"arrow-wrap\">\n              <div class=\"arrow-box\">\n                <p>CONTINUE<br> WITH<br> <strong>10-WEEK<br> INTERVAL<sup data-notes=\'schedule_monitoring\'></sup></strong></p>\n              </div>\n              <div class=\"tr\"></div>\n            </div>\n          </div>\n          <div class=\"item-text\">\n            <div class=\"arrow-wrap\">\n              <div class=\"arrow-box\">\n                <p>Further treatment<br>is based on visual<br>and/or anatomic<br>outcomes</p>\n              </div>\n              <div class=\"tr\"></div>\n            </div>\n          </div>\n        </div>\n        <ul class=\"dashed-lines\">\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n          <li></li>\n        </ul>\n      </div>\n    </figure>\n  </div>\n</article>");
app.cache.put("slides/StayStrong41Slide/StayStrong41Slide.html","\n<article class=\"slide\" id=\"StayStrong41Slide\">\n  <h1>PROACTIVE DOSING WITH EYLEA<sup>&reg;</sup></h1>\n  <h2></h2>\n  <div class=\"content-wrapper\">\n    <figure class=\"graph-wrapper\">\n      <div class=\"row labels-wrapper\">\n        <div class=\"column\">\n          <div class=\"skew\">\n            <p>Beyond 52 Weeks</p>\n          </div>\n        </div>\n        <div class=\"column\">\n          <div class=\"skew\">\n            <p>First 52 Weeks</p>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"arrow-wrapper\">\n          <p>Treat-and-extend</p>\n        </div>\n        <div class=\"arrow-wrapper\">\n          <p>Extend to <br>every 2 months <small>until 52 weeks</small></p>\n        </div>\n        <div class=\"arrow-wrapper\">\n          <p>5 monthly <small>injections</small></p>\n        </div>\n      </div>\n      <!--figcaption.oblique-tile//p \n      -->\n    </figure>\n    <div class=\"wrap-open-popup\">\n      <p>Dosing schedule</p>\n      <button class=\"arrow-btn open-popup\" data-viewer=\"slideshow\" href=\"stay_strong_41a_inline\" slide=\"StayStrong41A1PopupSlide\"></button>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/UniqueMoA50A1PopupSlide/UniqueMoA50A1PopupSlide.html","\n<article class=\"slide\" id=\"UniqueMoA50A1PopupSlide\">\n  <h1>THE PATHOGENESIS OF EARLY-TO-LATE DR AND DME IS LINKED<br> TO THE PRESENCE OF ELEVATED LEVELS OF VEGF<sup data-reference-id=\'kovacs,bhagat\'></sup> AND PGF<sup data-reference-id=\'kovacs,ando,rakic\'></sup></h1>\n  <figure class=\"figure-fragment graph-fragment diagram-graph\">\n    <figcaption>Vitreous biomarker concentrations<sup data-reference-id=\'kovacs\'></sup><span class=\"pointer-label\"></span></figcaption>\n    <div class=\"content-wrapper\">\n      <figure class=\"graph\">\n        <div class=\"g-scale g-scale-v\">\n          <ul>\n            <li><span>0</span></li>\n            <li><span>100</span></li>\n            <li><span>200</span></li>\n            <li><span>300</span></li>\n            <li><span>400</span></li>\n            <li><span>500</span></li>\n            <li><span>600</span></li>\n            <li><span>700</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>Mean concentration (pg/ml)</span><br/>\n          </p>\n        </div>\n        <div class=\"g-scale g-scale-h\">\n          <ul>\n            <li class=\"g-hide\"><span>0</span></li>\n            <li class=\"g-hide\"><span>1</span></li>\n            <li class=\"g-hide\"><span>2</span></li>\n            <li class=\"g-hide\"><span>3</span></li>\n            <li class=\"g-hide\"><span>4</span></li>\n            <li class=\"g-hide\"><span>5</span></li>\n            <li class=\"g-hide\"><span>6</span></li>\n            <li class=\"g-hide\"><span>7</span></li>\n          </ul>\n        </div>\n        <div class=\"g-content\">\n          <div class=\"g-bar-nest\">\n            <div class=\"g-bar gray-bar\">\n              <p class=\"g-bar-value\">36.0</p>\n            </div>\n            <div class=\"g-bar blue-bar\">\n              <p class=\"g-bar-value\">260.8</p>\n            </div>\n            <p class=\"g-nest-name\">VEGF-A</p>\n          </div>\n          <div class=\"g-bar-nest\">\n            <div class=\"g-bar gray-bar\">\n              <p class=\"g-bar-value\">73.4</p>\n            </div>\n            <div class=\"g-bar blue-bar\">\n              <p class=\"g-bar-value\">100.9</p>\n            </div>\n            <p class=\"g-nest-name\">VEGF-C</p>\n          </div>\n          <div class=\"g-bar-nest\">\n            <div class=\"g-bar gray-bar\">\n              <p class=\"g-bar-value\">59.0</p>\n            </div>\n            <div class=\"g-bar blue-bar\">\n              <p class=\"g-bar-value\">87.0</p>\n            </div>\n            <p class=\"g-nest-name\">VEGF-D</p>\n          </div>\n          <div class=\"g-bar-nest\">\n            <div class=\"g-bar gray-bar\">\n              <p class=\"g-bar-value\">3.5</p>\n            </div>\n            <div class=\"g-bar blue-bar\">\n              <p class=\"g-bar-value\">13.8</p>\n            </div>\n            <p class=\"g-nest-name\">PGF</p>\n          </div>\n          <div class=\"g-bar-nest\">\n            <div class=\"g-bar gray-bar\">\n              <p class=\"g-bar-value\">17.3</p>\n            </div>\n            <div class=\"g-bar blue-bar\">\n              <p class=\"g-bar-value\">579.7</p>\n            </div>\n            <p class=\"g-nest-name\">IL-6</p>\n          </div>\n          <div class=\"g-bar-nest\">\n            <div class=\"g-bar gray-bar\">\n              <p class=\"g-bar-value\">5.7</p>\n            </div>\n            <div class=\"g-bar blue-bar\">\n              <p class=\"g-bar-value\">56.4</p>\n            </div>\n            <p class=\"g-nest-name\">IL-8</p>\n          </div>\n          <div class=\"g-bar-nest\">\n            <div class=\"g-bar gray-bar\">\n              <p class=\"g-bar-value\">7.6</p>\n            </div>\n            <div class=\"g-bar blue-bar\">\n              <p class=\"g-bar-value\">11.7</p>\n            </div>\n            <p class=\"g-nest-name\">IL-18</p>\n          </div>\n        </div>\n      </figure>\n    </div>\n    <ul class=\"g-legend\">\n      <li class=\"legend-blue\">DME</li>\n      <li class=\"legend-gray\">Non-diabetic retinal disease</li>\n    </ul>\n  </figure>\n  <div class=\"footnote\">The pathogenesis of DME differs substantially from other retinal diseases, with increased levels of VEGF, PGF and inflammatory cytokines observed in patients with DME<sup data-reference-id=\'kovacs\'></sup></div>\n</article>");
app.cache.put("slides/UniqueMoA50A2PopupSlide/UniqueMoA50A2PopupSlide.html","\n<article class=\"slide unique-moa-scheme\" id=\"UniqueMoA50A2PopupSlide\">\n  <h1>IN DME, INFLAMMATION AND VASCULAR COMPLICATIONS REDUCE RETINAL <br>BLOOD FLOW, LEADING TO HYPOXIA AND UPREGULATION OF VEGF AND PGF<sup data-reference-id=\'kovacs-rakic\'></sup></h1>\n  <div class=\"scheme-wrapper state1\">\n    <div class=\"row-item\">\n      <p class=\"row-text\">DIABETES</p>\n    </div>\n    <div class=\"row-item\">\n      <p class=\"row-text\">IMPAIRED VASCULAR FUNCTION IMPAIRED RETINAL BLOOD FLOW</p>\n    </div>\n    <div class=\"row-item\">\n      <p class=\"row-text\">HYPOXIA</p>\n    </div>\n    <div class=\"row-wrapper\">\n      <div class=\"row-item\">\n        <p class=\"row-text\">NEOVASCULARIZATION</p>\n      </div>\n      <div class=\"row-item\">\n        <p class=\"row-text\">INFLAMMATORY CYTOKINES</p>\n      </div>\n    </div>\n    <div class=\"row-item\">\n      <p class=\"row-text\">VESSEL LEAKAGE AND EDEMA</p>\n    </div>\n    <div class=\"additional-wrapper\">\n      <figure>\n        <figcaption>VEGF</figcaption>\n      </figure>\n      <figure>\n        <figcaption>PGF</figcaption>\n      </figure>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/UniqueMoA50A3PopupSlide/UniqueMoA50A3PopupSlide.html","\n<article class=\"slide unique-moa-scheme\" id=\"UniqueMoA50A3PopupSlide\">\n  <h1>ACTIVATION OF VEGFR-1 AND VEGFR-2 RECEPTORS BY VEGF AND PGF <br>PROMOTES ANGIOGENESIS AND NEOVASCULARIZATION<sup data-reference-id=\'kovacs,bhagat\'></sup></h1>\n  <div class=\"scheme-wrapper state2\">\n    <div class=\"row-item\">\n      <p class=\"row-text\">DIABETES</p>\n    </div>\n    <div class=\"row-item\">\n      <p class=\"row-text\">IMPAIRED VASCULAR FUNCTION IMPAIRED RETINAL BLOOD FLOW</p>\n    </div>\n    <div class=\"row-item\">\n      <p class=\"row-text\">HYPOXIA</p>\n    </div>\n    <div class=\"row-wrapper\">\n      <div class=\"row-item\">\n        <p class=\"row-text\">NEOVASCULARIZATION</p>\n      </div>\n      <div class=\"row-item\">\n        <p class=\"row-text\">INFLAMMATORY CYTOKINES</p>\n      </div>\n    </div>\n    <div class=\"row-item\">\n      <p class=\"row-text\">VESSEL LEAKAGE AND EDEMA</p>\n    </div>\n    <div class=\"additional-wrapper\">\n      <figure>\n        <figcaption>VEGF</figcaption>\n      </figure>\n      <figure>\n        <figcaption>PGF</figcaption>\n      </figure>\n      <p class=\"additional-text\">VEGFR-1, VEGFR-2<br><span>in vascular endothelium</span></p>\n    </div>\n    <div class=\"state-wrapper\">\n      <div class=\"top-texts\">\n        <p class=\"state-text\">PGF</p>\n        <p class=\"state-text\">VEGF</p>\n      </div>\n      <div class=\"middle-texts\">\n        <p class=\"mid-text\">VEGFR-1</p>\n        <p class=\"mid-text\">VEGFR-2</p>\n      </div>\n      <p class=\"bottom-text\">Angiogenesis and <br>neovascularization</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/UniqueMoA50A4PopupSlide/UniqueMoA50A4PopupSlide.html","\n<article class=\"slide unique-moa-scheme\" id=\"UniqueMoA50A4PopupSlide\">\n  <h1>PGF ALSO RECRUITS IMMUNE CELLS WHICH RELEASE INFLAMMATORY CYTOKINES, <br>AMPLIFYING THE EFFECTS OF VEGF AND PGF IN THE RETINA<sup data-reference-id=\'bhagat\'></sup></h1>\n  <div class=\"scheme-wrapper state3\">\n    <div class=\"row-item\">\n      <p class=\"row-text\">DIABETES</p>\n    </div>\n    <div class=\"row-item\">\n      <p class=\"row-text\">IMPAIRED VASCULAR FUNCTION IMPAIRED RETINAL BLOOD FLOW</p>\n    </div>\n    <div class=\"row-item\">\n      <p class=\"row-text\">HYPOXIA</p>\n    </div>\n    <div class=\"row-wrapper\">\n      <div class=\"row-item\">\n        <p class=\"row-text\">NEOVASCULARIZATION</p>\n      </div>\n      <div class=\"row-item\">\n        <p class=\"row-text\">INFLAMMATORY CYTOKINES</p>\n      </div>\n    </div>\n    <div class=\"row-item\">\n      <p class=\"row-text\">VESSEL LEAKAGE AND EDEMA</p>\n    </div>\n    <div class=\"additional-wrapper\">\n      <figure>\n        <figcaption>VEGF</figcaption>\n      </figure>\n      <figure>\n        <figcaption>PGF</figcaption>\n      </figure>\n      <p class=\"additional-text\">VEGFR-1<br><span>in immune cells</span></p>\n    </div>\n    <div class=\"state-wrapper\">\n      <div class=\"top-texts\">\n        <p class=\"state-text\">PGF</p>\n      </div>\n      <div class=\"middle-texts\">\n        <p class=\"mid-text\">VEGFR-1</p>\n      </div>\n      <p class=\"bottom-text\">Increased <br>inflammatory <br>cytokines</p>\n    </div>\n  </div>\n</article>");
app.cache.put("slides/UniqueMoA50A5PopupSlide/UniqueMoA50A5PopupSlide.html","\n<article class=\"slide\" id=\"UniqueMoA50A5PopupSlide\">\n  <h1><mark>EYLEA<sup>&reg;</sup></mark> is a fusion protein designed to tightly bind or ‘trap’ VEGF and PGF<sup data-reference-id=\'eylea\'></sup></h1>\n  <figure>\n    <figcaption>EYLEA<sup>&reg;</sup> MOLECULE</figcaption>\n    <div class=\"figure-content\">\n      <div class=\"left-part\">\n        <p>VEGFR-2 domain<span class=\"line\"></span></p>\n        <p>Fc fragment of IgG<span class=\"line\"></span></p>\n        <p>VEGFR-1 domain<span class=\"line\"></span></p>\n      </div>\n      <div class=\"right-part\">\n        <div class=\"dimer-wrapper\">\n          <p>VEGF dimer</p>\n        </div>\n        <div class=\"dimer-wrapper\">\n          <p>PGF dimer</p>\n        </div>\n      </div>\n    </div>\n  </figure>\n  <div class=\"oblique-tile\">\n    <p>EYLEA<sup>&reg;</sup> BINDS MORE TIGHTLY TO VEGF THAN THE NATIVE VEGF RECEPTORS<sup data-reference-id=\'eylea\'></sup></p>\n  </div>\n</article>");
app.cache.put("slides/UniqueMoA50A6PopupSlide/UniqueMoA50A6PopupSlide.html","\n<article class=\"slide unique-moa-scheme\" id=\"UniqueMoA50A6PopupSlide\">\n  <h1><span>EYLEA<sup>&reg;</sup></span> INHIBITS VEGF AND PGF WITH GREATER BINDING AFFINITY THAN <br>THEIR NATURAL RECEPTORS, AND REDUCES DME DISEASE ACTIVITY<sup data-reference-id=\'eylea\'></sup></h1>\n  <div class=\"scheme-wrapper state4\">\n    <div class=\"row-item\">\n      <p class=\"row-text\">DIABETES</p>\n    </div>\n    <div class=\"row-item\">\n      <p class=\"row-text\">IMPAIRED VASCULAR FUNCTION IMPAIRED RETINAL BLOOD FLOW</p>\n    </div>\n    <div class=\"row-item\">\n      <p class=\"row-text\">HYPOXIA</p>\n    </div>\n    <div class=\"row-wrapper\">\n      <div class=\"row-item\">\n        <p class=\"row-text\">NEOVASCULARIZATION</p>\n      </div>\n      <div class=\"row-item\">\n        <p class=\"row-text\">INFLAMMATORY CYTOKINES</p>\n      </div>\n    </div>\n    <div class=\"row-item\">\n      <p class=\"row-text\">VESSEL LEAKAGE AND EDEMA</p>\n    </div>\n    <div class=\"additional-wrapper\">\n      <figure>\n        <figcaption></figcaption>\n      </figure>\n      <figure>\n        <figcaption></figcaption>\n      </figure>\n    </div>\n  </div>\n  <div class=\"oblique-tile\">\n    <p>REDUCED CENTRAL SUBFIELD THICKNESS AND IMPROVED VISUAL ACUITY</p>\n  </div>\n</article>");
app.cache.put("slides/UniqueMoA50B1PopupSlide/UniqueMoA50B1PopupSlide.html","\n<article class=\"slide\" id=\"UniqueMoA50B1PopupSlide\">\n  <h1>Anti-VEGF therapy vs DEXAMETHASONE INTRAVITREAL IMPLANT (DEX)</h1>\n  <dl class=\"chapter-criteria-list\">\n    <dt><span class=\"term-label\"></span>EFFICACY</dt>\n    <dd>Randomized clinical trials (MEAD, BEVORDEX and Callanan et al.) of DEX showed modest visual gains <br>of 3.5–5.6 letters and reductions in CRT of 107.9–187.0 μm<sup data-reference-id=\'boyer-callanan\'></sup></dd>\n    <dt><span class=\"term-label\"></span>DURABILITY</dt>\n    <dd>Treatment benefit with DEX appears to peak at about 2 months post-injection, earlier than the 6 month retreatment interval included in the product label<sup data-reference-id=\'boyer-ozurdex\'></sup></dd>\n    <dd>Peaks in response are aligned for functional, anatomic, and safety (intraocular pressure [IOP]) outcomes<sup data-reference-id=\'boyer\'></sup></dd>\n    <dt><span class=\"term-label\"></span>MOA</dt>\n    <dd>Compared with DEX, anti-VEGF therapy is more effective against VEGF, the predominant cytokine implicated in the pathogenesis of DME<sup data-reference-id=\'papadopoulos,kim\'></sup></dd>\n    <dd>EYLEA<sup>&reg;</sup> additionally blocks PGF, which can activate monocytes/inflammation and is involved in all stages <br>of diabetic eye disease<sup data-reference-id=\'eylea,papadopoulos\'></sup></dd>\n    <dt><span class=\"term-label\"></span>SAFETY</dt>\n    <dd>Incidence of AEs such as IOP elevation, cataract formation/progression, and endophthalmitis, appears to be greater in eyes treated with DEX than those treated with anti-VEGF therapy<sup data-reference-id=\'boyer-callanan\'></sup></dd>\n  </dl>\n  <p>DEX IS NOT RECOMMENDED AS A FIRST-LINE THERAPY FOR DME IN EUROPEAN TREATMENT GUIDELINES <br><span>(NICE Technical Appraisal Guidance 2015; Diabetic Macular Edema Treatment Guideline Working Group Perspective 2014)<sup data-reference-id=\'national,mitchell\'></sup></span></p>\n</article>");
app.cache.put("slides/UniqueMoA50B2PopupSlide/UniqueMoA50B2PopupSlide.html","\n<article class=\"slide\" id=\"UniqueMoA50B2PopupSlide\">\n  <h1 class=\"oblique-tile\">DEX HAS SHOWN MODEST GAINS IN BCVA ACROSS CLINICAL TRIALS<sup data-reference-id=\'boyer-callanan\'></sup></h1>\n  <p class=\"sub-header\">IN A POOLED ANALYSIS OF TWO PIVOTAL 3-YEAR PHASE 3 CLINICAL TRIALS<br> IN PATIENTS WITH DME, MEAN AVERAGE CHANGE IN BCVA WAS:<sup data-reference-id=\'boyer\'></sup></p>\n  <figure>\n    <div class=\"block block-left\">\n      <p class=\"header\">DEX 0.7 mg</p>\n      <p class=\"big-number gold\">3.5</p>\n      <div class=\"footer\">\n        <p>LETTERS</p>\n        <p>(n=351)<sup data-reference-id=\'boyer\'></sup></p>\n      </div>\n    </div>\n    <div class=\"block block-middle\">\n      <p><i>P</i>=0.023</p>\n    </div>\n    <div class=\"block block-right\">\n      <p class=\"header\">SHAM</p>\n      <p class=\"big-number gold\">2.0</p>\n      <div class=\"footer\">\n        <p>LETTERS</p>\n        <p>(n=350)<sup data-reference-id=\'boyer\'></sup></p>\n      </div>\n    </div>\n  </figure>\n</article>");
app.cache.put("slides/UniqueMoA50B3PopupSlide/UniqueMoA50B3PopupSlide.html","\n<article class=\"slide\" id=\"UniqueMoA50B3PopupSlide\">\n  <h1>AN OVERVIEW ACROSS MULTIPLE TRIALS SUGGESTS PEAK VISUAL ACUITY GAINS OCCUR ABOUT 2 MONTHS POST-INJECTION, FOLLOWED BY PROGRESSIVE DECLINE OVER TIME<sup data-reference-id=\'boyer-callanan\'></sup><sup>,</sup><sup data-notes=\'study_visits\'></sup></h1>\n  <figure class=\"figure-fragment graph-fragment\">\n    <figcaption>Mean change in BCVA from baseline<sup data-reference-id=\'boyer-callanan\'></sup><span class=\"pointer-label\"></span></figcaption>\n    <div class=\"content-wrapper\">\n      <figure class=\"graph\">\n        <div class=\"g-scale g-scale-v\">\n          <ul>\n            <li><span>0</span></li>\n            <li><span>1</span></li>\n            <li><span>2</span></li>\n            <li><span>3</span></li>\n            <li><span>4</span></li>\n            <li><span>5</span></li>\n            <li><span>6</span></li>\n            <li><span>7</span></li>\n            <li><span>8</span></li>\n            <li><span>9</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>Mean change</span><br/><span>in BCVA ETDRS letters</span><br/>\n          </p>\n        </div>\n        <div class=\"g-scale g-scale-h\">\n          <ul>\n            <li><span>0</span></li>\n            <li><span>0.5</span></li>\n            <li><span>1</span></li>\n            <li><span>1.5</span></li>\n            <li><span>2</span></li>\n            <li><span>2.5</span></li>\n            <li><span>3</span></li>\n            <li><span>3.5</span></li>\n            <li><span>4</span></li>\n            <li><span>4.5</span></li>\n            <li><span>5</span></li>\n            <li><span>5.5</span></li>\n            <li><span>6</span></li>\n          </ul>\n          <p class=\"g-scale-label\"><span>Month</span><br/>\n          </p>\n        </div>\n        <div class=\"g-content\">\n          <div class=\"g-lines\">\n            <div class=\"g-line\">\n              <div class=\"g-line-info\">\n                <div class=\"skew element-blue g-line-value\">\n                  <p>MEAD</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"g-line\">\n              <div class=\"g-line-info\">\n                <div class=\"skew element-blue g-line-value\">\n                  <p>BEVORDEX</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"g-line\">\n              <div class=\"g-line-info\">\n                <div class=\"skew element-blue g-line-value\">\n                  <p>Callanan <em>et al.</em></p>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class=\"buttons-wrapper\">\n            <div class=\"button-wrapper\">\n              <button class=\"arrow-btn plus\" data-goto-state=\"mead\"></button><span>MEAD</span>\n            </div>\n            <div class=\"button-wrapper\">\n              <button class=\"arrow-btn plus\" data-goto-state=\"bevordex\"></button><span>BEVORDEX</span>\n            </div>\n            <div class=\"button-wrapper\">\n              <button class=\"arrow-btn plus\" data-goto-state=\"callanan\"></button><span>Callanan <em>et al.</em></span>\n            </div>\n          </div>\n          <p class=\"dose-indicator\">Dose</p>\n        </div>\n      </figure>\n    </div>\n    <ul class=\"g-legend\">\n      <li class=\"legend-yellow\">DEX 0.7 mg (n=351)</li>\n    </ul>\n  </figure>\n</article>");
app.cache.put("slides/UniqueMoA50B4PopupSlide/UniqueMoA50B4PopupSlide.html","\n<article class=\"slide\" id=\"UniqueMoA50B4PopupSlide\">\n  <h1 class=\"oblique-tile\">DEX: less effective inhibition of VEGF<sup data-reference-id=\'papadopoulos,kim\'></sup></h1>\n  <ul>\n    <li>Anti-VEGF treatments <br>are more effective <br>against VEGF compared <br>with DEX<sup data-reference-id=\'papadopoulos,kim\'></sup></li>\n    <li><mark>EYLEA<sup>&reg;</sup></mark> also acts on PGF, <br>which is known to promote <br>leukocyte infiltration and <br>vascular inflammation<sup data-reference-id=\'eylea\'></sup></li>\n  </ul>\n</article>");
app.cache.put("slides/UniqueMoA50B5PopupSlide/UniqueMoA50B5PopupSlide.html","\n<article class=\"slide\" id=\"UniqueMoA50B5PopupSlide\">\n  <h1 class=\"oblique-tile\">Safety</h1>\n  <ul>\n    <li>The proportion of patients with elevated IOP was 30.8% in the DEX 0.7 mg group and 30.0% in the 0.35 mg group vs 3.4% in the sham group<sup data-reference-id=\'boyer\'></sup></li>\n    <li>Approximately one-third of patients in each DEX treatment group had a clinically significant increase in IOP requiring treatment during the study<sup data-reference-id=\'boyer\'></sup></li>\n    <li>The most common ocular AE in the study eye was cataract (37.8% in the DEX 0.7 mg group, 32.4% in the 0.35 mg group and 9.7% in the sham group)<sup data-reference-id=\'boyer\'></sup></li>\n    <li>Among patients with a phakic study eye at baseline, the overall incidence of cataract-related AEs was 67.9%, 64.1%, and 20.4% in the DEX 0.7 mg, 0.35 mg, and sham groups, respectively, and the rate of cataract surgery during the study was 59.2%, 52.3%, and 7.2%, respectively<sup data-reference-id=\'boyer\'></sup></li>\n  </ul>\n</article>");
app.cache.put("slides/UniqueMoA50Slide/UniqueMoA50Slide.html","\n<article class=\"slide\" id=\"UniqueMoA50Slide\">\n  <h1>EYLEA<sup>&reg;</sup>: UNIQUE MECHANISM OF ACTION</h1>\n  <h2>TARGETING VEGF AND PGF: TWO CRITICAL FACTORS RESPONSIBLE FOR DME<sup data-reference-id=\'eylea,kovacs-rakic\'></sup></h2>\n  <div class=\"factors content-wrapper\">\n    <div class=\"factor\">\n      <p class=\"factor-title\">VEGF</p>\n      <p class=\"factor-description\">Stimulates <mark>neovascularization</mark><sup data-reference-id=\'bhagat\'></sup></p>\n    </div>\n    <div class=\"factor\">\n      <p class=\"factor-title\">PGF</p>\n      <p class=\"factor-description\">Stimulates <mark>neovascularization</mark> and production <br>of <mark>inflammatory mediators</mark><sup data-reference-id=\'ando,rakic\'></sup></p>\n    </div>\n  </div>\n  <figure>\n    <div class=\"molecule\"></div>\n    <div class=\"descriptions\">\n      <div class=\"block\">\n        <p class=\"factor-title\">‘TRAP’ MECHANISM FOR TIGHTER BINDING<sup data-reference-id=\'eylea,papadopoulos\'></sup></p>\n        <div class=\"description\">\n          <p><b>EYLEA<sup>&reg;</sup> is the only approved</b> anti-angiogenic therapy that <b>targets <br>both VEGF-A</b> and <b>PGF</b>, with greater affinity than natural receptors<sup data-reference-id=\'eylea\'></sup></p>\n          <p>VEGF-A-binding affinity up to <mark>100-fold</mark> that of ranibizumab<sup data-reference-id=\'papadopoulos\'></sup></p>\n        </div>\n      </div>\n      <div class=\"block\">\n        <p class=\"factor-title\">LONG-LASTING EFFECT<sup data-reference-id=\'fauser-aflibercept\'></sup></p>\n        <div class=\"description\">\n          <p><b>Suppresses anterior chamber VEGF</b> for a mean of <b>&gt;10 weeks –</b> <br><mark>twice as long</mark> as ranibizumab<sup data-reference-id=\'fauser\'></sup></p>\n        </div>\n      </div>\n    </div>\n  </figure>\n  <div class=\"buttons\">\n    <div class=\"button-wrapper\">\n      <p>EYLEA<sup>&reg;</sup> MOA</p>\n      <button class=\"arrow-btn plus-white\" data-viewer=\"slideshow\" href=\"unique_moa_50a_inline\" slide=\"UniqueMoA50A1PopupSlide\"></button>\n    </div>\n    <div class=\"button-wrapper\">\n      <p>DEX</p>\n      <button class=\"arrow-btn plus-white\" data-viewer=\"slideshow\" href=\"unique_moa_50b_inline\" slide=\"UniqueMoA50B1PopupSlide\"></button>\n    </div>\n  </div>\n</article>");
app.cache.put("config.json","{\n  \"name\": \"Eylea DME EFFICACY Global Master eDetailer\",\n  \"model\": \"presentation.json\",\n  \"locale\": \"en\",\n  \"paths\": {\n    \"slides\": \"slides/<id>/\",\n    \"modules\": \"modules/<id>/\",\n    \"thumbs\": \"slides/<id>/<id>.png\",\n    \"scripts\": \"slides/<id>/<id>.js\",\n    \"styles\": \"slides/<id>/<id>.css\",\n    \"references\": \"shared/references/<id>.pdf\"\n  },\n  \"startPath\": \"start_collection/home/HomePageSlide\",\n  \"lang\": \"en\",\n  \"transition\": \"linear\",\n  \"transitionSpeed\": \"default\",\n  \"plugins\": [\n    \"storyboard\"\n  ],\n  \"bundle\": {\n    \"presentation\": {\n      \"styles\": [\"accelerator/css/styles.css\", \"global_styles/**/*.{css,styl}\", \"modules/**/*.{css,styl}\", \"slides/**/*.{css,styl}\", \"accelerator/css/textalign.css\", \"global_styles/**/*.{css,styl}\", \"modules/**/*.{css,styl}\", \"slides/**/*.{css,styl}\"],\n      \"scripts\": [\n        \"accelerator/js/init.js\",\n        \"accelerator/lib/head.min.js\",\n        \"modules/**/*.{js,coffee,html,pug,md}\",\n        \"slides/**/*.{js,html,coffee,pug,md}\",\n        \"config.json\",\n        \"presentation.json\",\n        \"references.json\",\n        \"references-strings.json\",\n        \"media.json\"\n      ]\n    }\n  },\n  \"dependencies\": [\n    { \"src\": \"accelerator/lib/draggy.js\" },\n    { \"src\": \"accelerator/lib/touchy.js\" },\n    { \"src\": \"accelerator/lib/jquery.min.js\"},\n    { \"src\": \"_vendor/jquery-ui.min.js\"},\n    { \"src\": \"_vendor/jquery.ui.touch-punch.js\"},\n    { \"src\": \"_vendor/jquery.requestAnimationFrame.js\"},\n    { \"src\": \"_vendor/cdm.sketcher.js\"},\n    { \"src\": \"_vendor/iscroll.js\"},\n    { \"src\": \"_vendor/apprise-1.5.full.js\"},\n    { \"src\": \"_vendor/apprise.css\"}\n  ],\n  \"iPlanner\": {\n    \"bounce\": false\n  },\n  \"ag-microsites\": {\n    \"startPath\": \"ag-microsites\"\n  },\n  \"ag-engager\": {\n    \"width\": 1024,\n    \"height\": 760\n  },\n  \"debug\": {\n    \"dependencies\": [\n      { \"src\": \"accelerator/lib/draggy.js\" },\n      { \"src\": \"accelerator/lib/jquery.min.js\"}\n    ]\n  },\n  \"width\": \"100%\",\n  \"height\": \"100%\",\n  \"margin\": 0,\n  \"padding\": 0\n}\n");
app.cache.put("presentation.json","{\n	\"slides\": {\n		\"HomePageSlide\": {\n			\"id\": \"HomePageSlide\",\n			\"name\": \"DME_Eff_StartScreen_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/HomePageSlide/HomePageSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/HomePageSlide/HomePageSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/HomePageSlide/HomePageSlide.styl\"\n				]\n			}\n		},\n		\"RecentInsights10Slide\": {\n			\"id\": \"RecentInsights10Slide\",\n			\"name\": \"DME_Eff_InsightsGoals_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RecentInsights10Slide/RecentInsights10Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/RecentInsights10Slide/RecentInsights10Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/RecentInsights10Slide/RecentInsights10Slide.styl\"\n				]\n			}\n		},\n		\"RecentInsights11Slide\": {\n			\"id\": \"RecentInsights11Slide\",\n			\"name\": \"DME_Eff_InsightsConcernInput_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RecentInsights11Slide/RecentInsights11Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/RecentInsights11Slide/RecentInsights11Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/RecentInsights11Slide/RecentInsights11Slide.styl\"\n				]\n			}\n		},\n		\"RecentInsights12Slide\": {\n			\"id\": \"RecentInsights12Slide\",\n			\"name\": \"DME_Eff_InsightsAntiVEGFDose_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RecentInsights12Slide/RecentInsights12Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/RecentInsights12Slide/RecentInsights12Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/RecentInsights12Slide/RecentInsights12Slide.styl\"\n				]\n			}\n		},\n		\"RecentInsights13Slide\": {\n			\"id\": \"RecentInsights13Slide\",\n			\"name\": \"DME_Eff_InsightsAntiVEGFTherapy_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RecentInsights13Slide/RecentInsights13Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/RecentInsights13Slide/RecentInsights13Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/RecentInsights13Slide/RecentInsights13Slide.styl\"\n				]\n			}\n		},\n		\"RecentInsights14Slide\": {\n			\"id\": \"RecentInsights14Slide\",\n			\"name\": \"DME_Eff_InsightsMonthyDosesInput_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RecentInsights14Slide/RecentInsights14Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/RecentInsights14Slide/RecentInsights14Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/RecentInsights14Slide/RecentInsights14Slide.styl\"\n				]\n			}\n		},\n		\"RecentInsights15Slide\": {\n			\"id\": \"RecentInsights15Slide\",\n			\"name\": \"DME_Eff_InsightsRapidInitialVisionGains_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RecentInsights15Slide/RecentInsights15Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/RecentInsights15Slide/RecentInsights15Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/RecentInsights15Slide/RecentInsights15Slide.styl\"\n				]\n			}\n		},\n		\"RecentInsights16Slide\": {\n			\"id\": \"RecentInsights16Slide\",\n			\"name\": \"DME_Eff_InsightsDifferencesEfficacy_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RecentInsights16Slide/RecentInsights16Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/RecentInsights16Slide/RecentInsights16Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/RecentInsights16Slide/RecentInsights16Slide.styl\"\n				]\n			}\n		},\n		\"RecentInsights17Slide\": {\n			\"id\": \"RecentInsights17Slide\",\n			\"name\": \"DME_Eff_InsightsDifferencesPathology_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RecentInsights17Slide/RecentInsights17Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/RecentInsights17Slide/RecentInsights17Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/RecentInsights17Slide/RecentInsights17Slide.styl\"\n				]\n			}\n		},\n		\"RecentInsights18Slide\": {\n			\"id\": \"RecentInsights18Slide\",\n			\"name\": \"DME_Eff_InsightsDifferencesMoA_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RecentInsights18Slide/RecentInsights18Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/RecentInsights18Slide/RecentInsights18Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/RecentInsights18Slide/RecentInsights18Slide.styl\"\n				]\n			}\n		},\n		\"RecentInsights19Slide\": {\n			\"id\": \"RecentInsights19Slide\",\n			\"name\": \"DME_Eff_InsightsDifferencesMoAInput_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RecentInsights19Slide/RecentInsights19Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/RecentInsights19Slide/RecentInsights19Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/RecentInsights19Slide/RecentInsights19Slide.styl\"\n				]\n			}\n		},\n		\"RecentInsights110Slide\": {\n			\"id\": \"RecentInsights110Slide\",\n			\"name\": \"DME_Eff_InsightsClinicalEfficacy_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RecentInsights110Slide/RecentInsights110Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/RecentInsights110Slide/RecentInsights110Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/RecentInsights110Slide/RecentInsights110Slide.styl\"\n				]\n			}\n		},\n		\"RecentInsights111Slide\": {\n			\"id\": \"RecentInsights111Slide\",\n			\"name\": \"DME_Eff_InsightsLastPrescribed_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RecentInsights111Slide/RecentInsights111Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/RecentInsights111Slide/RecentInsights111Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/RecentInsights111Slide/RecentInsights111Slide.styl\"\n				]\n			}\n		},\n		\"RecentInsights112Slide\": {\n			\"id\": \"RecentInsights112Slide\",\n			\"name\": \"DME_Eff_InsightsAnnualDosing_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RecentInsights112Slide/RecentInsights112Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/RecentInsights112Slide/RecentInsights112Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/RecentInsights112Slide/RecentInsights112Slide.styl\"\n				]\n			}\n		},\n		\"Eylea20Slide\": {\n			\"id\": \"Eylea20Slide\",\n			\"name\": \"DRSS_EyleaStartStayUnique_State1_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/Eylea20Slide/Eylea20Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/Eylea20Slide/Eylea20Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/Eylea20Slide/Eylea20Slide.styl\"\n				]\n			}\n		},\n		\"StartStrong30Slide\": {\n			\"id\": \"StartStrong30Slide\",\n			\"name\": \"DME_Eff_StartStrongVividDME_State1_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/StartStrong30Slide/StartStrong30Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StartStrong30Slide/StartStrong30Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StartStrong30Slide/StartStrong30Slide.styl\"\n				]\n			}\n		},\n		\"StartStrong31Slide\": {\n			\"id\": \"StartStrong31Slide\",\n			\"name\": \"DME_Eff_PatientsVisionGainInput_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/StartStrong31Slide/StartStrong31Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StartStrong31Slide/StartStrong31Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StartStrong31Slide/StartStrong31Slide.styl\"\n				]\n			}\n		},\n		\"StartStrong32Slide\": {\n			\"id\": \"StartStrong32Slide\",\n			\"name\": \"DME_Eff_80PctAchievedVisionGain_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/StartStrong32Slide/StartStrong32Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StartStrong32Slide/StartStrong32Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StartStrong32Slide/StartStrong32Slide.styl\"\n				]\n			}\n		},\n		\"StartStrong35Slide\": {\n			\"id\": \"StartStrong35Slide\",\n			\"name\": \"DME_Eff_StartStrongProtocolT_State1_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/StartStrong35Slide/StartStrong35Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StartStrong35Slide/StartStrong35Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StartStrong35Slide/StartStrong35Slide.styl\"\n				]\n			}\n		},\n		\"StayStrong40Slide\": {\n			\"id\": \"StayStrong40Slide\",\n			\"name\": \"DME_Eff_StayStrongMaintainGains_State1_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/StayStrong40Slide/StayStrong40Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StayStrong40Slide/StayStrong40Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StayStrong40Slide/StayStrong40Slide.styl\"\n				]\n			}\n		},\n		\"StayStrong41Slide\": {\n			\"id\": \"StayStrong41Slide\",\n			\"name\": \"DME_Eff_StayStrongExtendedDosing_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/StayStrong41Slide/StayStrong41Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StayStrong41Slide/StayStrong41Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StayStrong41Slide/StayStrong41Slide.styl\"\n				]\n			}\n		},\n		\"UniqueMoA50Slide\": {\n			\"id\": \"UniqueMoA50Slide\",\n			\"name\": \"DME_Eff_UniqueMOA_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/UniqueMoA50Slide/UniqueMoA50Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/UniqueMoA50Slide/UniqueMoA50Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/UniqueMoA50Slide/UniqueMoA50Slide.styl\"\n				]\n			}\n		},\n		\"Discussion60Slide\": {\n			\"id\": \"Discussion60Slide\",\n			\"name\": \"DME_Eff_DiscussionOverview_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/Discussion60Slide/Discussion60Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/Discussion60Slide/Discussion60Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/Discussion60Slide/Discussion60Slide.styl\"\n				]\n			}\n		},\n		\"Discussion61Slide\": {\n			\"id\": \"Discussion61Slide\",\n			\"name\": \"DME_Eff_DiscussionMonthlyDosesInput_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/Discussion61Slide/Discussion61Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/Discussion61Slide/Discussion61Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/Discussion61Slide/Discussion61Slide.styl\"\n				]\n			}\n		},\n		\"Discussion62Slide\": {\n			\"id\": \"Discussion62Slide\",\n			\"name\": \"DME_Eff_DiscussionIntensiveDosingBenefits_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/Discussion62Slide/Discussion62Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/Discussion62Slide/Discussion62Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/Discussion62Slide/Discussion62Slide.styl\"\n				]\n			}\n		},\n		\"Discussion63Slide\": {\n			\"id\": \"Discussion63Slide\",\n			\"name\": \"DME_Eff_DiscussionDosingVisionGain_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/Discussion63Slide/Discussion63Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/Discussion63Slide/Discussion63Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/Discussion63Slide/Discussion63Slide.styl\"\n				]\n			}\n		},\n		\"Discussion64Slide\": {\n			\"id\": \"Discussion64Slide\",\n			\"name\": \"DME_Eff_DiscussionLaserTreatment_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/Discussion64Slide/Discussion64Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/Discussion64Slide/Discussion64Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/Discussion64Slide/Discussion64Slide.styl\"\n				]\n			}\n		},\n		\"Discussion65Slide\": {\n			\"id\": \"Discussion65Slide\",\n			\"name\": \"DME_Eff_Discussion5InitialMonthlyDose_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/Discussion65Slide/Discussion65Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/Discussion65Slide/Discussion65Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/Discussion65Slide/Discussion65Slide.styl\"\n				]\n			}\n		},\n		\"Discussion66Slide\": {\n			\"id\": \"Discussion66Slide\",\n			\"name\": \"DME_Eff_DiscussionTreatmentOfChoice_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/Discussion66Slide/Discussion66Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/Discussion66Slide/Discussion66Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/Discussion66Slide/Discussion66Slide.styl\"\n				]\n			}\n		},\n		\"Discussion67Slide\": {\n			\"id\": \"Discussion67Slide\",\n			\"name\": \"DME_Eff_DiscussionEyleaPositioning_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/Discussion67Slide/Discussion67Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/Discussion67Slide/Discussion67Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/Discussion67Slide/Discussion67Slide.styl\"\n				]\n			}\n		},\n		\"Discussion68Slide\": {\n			\"id\": \"Discussion68Slide\",\n			\"name\": \"DME_Eff_DiscussionInensiveDosingBenefits_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/Discussion68Slide/Discussion68Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/Discussion68Slide/Discussion68Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/Discussion68Slide/Discussion68Slide.styl\"\n				]\n			}\n		},\n		\"Discussion69Slide\": {\n			\"id\": \"Discussion69Slide\",\n			\"name\": \"DME_Eff_DiscussionMaxAndMaintain_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/Discussion69Slide/Discussion69Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/Discussion69Slide/Discussion69Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/Discussion69Slide/Discussion69Slide.styl\"\n				]\n			}\n		},\n		\"Discussion610Slide\": {\n			\"id\": \"Discussion610Slide\",\n			\"name\": \"DME_Eff_DiscussionImproveAndMaintain_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/Discussion610Slide/Discussion610Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/Discussion610Slide/Discussion610Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/Discussion610Slide/Discussion610Slide.styl\"\n				]\n			}\n		},\n		\"Discussion611Slide\": {\n			\"id\": \"Discussion611Slide\",\n			\"name\": \"DME_Eff_DiscussionFlexibleDosing_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/Discussion611Slide/Discussion611Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/Discussion611Slide/Discussion611Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/Discussion611Slide/Discussion611Slide.styl\"\n				]\n			}\n		},\n		\"Discussion612Slide\": {\n			\"id\": \"Discussion612Slide\",\n			\"name\": \"DME_Eff_DiscussionDosingSchedules_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/Discussion612Slide/Discussion612Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/Discussion612Slide/Discussion612Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/Discussion612Slide/Discussion612Slide.styl\"\n				]\n			}\n		},\n		\"StartStrong30A1PopupSlide\": {\n			\"id\": \"StartStrong30A1PopupSlide\",\n			\"name\": \"CRT\",\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_CRTRapidReductionsVividDME_PopUp\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/StartStrong30A1PopupSlide/StartStrong30A1PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StartStrong30A1PopupSlide/StartStrong30A1PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StartStrong30A1PopupSlide/StartStrong30A1PopupSlide.styl\"\n				]\n			}\n		},\n		\"StartStrong30B1PopupSlide\": {\n			\"id\": \"StartStrong30B1PopupSlide\",\n			\"name\": \"STUDY<br> DESIGN\",\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_SDVividDME_PopUp\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/StartStrong30B1PopupSlide/StartStrong30B1PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StartStrong30B1PopupSlide/StartStrong30B1PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StartStrong30B1PopupSlide/StartStrong30B1PopupSlide.styl\"\n				]\n			}\n		},\n		\"StartStrong30B2PopupSlide\": {\n			\"id\": \"StartStrong30B2PopupSlide\",\n			\"name\": \"DOSING AND<br> MONITORING SCHEDULE\",\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_SDVividDMEDosingSchedule_PopUp\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/StartStrong30B2PopupSlide/StartStrong30B2PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StartStrong30B2PopupSlide/StartStrong30B2PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StartStrong30B2PopupSlide/StartStrong30B2PopupSlide.styl\"\n				]\n			}\n		},\n		\"StartStrong30B3PopupSlide\": {\n			\"id\": \"StartStrong30B3PopupSlide\",\n			\"name\": \"BASELINE<br> CHARACTERISTICS\",\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_SDVividDMEBaselineCharacteristics_PopUp\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/StartStrong30B3PopupSlide/StartStrong30B3PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StartStrong30B3PopupSlide/StartStrong30B3PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StartStrong30B3PopupSlide/StartStrong30B3PopupSlide.styl\"\n				]\n			}\n		},\n		\"StartStrong30B4PopupSlide\": {\n			\"id\": \"StartStrong30B4PopupSlide\",\n			\"name\": \"BASELINE DISEASE<br> CHARACTERISTICS\",\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_SDVividDMEBaselineDiseaseCharacteristics_PopUp\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/StartStrong30B4PopupSlide/StartStrong30B4PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StartStrong30B4PopupSlide/StartStrong30B4PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StartStrong30B4PopupSlide/StartStrong30B4PopupSlide.styl\"\n				]\n			}\n		},\n		\"StartStrong30B5PopupSlide\": {\n			\"id\": \"StartStrong30B5PopupSlide\",\n			\"name\": \"ELIGIBILITY<br> CRITERIA\",\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_SDVividDMEEligibilityCriteria_PopUp\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/StartStrong30B5PopupSlide/StartStrong30B5PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StartStrong30B5PopupSlide/StartStrong30B5PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StartStrong30B5PopupSlide/StartStrong30B5PopupSlide.styl\"\n				]\n			}\n		},\n		\"StartStrong30C1PopupSlide\": {\n			\"id\": \"StartStrong30C1PopupSlide\",\n			\"name\": \"BCVA\",\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_BCVAVistaDME_PopUp\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/StartStrong30C1PopupSlide/StartStrong30C1PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StartStrong30C1PopupSlide/StartStrong30C1PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StartStrong30C1PopupSlide/StartStrong30C1PopupSlide.styl\"\n				]\n			}\n		},\n		\"StartStrong30C3PopupSlide\": {\n			\"id\": \"StartStrong30C3PopupSlide\",\n			\"name\": \"CRT\",\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_CRTVistaDME_PopUp\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/StartStrong30C3PopupSlide/StartStrong30C3PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StartStrong30C3PopupSlide/StartStrong30C3PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StartStrong30C3PopupSlide/StartStrong30C3PopupSlide.styl\"\n				]\n			}\n		},\n		\"StartStrong35A2PopupSlide\": {\n			\"id\": \"StartStrong35A2PopupSlide\",\n			\"name\": \"BASELINE<br> CHARACTERISTICS\",\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_SDProtocolTBaselineCharacteristics_PopUp\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/StartStrong35A2PopupSlide/StartStrong35A2PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StartStrong35A2PopupSlide/StartStrong35A2PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StartStrong35A2PopupSlide/StartStrong35A2PopupSlide.styl\"\n				]\n			}\n		},\n		\"StartStrong35A3PopupSlide\": {\n			\"id\": \"StartStrong35A3PopupSlide\",\n			\"name\": \"BASELINE DISEASE<br> CHARACTERISTICS\",\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_SDProtocolTBaselineDiseaseCharacteristics_PopUp\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/StartStrong35A3PopupSlide/StartStrong35A3PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StartStrong35A3PopupSlide/StartStrong35A3PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StartStrong35A3PopupSlide/StartStrong35A3PopupSlide.styl\"\n				]\n			}\n		},\n		\"StartStrong35A4PopupSlide\": {\n			\"id\": \"StartStrong35A4PopupSlide\",\n			\"name\": \"ELIGIBILITY<br> CRITERIA\",\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_SDProtocolTEligibilityCriteria_PopUp\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/StartStrong35A4PopupSlide/StartStrong35A4PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StartStrong35A4PopupSlide/StartStrong35A4PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StartStrong35A4PopupSlide/StartStrong35A4PopupSlide.styl\"\n				]\n			}\n		},\n		\"StartStrong35B1PopupSlide\": {\n			\"id\": \"StartStrong35B1PopupSlide\",\n			\"name\": \"CRT\",\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_CRTProtocolT_PopUp\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/StartStrong35B1PopupSlide/StartStrong35B1PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StartStrong35B1PopupSlide/StartStrong35B1PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StartStrong35B1PopupSlide/StartStrong35B1PopupSlide.styl\"\n				]\n			}\n		},\n		\"StayStrong40A1PopupSlide\": {\n			\"id\": \"StayStrong40A1PopupSlide\",\n			\"isSkipped\": true,\n			\"name\": \"BCVA\",\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_BCVALongTermDosingGains_State1_PopUp\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/StayStrong40A1PopupSlide/StayStrong40A1PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StayStrong40A1PopupSlide/StayStrong40A1PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StayStrong40A1PopupSlide/StayStrong40A1PopupSlide.styl\"\n				]\n			}\n		},\n		\"StayStrong40A2PopupSlide\": {\n			\"id\": \"StayStrong40A2PopupSlide\",\n			\"name\": \"CRT\",\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_CRTReductions_PopUp\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/StayStrong40A2PopupSlide/StayStrong40A2PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StayStrong40A2PopupSlide/StayStrong40A2PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StayStrong40A2PopupSlide/StayStrong40A2PopupSlide.styl\"\n				]\n			}\n		},\n		\"StayStrong40B1PopupSlide\": {\n			\"id\": \"StayStrong40B1PopupSlide\",\n			\"name\": \"CRT\",\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_CRTReductionsvsLaser_PopUp\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/StayStrong40B1PopupSlide/StayStrong40B1PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StayStrong40B1PopupSlide/StayStrong40B1PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StayStrong40B1PopupSlide/StayStrong40B1PopupSlide.styl\"\n				]\n			}\n		},\n		\"StayStrong40C1PopupSlide\": {\n			\"id\": \"StayStrong40C1PopupSlide\",\n			\"name\": \"BCVA\",\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_BCVATreatment_PopUp\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/StayStrong40C1PopupSlide/StayStrong40C1PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StayStrong40C1PopupSlide/StayStrong40C1PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StayStrong40C1PopupSlide/StayStrong40C1PopupSlide.styl\"\n				]\n			}\n		},\n		\"StayStrong40C2PopupSlide\": {\n			\"id\": \"StayStrong40C2PopupSlide\",\n			\"name\": \"CRT\",\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_CRTvsLaser_PopUp\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/StayStrong40C2PopupSlide/StayStrong40C2PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StayStrong40C2PopupSlide/StayStrong40C2PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StayStrong40C2PopupSlide/StayStrong40C2PopupSlide.styl\"\n				]\n			}\n		},\n		\"StayStrong41A1PopupSlide\": {\n			\"id\": \"StayStrong41A1PopupSlide\",\n			\"name\": \"DOSING SCHEDULE<br> IN YEAR 1\",\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_DosingScheduleYear1_PopUp\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/StayStrong41A1PopupSlide/StayStrong41A1PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StayStrong41A1PopupSlide/StayStrong41A1PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StayStrong41A1PopupSlide/StayStrong41A1PopupSlide.styl\"\n				]\n			}\n		},\n		\"StayStrong41A3PopupSlide\": {\n			\"id\": \"StayStrong41A3PopupSlide\",\n			\"name\": \"EXAMPLE EXTENSION<br> SCHEDULES 1\",\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_ExtensionSchedules1_PopUp\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/StayStrong41A3PopupSlide/StayStrong41A3PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StayStrong41A3PopupSlide/StayStrong41A3PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StayStrong41A3PopupSlide/StayStrong41A3PopupSlide.styl\"\n				]\n			}\n		},\n		\"StayStrong41A4PopupSlide\": {\n			\"id\": \"StayStrong41A4PopupSlide\",\n			\"name\": \"EXAMPLE EXTENSION<br> SCHEDULES 2\",\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_ExtensionSchedules2_PopUp\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/StayStrong41A4PopupSlide/StayStrong41A4PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/StayStrong41A4PopupSlide/StayStrong41A4PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/StayStrong41A4PopupSlide/StayStrong41A4PopupSlide.styl\"\n				]\n			}\n		},\n		\"MultiTargetApproachSlide\": {\n			\"id\": \"MultiTargetApproachSlide\",\n			\"name\": \"Multi Target Approach\",\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_Multitarget_approach\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/MultiTargetApproachSlide/MultiTargetApproachSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/MultiTargetApproachSlide/MultiTargetApproachSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/MultiTargetApproachSlide/MultiTargetApproachSlide.styl\"\n				]\n			}\n		},\n		\"UniqueMoA50A1PopupSlide\": {\n			\"id\": \"UniqueMoA50A1PopupSlide\",\n			\"name\": \"DME_Eff_MoAChartPathogenesis_PopUp\",\n			\"files\": {\n				\"templates\": [\n					\"slides/UniqueMoA50A1PopupSlide/UniqueMoA50A1PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/UniqueMoA50A1PopupSlide/UniqueMoA50A1PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/UniqueMoA50A1PopupSlide/UniqueMoA50A1PopupSlide.styl\"\n				]\n			}\n		},\n		\"UniqueMoA50A2PopupSlide\": {\n			\"id\": \"UniqueMoA50A2PopupSlide\",\n			\"name\": \"DME_Eff_MoADiseaseCascade_PopUp\",\n			\"files\": {\n				\"templates\": [\n					\"slides/UniqueMoA50A2PopupSlide/UniqueMoA50A2PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/UniqueMoA50A2PopupSlide/UniqueMoA50A2PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/UniqueMoA50A2PopupSlide/UniqueMoA50A2PopupSlide.styl\"\n				]\n			}\n		},\n		\"UniqueMoA50A3PopupSlide\": {\n			\"id\": \"UniqueMoA50A3PopupSlide\",\n			\"name\": \"DME_Eff_MoADiseaseVEGF_PopUp\",\n			\"files\": {\n				\"templates\": [\n					\"slides/UniqueMoA50A3PopupSlide/UniqueMoA50A3PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/UniqueMoA50A3PopupSlide/UniqueMoA50A3PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/UniqueMoA50A3PopupSlide/UniqueMoA50A3PopupSlide.styl\"\n				]\n			}\n		},\n		\"Eylea50Slide\": {\n			\"id\": \"Eylea50Slide\",\n			\"name\": \"DME_Eff_50Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/Eylea50Slide/Eylea50Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/Eylea50Slide/Eylea50Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/Eylea50Slide/Eylea50Slide.styl\"\n				]\n			}\n		},\n		\"UniqueMoA50A4PopupSlide\": {\n			\"id\": \"UniqueMoA50A4PopupSlide\",\n			\"name\": \"DME_Eff_MoADiseasePGF_PopUp\",\n			\"files\": {\n				\"templates\": [\n					\"slides/UniqueMoA50A4PopupSlide/UniqueMoA50A4PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/UniqueMoA50A4PopupSlide/UniqueMoA50A4PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/UniqueMoA50A4PopupSlide/UniqueMoA50A4PopupSlide.styl\"\n				]\n			}\n		},\n		\"UniqueMoA50A5PopupSlide\": {\n			\"id\": \"UniqueMoA50A5PopupSlide\",\n			\"name\": \"DME_Eff_MoAEyleaMolecule_PopUp\",\n			\"files\": {\n				\"templates\": [\n					\"slides/UniqueMoA50A5PopupSlide/UniqueMoA50A5PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/UniqueMoA50A5PopupSlide/UniqueMoA50A5PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/UniqueMoA50A5PopupSlide/UniqueMoA50A5PopupSlide.styl\"\n				]\n			}\n		},\n		\"UniqueMoA50A6PopupSlide\": {\n			\"id\": \"UniqueMoA50A6PopupSlide\",\n			\"name\": \"DME_Eff_MoAEyleaMoA_PopUp\",\n			\"files\": {\n				\"templates\": [\n					\"slides/UniqueMoA50A6PopupSlide/UniqueMoA50A6PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/UniqueMoA50A6PopupSlide/UniqueMoA50A6PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/UniqueMoA50A6PopupSlide/UniqueMoA50A6PopupSlide.styl\"\n				]\n			}\n		},\n		\"UniqueMoA50B1PopupSlide\": {\n			\"id\": \"UniqueMoA50B1PopupSlide\",\n			\"name\": \"SUMMARY\",\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_DEXSummary_PopUp\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/UniqueMoA50B1PopupSlide/UniqueMoA50B1PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/UniqueMoA50B1PopupSlide/UniqueMoA50B1PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/UniqueMoA50B1PopupSlide/UniqueMoA50B1PopupSlide.styl\"\n				]\n			}\n		},\n		\"UniqueMoA50B2PopupSlide\": {\n			\"id\": \"UniqueMoA50B2PopupSlide\",\n			\"name\": \"EFFICACY\",\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_DEXEfficacy_PopUp\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/UniqueMoA50B2PopupSlide/UniqueMoA50B2PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/UniqueMoA50B2PopupSlide/UniqueMoA50B2PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/UniqueMoA50B2PopupSlide/UniqueMoA50B2PopupSlide.styl\"\n				]\n			}\n		},\n		\"UniqueMoA50B3PopupSlide\": {\n			\"id\": \"UniqueMoA50B3PopupSlide\",\n			\"name\": \"DURABILITY\",\n			\"isSkipped\": true,\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_DEXDurability_State1_PopUp\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/UniqueMoA50B3PopupSlide/UniqueMoA50B3PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/UniqueMoA50B3PopupSlide/UniqueMoA50B3PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/UniqueMoA50B3PopupSlide/UniqueMoA50B3PopupSlide.styl\"\n				]\n			}\n		},\n		\"UniqueMoA50B4PopupSlide\": {\n			\"id\": \"UniqueMoA50B4PopupSlide\",\n			\"name\": \"MOA\",\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_DEXMoA_PopUp\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/UniqueMoA50B4PopupSlide/UniqueMoA50B4PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/UniqueMoA50B4PopupSlide/UniqueMoA50B4PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/UniqueMoA50B4PopupSlide/UniqueMoA50B4PopupSlide.styl\"\n				]\n			}\n		},\n		\"UniqueMoA50B5PopupSlide\": {\n			\"id\": \"UniqueMoA50B5PopupSlide\",\n			\"name\": \"SAFETY\",\n			\"monitoring\" :{\n				\"name\": \"DME_Eff_DEXSafety_PopUp\"\n			},\n			\"files\": {\n				\"templates\": [\n					\"slides/UniqueMoA50B5PopupSlide/UniqueMoA50B5PopupSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/UniqueMoA50B5PopupSlide/UniqueMoA50B5PopupSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/UniqueMoA50B5PopupSlide/UniqueMoA50B5PopupSlide.styl\"\n				]\n			}\n		},\n		\"ContraindicationsSlide\": {\n			\"id\": \"ContraindicationsSlide\",\n			\"name\": \"ContraindicationsSlide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/ContraindicationsSlide/ContraindicationsSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/ContraindicationsSlide/ContraindicationsSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/ContraindicationsSlide/ContraindicationsSlide.styl\"\n				]\n			}\n		},\n		\"MostFrequentlyObservedSlide\": {\n			\"id\": \"MostFrequentlyObservedSlide\",\n			\"name\": \"MostFrequentlyObservedSlide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/MostFrequentlyObservedSlide/MostFrequentlyObservedSlide.pug\"\n				],\n				\"scripts\": [\n					\"slides/MostFrequentlyObservedSlide/MostFrequentlyObservedSlide.coffee\"\n				],\n				\"styles\": [\n					\"slides/MostFrequentlyObservedSlide/MostFrequentlyObservedSlide.styl\"\n				]\n			}\n		},\n		\"RecentInsights10Slide\": {\n			\"id\": \"RecentInsights10Slide\",\n			\"name\": \"DME_Eff_InsightsGoals_Slide\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RecentInsights10Slide/RecentInsights10Slide.pug\"\n				],\n				\"scripts\": [\n					\"slides/RecentInsights10Slide/RecentInsights10Slide.coffee\"\n				],\n				\"styles\": [\n					\"slides/RecentInsights10Slide/RecentInsights10Slide.styl\"\n				]\n			}\n		},\n		\"RI_Quiz_1\": {\n			\"id\": \"RI_Quiz_1\",\n			\"name\": \"DME_Eff_RI Quiz 1\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RI_Quiz_1/RI_Quiz_1.pug\"\n				],\n				\"scripts\": [\n					\"slides/RI_Quiz_1/RI_Quiz_1.coffee\"\n				],\n				\"styles\": [\n					\"slides/RI_Quiz_1/RI_Quiz_1.styl\"\n				]\n			}\n		},\n		\"RI_Quiz_2\": {\n			\"id\": \"RI_Quiz_2\",\n			\"name\": \"DME_Eff_RI Quiz 2\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RI_Quiz_2/RI_Quiz_2.pug\"\n				],\n				\"scripts\": [\n					\"slides/RI_Quiz_2/RI_Quiz_2.coffee\"\n				],\n				\"styles\": [\n					\"slides/RI_Quiz_2/RI_Quiz_2.styl\"\n				]\n			}\n		},\n		\"RI_Quiz_3\": {\n			\"id\": \"RI_Quiz_3\",\n			\"name\": \"DME_Eff_RI Quiz 3\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RI_Quiz_3/RI_Quiz_3.pug\"\n				],\n				\"scripts\": [\n					\"slides/RI_Quiz_3/RI_Quiz_3.coffee\"\n				],\n				\"styles\": [\n					\"slides/RI_Quiz_3/RI_Quiz_3.styl\"\n				]\n			}\n		},\n		\"RI_Quiz_4\": {\n			\"id\": \"RI_Quiz_4\",\n			\"name\": \"DME_Eff_RI Quiz 4\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RI_Quiz_4/RI_Quiz_4.pug\"\n				],\n				\"scripts\": [\n					\"slides/RI_Quiz_4/RI_Quiz_4.coffee\"\n				],\n				\"styles\": [\n					\"slides/RI_Quiz_4/RI_Quiz_4.styl\"\n				]\n			}\n		},\n		\"RI_Quiz_5\": {\n			\"id\": \"RI_Quiz_5\",\n			\"name\": \"DME_Eff_RI Quiz 5\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RI_Quiz_5/RI_Quiz_5.pug\"\n				],\n				\"scripts\": [\n					\"slides/RI_Quiz_5/RI_Quiz_5.coffee\"\n				],\n				\"styles\": [\n					\"slides/RI_Quiz_5/RI_Quiz_5.styl\"\n				]\n			}\n		},\n		\"RI_Quiz_6\": {\n			\"id\": \"RI_Quiz_6\",\n			\"name\": \"DME_Eff_RI Quiz 6\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RI_Quiz_6/RI_Quiz_6.pug\"\n				],\n				\"scripts\": [\n					\"slides/RI_Quiz_6/RI_Quiz_6.coffee\"\n				],\n				\"styles\": [\n					\"slides/RI_Quiz_6/RI_Quiz_6.styl\"\n				]\n			}\n		},\n		\"RI_Quiz_7\": {\n			\"id\": \"RI_Quiz_7\",\n			\"name\": \"DME_Eff_RI Quiz 7\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RI_Quiz_7/RI_Quiz_7.pug\"\n				],\n				\"scripts\": [\n					\"slides/RI_Quiz_7/RI_Quiz_7.coffee\"\n				],\n				\"styles\": [\n					\"slides/RI_Quiz_7/RI_Quiz_7.styl\"\n				]\n			}\n		},\n		\"RI_Quiz_8\": {\n			\"id\": \"RI_Quiz_8\",\n			\"name\": \"DME_Eff_RI Quiz 8\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RI_Quiz_8/RI_Quiz_8.pug\"\n				],\n				\"scripts\": [\n					\"slides/RI_Quiz_8/RI_Quiz_8.coffee\"\n				],\n				\"styles\": [\n					\"slides/RI_Quiz_8/RI_Quiz_8.styl\"\n				]\n			}\n		},\n		\"RI_Quiz_9\": {\n			\"id\": \"RI_Quiz_9\",\n			\"name\": \"DME_Eff_RI Quiz 9\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RI_Quiz_9/RI_Quiz_9.pug\"\n				],\n				\"scripts\": [\n					\"slides/RI_Quiz_9/RI_Quiz_9.coffee\"\n				],\n				\"styles\": [\n					\"slides/RI_Quiz_9/RI_Quiz_9.styl\"\n				]\n			}\n		},\n		\"RI_Quiz_10\": {\n			\"id\": \"RI_Quiz_10\",\n			\"name\": \"DME_Eff_RI Quiz 10\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RI_Quiz_10/RI_Quiz_10.pug\"\n				],\n				\"scripts\": [\n					\"slides/RI_Quiz_10/RI_Quiz_10.coffee\"\n				],\n				\"styles\": [\n					\"slides/RI_Quiz_10/RI_Quiz_10.styl\"\n				]\n			}\n		},\n		\"RI_Quiz_11\": {\n			\"id\": \"RI_Quiz_11\",\n			\"name\": \"DME_Eff_RI Quiz 11\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RI_Quiz_11/RI_Quiz_11.pug\"\n				],\n				\"scripts\": [\n					\"slides/RI_Quiz_11/RI_Quiz_11.coffee\"\n				],\n				\"styles\": [\n					\"slides/RI_Quiz_11/RI_Quiz_11.styl\"\n				]\n			}\n		},\n		\"RI_Quiz_12\": {\n			\"id\": \"RI_Quiz_12\",\n			\"name\": \"DME_Eff_RI Quiz 12\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RI_Quiz_12/RI_Quiz_12.pug\"\n				],\n				\"scripts\": [\n					\"slides/RI_Quiz_12/RI_Quiz_12.coffee\"\n				],\n				\"styles\": [\n					\"slides/RI_Quiz_12/RI_Quiz_12.styl\"\n				]\n			}\n		},\n		\"RI_Quiz_13\": {\n			\"id\": \"RI_Quiz_13\",\n			\"name\": \"DME_Eff_RI Quiz 13\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RI_Quiz_13/RI_Quiz_13.pug\"\n				],\n				\"scripts\": [\n					\"slides/RI_Quiz_13/RI_Quiz_13.coffee\"\n				],\n				\"styles\": [\n					\"slides/RI_Quiz_13/RI_Quiz_13.styl\"\n				]\n			}\n		},\n		\"RI_Quiz_14\": {\n			\"id\": \"RI_Quiz_14\",\n			\"name\": \"DME_Eff_RI Quiz 14\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RI_Quiz_14/RI_Quiz_14.pug\"\n				],\n				\"scripts\": [\n					\"slides/RI_Quiz_14/RI_Quiz_14.coffee\"\n				],\n				\"styles\": [\n					\"slides/RI_Quiz_14/RI_Quiz_14.styl\"\n				]\n			}\n		},\n		\"RI_Quiz_15\": {\n			\"id\": \"RI_Quiz_15\",\n			\"name\": \"DME_Eff_RI Quiz 15\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RI_Quiz_15/RI_Quiz_15.pug\"\n				],\n				\"scripts\": [\n					\"slides/RI_Quiz_15/RI_Quiz_15.coffee\"\n				],\n				\"styles\": [\n					\"slides/RI_Quiz_15/RI_Quiz_15.styl\"\n				]\n			}\n		},\n		\"RI_Quiz_16\": {\n			\"id\": \"RI_Quiz_16\",\n			\"name\": \"DME_Eff_RI Quiz 16\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RI_Quiz_16/RI_Quiz_16.pug\"\n				],\n				\"scripts\": [\n					\"slides/RI_Quiz_16/RI_Quiz_16.coffee\"\n				],\n				\"styles\": [\n					\"slides/RI_Quiz_16/RI_Quiz_16.styl\"\n				]\n			}\n		},\n		\"RI_Quiz_17\": {\n			\"id\": \"RI_Quiz_17\",\n			\"name\": \"DME_Eff_RI Quiz 17\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RI_Quiz_17/RI_Quiz_17.pug\"\n				],\n				\"scripts\": [\n					\"slides/RI_Quiz_17/RI_Quiz_17.coffee\"\n				],\n				\"styles\": [\n					\"slides/RI_Quiz_17/RI_Quiz_17.styl\"\n				]\n			}\n		},\n		\"RI_Quiz_18\": {\n			\"id\": \"RI_Quiz_18\",\n			\"name\": \"DME_Eff_RI Quiz 18\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RI_Quiz_18/RI_Quiz_18.pug\"\n				],\n				\"scripts\": [\n					\"slides/RI_Quiz_18/RI_Quiz_18.coffee\"\n				],\n				\"styles\": [\n					\"slides/RI_Quiz_18/RI_Quiz_18.styl\"\n				]\n			}\n		},\n		\"RI_Quiz_19\": {\n			\"id\": \"RI_Quiz_19\",\n			\"name\": \"DME_Eff_RI Quiz 19\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RI_Quiz_19/RI_Quiz_19.pug\"\n				],\n				\"scripts\": [\n					\"slides/RI_Quiz_19/RI_Quiz_19.coffee\"\n				],\n				\"styles\": [\n					\"slides/RI_Quiz_19/RI_Quiz_19.styl\"\n				]\n			}\n		},\n		\"RI_Quiz_20\": {\n			\"id\": \"RI_Quiz_20\",\n			\"name\": \"DME_Eff_RI Quiz 20\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RI_Quiz_20/RI_Quiz_20.pug\"\n				],\n				\"scripts\": [\n					\"slides/RI_Quiz_20/RI_Quiz_20.coffee\"\n				],\n				\"styles\": [\n					\"slides/RI_Quiz_20/RI_Quiz_20.styl\"\n				]\n			}\n		},\n		\"RI_Quiz_21\": {\n			\"id\": \"RI_Quiz_21\",\n			\"name\": \"DME_Eff_RI Quiz 21\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RI_Quiz_21/RI_Quiz_21.pug\"\n				],\n				\"scripts\": [\n					\"slides/RI_Quiz_21/RI_Quiz_21.coffee\"\n				],\n				\"styles\": [\n					\"slides/RI_Quiz_21/RI_Quiz_21.styl\"\n				]\n			}\n		},\n		\"RI_Quiz_22\": {\n			\"id\": \"RI_Quiz_22\",\n			\"name\": \"DME_Eff_RI Quiz 22\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RI_Quiz_22/RI_Quiz_22.pug\"\n				],\n				\"scripts\": [\n					\"slides/RI_Quiz_22/RI_Quiz_22.coffee\"\n				],\n				\"styles\": [\n					\"slides/RI_Quiz_22/RI_Quiz_22.styl\"\n				]\n			}\n		},\n		\"RI_Quiz_23\": {\n			\"id\": \"RI_Quiz_23\",\n			\"name\": \"DME_Eff_RI Quiz 23\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RI_Quiz_23/RI_Quiz_23.pug\"\n				],\n				\"scripts\": [\n					\"slides/RI_Quiz_23/RI_Quiz_23.coffee\"\n				],\n				\"styles\": [\n					\"slides/RI_Quiz_23/RI_Quiz_23.styl\"\n				]\n			}\n		},\n		\"RI_Quiz_24\": {\n			\"id\": \"RI_Quiz_24\",\n			\"name\": \"DME_Eff_RI Quiz 24\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RI_Quiz_24/RI_Quiz_24.pug\"\n				],\n				\"scripts\": [\n					\"slides/RI_Quiz_24/RI_Quiz_24.coffee\"\n				],\n				\"styles\": [\n					\"slides/RI_Quiz_24/RI_Quiz_24.styl\"\n				]\n			}\n		},\n		\"RI_Quiz_25\": {\n			\"id\": \"RI_Quiz_25\",\n			\"name\": \"DME_Eff_RI Quiz 25\",\n			\"files\": {\n				\"templates\": [\n					\"slides/RI_Quiz_25/RI_Quiz_25.pug\"\n				],\n				\"scripts\": [\n					\"slides/RI_Quiz_25/RI_Quiz_25.coffee\"\n				],\n				\"styles\": [\n					\"slides/RI_Quiz_25/RI_Quiz_25.styl\"\n				]\n			}\n		}\n	},\n	\"modules\": {\n		\"ag-slide-analytics\": {\n			\"name\": \"Agnitio Slide Analytics\",\n			\"description\": \"Save data about the slides visited. Will call ag.submit.slide(data).\",\n			\"files\": {\n				\"scripts\": [\n					\"modules/rainmaker_modules/ag-slide-analytics/ag-slide-analytics.js\"\n				]\n			},\n			\"version\": \"0.7.0\"\n		},\n		\"ag-auto-menu\": {\n			\"name\": \"Agnitio Auto Menu\",\n			\"description\": \"Menu that automatically builds.\",\n			\"files\": {\n				\"styles\": [\n					\"modules/rainmaker_modules/ag-auto-menu/ag-auto-menu.css\"\n				],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ag-auto-menu/ag-auto-menu.js\"\n				]\n			},\n			\"version\": \"0.8.5\"\n		},\n		\"ag-slide-popup\": {\n			\"files\": {\n				\"scripts\": [\n					\"modules/rainmaker_modules/ag-slide-popup/ag-slide-popup.js\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ag-slide-popup/ag-slide-popup.css\"\n				]\n			}\n		},\n		\"ag-overlay\": {\n			\"name\": \"Agnitio Overlay\",\n			\"type\": \"universal\",\n			\"description\": \"Creates an overlay to the presentation.\",\n			\"files\": {\n				\"styles\": [\n					\"modules/rainmaker_modules/ag-overlay/ag-overlay.css\"\n				],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ag-overlay/ag-overlay.js\"\n				]\n			},\n			\"version\": \"0.5.0\"\n		},\n		\"ag-video\": {\n			\"name\": \"Agnitio Video Module\",\n			\"description\": \"Wrapper for HTML5 video tag to add states and custom controls\",\n			\"files\": {\n				\"templates\": [\n					\"modules/rainmaker_modules/ag-video/ag-video.html\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ag-video/ag-video.css\"\n				],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ag-video/ag-video.js\"\n				]\n			},\n			\"version\": \"0.9.6\"\n		},\n		\"ag-viewer\": {\n			\"id\": \"ag-viewer\",\n			\"files\": {\n				\"scripts\": [\n					\"modules/rainmaker_modules/ag-viewer/ag-viewer.js\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ag-viewer/ag-viewer.css\"\n				]\n			}\n		},\n		\"ap-toolbar\": {\n			\"id\": \"ap-toolbar\",\n			\"files\": {\n				\"templates\": [\n					\"modules/rainmaker_modules/ap-toolbar/ap-toolbar.html\"\n				],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ap-toolbar/ap-toolbar.js\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ap-toolbar/ap-toolbar.css\"\n				]\n			}\n		},\n		\"ap-overview\": {\n			\"id\": \"ap-overview\",\n			\"files\": {\n				\"templates\": [\n					\"modules/rainmaker_modules/ap-overview/ap-overview.html\"\n				],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ap-overview/ap-overview.js\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ap-overview/ap-overview.css\"\n				]\n			}\n		},\n		\"ap-module\": {\n			\"id\": \"ap-module\",\n			\"files\": {\n				\"templates\": [],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ap-module/ap-module.js\"\n				],\n				\"styles\": []\n			}\n		},\n		\"ap-auto-menu-handle\": {\n			\"id\": \"ap-auto-menu-handle\",\n			\"files\": {\n				\"templates\": [],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ap-auto-menu-handle/ap-auto-menu-handle.js\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ap-auto-menu-handle/ap-auto-menu-handle.css\"\n				]\n			}\n		},\n		\"ap-custom-collections-storage\": {\n			\"id\": \"ap-custom-collections-storage\",\n			\"files\": {\n				\"templates\": [],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ap-custom-collections-storage/ap-custom-collections-storage.js\"\n				],\n				\"styles\": []\n			}\n		},\n		\"ap-custom-collections-menu\": {\n			\"id\": \"ap-custom-collections-menu\",\n			\"files\": {\n				\"templates\": [\n					\"modules/rainmaker_modules/ap-custom-collections-menu/ap-custom-collections-menu.html\"\n				],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ap-custom-collections-menu/ap-custom-collections-menu.js\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ap-custom-collections-menu/ap-custom-collections-menu.css\"\n				]\n			}\n		},\n		\"ap-custom-collections\": {\n			\"id\": \"ap-custom-collections\",\n			\"files\": {\n				\"templates\": [\n					\"modules/rainmaker_modules/ap-custom-collections/ap-custom-collections.html\"\n				],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ap-custom-collections/ap-custom-collections.js\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ap-custom-collections/ap-custom-collections.css\"\n				]\n			}\n		},\n		\"ap-content-groups\": {\n			\"id\": \"ap-content-groups\",\n			\"files\": {\n				\"templates\": [],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ap-content-groups/ap-content-groups.js\"\n				],\n				\"styles\": []\n			}\n		},\n		\"ap-auto-references-popup\": {\n			\"id\": \"ap-auto-references-popup\",\n			\"files\": {\n				\"templates\": [],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ap-auto-references-popup/ap-auto-references-popup.js\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ap-auto-references-popup/ap-auto-references-popup.css\"\n				]\n			}\n		},\n		\"ah-auto-references-popup\": {\n			\"id\": \"ah-auto-references-popup\",\n			\"files\": {\n				\"templates\": [],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ah-auto-references-popup/ah-auto-references-popup.js\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ah-auto-references-popup/ah-auto-references-popup.css\"\n				]\n			}\n		},\n		\"ap-auto-side-clip\": {\n			\"id\": \"ap-auto-side-clip\",\n			\"files\": {\n				\"templates\": [],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ap-auto-side-clip/ap-auto-side-clip.js\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ap-auto-side-clip/ap-auto-side-clip.css\"\n				]\n			}\n		},\n		\"ap-media-repository\": {\n			\"id\": \"ap-media-repository\",\n			\"files\": {\n				\"templates\": [],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ap-media-repository/ap-media-repository.js\"\n				]\n			}\n		},\n		\"ap-back-navigation\": {\n			\"id\": \"ap-back-navigation\",\n			\"files\": {\n				\"templates\": [],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ap-back-navigation/ap-back-navigation.js\"\n				],\n				\"styles\": []\n			}\n		},\n		\"ap-favorite-presentations-buttons\": {\n			\"id\": \"ap-favorite-presentations-buttons\",\n			\"files\": {\n				\"templates\": [\n					\"modules/rainmaker_modules/ap-favorite-presentations-buttons/ap-favorite-presentations-buttons.html\"\n				],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ap-favorite-presentations-buttons/ap-favorite-presentations-buttons.js\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ap-favorite-presentations-buttons/ap-favorite-presentations-buttons.css\"\n				]\n			}\n		},\n		\"ap-media-library\": {\n			\"id\": \"ap-media-library\",\n			\"files\": {\n				\"templates\": [\n					\"modules/rainmaker_modules/ap-media-library/ap-media-library.html\"\n				],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ap-media-library/ap-media-library.js\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ap-media-library/ap-media-library.css\"\n				]\n			}\n		},\n		\"ap-reference-library\": {\n			\"id\": \"ap-reference-library\",\n			\"files\": {\n				\"templates\": [\n					\"modules/rainmaker_modules/ap-reference-library/ap-reference-library.html\"\n				],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ap-reference-library/ap-reference-library.js\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ap-reference-library/ap-reference-library.css\"\n				]\n			}\n		},\n		\"ap-video-library\": {\n			\"id\": \"ap-video-library\",\n			\"files\": {\n				\"templates\": [\n					\"modules/rainmaker_modules/ap-video-library/ap-video-library.html\"\n				],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ap-video-library/ap-video-library.js\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ap-video-library/ap-video-library.css\"\n				]\n			}\n		},\n		\"ap-specific-product-characteristics\": {\n			\"id\": \"ap-specific-product-characteristics\",\n			\"files\": {\n				\"templates\": [],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ap-specific-product-characteristics/ap-specific-product-characteristics.js\"\n				],\n				\"styles\": []\n			}\n		},\n		\"ap-follow-up-mail\": {\n			\"id\": \"ap-follow-up-mail\",\n			\"files\": {\n				\"templates\": [\n					\"modules/rainmaker_modules/ap-follow-up-mail/ap-follow-up-mail.html\"\n				],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ap-follow-up-mail/ap-follow-up-mail.js\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ap-follow-up-mail/ap-follow-up-mail.css\"\n				]\n			}\n		},\n		\"ap-frequently-asked-questions\": {\n			\"id\": \"ap-frequently-asked-questions\",\n			\"files\": {\n				\"templates\": [\n					\"modules/rainmaker_modules/ap-frequently-asked-questions/ap-frequently-asked-questions.html\"\n				],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ap-frequently-asked-questions/ap-frequently-asked-questions.js\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ap-frequently-asked-questions/ap-frequently-asked-questions.css\"\n				]\n			}\n		},\n		\"ap-notepad\": {\n			\"id\": \"ap-notepad\",\n			\"files\": {\n				\"templates\": [\n					\"modules/rainmaker_modules/ap-notepad/ap-notepad.html\"\n				],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ap-notepad/ap-notepad.js\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ap-notepad/ap-notepad.css\"\n				]\n			}\n		},\n		\"ap-slide-indicator\": {\n			\"id\": \"ap-slide-indicator\",\n			\"files\": {\n				\"templates\": [\n					\"modules/rainmaker_modules/ap-slide-indicator/ap-slide-indicator.html\"\n				],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ap-slide-indicator/ap-slide-indicator.js\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ap-slide-indicator/ap-slide-indicator.css\"\n				]\n			}\n		},\n		\"bhc-gpc-number\": {\n			\"id\": \"bhc-gpc-number\",\n			\"files\": {\n				\"templates\": [\n					\"modules/rainmaker_modules/bhc-gpc-number/bhc-gpc-number.html\"\n				],\n				\"scripts\": [\n					\"modules/rainmaker_modules/bhc-gpc-number/bhc-gpc-number.js\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/bhc-gpc-number/bhc-gpc-number.css\"\n				]\n			}\n		},\n		\"ah-cleanup\": {\n			\"name\": \"Anthill Cleanup Module\",\n			\"description\": \"Clean up slides not in use\",\n			\"type\": \"global\",\n			\"files\": {\n				\"scripts\": [\n					\"modules/rainmaker_modules/ah-cleanup/ah-cleanup.js\"\n				]\n			},\n			\"version\": \"0.9.0\"\n		},\n		\"ah-auto-references\": {\n			\"name\": \"Anthill AUTO REFERENCES\",\n			\"type\": \"global\",\n			\"files\": {\n				\"scripts\": [\n					\"modules/rainmaker_modules/ah-auto-references/ah-auto-references.js\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ah-auto-references/ah-auto-references.css\"\n				]\n			},\n			\"version\": \"0.9.0\"\n		},\n		\"ah-reference-library\": {\n			\"id\": \"ah-reference-library\",\n			\"files\": {\n				\"templates\": [\n					\"modules/rainmaker_modules/ah-reference-library/ah-reference-library.html\"\n				],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ah-reference-library/ah-reference-library.js\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ah-reference-library/ah-reference-library.css\"\n				]\n			}\n		},\n		\"ah-media-library\": {\n			\"id\": \"ah-media-library\",\n			\"files\": {\n				\"templates\": [\n					\"modules/rainmaker_modules/ah-media-library/ah-media-library.html\"\n				],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ah-media-library/ah-media-library.js\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ah-media-library/ah-media-library.css\"\n				]\n			}\n		},\n		\"ah-notes\": {\n			\"id\": \"ah-notes\",\n			\"files\": {\n				\"scripts\": [\n					\"modules/rainmaker_modules/ah-notes/ah-notes.coffee\"\n				]\n			}\n		},\n		\"ah-media-repository\": {\n			\"id\": \"ah-media-repository\",\n			\"files\": {\n				\"templates\": [],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ah-media-repository/ah-media-repository.js\"\n				],\n				\"styles\": []\n			}\n		},\n		\"ag-cleanup\": {\n			\"id\": \"ag-cleanup\",\n			\"files\": {\n				\"templates\": [],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ag-cleanup/ag-cleanup.js\"\n				],\n				\"styles\": []\n			}\n		},\n		\"ah-utils\": {\n			\"id\": \"ah-utils\",\n			\"files\": {\n				\"templates\": [],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ah-utils/ah-utils.coffee\"\n				],\n				\"styles\": []\n			}\n		},\n		\"ah-correct-events\": {\n			\"id\": \"ah-validator\",\n			\"files\": {\n				\"scripts\": [\n					\"modules/rainmaker_modules/ah-correct-events/ah-correct-events.coffee\"\n				]\n			}\n		},\n		\"ah-history\": {\n			\"id\": \"ah-history\",\n			\"files\": {\n				\"scripts\": [\n					\"modules/rainmaker_modules/ah-history/ah-history.coffee\"\n				]\n			}\n		},\n		\"ah-develop-sitemap\": {\n			\"id\": \"ah-develop-sitemapr\",\n			\"files\": {\n				\"templates\": [],\n				\"scripts\": [\n					\"modules/rainmaker_modules/ah-develop-sitemap/ah-develop-sitemap.coffee\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ah-develop-sitemap/ah-develop-sitemap.styl\"\n				]\n			}\n		},\n		\"ah-fix-slide-render\": {\n			\"id\": \"ah-fix-slide-render\",\n			\"files\": {\n				\"scripts\": [\n					\"modules/rainmaker_modules/ah-fix-slide-render/ah-fix-slide-render.coffee\"\n				]\n			}\n		},\n		\"ah-bottom-logo\": {\n			\"id\": \"ah-bottom-logo\",\n			\"files\": {\n				\"scripts\": [\n					\"modules/rainmaker_modules/ah-bottom-logo/ah-bottom-logo.coffee\"\n				]\n			}\n		},\n		\"ah-video\": {\n			\"id\": \"ah-video\",\n			\"files\": {\n				\"scripts\": [\n					\"modules/rainmaker_modules/ah-video/ah-video.coffee\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ah-video/ah-video.styl\"\n				]\n			}\n		},\n		\"ah-inline-indicators\": {\n			\"id\": \"ah-inline-indicators\",\n			\"files\": {\n				\"scripts\": [\n					\"modules/rainmaker_modules/ah-inline-indicators/ah-inline-indicators.coffee\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ah-inline-indicators/ah-inline-indicators.styl\"\n				]\n			}\n		},\n		\"ah-inline-menu\": {\n			\"id\": \"ah-inline-indicators\",\n			\"files\": {\n				\"scripts\": [\n					\"modules/rainmaker_modules/ah-inline-menu/ah-inline-menu.coffee\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ah-inline-menu/ah-inline-menu.styl\"\n				]\n			}\n		},\n		\"ah-inline-slideshow\": {\n			\"id\": \"ah-inline-slideshow\",\n			\"files\": {\n				\"scripts\": [\n					\"modules/rainmaker_modules/ah-inline-slideshow/ah-inline-slideshow.coffee\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ah-inline-slideshow/ah-inline-slideshow.styl\"\n				]\n			}\n		},\n		\"ah-binding-handler\": {\n			\"id\": \"ah-binding-handler\",\n			\"files\": {\n				\"scripts\": [\n					\"modules/rainmaker_modules/ah-binding-handler/ah-binding-handler.js\"\n				],\n				\"styles\": []\n			}\n		},\n		\"ah-fix-slide-animation\": {\n			\"id\": \"ah-fix-slide-animation\",\n			\"files\": {\n				\"scripts\": [\n					\"modules/rainmaker_modules/ah-fix-slide-animation/ah-fix-slide-animation.coffee\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ah-fix-slide-animation/ah-fix-slide-animation.styl\"\n				]\n			}\n		},\n		\"ah-listener\": {\n			\"id\": \"ah-listener\",\n			\"files\": {\n				\"scripts\": [\n					\"modules/rainmaker_modules/ah-listener/ah-listener.coffee\"\n				]\n			}\n		},\n		\"ah-refs-notes-popup\": {\n			\"id\": \"ah-refs-notes-popup\",\n			\"files\": {\n				\"scripts\": [\n					\"modules/rainmaker_modules/ah-refs-notes-popup/ah-refs-notes-popup.coffee\"\n				],\n				\"templates\": [\n					\"modules/rainmaker_modules/ah-refs-notes-popup/ah-refs-notes-popup.pug\"\n				],\n				\"styles\": [\n					\"modules/rainmaker_modules/ah-refs-notes-popup/ah-refs-notes-popup.styl\"\n				]\n			}\n		},\n		\"ah-slide-analytics\": {\n			\"id\": \"ah-slide-analytics\",\n			\"files\": {\n				\"scripts\": [\n					\"modules/rainmaker_modules/ah-slide-analytics/ah-slide-analytics.coffee\"\n				]\n			}\n		}\n	},\n	\"structures\": {\n		\"home\": {\n			\"name\": \"Home\",\n			\"id\": \"home\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"HomePageSlide\"\n			]\n		},\n		\"contraindications\": {\n			\"name\": \"contraindications\",\n			\"id\": \"ContraindicationsSlide\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"ContraindicationsSlide\"\n			]\n		},\n		\"most_frequently_observed\": {\n			\"name\": \"MostFrequentlyObservedSlide\",\n			\"id\": \"most_frequently_observed\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"MostFrequentlyObservedSlide\"\n			]\n		},\n		\"quiz_1\": {\n			\"name\": \"Question 1\",\n			\"id\": \"quiz_1\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RI_Quiz_1\"\n			]\n		},\n		\"quiz_2\": {\n			\"name\": \"Question 2\",\n			\"id\": \"quiz_2\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RI_Quiz_2\"\n			]\n		},\n		\"quiz_3\": {\n			\"name\": \"Question 3\",\n			\"id\": \"quiz_3\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RI_Quiz_3\"\n			]\n		},\n		\"quiz_4\": {\n			\"name\": \"Question 4\",\n			\"id\": \"quiz_4\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RI_Quiz_4\"\n			]\n		},\n		\"quiz_5\": {\n			\"name\": \"Question 5\",\n			\"id\": \"quiz_1\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RI_Quiz_5\"\n			]\n		},\n		\"quiz_6\": {\n			\"name\": \"Question 6\",\n			\"id\": \"quiz_6\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RI_Quiz_6\"\n			]\n		},\n		\"quiz_7\": {\n			\"name\": \"Question 7\",\n			\"id\": \"quiz_7\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RI_Quiz_7\"\n			]\n		},\n		\"quiz_8\": {\n			\"name\": \"Question 8\",\n			\"id\": \"quiz_8\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RI_Quiz_8\"\n			]\n		},\n		\"quiz_9\": {\n			\"name\": \"Question 9\",\n			\"id\": \"quiz_9\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RI_Quiz_9\"\n			]\n		},\n		\"quiz_10\": {\n			\"name\": \"Question 10\",\n			\"id\": \"quiz_10\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RI_Quiz_10\"\n			]\n		},\n		\"quiz_11\": {\n			\"name\": \"Question 11\",\n			\"id\": \"quiz_11\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RI_Quiz_11\"\n			]\n		},\n		\"quiz_12\": {\n			\"name\": \"Question 12\",\n			\"id\": \"quiz_12\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RI_Quiz_12\"\n			]\n		},\n		\"quiz_13\": {\n			\"name\": \"Question 13\",\n			\"id\": \"quiz_13\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RI_Quiz_13\"\n			]\n		},\n		\"quiz_14\": {\n			\"name\": \"Question 14\",\n			\"id\": \"quiz_14\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RI_Quiz_14\"\n			]\n		},\n		\"quiz_15\": {\n			\"name\": \"Question 15\",\n			\"id\": \"quiz_15\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RI_Quiz_15\"\n			]\n		},\n		\"quiz_16\": {\n			\"name\": \"Question 16\",\n			\"id\": \"quiz_16\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RI_Quiz_16\"\n			]\n		},\n		\"quiz_17\": {\n			\"name\": \"Question 17\",\n			\"id\": \"quiz_17\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RI_Quiz_17\"\n			]\n		},\n		\"quiz_18\": {\n			\"name\": \"Question 18\",\n			\"id\": \"quiz_18\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RI_Quiz_18\"\n			]\n		},\n		\"quiz_19\": {\n			\"name\": \"Question 19\",\n			\"id\": \"quiz_19\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RI_Quiz_19\"\n			]\n		},\n		\"quiz_20\": {\n			\"name\": \"Question 20\",\n			\"id\": \"quiz_20\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RI_Quiz_20\"\n			]\n		},\n		\"quiz_21\": {\n			\"name\": \"Question 21\",\n			\"id\": \"quiz_21\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RI_Quiz_21\"\n			]\n		},\n		\"quiz_22\": {\n			\"name\": \"Question 22\",\n			\"id\": \"quiz_22\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RI_Quiz_22\"\n			]\n		},\n		\"quiz_23\": {\n			\"name\": \"Question 23\",\n			\"id\": \"quiz_23\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RI_Quiz_23\"\n			]\n		},\n		\"quiz_24\": {\n			\"name\": \"Question 24\",\n			\"id\": \"quiz_24\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RI_Quiz_24\"\n			]\n		},\n		\"quiz_25\": {\n			\"name\": \"Question 25\",\n			\"id\": \"quiz_25\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RI_Quiz_25\"\n			]\n		},\n		\"recent_insights\": {\n			\"name\": \"Recent Insights\",\n			\"id\": \"recent_insights\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"RecentInsights10Slide\",\n				\"RecentInsights11Slide\",\n				\"RecentInsights12Slide\",\n				\"RecentInsights13Slide\",\n				\"RecentInsights14Slide\",\n				\"RecentInsights15Slide\",\n				\"RecentInsights16Slide\",\n				\"RecentInsights17Slide\",\n				\"RecentInsights18Slide\",\n				\"RecentInsights19Slide\",\n				\"RecentInsights110Slide\",\n				\"RecentInsights111Slide\",\n				\"RecentInsights112Slide\"\n			]\n		},\n		\"eylea\": {\n			\"name\": \"Eylea\",\n			\"id\": \"eylea\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"Eylea20Slide\"\n			]\n		},\n		\"start_strong_30\": {\n			\"name\": \"Start Strong 30\",\n			\"id\": \"start_strong_30\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"StartStrong30Slide\"\n			]\n		},\n		\"start_strong_31\": {\n			\"name\": \"Start Strong 31\",\n			\"id\": \"start_strong_31\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"StartStrong31Slide\"\n			]\n		},\n		\"start_strong_32\": {\n			\"name\": \"Start Strong 32\",\n			\"id\": \"start_strong_32\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"StartStrong32Slide\"\n			]\n		},\n		\"start_strong_35\": {\n			\"name\": \"Start Strong 35\",\n			\"id\": \"start_strong_35\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"StartStrong35Slide\"\n			]\n		},\n		\"stay_strong_40\": {\n			\"name\": \"Stay Strong 40\",\n			\"id\": \"stay_strong_40\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"StayStrong40Slide\"\n				\n			]\n		},\n		\"stay_strong_41\": {\n			\"name\": \"Stay Strong 41\",\n			\"id\": \"stay_strong_41\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"StayStrong41Slide\"\n				\n			]\n		},\n		\"multi_target_approach\": {\n			\"name\": \"Multi Target Approach\",\n			\"id\": \"multi_target_approach\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"MultiTargetApproachSlide\"\n				\n			]\n		},\n		\"unique_moa\": {\n			\"name\": \"Unique MoA\",\n			\"id\": \"unique_moa\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"UniqueMoA50Slide\"\n			]\n		},\n		\"discussion\": {\n			\"name\": \"Discussion\",\n			\"id\": \"discussion\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"Discussion60Slide\"\n			]\n		},\n		\"start_strong_30b_inline\": {\n			\"name\": \"Start Strong 3.0 B inline\",\n			\"id\": \"start_strong_30b_inline\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"StartStrong30B1PopupSlide\",\n				\"StartStrong30B2PopupSlide\",\n				\"StartStrong30B3PopupSlide\",\n				\"StartStrong30B4PopupSlide\",\n				\"StartStrong30B5PopupSlide\"\n			]\n		},\n		\"start_strong_30c_inline\": {\n			\"name\": \"Start Strong 3.0 C inline\",\n			\"id\": \"start_strong_30c_inline\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"StartStrong30C1PopupSlide\",\n				\"StartStrong30C3PopupSlide\"\n			]\n		},\n		\"start_strong_35a_inline\": {\n			\"name\": \"Start Strong 3.5 A inline\",\n			\"id\": \"start_strong_35a_inline\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"StartStrong35A2PopupSlide\",\n				\"StartStrong35A3PopupSlide\",\n				\"StartStrong35A4PopupSlide\"\n			]\n		},\n		\"start_strong_35b_inline\": {\n			\"name\": \"Start Strong 3.5 B inline\",\n			\"id\": \"start_strong_35b_inline\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"StartStrong35B1PopupSlide\"\n			]\n		},\n		\"stay_strong_40a_inline\": {\n			\"name\": \"Stay Strong 4.0 A inline\",\n			\"id\": \"stay_strong_40a_inline\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"StayStrong40A1PopupSlide\",\n				\"StayStrong40A2PopupSlide\"\n			]\n		},\n		\"stay_strong_40b_inline\": {\n			\"name\": \"Stay Strong 4.0 B inline\",\n			\"id\": \"stay_strong_40b_inline\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"StayStrong40B1PopupSlide\"\n			]\n		},\n		\"stay_strong_40c_inline\": {\n			\"name\": \"Stay Strong 4.0 C inline\",\n			\"id\": \"stay_strong_40c_inline\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"StayStrong40C1PopupSlide\",\n				\"StayStrong40C2PopupSlide\"\n			]\n		},\n		\"stay_strong_41a_inline\": {\n			\"name\": \"Stay Strong 4.1 A inline\",\n			\"id\": \"stay_strong_41a_inline\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"StayStrong41A1PopupSlide\",\n				\"StayStrong41A3PopupSlide\",\n				\"StayStrong41A4PopupSlide\"\n			]\n		},\n		\"unique_moa_50a_inline\": {\n			\"name\": \"Unique MoA 5.0 A inline\",\n			\"id\": \"unique_moa_50a_inline\",\n			\"shareable\": {},\n			\"indicators\": true,\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"UniqueMoA50A1PopupSlide\",\n				\"UniqueMoA50A2PopupSlide\",\n				\"UniqueMoA50A3PopupSlide\",\n				\"UniqueMoA50A4PopupSlide\",\n				\"UniqueMoA50A5PopupSlide\",\n				\"UniqueMoA50A6PopupSlide\"\n			]\n		},\n		\"unique_moa_50b_inline\": {\n			\"name\": \"Unique MoA 5.0 B inline\",\n			\"id\": \"unique_moa_50b_inline\",\n			\"shareable\": {},\n			\"type\": \"slideshow\",\n			\"content\": [\n				\"UniqueMoA50B1PopupSlide\",\n				\"UniqueMoA50B2PopupSlide\",\n				\"UniqueMoA50B3PopupSlide\",\n				\"UniqueMoA50B4PopupSlide\",\n				\"UniqueMoA50B5PopupSlide\"\n			]\n		}\n	},\n	\"storyboards\": {\n		\"start_collection\": {\n			\"name\": \"Start presentation\",\n			\"id\": \"start_collection\",\n			\"type\": \"collection\",\n			\"content\": [\n				\"home\",\n				\"eylea\",\n				\"start_strong_30\",\n				\"start_strong_31\",\n				\"start_strong_32\",\n				\"start_strong_35\",\n				\"stay_strong_40\",\n				\"stay_strong_41\",\n				\"multi_target_approach\",\n				\"contraindications\",\n				\"most_frequently_observed\",\n				\"discussion\",\n				\"quiz_1\",\n				\"quiz_2\",\n				\"quiz_3\",\n				\"quiz_4\",\n				\"quiz_5\",\n				\"quiz_6\",\n				\"quiz_7\",\n				\"quiz_8\",\n				\"quiz_9\",\n				\"quiz_10\",\n				\"quiz_11\",\n				\"quiz_12\",\n				\"quiz_13\",\n				\"quiz_14\",\n				\"quiz_15\",\n				\"quiz_16\",\n				\"quiz_17\",\n				\"quiz_18\",\n				\"quiz_19\",\n				\"quiz_20\",\n				\"quiz_21\",\n				\"quiz_22\",\n				\"quiz_23\",\n				\"quiz_24\",\n				\"quiz_25\"\n			]\n		}\n	},\n	\"storyboard\": [\n		\"start_collection\"\n	]\n}");
app.cache.put("media.json","{\n	\"content://Korobelnik J-F, Do DV, Schmidt-Erfurth U, et al. Intravitreal Aflibercept for Diabetic Macular Edema. <i>Ophthalmology</i>. 2014;121(11):2247-2254.\": {\n		\"title\": \"Korobelnik J-F, Do DV, Schmidt-Erfurth U, et al. Intravitreal Aflibercept for Diabetic Macular Edema. <i>Ophthalmology</i>. 2014;121(11):2247-2254.\",\n		\"referenceId\": \"korobelnik\"\n	},\n	\"content://Heier JS, Korobelnik JF, Brown DM. Intravitreal Aflibercept for Diabetic Macular Edema: <br>148-Week Results from the VISTA and VIVID Studies. <i>Ophthalmology</i>. 2016;123(11):2376-2385.\": {\n		\"title\": \"Heier JS, Korobelnik JF, Brown DM. Intravitreal Aflibercept for Diabetic Macular Edema: <br>148-Week Results from the VISTA and VIVID Studies. <i>Ophthalmology</i>. 2016;123(11):2376-2385.\",\n		\"referenceId\": \"heier\"\n	},\n	\"content://EYLEA (aflibercept solution for injection) Summary of Product Characteristics. Berlin, Germany: Bayer Pharma AG; 2016.\": {\n		\"title\": \"EYLEA (aflibercept solution for injection) Summary of Product Characteristics. Berlin, Germany: Bayer Pharma AG; 2016.\",\n		\"referenceId\": \"eylea\"\n	},\n	\"content://Data on file (VIVID and VISTA BCVA plot points through Year 1).\": {\n		\"title\": \"Data on file (VIVID and VISTA BCVA plot points through Year 1).\",\n		\"referenceId\": \"vivid\"\n	},\n	\"content://Ziemssen F, et al. Initiation of intravitreal aflibercept injection treatment in patients with diabetic macular edema: a review of VIVID‑DME and VISTA‑DME data. <i>Int J Retin Vitr.</i> 2016;2:16.\": {\n		\"title\": \"Ziemssen F, et al. Initiation of intravitreal aflibercept injection treatment in patients with diabetic macular edema: a review of VIVID‑DME and VISTA‑DME data. <i>Int J Retin Vitr.</i> 2016;2:16.\",\n		\"referenceId\": \"ziemssen\"\n	},\n	\"content://Davidson A, Ciulla TA, McGill JB, et al. How the diabetic eye loses vision. <br><i>Endocrine</i>. 2007;32(1):107-116.\": {\n		\"title\": \"Davidson A, Ciulla TA, McGill JB, et al. How the diabetic eye loses vision. <br><i>Endocrine</i>. 2007;32(1):107-116.\",\n		\"referenceId\": \"davidson\"\n	},\n	\"content://Globe DR, Wu J, Azen SP, et al. The impact of visual impairment on self-reported visual functioning in Latinos: The Los Angeles Latino Eye Study. <i>Ophthalmology</i>. 2004;111(6):1141-1149.\": {\n		\"title\": \"Globe DR, Wu J, Azen SP, et al. The impact of visual impairment on self-reported visual functioning in Latinos: The Los Angeles Latino Eye Study. <i>Ophthalmology</i>. 2004;111(6):1141-1149.\",\n		\"referenceId\": \"globe\"\n	},\n	\"content://Data on file (VIVID-EAST Year 1 data).\": {\n		\"title\": \"Data on file (VIVID-EAST Year 1 data).\",\n		\"referenceId\": \"vividEast\"\n	},\n	\"content://The Diabetic Retinopathy Clinical Research Network. Aflibercept, Bevacizumab, or Ranibizumab for Diabetic Macular Edema. <i>N Engl J Med.</i> 2015;372(13):1193-1203.\": {\n		\"title\": \"The Diabetic Retinopathy Clinical Research Network. Aflibercept, Bevacizumab, or Ranibizumab for Diabetic Macular Edema. <i>N Engl J Med.</i> 2015;372(13):1193-1203.\",\n		\"referenceId\": \"diabetic\"\n	},\n	\"content://Wells JA, Glassman AR, Ayala AR, et al. Aflibercept, Bevacizumab, or Ranibizumab for Diabetic Macular Edema. <i>Ophthalmology</i>. 2016;123(6):1351-1359.\": {\n		\"title\": \"Wells JA, Glassman AR, Ayala AR, et al. Aflibercept, Bevacizumab, or Ranibizumab for Diabetic Macular Edema. <i>Ophthalmology</i>. 2016;123(6):1351-1359.\",\n		\"referenceId\": \"wells\"\n	},\n	\"content://Brown DM, Schmidt-Erfurth U, Do DV, et al. Intravitreal Aflibercept for Diabetic Macular Edema: 100-Week Results From the VISTA and VIVID Studies. <i>Ophthalmology</i>. 2015;1122(10):2044-2052.\": {\n		\"title\": \"Brown DM, Schmidt-Erfurth U, Do DV, et al. Intravitreal Aflibercept for Diabetic Macular Edema: 100-Week Results From the VISTA and VIVID Studies. <i>Ophthalmology</i>. 2015;1122(10):2044-2052.\",\n		\"referenceId\": \"brown\"\n	},\n	\"content://Jampol LM, Glassman AR, Bressler NM, et al. Anti–Vascular Endothelial Growth Factor Comparative Effectiveness Trial for Diabetic Macular Edema: Additional Efficacy Post Hoc Analyses of a Randomized Clinical Trial. <i>JAMA Ophthalmol</i>. 2016;134(12):1429-1434.\": {\n		\"title\": \"Jampol LM, Glassman AR, Bressler NM, et al. Anti–Vascular Endothelial Growth Factor Comparative Effectiveness Trial for Diabetic Macular Edema: Additional Efficacy Post Hoc Analyses of a Randomized Clinical Trial. <i>JAMA Ophthalmol</i>. 2016;134(12):1429-1434.\",\n		\"referenceId\": \"jampol\"\n	},\n	\"content://Kovacs K, Marra KV, Yu G, et al. Angiogenic and Inflammatory Vitreous Biomarkers Associated With Increasing Levels of Retinal Ischemia. <i>Invest Ophthalmol Vis Sci</i>. 2015;56(11):6523-6530.\": {\n		\"title\": \"Kovacs K, Marra KV, Yu G, et al. Angiogenic and Inflammatory Vitreous Biomarkers Associated With Increasing Levels of Retinal Ischemia. <i>Invest Ophthalmol Vis Sci</i>. 2015;56(11):6523-6530.\",\n		\"referenceId\": \"kovacs\"\n	},\n	\"content://Bhagat N, Grigorian RA, Tutela A, et al. Diabetic macular edema: pathogenesis and treatment. <i>Surv Ophthalmol.</i> 2009;54(1):1-32.\": {\n		\"title\": \"Bhagat N, Grigorian RA, Tutela A, et al. Diabetic macular edema: pathogenesis and treatment. <i>Surv Ophthalmol.</i> 2009;54(1):1-32.\",\n		\"referenceId\": \"bhagat\"\n	},\n	\"content://Ando R, Noda K, Namba S, et al. Aqueous humour levels of placental growth factor in diabetic retinopathy. <i>Acta Ophthalmol.</i> 2014;92(3):e245-246.\": {\n		\"title\": \"Ando R, Noda K, Namba S, et al. Aqueous humour levels of placental growth factor in diabetic retinopathy. <i>Acta Ophthalmol.</i> 2014;92(3):e245-246.\",\n		\"referenceId\": \"ando\"\n	},\n	\"content://Rakic JM, Lambert V, Devy L, et al. Placental growth factor, a member of the VEGF family, contributes to the development of choroidal neovascularization. <i>Invest Ophthalmol Vis Sci.</i> 2003;44(7):3186-3193.\": {\n		\"title\": \"Rakic JM, Lambert V, Devy L, et al. Placental growth factor, a member of the VEGF family, contributes to the development of choroidal neovascularization. <i>Invest Ophthalmol Vis Sci.</i> 2003;44(7):3186-3193.\",\n		\"referenceId\": \"rakic\"\n	},\n	\"content://Papadopoulos N, Martin J, Ruan Q, et al. Binding and neutralization of vascular endothelial growth factor (VEGF) and related ligands by VEGF Trap, ranibizumab and bevacizumab. <i>Angiogenesis.</i> 2012;15(2):171-185.\": {\n		\"title\": \"Papadopoulos N, Martin J, Ruan Q, et al. Binding and neutralization of vascular endothelial growth factor (VEGF) and related ligands by VEGF Trap, ranibizumab and bevacizumab. <i>Angiogenesis.</i> 2012;15(2):171-185.\",\n		\"referenceId\": \"papadopoulos\"\n	},\n	\"content://Fauser S, Schwabecker V, Muether PS. Suppression of intraocular vascular endothelial growth factor during aflibercept treatment of age-related macular degeneration. <i>Am J Ophthalmol.</i> 2014 Sep;158(3):532-536.\": {\n		\"title\": \"Fauser S, Schwabecker V, Muether PS. Suppression of intraocular vascular endothelial growth factor during aflibercept treatment of age-related macular degeneration. <i>Am J Ophthalmol.</i> 2014 Sep;158(3):532-536.\",\n		\"referenceId\": \"fauser\"\n	},\n	\"content://Stewart MW, Rosenfeld PJ. Predicted biological activity of intravitreal VEGF Trap. <i>Br J Ophthalmol.</i> 2008;92:667-668.\": {\n		\"title\": \"Stewart MW, Rosenfeld PJ. Predicted biological activity of intravitreal VEGF Trap. <i>Br J Ophthalmol.</i> 2008;92:667-668.\",\n		\"referenceId\": \"stewart\"\n	},\n	\"content://Muether PS, Hermann MM, Dröge K, et al. Long-term stability of vascular endothelial growth factor suppression time under ranibizumab treatment in age-related macular degeneration. <i>Am J Ophthalmol.</i> 2013;156(5):989-993.e2.\": {\n		\"title\": \"Muether PS, Hermann MM, Dröge K, et al. Long-term stability of vascular endothelial growth factor suppression time under ranibizumab treatment in age-related macular degeneration. <i>Am J Ophthalmol.</i> 2013;156(5):989-993.e2.\",\n		\"referenceId\": \"muether\"\n	},\n	\"content://Stewart MW. Aflibercept (VEGF Trap-eye): the newest anti-VEGF drug. <i>Br J Ophthalmol.</i> 2012;96(9):1157-1158.\": {\n		\"title\": \"Stewart MW. Aflibercept (VEGF Trap-eye): the newest anti-VEGF drug. <i>Br J Ophthalmol.</i> 2012;96(9):1157-1158.\",\n		\"referenceId\": \"aflibercept\"\n	},\n	\"content://Boyer DS, Yoon YH, Belfort R Jr, et al. Three-Year, Randomized, Sham-Controlled Trial of Dexamethasone Intravitreal Implant in Patients with Diabetic Macular Edema. <i>Ophthalmology.</i> 2014;121(10):1904-1914.\": {\n		\"title\": \"Boyer DS, Yoon YH, Belfort R Jr, et al. Three-Year, Randomized, Sham-Controlled Trial of Dexamethasone Intravitreal Implant in Patients with Diabetic Macular Edema. <i>Ophthalmology.</i> 2014;121(10):1904-1914.\",\n		\"referenceId\": \"boyer\"\n	},\n	\"content://Gilles MC, Lim LL, Campain A, et al. A randomized clinical trial of intravitreal bevacizumab versus intravitreal dexamethasone for diabetic macular edema: the BEVORDEX study. <i>Ophthalmology.</i> 2014;121(12):2473-2481.\": {\n		\"title\": \"Gilles MC, Lim LL, Campain A, et al. A randomized clinical trial of intravitreal bevacizumab versus intravitreal dexamethasone for diabetic macular edema: the BEVORDEX study. <i>Ophthalmology.</i> 2014;121(12):2473-2481.\",\n		\"referenceId\": \"gilles\"\n	},\n	\"content://Callanan DG, Loewenstein A, Patel SS, et al. A multicenter, 12-month randomized study comparing dexamethasone intravitreal implant with ranibizumab in patients with diabetic macular edema. <i>Graefes Arch Clin Exp Ophthalmol.</i> 2016 Sept 8. [Epub ahead of print]\": {\n		\"title\": \"Callanan DG, Loewenstein A, Patel SS, et al. A multicenter, 12-month randomized study comparing dexamethasone intravitreal implant with ranibizumab in patients with diabetic macular edema. <i>Graefes Arch Clin Exp Ophthalmol.</i> 2016 Sept 8. [Epub ahead of print]\",\n		\"referenceId\": \"callanan\"\n	},\n	\"content://Boyer DS, Faber D, Gupta S, et al. Dexamethasone intravitreal implant for treatment of diabetic macular edema in vitrectomized patients. <i>Retina.</i> 2011;31(5):915-923.\": {\n		\"title\": \"Boyer DS, Faber D, Gupta S, et al. Dexamethasone intravitreal implant for treatment of diabetic macular edema in vitrectomized patients. <i>Retina.</i> 2011;31(5):915-923.\",\n		\"referenceId\": \"faber\"\n	},\n	\"content://OZURDEX (dexamethasone intravitreal implant) Summary of Product Characteristics. Westport, Ireland: Allergan; 2015.\": {\n		\"title\": \"OZURDEX (dexamethasone intravitreal implant) Summary of Product Characteristics. Westport, Ireland: Allergan; 2015.\",\n		\"referenceId\": \"ozurdex\"\n	},\n	\"content://Kim M, Kim Y, Lee S-J. Comparison of aqueous concentrations of angiogenic and inflammatory cytokines based on optical coherence tomography patterns of diabetic macular edema. <i>Indian J Ophthalmol.</i> 2015;63(4):312-317.\": {\n		\"title\": \"Kim M, Kim Y, Lee S-J. Comparison of aqueous concentrations of angiogenic and inflammatory cytokines based on optical coherence tomography patterns of diabetic macular edema. <i>Indian J Ophthalmol.</i> 2015;63(4):312-317.\",\n		\"referenceId\": \"kim\"\n	},\n	\"content://National Institute for Clinical Excellence. Dexamethasone intravitreal implant for treating diabetic macular oedema: Technology appraisal guidance TA349. 2015. Available at: http://nice.org.uk/guidance/ta349 [accessed December 2016]\": {\n		\"title\": \"National Institute for Clinical Excellence. Dexamethasone intravitreal implant for treating diabetic macular oedema: Technology appraisal guidance TA349. 2015. Available at: http://nice.org.uk/guidance/ta349 [accessed December 2016]\",\n		\"referenceId\": \"national\"\n	},\n	\"content://Mitchell P, Wong TY, Diabetic Macular Edema Treatment Guideline Working Group. Management paradigms for diabetic macular edema. <i>Am J Ophthalmology.</i> 2014;157(3):505-513:e1-8.\": {\n		\"title\": \"Mitchell P, Wong TY, Diabetic Macular Edema Treatment Guideline Working Group. Management paradigms for diabetic macular edema. <i>Am J Ophthalmology.</i> 2014;157(3):505-513:e1-8.\",\n		\"referenceId\": \"mitchell\"\n	}\n}\n");