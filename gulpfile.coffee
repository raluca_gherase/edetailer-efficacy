#"use strict"
#
#gulp = require 'gulp'
#pug = require 'gulp-pug'
#data = require 'gulp-data'
#path = require 'path'
#changed = require 'gulp-changed' #pipe trough only changed files
#
#exec = require('child_process').exec
#
#
#gulp.task 'pug', ->
#  return gulp.src 'app/slides/**/*.pug'
#  .pipe(data((file)->
#    return require(path.dirname(file.path) + '/translations/en.json')
#  ))
#  .pipe(pug(
#    pretty: true
#  ))
#  .pipe(gulp.dest('./build/slides/'))
#
#gulp.task 'coffee', ->
#  return gulp.src ['app/**/*.coffee']
#  .pipe(changed('./build/'))
#  .pipe(gulp.dest('./build/'))
#  .on 'end', -> console.log "=====Coffee========"
#
#gulp.task 'js', ->
#  return gulp.src ['app/**/*.js']
#  .pipe(gulp.dest('./build/'))
#
#gulp.task 'html', ->
#  return gulp.src ['app/**/*.html']
#  .pipe(gulp.dest('./build/'))
#
#gulp.task 'styl', ->
#  return gulp.src ['app/**/*.styl']
#  .pipe(gulp.dest('./build/'))
#
#gulp.task 'css', ->
#  return gulp.src ['app/**/*.css']
#  .pipe(gulp.dest('./build/'))
#
#gulp.task 'build', ['coffee', 'pug', 'js', 'html', 'styl', 'css'], ->
#  console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
#
#
#gulp.task 'watch', ->
##  gulp.watch('app/**/*.*', ['build'])
#  gulp.watch('app/**/*.coffee', ['coffee'])
#  gulp.watch('app/**/*.pug', ['pug'])
#  gulp.watch('app/**/*.js', ['js'])
#  gulp.watch('app/**/*.html', ['html'])
#  gulp.watch('app/**/*.styl', ['styl'])
#  gulp.watch('app/**/*.css', ['css'])
#
#  exec('agnitio run', {cwd: './build'}
#  )
#  .stdout.on 'data', (data)->
#    console.log data

# We create only agnitio platform.
# Please check argv for extend and conditionals.
platformConfig = require './gulp_build/configs/config.rainmaker.coffee'
RainmakerPlatform = require './gulp_build/platforms/RainmakerPlatform'
new RainmakerPlatform(platformConfig).register()

#Veeva platform
veevaPlatformConfig = require './gulp_build/configs/config.veeva.coffee'
VeevaPlatform = require './gulp_build/platforms/VeevaPlatform'
new VeevaPlatform(veevaPlatformConfig).register()

#CSV file generator
config = require "./gulp_build/configs/config.veeva.coffee"
localConfig = "./app/config.json"
VeevaCSVGenerator = require './gulp_build/platforms/VeevaCSVGenerator'
new VeevaCSVGenerator(config, localConfig)

#Cegedim platform
cegedimPlatformConfig = require './gulp_build/configs/config.cegedim.coffee'
CegedimPlatform = require './gulp_build/platforms/CegedimPlatform'
new CegedimPlatform(cegedimPlatformConfig).register()

#Veeva platform new package format
veevaPlatformConfig = require './gulp_build/configs/config.veeva.coffee'
VeevaPlatformNewPackageFormat = require './gulp_build/platforms/VeevaPlatformNewPackageFormat'
new VeevaPlatformNewPackageFormat(veevaPlatformConfig).register()