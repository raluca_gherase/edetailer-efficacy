
class StartStrong31Slide
  constructor: ()->
    @events = {}
    @states = []

  onRender: (element)->
    $slider = element.getElementsByClassName("before-after-slider")[0]
    baseFont = parseFloat($("body").css("font-size"))
    controlPoints = [72, 127, 189, 243, 316, 387, 445, 511, 571, 641]
    controlPoints2 = [93, 165, 244, 318, 413, 505, 586, 669, 745, 842]
    @slider = new BeforeAfterSlider($slider, if baseFont < 21 then controlPoints else controlPoints2)
    app.BeforeAfterSlider = {}
    @slider.touchEnd = (index)->
      app.BeforeAfterSlider.value = index
#      monitoring here

  onEnter: (element)->

  onExit: (element)->
    @slider.reset()

app.register('StartStrong31Slide', -> new StartStrong31Slide())