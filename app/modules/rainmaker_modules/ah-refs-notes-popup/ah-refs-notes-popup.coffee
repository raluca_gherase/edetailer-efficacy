app.register 'ah-refs-notes-popup', ->
  {
    publish: {
    }
    events: {
      "tap .x": "hide"
      "swipeup": "stopSwipe"
      "swipedown": "stopSwipe"
      "swipeleft": "stopSwipe"
      "swiperight": "stopSwipe"
    }
    states: [
      {
        id: "show"
      }
    ]

    onRender: (element) ->
      @element = $(element)
      @refModule = app.module.get 'ah-auto-references-popup'
      @loadedNotes = window.ahstrings.notes

      app.on 'history:update', @autoPopupHandler.bind(@)
      app.on 'state:update', @autoPopupHandler.bind(@)
      app.on 'notes:update', @getNotesList.bind(@)
      app.on 'references:update', @getRefsList.bind(@)

      @$refWrapper = @element.find('.refs-wrapper')
      @$listRefs = @element.find('.references')
      @notesWrapper = @element.find('.notes-wrapper')
      @$listNotes = @element.find('.notes')
      @$scroll = @element.find('.scroll')[0]
      @scroll = new IScroll(@$scroll, {scrollbars: 'custom'})

    getNotesList: (data)-> @notesList = data.list

    getRefsList: (data)-> @refsList = data.list

    refWithReplaceIndex: ($refElement, index)->
      @refModule.replaceIndex $refElement, index

    disable: (node, list)->
      node.addClass 'disable' if list.length <=0

    enable: (nodeElem)-> nodeElem.removeClass 'disable'

    generateRefsList: (tmpReferences)->
      Object.keys(tmpReferences).map (file, index) =>
        $refElement = window.mediaRepository.render file, tmpReferences[file]
        @refWithReplaceIndex $refElement, index
        $refElement

    generateNotesList: (notesList)->
      notesList.map (item)=>
        $("<li>#{@loadedNotes[item].description}</li>")

    autoPopupHandler: (data)->
      $slide = $(data.slide.el)
      $slide.find('[data-reference-id], [data-notes]')
        .off('tap.auto-references-popup')
        .on 'tap.auto-references-popup', (event)=>
          @initRefAndNotesPopup(data, event)
          do @show

    initRefAndNotesPopup: (data, event)->
      event.stopPropagation()
      @$listRefs.empty()
      @$listNotes.empty()
      @addRefsNodes @refsList
      @addNotesNodes @notesList
      @scroll.refresh()
      @scroll.scrollTo(0, 0)

    addRefsNodes: (refsList)->
      @disable( @$refWrapper, refsList)
      @$listRefs.append @generateRefsList(refsList)
      @$refWrapper.append @$listRefs

    addNotesNodes: (notesList)->
      @disable(@notesWrapper, notesList)
      @$listNotes.append @generateNotesList(notesList)
      @notesWrapper.append @$listNotes

    stopSwipe: ()->  event.stopPropagation()

    show: -> @goTo 'show'

    hide: ->
      @reset()
      @enable @$refWrapper
      @enable @notesWrapper
  }