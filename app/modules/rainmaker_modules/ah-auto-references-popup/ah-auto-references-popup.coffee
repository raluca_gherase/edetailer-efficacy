###
# Implements auto reference popups.
# ---------------------------------
#
# Adds tap handlers to every element that has the [data-reference-id] data attribute.
# On tap a popup with a list of all references in the slide appears.
#
# The module is loaded within index.html
#
# @module ah-auto-references-popup.js
# @requires jquery.js, touchy.js
# @author Andreas Tietz, antwerpes ag

# Modified by Anthill/Infopulse
###

app.register 'ah-auto-references-popup', ->
  indexSeparator = ','
  groupSeparator = '-'
  resources = []
  {
    publish: {
      isglobalnumeric: false
      isfiltertrack: false
    }
    events: {

    }
    states: []

    getCurrentSlide: ()->
      app.slide.get(app.slideshow.get())?.el

    onRender: (el) ->
      @isGlobalNumeric = @props.isglobalnumeric
      @isFilterTrack = @props.isfiltertrack
      @data = window.mediaRepository.metadata()
      app.on 'close:overlay', ()=>
        @autoReferences @getCurrentSlide()
      app.on 'history:update', @autoReferences.bind(this)
      app.listenTo app.slideshow, 'update:current', ()=>
        currentSlide = @getCurrentSlide()
        if currentSlide then @parseReferences currentSlide

    onRemove: (el) ->

    onEnter: (el) ->

    onExit: (el) ->

    removePopup: ()->
      $popupOverlay.remove() if $popupOverlay

    isReferenceId: (data, media) ->
      if(data[media] && data[media].hasOwnProperty("referenceId"))
        return data[media]
      false

    filterTrack: (references) ->
      currentTrack = app.slideshow.getId()
      filterReferences = {}
      if(@isFilterTrack)
        for key of references
          currentFlow = references[key].flow.split(',')
          if(currentFlow.indexOf(currentTrack) != -1)
            filterReferences[key] = references[key]
      if Object.keys(filterReferences).length != 0
        return filterReferences
      else
        return references

    getIndex: (id)->
      if @isGlobalNumeric
        @getGlobalIndex(id)
      else
        @getLocalIndex(id)

    getLocalIndex: (id)->
      filterReference = []
      references = []
      for index of @refList
        if @refList[index].indexOf(groupSeparator) > -1
          range = @refList[index].split(groupSeparator)
          references.push range[0]
          references.push range[1]
        else
          if @refList[index].indexOf(indexSeparator) > -1
            range = @refList[index].split(indexSeparator)
            for i of range
              references.push range[i]
          else
            references.push @refList[index]

      for index of references
        filterReference.push references[index] if filterReference.indexOf(references[index]) == -1
      index = filterReference.indexOf(id) + 1
      index

    getGlobalIndex: (id)->
      dataRef = {}
      data = window.mediaRepository.metadata()
      for media of data
        if @isReferenceId(data, media)
          dataRef[media] = @isReferenceId(data, media)
      dataRefFilter = @filterTrack(dataRef)
      for media of dataRefFilter
        for key of dataRefFilter[media]
          if key is 'referenceId' and dataRefFilter[media][key] is id
            mediaIndex = Object.keys(dataRefFilter).indexOf(media) + 1
      mediaIndex

    getRefByIndex: (index)->
      data = window.mediaRepository.metadata()
      i = 1
      for key of data
        refId = data[key]['referenceId'] if i is index
        i++
      refId

    getReferencesIds: (wrapper)->
      @refList = []
      @elements = wrapper.querySelectorAll('[data-reference-id]')
      for ele in  @elements
        @refList.push ele.getAttribute('data-reference-id')
      @refList

    parseReferences: (data, references)->
      @referenceIds = []
      # Collect unique reference ids:
      if !references
        $slide = app.dom.get(data.id)
        references = @getReferencesIds $slide

      for id, index in references
        @referenceMap(id, index)
      resources = Object.keys(@referenceIds)
      app.trigger 'references:update', {list: @getResources()}

    referenceMap: (referenceId, index)->
      allIndexes = []
      referenceId.split(indexSeparator).forEach (referencesList)=>
        references = referencesList.split(groupSeparator)
        reference = references[0]
        if references.length > 1
          indexes = []
          from = @getIndex reference
          to = @getIndex references[1]
          i = from
          while i <= to
            @referenceIds[@getRefByIndex(i)] = true
            i++
          indexes.push from, to
          indexesString = indexes.join(groupSeparator)
        else
          indexesString = @getIndex reference
          @referenceIds[reference] = true
        if allIndexes.length is 0
          allIndexes.push indexesString
        else
          allIndexes = [allIndexes, indexesString].join(indexSeparator)
      @render(allIndexes, index)
      @referenceIds

    render: (indexes, index)->
      if !@isGlobalNumeric
        if indexes.indexOf(indexSeparator) > -1
          range = indexes.split(indexSeparator)
          if range.length > 2
            indexes = range[0] + "-" +range[range.length - 1]
      @elements[index].textContent = indexes

    getResources: ()->
# Find media resources associated with the collected reference ids:
      references = {}
      if @isGlobalNumeric
        $.each window.mediaRepository.metadata(), (file, meta) ->
          if resources.indexOf('' + meta.referenceId) > -1
            references[file] = meta
      else
        $.each resources, (index) ->
          $.each window.mediaRepository.metadata(), (file, meta) ->
            if meta.referenceId == resources[index]
              references[file] = meta
      references

    autoReferences: (data) ->
      @parseReferences(data)

    getLocalIndex: (index) -> "[" + index + "]"

    replaceIndex: ($ref, index)->
      unless @isGlobalNumeric
        $refNum = $ref[0].getElementsByClassName('referenceId')?[0]
        $refNum.innerHTML = @replaceLocalIndex(++index)
  }