app.register 'ah-inline-menu', ->
  {
    events: {}
    states: []
    excludedSlides: []
    updatedSlide: []

    onRender: () ->
      app.on('init:inlineSlideshow', @load.bind(@))
      app.on('slideEnter:inlineSlideshow', @setActive.bind(@))

    hide: ()->
      document.getElementsByClassName('ah-top-inline-sidebar')[0].classList.add 'hide'
      document.getElementsByClassName('inline-slideshow-wrapper')[0].classList.remove 'menu-indicator'

    show: ()->
      document.getElementsByClassName('ah-top-inline-sidebar')[0].classList.remove 'hide'
      document.getElementsByClassName('inline-slideshow-wrapper')[0].classList.add 'menu-indicator'

    load: (props)->
      @indicatorWrappers = props.data.view.container.getElementsByClassName('inline-menu')[0]
      inlineSlideshow = app.model.get().structures[props.data.slideshow]
      @show()
      if !@indicatorWrappers or inlineSlideshow.indicators
        @hide()
        return

      indicatorWrapper = document.createElement 'nav'

      @slides = inlineSlideshow.content
      @slides.forEach (slide)=>
        indicator = document.createElement 'button'
        indicator.innerHTML = app.model.get().slides[slide].name
        indicator.classList.add 'inline-navigator'
        indicator.setAttribute 'indicator', slide
        indicator.setAttribute 'data-goto-inline-slide', slide
        indicatorWrapper.appendChild indicator
      @indicatorWrappers.appendChild indicatorWrapper

    setActive: (props)->
      @indicatorWrappers.getElementsByClassName('active')?[0]?.classList.remove 'active'
      @indicatorWrappers.querySelector('[indicator="' + props.slide.el.id + '"]')?.classList.add 'active'
  }