# This module adds data attributes event handlers on slide enter

# * How to use:
    Open popup on element tap:
        div(data-popup="PopupSlideId")
    Change slide state on element tap:
        div(data-goto-state="animate")
    Go to other slide on element tap:
        div(data-goto-slide="SlideId")
    Disable swipe on definite element:
        div(data-stop-swipe)