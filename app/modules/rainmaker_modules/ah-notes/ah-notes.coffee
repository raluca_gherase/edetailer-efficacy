app.register 'ah-notes', ->
  {
    publish: {
      useGlobal: false
      attr: "data-notes"
      separator: ' '
      separateValue: 1
      useLatinNumbers: true
      startIndexLetter: 64
      parseNumberLetter: 26
    }
    events: {
      "tap .ag-overlay-x": "hide"
    }
    states: [
      {
        id: "show"
      }
    ]
    loadedData: null
    _renderedNotes: {}


    onRender: () ->
      @$root = $("#notes")
      @$list = @$root.find 'ul'
      @_preventSwipe()
      @_dataSetPropName = @_getDatasetProp()
      do @init # fix
      if @scroll
        @scroll.destroy()
      @scroll = new IScroll($(".scroll")[0], scrollbars: false, bounce: false)

    handleEnter: (el) ->
      @resetList()
      notesForCurrentSlide = @_renderedNotes[el.id]
      return unless notesForCurrentSlide
      console.log notesForCurrentSlide
      app.trigger 'notes:update', {list: notesForCurrentSlide}

    handleRender: (el) ->
      @_$el = $(".slide[id=#{el.id}]")
      @updateNotes(@_$el) if @_$el.length

    handleLeave: () ->
      # @observer.disconnect()  #TODO

    handleCloseOverlay: ()->
      el = {}
      el.id = app.slideshow.get()
      @handleEnter el

    _preventSwipe: ->
      @$root.on "swipedown swipeup swiperight swipeleft", (e) ->
        e.stopPropagation()

    show: ->
      if @$list.find('li').length
        @goTo 'show'

    hide: ->
      @reset()

    init: () ->
      @loadedData = window.ahstrings.notes
      @handleRender({id: app.slideshow.get()}) #fix rendering on first slide
      app.listenTo(app.slide, 'slide:render', @handleRender.bind(@))
      app.on('history:update', @handleEnter.bind(@))
      app.listenTo(app.slide, 'slide:exit', @handleLeave.bind(@))
      app.listenTo(app, 'close:overlay', @handleCloseOverlay.bind(@)) #add parse notes on overlay exit

    _fiterUnique: (keys) ->
      keys.filter (el, index, array) ->
        array.indexOf(el) == index

    _parseDOM: (el) ->
      keys = []
      elements = []
      elId = el[0].id
      el.find("[#{@props.attr}]").each (iterator, element) =>
        noteKey = @_getNotesKey(element)
        if !noteKey.length
          @_throwException "Invalid notes in #{elId}"
        keys.push noteKey.trim()
        elements.push(element)

      notesKeys = if keys.length then @_fiterUnique(@_devideArrray(keys)) else []
      bindedData = @_getBindedData notesKeys, elements
      @_setCounters(bindedData)
      @_renderedNotes[elId] = notesKeys
      notesKeys

    _throwException: (message) ->
      throw new Error(message)

    _getBindedData: (notesKeys, elements) ->
      binding = elements.map (element) =>
        containedNotes = []
        notesKeys.forEach (key, index) =>
          if @_contains(@_getNotesKey(element), key)
            containedNotes.push {
              key
              index: if !@props.useGlobal then index else @_getAbsoluteCounter(key)
            }
        {
          element
          containedNotes
        }

    _setCounters: (bindedData) ->
      bindedData.forEach (item) =>
        counters = @_getCounters item.containedNotes
        sequenced = @_getSequence(counters)
        item.element.textContent = sequenced.toString()

    resetList: ->
      @$list.empty()

    updateNotes: (el) ->
      @_parseDOM(el)

    _getSequence: (counters) ->
      sequenced = []
      counters.reduce (accum, curr, index, array) =>
        if curr == array[index + 1] - 1
          ++accum
        else
          if accum > @props.separateValue
            sequenced.push("#{@_generateLetterOrNumber(index-accum)}-#{@_generateLetterOrNumber(index)}")
          else
            for i in [index - accum..index]
              sequenced.push @_generateLetterOrNumber(array[i])
          accum = 0
      , 0
      sequenced

    _generateLetterOrNumber: (number)->
      if @props.useLatinNumbers
        String.fromCharCode(@props.startIndexLetter + (number % @props.parseNumberLetter)).toLowerCase()
      else
        number

    _getCounters: (containedNotes) ->
      containedNotes.map (item) ->
        ++item.index

    _getNotesKey: (element) ->
      element.dataset[@_dataSetPropName]

    _devideArrray: (keys) ->
      keys.join(@props.separator).split(@props.separator)

    _contains: (string, key) ->
      new RegExp("\\b#{key}\\b", "g").test string

    _getAbsoluteCounter: (key) ->
      Object.keys(@loadedData).indexOf(key)

    _getDatasetProp: ->
      propName = @props.attr.slice(5, @props.attr.length)
      camelCased = propName.replace(/-([a-z])/g, (letter) ->
        letter[1].toUpperCase()
      )
  }