/**
 * Implements Custom Collections Menu.
 * -----------------------------------
 *
 * This module allows the creation / naming / deletion and loading of custom collections.
 *
 * @module ap-custom-collections-menu.js
 * @requires jquery.js, iscroll.js, custom-collections-storage.js
 * @author David Buezas, antwerpes ag
 */
app.register("ap-custom-collections-menu", function () {


    var translation = {
        "ENTER_PRESENTATION_NAME": "Please enter a name for your presentation",
        "PRESENTATION_NAME": "Presentation name",
        "NAME_ALREADY_EXISTS": "Another presentation exists with the same name, please choose a new one",
        "$PRESENTATION_NAME$_WILL_BE_REMOVED": "'$PRESENTATION_NAME$' will be erased",
        "OK": "OK",
        "CANCEL": "Cancel",
        "YES": "Yes",
        "NO": "No",
        "PRESENTATION_IS_EMPTY": "This presentation is empty.",
        "NOT_ALLOWED_TO_DELETE": "Deleting the current presentation is not allowed"
    };

    /**
     * Implements Custom Collections Menu
     * ------------------------------------
     * This module allows the creation / naming / deletion and loading of custom collections.
     *
     * @class ap-custom-collections-menu
     * @constructor
     */

    var self;
    return {
        publish: {},
        events: {
            "tap .newPresentation": "createNewPresentation",
            "tap .trash": "deletePresentation",
            "tap .rename": "renamePresentation",
            "tap .presentation": "openPresentation",
            "tap .edit": "editPresentation",
            "tap .favorite": "favorisePresentation"
        },
        states: [
            {
                id: "visible"
            }
        ],
        onRender: function (el) {

            self = this;

            app.$.customCollectionsMenu = this;

            app.$.on('open:ap-custom-collections-menu', function(){

                this.show();

            }.bind(this));

            app.$.on('close:ap-custom-collections-menu', function(){
                this.hide();
            }.bind(this));

            app.$.on('toolbar:hidden', function(){
                this.hide();
            }.bind(this));



            self.appriseDefaults = {
                textOk: translation["OK"],
                textCancel: translation["CANCEL"],
                textYes: translation["YES"],
                textNo: translation["NO"]
            };

            $(".presentationsContainer").sortable({
                items: ".row:not(.newPresentationButtonContainer)",
                scroll: true,
                update: function (event, ui) {
                    self.updateCustomPresentationOrder();
                }
            });


            $.each(app.$.customCollectionsStorage.getAll(), function (index, presentationObject) {
                self._addPresentationToView(presentationObject.name, presentationObject, /* animated: */false);
            });
        },
        onRemove: function (el) {

        },
        onEnter: function (el) {

        },
        onExit: function (el) {

        },

        hide: function () {
            app.unlock();
            this.reset();
        },

        show: function () {
            app.lock();
            this.goTo('visible');
        },


        /**
         * Adds a new entry to the menu
         * -----------------------------------
         *
         * @param {string} presentationName
         * @param {object} presentationObject
         * @param {boolean} animated
         * @private
         * @method _addPresentationToView
         */
        _addPresentationToView: function (presentationName, presentationObject, animated) {
            var self = this;
            var $row = $(".row.template").clone();


            $row.removeClass("template");
            $row.data("presentationName", presentationName);
            $row.find(".presentation .name").text(presentationName);
            if (presentationObject.isFavorite)
                $row.find(".favorite").addClass('selected');
            $row.insertBefore(self.$(".newPresentationButtonContainer"));


        },

        /**
         * Updates the current order of custom presentations
         * @public
         * @method updateCustomPresentationOrder
         */
        updateCustomPresentationOrder: function () {
            var self = this;

            var rowsArray = [];

            $('.presentationsContainer').children('.row').each(function () {
                var presentationName = $(this).data("presentationName");

                if (presentationName != undefined) {

                    if (app.$.customCollectionsStorage.getFavorites().length < 3) {
                        var isFavorite = $(this).find('.favorite').hasClass('selected');

                        var collection = app.$.customCollectionsStorage.get(presentationName);

                        collection.isFavorite = isFavorite;

                        app.$.customCollectionsStorage.add(presentationName, collection);
                    }

                    rowsArray.push(presentationName);
                }


            });

            if (rowsArray.length > 1)
                app.$.customCollectionsStorage.updateOrder(rowsArray);

            app.$.trigger("update:favorites");

        },

        /**
         * creates a new presentation
         * @public
         * @method createNewPresentation
         */
        createNewPresentation: function(){
            function askName(defaultName) {
                var optn = $.extend({}, self.appriseDefaults, {input: defaultName});
                apprise(translation["ENTER_PRESENTATION_NAME"], optn, function (newName) {
                    if (newName === false || newName === "") {
                        return; //user canceled
                    } else if (app.$.customCollectionsStorage.get(newName) != undefined) {
                        apprise(translation["NAME_ALREADY_EXISTS"], self.appriseDefaults, function () {
                            askName(newName); //ask again
                        });
                    } else {
                        var collectionId = "custom-collection-" + Math.floor((Math.random() * 10000) +1);

                        var presentationObject = {
                            id: collectionId,
                            name: newName,
                            type: "collection",
                            slideshows: [{
                                "id": "home",
                                "name": "Home",
                                "type": "slideshow",
                                "content": ["HomePageSlide"]
                            }, {
                                "id": "discussion",
                                "name": "Discussion",
                                "type": "slideshow",
                                "content": ["Discussion60Slide"]
                            }],
                            slides: ["HomePageSlide", "Discussion60Slide"],
                            presentation: [
                              ["HomePageSlide"],
                              ["Discussion60Slide"]
                            ],
                            isCustomPresentation: true,
                            isFavorite: false
                        };
                        app.$.customCollectionsStorage.add(newName, presentationObject);
                        self._addPresentationToView(newName, presentationObject, true);
                    }
                });
            }

            var standardName = translation["PRESENTATION_NAME"];
            askName(standardName);
        },


        /**
         * deletes the presentation
         * @public
         * @method deletePresentation
         */
        deletePresentation: function(event){

            var $row = $(event.target).parent();
            var presentationName = $row.data("presentationName");
            var confirmationTitle = translation["$PRESENTATION_NAME$_WILL_BE_REMOVED"].replace("$PRESENTATION_NAME$", presentationName);
            var optn = $.extend({}, self.appriseDefaults, {confirm: true});
            var current = app.slideshow.getId();
            var collections = app.$.customCollectionsStorage.getAll();
            var collectionData = collections[presentationName];
            if (collectionData && collectionData.id !== current) {
              apprise(confirmationTitle, optn, function (answer) {
                  if (answer) {
                      app.$.customCollectionsStorage.delete(presentationName);
                      $row.animate({height: 0}, function () {
                          $(this).remove();

                      });
                  }
              });

              app.$.trigger("update:favorites");
            }
            else {
              apprise(translation["NOT_ALLOWED_TO_DELETE"], self.appriseDefaults);
            }
        },


        /**
         * renames the presentation
         * @public
         * @method renamePresentation
         */
        renamePresentation: function(event){
            var $row = $(event.target).parent();

            function askName(defaultName) {
                var optn = $.extend({}, self.appriseDefaults, {input: defaultName});
                apprise(translation["ENTER_PRESENTATION_NAME"], optn, function (newName) {
                    if (newName === false || newName == oldName || newName === "") {
                        return; //user canceled
                    } else if (app.$.customCollectionsStorage.get(newName) != undefined) {
                        apprise(translation["NAME_ALREADY_EXISTS"], self.appriseDefaults, function () {
                            askName(newName); //ask again
                        });
                    } else {
                        app.$.customCollectionsStorage.rename(oldName, newName);
                        $row.data("presentationName", newName);
                        var $name = $row.find(".name");
                        $name.css({opacity: 0});
                        setTimeout(function () {
                            $name.text(newName);
                            $name.css({opacity: 1});
                        }, 500);
                    }
                });
            }

            var oldName = $row.data("presentationName");
            askName(oldName);
        },

        /**
         * open the presentation
         * @public
         * @method openPresentation
         */

        openPresentation: function(event){

            var presentationName = $(event.target).parent().parent().data("presentationName");

            if(presentationName == undefined)
                presentationName = $(event.target).parent().data("presentationName");


            var collection = app.$.customCollectionsStorage.get(presentationName);

            //app.menu.linksConfig[homeSlideName] = {title: "<div class='homeIcon' />", classname: "home"}

            if (collection.slideshows.length > 0) {
                var slideshowIdArray = [];
                $.each(collection.slideshows, function (i, slideshow) {
                    slideshowIdArray.push(slideshow.id);
                    var slidesArrary = [];
                    $.each(slideshow.content, function (i, slide) {
                        slidesArrary.push(slide);
                    });

                    var temp = {
                        id: slideshow.id,
                        name: slideshow.name,
                        content: slidesArrary,
                        type: "slideshow"
                    };

                    if(!app.model.hasStructure(slideshow.id))
                        app.model.addStructure(slideshow.id, temp);

                });

                var storyboardData = {
                    id: collection.id,
                    name: collection.name,
                    content: slideshowIdArray
                };

                if(!app.model.hasStoryboard(collection.id))
                    app.model.addStoryboard(collection.id, storyboardData);

                app.slideshow.init(collection.id);
                app.slideshow.load(collection.id);
                app.$.toolbar.hide();

            } else {
                apprise(translation["PRESENTATION_IS_EMPTY"], self.appriseDefaults);
            }
        },


        /**
         * edit the presentation
         * @public
         * @method editPresentation
         */

        editPresentation: function(event) {
            // load custom collections editor into own container
            /*
            new CustomCollections({
                $container: self.$container,
                presentationName: $(this).parent().data("presentationName")
            });
            */
            self.hide();
            var trigger = "open:ap-custom-collections";
            var presentationName = $(event.target).parent().data("presentationName");

            app.$.trigger(trigger, {presentationName: presentationName});


        },

        /**
         * favorise the presentation
         * @public
         * @method favorisePresentation
         */

        favorisePresentation:function(event) {
            // set favorite
            var $this = $(event.target);

            var presentationName = $this.parent().data("presentationName");
            var collection = app.$.customCollectionsStorage.get(presentationName);

            if (app.$.customCollectionsStorage.getFavorites().length < 3 || collection.isFavorite) {
                $this.toggleClass("selected");

                collection.isFavorite = !collection.isFavorite;

                app.$.customCollectionsStorage.add(presentationName, collection);
            }
            else {
                var $popup = $('#maxFavoritesPopUp');

                $popup.fadeIn();

                $popup.delay(1600).fadeOut();
            }

            app.$.trigger("update:favorites");
        }


    }

});
