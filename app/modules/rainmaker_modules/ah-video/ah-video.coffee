app.register 'ah-video', ->
  {
    publish: {
      assets: "shared/video/"
    }
    events: {}
    states: []

    onRender: (element) ->
      @isOpened = false
      @agVideo = app.module.get('agVideo')
      fullscreenButton = @agVideo.el.getElementsByClassName('ag-video-fullscreen-button')[0]
      fullscreenButton.addEventListener 'tap', ()=>
        if @agVideo.el.classList.contains 'ag-video-fullscreen'
          @agVideo.el.classList.remove 'ag-video-fullscreen'
        else
          @agVideo.el.classList.add 'ag-video-fullscreen'
        event.preventDefault()
        event.stopPropagation()
        @agVideo.updateSeekHandle()
      app.on 'close:overlay', ()=>
        @closeVideo() if @isOpened

    initVideo: (videoSet)->
      @agVideo.videoEle.src = @props.assets + videoSet.src
      @agVideo.props.monitor = true if videoSet.monitor
      @agVideo.videoEle.poster = videoSet.poster
      @agVideo.el.getElementsByClassName('placeholder')[0].innerText = videoSet.placeholder
      @agVideo.videoEle.load()

    openVideo: ()->
      setTimeout =>
        @agVideo.videoEle.currentTime = 0
      , 0
      @agVideo.onEnter(@agVideo.el)

    closeVideo: ()->
      @agVideo.onExit(@agVideo.el)
      setTimeout =>
        @agVideo.videoEle.currentTime = 0
        @agVideo.reset()
      , 0
      @hide()

    show: ()->
      @openVideo()
      @agVideo.el.classList.add 'show'
      app.lock()
      @isOpened = true

    hide: ()->
      @agVideo.el.classList.remove 'show'
      app.unlock()
      @isOpened = false
  }