/**
 * Provides a media library section.
 * ---------------------------------
 * Provides a user interface for searching and viewing arbitrary
 * media meta data and content from the media repository.
 *
 * @module ap-media-library.js
 * @requires jquery.js, iscroll.js, media-repository.js
 * @author Andreas Tietz, antwerpes ag
 */
app.register("ah-media-library", function () {

    /**
     * Implements a media library section.
     * -----------------------------------
     * Implements a user interface for searching and viewing arbitrary
     * media meta data and content from the media repository.
     * Shows all media entries by default.
     * The basic set of listed entries may be optionally limited by some constraint,
     * e.g. "all media entries that are attachable to emails"
     * or "all media entries that have the 'pdf' and 'reference' tags in it".
     *
     * @class ap-media-library
     * @constructor
     * @param {object} options Valid properties are:
     *     - preFilterSearchTerms: limits basic set of entries: what is to be searched (leave undefined to show all)
     *     - preFilterAttributesToBeSearched: limits basic set of entries: where is it to be found
     *     - renderOptions: options to be passed to renderers
     */
    var self;
    return {
        publish: {
            prefiltersearchterms: undefined,
            prefilterattributestobesearched: undefined,
            renderOptions: {},
            attachment: false,
            followupmail: false,
            hide: false
        },
        events: {

        },
        states: [
            {
                id: "hide"
            },
            {
                id: "visible"
            },
            {
                id: "followupmail"
            },
            {
                id: "attachment"
            }
        ],
        onRender: function (el) {

            this.extend = app.registry.get('ap-media-library');
            this.hide = this.extend.hide;
            this.show = this.extend.show;
            this.unload = this.extend.unload;
            this.refreshScroll = this.extend.refreshScroll;
            self = this;
            app.$.mediaLibrary = this;

            this.load(el);

            if (this.props.hide) {
                this.hide();
            }

            if (this.props.attachment) {
                this.goTo('attachment');
            }
            if (this.props.followupmail) {
                this.goTo('followupmail');
            }

            app.$.on('open:ah-media-library', function () {
                if (this.stateIs("hide"))
                    this.show();
            }.bind(this));

            app.$.on('close:ah-media-library', function () {
                if (this.stateIs("visible"))
                    this.hide();
            }.bind(this));

            app.$.on('toolbar:hidden', function () {
                if (this.stateIs("visible"))
                    this.hide();
            }.bind(this));

            app.$.on('media-library:refresh', function () {
                if(this.stateIs("attachment"))
                    this.scroll.refresh();
            }.bind(this));

        },
        onRemove: function (el) {

        },
        onEnter: function (el) {

        },
        onExit: function (el) {

        },

        sortReferences: function(object){
            var tempArray = [], tempArraySort, result = {}, tempMedia = [];
            Object.keys(object).forEach(function (item) {
                if(object.hasOwnProperty(item))
                    if(object[item].hasOwnProperty('referenceId'))
                        tempArray.push(object[item].title.toLowerCase())
                    else
                        tempMedia.push(object[item].title.toLowerCase())
            });
            tempArraySort = tempArray.sort();
            tempMedia.forEach(function (item) {
                tempArraySort.push(item);
            });
            tempArraySort.forEach(function (item, index) {
                Object.keys(object).forEach(function (item) {
                    if(object.hasOwnProperty(item) && tempArraySort[index] == object[item].title.toLowerCase())
                        result[item] = object[item]
                });
            });
            return result
        },
        load: function (el) {
            var $list = $(el).find("ul");
            $list.empty();
            if(self.scroll) self.scroll.destroy();
            // Initialize scrolling:
            self.scroll = new IScroll(self.$(".scroll")[0], {scrollbars: true});


            // Fill the list with a basic set of media entries (e.g. "all media entries that are attachable to emails"):



            /**
             * Implements a media library section.
             * -----------------------------------
             * Implements a user interface for searching and viewing arbitrary
             * media meta data and content from the media repository.
             * Shows all media entries by default.
             * The basic set of listed entries may be optionally limited by some constraint,
             * e.g. "all media entries that are attachable to emails"
             * or "all media entries that have the 'pdf' and 'reference' tags in it".
             *
             * @param {object} options Valid properties are:
             *     - preFilterSearchTerms: limits basic set of entries: what is to be searched (leave undefined to show all)
             *     - preFilterAttributesToBeSearched: limits basic set of entries: where is it to be found
             *     - renderOptions: options to be passed to renderers
             */

            var media = this.sortReferences(window.mediaRepository.find(this.props.prefiltersearchterms, this.props.prefilterattributestobesearched));
            if (media) {
                var index = 1;
                $.each(media, function (file, meta) {
                    var listElement = window.mediaRepository.render(file, meta, self.renderOptions);
                    if(listElement[0].getElementsByClassName('referenceId').length != 0){
                        listElement[0].getElementsByClassName('referenceId')[0].innerHTML = '[' + index + ']';
                        index ++;
                    }
                    if (listElement) {
                        $list.append(listElement);
                    }
                });
                self.scroll.refresh();
            }

            // Set up live-search (on each key stroke):
            var $listElements = $(self.el).find("ul li");
            var $input = $(self.el).find("input");
            $input.keyup(function (e) {
                // Dismiss on enter:
                if (e.keyCode == 13) {
                    $input.blur();
                    return;
                }
                // Search and update list:
                var searchString = $input.val().toLowerCase();
                var searchTerms = searchString.split(/\s+/g);
                $listElements.each(function () {
                    var $listElement = $(this);
                    var found = searchTerms.reduce(function (found, searchTerm) {
                        return found && ($listElement.text().toLowerCase().indexOf(searchTerm) !== -1);
                    }, true);
                    $listElement.toggleClass("hidden", !found);
                });
                self.scroll.refresh();
            });
        }
    }

});