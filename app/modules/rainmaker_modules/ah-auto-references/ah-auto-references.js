/**
 * Implements auto reference popups.
 * ---------------------------------
 *
 * Adds tap handlers to every element that has the [data-reference-id] data attribute.
 * On tap a popup with a list of all references in the slide appears.
 *
 * The module is loaded within index.html
 *
 * @module ap-auto-references-popup.js
 * @requires jquery.js, touchy.js
 * @author Andreas Tietz, antwerpes ag
 */
/**
 * Modified ap-auto-references-popup module to read keys instead of numbers.
 * Grouping them when connected references are used.
 * Adding reference-numbers to the sup-elements
 */


app.register("ah-auto-references", function () {

    return {
        publish: {},
        events: {},
        states: [],
        onRender: function (el) {
            app.listenTo(app.slide, 'slide:render', this.autoReferencePopupHandler.bind(this));
            var slide = app.slideshow.get();
            if(slide)
                this.autoReferencePopupHandler({id: slide});
        },
        onRemove: function (el) {

        },
        onEnter: function (el) {

        },
        onExit: function (el) {

        },

        autoReferencePopupHandler: function (data) {
            var $slide = $("#" + data.id);
            var self = this;

            var refindexes = [], point = 0, groups = [];

            $slide.find("[data-reference-id]").each(function () {
                var referenceIds = $(this).attr("data-reference-id"),
                    reflist = referenceIds.split(',');

                $.each(window.mediaRepository.metadata(), function (file, meta) {
                    if (reflist.indexOf("" + meta.referenceKey) > -1) {
                        refindexes.push(meta.referenceId);
                    }
                });

                if(refindexes.length==0)
                    return

                refindexes.reduce(function(prev, next, index){
                    if((next - prev) !== 1){
                        groups.push(refindexes.slice(point, index));
                        point = index;
                    }
                    return next;
                });

                groups.push(refindexes.slice(point));

                $(this)[0].textContent = groups.map(function(group){
                    var result = group;
                    if(group.length > 2){
                        result = [group[0] + "-" + group[group.length - 1]];
                    }

                    return result;
                }).reduce(function(prev, next){
                    return prev.concat(next);
                }).join(",");

                refindexes = [];
                point = 0;
                groups = [];
            });

            $slide.find("[data-reference-id]")
                .off('tap.auto-references-popup')
                .on('tap.auto-references-popup', function () {

                    // Collect unique reference ids:
                    var referenceIds = {};
                    $slide.find("[data-reference-id]").each(function () {

                        var referenceId = $(this).attr("data-reference-id"),
                            reflist = referenceId.split(',');

                        $.each(window.mediaRepository.metadata(), function (file, meta) {
                            if (reflist.indexOf("" + meta.referenceKey) > -1) {
                                refindexes.push(meta.referenceId);
                                referenceIds[meta.referenceId] = true;
                            }
                        });
                    });

                    referenceIds = Object.keys(referenceIds);

                    // Find media resources associated with the collected reference ids:
                    var references = {};
                    $.each(window.mediaRepository.metadata(), function (file, meta) {
                        if (referenceIds.indexOf("" + meta.referenceId) > -1) {
                            references[file] = meta;
                        }
                    });

                    // Render all references into a list:
                    var $scroll = $('<div class="scroll"/>');
                    var $list = $('<ul class="references"/>');
                    $list.append($.map(references, function (meta, file) {
                        return window.mediaRepository.render(file, meta);
                    }));
                    $scroll.append($list);


                    // Put list in popup:
                    var $popup = $('<div class="auto-references-popup" />')
                        .append('<header><div class="x">⊗</div><h1>References</h1></header>')
                        .append($scroll);

                    // Put popup in overlay:
                    $('<div class="auto-references-popup-overlay" />')
                        .append($popup)
                        .on("swipedown swipeup swiperight swipeleft", function (e) {
                            e.stopPropagation();
                        })
                        .on("tap", function (event) {
                            if ($(event.target).is(":not(.auto-references-popup, .auto-references-popup *) .x")) $(this).remove();
                        }).appendTo("#presentation");


                      if(self.scroll) self.scroll.destroy();
                      self.scroll = new IScroll($scroll[0], {scrollbars: true});

                });
        }
    }

});
