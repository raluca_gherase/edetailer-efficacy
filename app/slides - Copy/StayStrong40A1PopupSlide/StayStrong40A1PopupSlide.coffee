
class StayStrong40A1PopupSlide
  constructor: ()->
    @events = {
      "tap .arrow-btn": ()-> @changeState()
    }
    @states = [
      {
        id:"default",
        name: "DME_Eff_BCVALongTermDosingGains_State1_PopUp",
        onEnter: ()-> @submitMonitoring()
      }
      {
        id:"first",
        name: "DME_Eff_BCVALongTermDosingGains_State2_PopUp",
        onEnter: ()-> @submitMonitoring()
      }
      {
        id:"second",
        name: "DME_Eff_BCVALongTermDosingGains_State3_PopUp",
        onEnter: ()-> @submitMonitoring()
      }
      {
        id:"third",
        name: "DME_Eff_BCVALongTermDosingGains_State4_PopUp",
        onEnter: ()-> @submitMonitoring()
      }
    ]
    @$buttons = null

  onRender: (element)->
    @element = element
    @$buttons = element.getElementsByClassName("arrow-btn")
    @analitycsModule = app.module.get 'ahAnalitycs'

  changeState: ()->
    button = event.target
    if button.classList.contains("plus")
      @resetButtons()
      button.classList.remove("plus")
      button.classList.add("minus")
    else
      @resetButtons()
      @reset()
      button.classList.remove("minus")
      button.classList.add("plus")

  resetButtons: ()->
    for button in @$buttons
      button.classList.remove("minus")
      button.classList.add("plus")

  onEnter: (element)->
    @_setState 1

  onExit: (element)->
    @reset()
    @resetButtons()

  submitMonitoring: ()->
    @analitycsModule.submitCustomSlideMonitoring {id: @element.id, name: @getState().name}

app.register('StayStrong40A1PopupSlide', -> new StayStrong40A1PopupSlide())