
class HomePageSlide
  constructor: ()->
    @events = { }
    @states = []

  onRender: (element)->
    ['swipeup', 'swipedown', 'swiperight', 'tap'].forEach (event)->
      element.addEventListener event, (ev)->
        do ev.stopPropagation
        app.slideshow.next()

  onEnter: (element)->

  onExit: (element)->

app.register('HomePageSlide', -> new HomePageSlide())