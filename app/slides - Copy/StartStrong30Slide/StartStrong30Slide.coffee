class StartStrong30Slide
  constructor: () ->
    @events = {
      'tap button.left': ()-> @goToState1()
      'tap button.state2': ()-> @goToState2()
      'tap button.state3': ()-> @goToState3()

    }

    @states = [
      {id: '0'}
      {id: '1'}
      {id: '2'}
      {id: '3'}
    ]

    @config = {
        "figcaption": ['state1', 'state2', 'state3', 'state3']
        "headline": ['main', 'main', 'second', 'second']
        "headline3": ['main', 'main', 'second', 'second']
        "holder": [false, true, true, true]
        "button2": [true, true, false, false]
        "button3": [true, true, true, false]
        "monitoringNames": ["DME_Eff_StartStrongVividDME_State1_Slide", "DME_Eff_StartStrongVividDME_State2_Slide", "DME_Eff_StartStrongVividDME_State3_Slide", "DME_Eff_StartStrongVividDME_State4_Slide"]
    }

  updateGraph: (element)->
    tag = document.createElement('div')
    tag.classList.add('white-holder')
    element.getElementsByClassName('g-line')[1].appendChild(tag)
    tag

  onRender: (element) ->
    @element = element
    app.module.get('ahUtils').getSlideData @el.id, (data)=>
      @texts = data
    @refs = app.module.get 'ah-auto-references-popup'
    @refsPopup = app.module.get 'ahRefsNotesPopup'
    @analitycsModule = app.module.get 'ahAnalitycs'
    @tag = @updateGraph(element)
    @figcaption = element.getElementsByTagName('figcaption')[0]
    @h2 = element.getElementsByTagName('h2')[0]
    @h3 = element.getElementsByTagName('h3')[0]
    @graphText3 = element.getElementsByClassName('graph-text-data')[0]
    @button_state2 = element.getElementsByClassName('state2')[0]
    @button_state3 = element.getElementsByClassName('state3')[0]

  toggleHolder: (param) ->
    if param then @tag.classList.add 'hidden' else @tag.classList.remove 'hidden'

  changeText: (element, text) ->
    element.innerHTML = text

  setButtonState: (button, param) ->
    if param then button.classList.remove 'minus' else button.classList.add 'minus'
    if param then button.classList.add 'plus' else button.classList.remove 'plus'

  stateCommon: (state)->
    @goTo(state)
    @changeText(@figcaption, @texts.graph_captions[@config.figcaption[state]])
    @changeText(@h2, @texts.lead_paragraphs[@config.headline[state]])
    @changeText(@h3, @texts.headline3[@config.headline3[state]])
    @changeText(@graphText3, @texts.graph.graph_block_text) if @stateIs '3'
    @refs.autoReferences @el
    app.trigger('state:update',{slide: {el: @el}}) #content of slide was changes, and we listen in module only 'history:update'
    @toggleHolder(@config.holder[state])
    @setButtonState(@button_state2, @config.button2[state])
    @setButtonState(@button_state3, @config.button3[state])
    @submitMonitoring(@config.monitoringNames[state])

  goToState1: () ->
    @stateCommon (+(this.getState().id is '0')).toString()

  goToState2: () ->
    @stateCommon(if ['0', '1'].indexOf(this.getState().id) > -1 then '2' else '1')

  goToState3: () ->
    @stateCommon(if this.getState().id is '2' then '3' else '2')

  onEnter: (element)->
    @_setState 1

  onExit: (element)->
    @changeText(@graphText3, "")
    @_setState 1

  submitMonitoring: (name)->
    @analitycsModule.submitCustomSlideMonitoring {id: @element.id, name: name}

app.register('StartStrong30Slide', -> new StartStrong30Slide())