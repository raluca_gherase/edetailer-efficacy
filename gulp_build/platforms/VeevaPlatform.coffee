gulp = require 'gulp'
fs = require 'fs'
$ = require("gulp-load-plugins")(lazy: true)

BasePlatform = require './BasePlatform'

class VeevaPlatform extends BasePlatform

  createGulpTasks: =>
    pn = @config.platformname_short

    gulp.task pn+'-single', [pn+'-rename-thumb'],         @single
    gulp.task pn+'-get-configs',                          @getConfigs
    gulp.task pn+'-clean-dist', [pn+'-get-configs'],      @commonBuilder.clean
    gulp.task pn+'-copy-dist', [pn+'-clean-dist'],        @copyToDist
    gulp.task pn+'-rename-index', [pn+'-copy-dist'],      @renameIndex
    gulp.task pn+'-rename-thumb', [pn+'-rename-index'],   @renameThumb

    gulp.task pn+'-read', @getConfigs


#  build for deep (single) slide-presentation
  single: =>
    @buildToZip()

  getConfigs:=>
    try
      @keyMessageConfigs = fs.readFileSync @config.dir.platform_files.app_config.configs, "utf-8"
      @keyMessageId = @config.dir.platform_files.app_config.key_message
      @data = JSON.parse @keyMessageConfigs
      @keyMessageName = @data[@keyMessageId]
    catch error
      console.log error

  copyToDist:=>
    gulp.src ["**"], cwd: @config.dir.build_content_folder
    .pipe gulp.dest @config.dir.distribution_build + @keyMessageName
    .on 'end', =>
      console.log "copy rainmaker build done"

  renameIndex:=>
    gulp.src ["*.html"], cwd: @config.dir.distribution_build + @keyMessageName
    .pipe $.rename basename: @keyMessageName
    .pipe gulp.dest @config.dir.distribution_build + @keyMessageName
    .on 'end', =>
      console.log 'rename index done'

  renameThumb:=>
    gulp.src ["thumbnail.jpg"], cwd: @config.dir.distribution_build + @keyMessageName
    .pipe $.rename basename: @keyMessageName + "-thumb"
    .pipe gulp.dest @config.dir.distribution_build + @keyMessageName
    .on 'end', =>
      console.log 'rename thumb done'

  buildToZip:=>
    gulp.src ["**"] ,cwd: @config.dir.distribution_build
    .pipe $.zip @keyMessageName + '.zip', cwd: @config.dir.build_path_single
    .pipe gulp.dest @config.dir.dest
    .on 'end', =>
      console.log 'build to zip done'

module.exports = VeevaPlatform

