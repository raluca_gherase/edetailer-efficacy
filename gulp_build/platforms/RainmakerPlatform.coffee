gulp = require 'gulp'
watch = require 'gulp-watch'
path = require 'path'

BasePlatform = require './BasePlatform'

class RainmakerPlatform extends BasePlatform

  createGulpTasks: ->
    pn = @config.platformname_short

    gulp.task pn, [
      "#{pn}-slides-pug"
      "#{pn}-index-pug"
      "#{pn}-modules-html"
      "#{pn}-slides-json"

      "#{pn}-slides-styles"
      "#{pn}-modules-styles"
      "#{pn}-global-styles"

      "#{pn}-slides-scripts"
      "#{pn}-modules-scripts"

      "#{pn}-slides-assets"
      "#{pn}-modules-assets"
      "#{pn}-global-assets"

      "#{pn}-copy-vendor"
      "#{pn}-copy-accelerator"
      "#{pn}-copy-jsons"
      "#{pn}-copy-shared"
      "#{pn}-copy-icons"

      "#{pn}-thumbnails"
      "#{pn}-add-formatter"

    ], ->


    gulp.task "#{pn}-clean", @commonBuilder.clean

    #HTML/PUG
    gulp.task "#{pn}-index-pug", ["#{pn}-modules-pug", "#{pn}-modules-strings", "#{pn}-global-strings"], @htmlBuilder.indexPug
    gulp.task "#{pn}-slides-pug", @htmlBuilder.slidesPug
    gulp.task "#{pn}-modules-pug", @htmlBuilder.modulesPug
    gulp.task "#{pn}-global-strings", @htmlBuilder.readGlobalStrings
    gulp.task "#{pn}-modules-strings", @htmlBuilder.readModulesStrings

    gulp.task "#{pn}-modules-html", @htmlBuilder.modulesHtml
    gulp.task "#{pn}-slides-json", @htmlBuilder.slidesStringsJSONs

    #STYLES
    gulp.task "#{pn}-slides-styles", @styleBuilder.slidesStyles
    gulp.task "#{pn}-modules-styles", @styleBuilder.modulesStyles
    gulp.task "#{pn}-global-styles", @styleBuilder.globalStyles

    #SCRIPTS
    gulp.task "#{pn}-slides-scripts", @scriptsBuilder.slidesScripts
    gulp.task "#{pn}-modules-scripts", @scriptsBuilder.modulesScripts

    #ASSETS
    gulp.task "#{pn}-slides-assets", @assetsBuilder.slidesAssets
    gulp.task "#{pn}-modules-assets", @assetsBuilder.modulesAssets
    gulp.task "#{pn}-global-assets", @assetsBuilder.globalAssets

    #COMMON
    gulp.task "#{pn}-copy-vendor", @commonBuilder.copyVendor
    gulp.task "#{pn}-copy-accelerator", @commonBuilder.copyAccelerator
    gulp.task "#{pn}-copy-jsons", @commonBuilder.copyJSONS
    gulp.task "#{pn}-copy-shared", @commonBuilder.copyShared
    gulp.task "#{pn}-copy-icons", @commonBuilder.copyIcons

    #thumbnails
    gulp.task "#{pn}-thumbnails", @commonBuilder.slidesThumbnails

    #images optimizer
    gulp.task "#{pn}-images-optimizer", @commonBuilder.imgOptimization

    #WATCHERS
    gulp.task "#{pn}-watch", @watch
    #CLEANER
    gulp.task "#{pn}-build-prod", @buildCleaner.build
    gulp.task "#{pn}-build-cleaner", @buildCleaner.cleaner

    #cldr-data
    gulp.task "#{pn}-add-formatter", @commonBuilder.getNumbersFormatter

  watch: =>
    @watchSlides()
    @watchModules()
    @watchGlobals()

    console.log("\nWATCH'S READY")
    console.log("\n--------------------------------------------------------\n")
    console.log("exec 'agnitio run'\n")
    @commonBuilder.agnitioRun()

  watchGlobals: =>
    globalStylesGlobs = @config.dir.source + @config.dir.global_styles + '**/*.{css,styl}'
    vendorGlobs = @config.dir.source + @config.dir.vendor + "**/*.*"
    acceleratorGlobs = @config.dir.source + @config.dir.accelerator + "**/*.*"
    JSONGlobs = @config.dir.json_sources
    globalStringsGlobs = @config.dir.source + "global-strings.json"

    watch(globalStylesGlobs, (file)=>
      @styleBuilder.globalStyles(file))

    watch(vendorGlobs, (file)=>
      @commonBuilder.copyVendor(file))

    watch(acceleratorGlobs, (file)=>
      @commonBuilder.copyAccelerator(file))

    watch(JSONGlobs, (file)=>
      @commonBuilder.copyJSONS(file))


    watch(globalStringsGlobs, =>
      @htmlBuilder.slidesPug()
    )
    console.log("\nWatching globals")

  watchModules: =>
    modulesScriptsGlobs = @config.dir.source + @config.dir.modules_root + '**/*.{js,coffee}'
    modulesPugGlobs = @config.dir.source + @config.dir.modules_root + '**/*.{html,pug}'
    modulesStylesGlobs = @config.dir.source + @config.dir.modules_root + '**/*.{css,styl}'

    watch(modulesScriptsGlobs, (file)=>
      @scriptsBuilder.modulesScripts(file))

    watch(modulesStylesGlobs, (file)=>
      @styleBuilder.modulesStyles(file))

    watch(modulesPugGlobs, (file)=>
      @htmlBuilder.modulesPug(file))

    console.log("\nWatching modules")

  watchSlides: =>
    slidesPugGlobs = @config.dir.source + @config.dir.slides_root + '**/*.{json,pug}'
    slidesStylesGlobs = @config.dir.source + @config.dir.slides_root + '**/*.{styl}'
    slidesScriptsGlobs = @config.dir.source + @config.dir.slides_root + '**/*.{coffee}'

    watch(slidesPugGlobs, (file)=>
      @htmlBuilder.oneSlidePug(path.dirname(file.path).split(path.sep).pop())
      @htmlBuilder.oneSlidesStringsJSONs(path.dirname(file.path).split(path.sep).pop())
    )

    watch(slidesStylesGlobs, (file)=>
      @styleBuilder.oneSlideStyles(path.dirname(file.path).split(path.sep).pop()))

    watch(slidesScriptsGlobs, (file)=>
      @scriptsBuilder.oneSlideScripts(path.dirname(file.path).split(path.sep).pop()))

    console.log("\nWatching slides")

module.exports = RainmakerPlatform