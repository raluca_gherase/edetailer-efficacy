gulp = require 'gulp'
fs = require 'fs'
del = require 'del'
$ = require("gulp-load-plugins")(lazy: true)

BasePlatform = require './BasePlatform'

class VeevaPlatformNewPackageFormat extends BasePlatform

  createGulpTasks: =>
    pn = @config.platformname_short

    gulp.task pn + '-single-new', [pn + '-remove-images'], @single
    gulp.task pn + '-get-new-configs', @getConfigs
    gulp.task pn + '-clean-new-dist', [pn + '-get-new-configs'], @commonBuilder.clean
    gulp.task pn + '-copy-new-dist', [pn + '-clean-new-dist'], @copyToDist
    gulp.task pn + '-remove-images', [pn + '-copy-new-dist'], @removeUnUsedImages

#  build for deep (single) slide-presentation
  single: =>
    @buildToZip()

  getConfigs: =>
    try
      @keyMessageConfigs = fs.readFileSync @config.dir.platform_files.app_config.configs, "utf-8"
      @keyMessageId = @config.dir.platform_files.app_config.key_message
      @data = JSON.parse @keyMessageConfigs
      @keyMessageName = @data[@keyMessageId]
    catch error
      console.log error

  copyToDist: =>
    gulp.src ["**"], cwd: @config.dir.build_content_folder
    .pipe gulp.dest @config.dir.distribution_build
    .on 'end', =>
      console.log "copy rainmaker build done"

  removeUnUsedImages: =>
    del([@config.dir.distribution_build + "/*.jpg"])

  buildToZip: =>
    gulp.src ["**"], cwd: @config.dir.distribution_build
    .pipe $.zip @keyMessageName + '.zip', cwd: @config.dir.build_path_single
    .pipe gulp.dest @config.dir.dest
    .on 'end', =>
      console.log 'build to zip done'

module.exports = VeevaPlatformNewPackageFormat

