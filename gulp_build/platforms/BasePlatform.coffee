#Builders Classes
HTMLBuilder = require './../builders/HTMLBuilder'
ScriptsBuilder = require './../builders/ScriptsBuilder'
StyleBuilder = require './../builders/StyleBuilder'
AssetsBuilder = require './../builders/AssetsBuilder'
CommonBuilder = require './../builders/CommonBuilder'
BuildCleaner = require './../builders/BuildCleaner'

class BasePlatform
  constructor: (@config)->


  register: ()->
#Create builder instances
    console.log('\n===================================\n\tRegister builders\n===================================\n')

    console.log("HTML Builder")
    @htmlBuilder = new HTMLBuilder(@config)

    console.log("Scripts Builder")
    @scriptsBuilder = new ScriptsBuilder(@config)

    console.log("Style Builder")
    @styleBuilder = new StyleBuilder(@config)

    console.log("Assets Builder")
    @assetsBuilder = new AssetsBuilder(@config)

    console.log("Common Builder")
    @commonBuilder = new CommonBuilder(@config)
    
    console.log("Build Cleaner")
    @buildCleaner = new BuildCleaner(@config)

    console.log('\n===================================\n\tCreate tasks\n===================================\n')
    @createGulpTasks()

#abstract method, expect to be overridden in child classes
  createGulpTasks: ->

module.exports = BasePlatform