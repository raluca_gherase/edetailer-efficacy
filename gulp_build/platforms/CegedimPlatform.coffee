gulp = require 'gulp'
fs = require 'fs'
$ = require("gulp-load-plugins")(lazy: true)

BasePlatform = require './BasePlatform'

class CegedimPlatform extends BasePlatform

  createGulpTasks: =>
    pn = @config.platformname_short

    gulp.task pn+'-single', [pn+'-clean-dist'],           @single
    gulp.task pn+'-add-params',                           @addParametersXML
    gulp.task pn+'-clean-dist', [pn+'-copy-thumb'],       @commonBuilder.clean
    gulp.task pn+'-add-exports',[pn+'-add-params'],       @addExportFiles
    gulp.task pn+'-copy-thumb', [pn+'-add-exports'],      @addThumbnail

  #---------------------------------------------------------------------------------------------------
  # update ag build for cegedim
  #---------------------------------------------------------------------------------------------------
  single:=>
    @buildToZip()

  addParametersXML:=>
    buildPathSingle = @config.dir.build_path_single
    parametersPath = @config.dir.platform_files.app_config.src + 'parameters/**'
    gulp.src parametersPath
    .pipe gulp.dest buildPathSingle + 'parameters/'
    .on 'end', ()=>
      console.log "=========================="
      console.log "    Parameters copied     "
      console.log "=========================="

  addExportFiles: =>
    @pathSrc = @config.dir.platform_files.app_config.src
    buildPathSingle = @config.dir.build_path_single
    exportPath = @pathSrc + 'export/**'
    exportTemplatePath = @pathSrc + 'export.html'
    exportPublicPath = @pathSrc + 'public/**'

    gulp.src exportPath
    .pipe gulp.dest buildPathSingle + 'export'

    gulp.src exportTemplatePath
    .pipe gulp.dest buildPathSingle

    gulp.src exportPublicPath
    .pipe gulp.dest buildPathSingle + 'public'

    .on 'end', ()=>
      console.log "=========================="
      console.log "    Export Files copied   "
      console.log "=========================="

  addThumbnail: =>
    buildPathSingle = @config.dir.build_path_single
    thumbnailPath = buildPathSingle + @config.dir.thumbnail_simple
    thumbnail = buildPathSingle + "200x150.jpg"

    gulp.src thumbnail
    .pipe gulp.dest thumbnailPath
    .on 'end', ()=>
      console.log "=========================="
      console.log "      Thumbnail added "
      console.log "=========================="

  buildToZip:=>
    gulp.src ["**"] ,cwd:@config.dir.build_path_single
    .pipe $.zip  @config.dir.zip_name, cwd: @config.dir.build
    .pipe gulp.dest @config.dir.distribution
    .on 'end', =>
      console.log  "====== DEPLOYED for single " + @config.platformname + "=========="

module.exports = CegedimPlatform