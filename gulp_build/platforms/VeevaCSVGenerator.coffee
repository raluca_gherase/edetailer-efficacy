gulp = require "gulp"
fs = require "fs-extra"
jsoncsv = require "json-csv"
CSV_NAME = "multichannelDataCreate.csv"

class Item
  constructor: (item, index, link, json)->
    @id = item
    @name = json.slides[item].name
    @type = "Slide"
    @presentation_link = link.presentation_name
    @full_name = link.presentation_prefix + item
    @media_file_name = link.presentation_prefix + item + '.zip'
    @document_id = if link.document_start_slide__id then link.document_start_slide__id + index else ''

class VVCSVBuild
  constructor: (@config, @localConfig)->
    @getLocalData()
    @createGulpTasks()

  getLocalData: ()=>
    @localData = fs.readJsonSync(@localConfig).veeva
    @product = @localData.product_name
    @country = @localData.country
    @relatedResourceId = @localData.related_resource_id

  getStructure: (slideshowId)=>
    @json = fs.readJsonSync @config.dir.source + 'platforms/rainmaker/presentation.json'
    @json.structures[slideshowId].content

  checkArrayPresentations:(array, currentPresentation)->
    array.find ((item) -> item.presentation_id == currentPresentation.presentation_id)

  createItemsCollections: ()=>
    itemsArray = []
    @localData.presentations.forEach (presentation)=>
      if !@checkArrayPresentations(itemsArray, presentation)
        itemsArray.push(presentation)
      @getStructure(presentation.slideshow_id).forEach (item, index)=>
        itemsArray.push(new Item(item, index, presentation, @json))
    itemsArray

  createGulpTasks: =>
    gulp.task "vv-csv", ()=>
      @generateCSV(@createItemsCollections(), CSV_NAME)

  generateCSV: (itemsCollections, CSVName)=>
    createCsvFile = (err, csv)->
      fs.writeFile(CSVName, csv, {encoding: 'utf8'})

    createSlidesData = (itemsArray)=>
      itemsArray.map (item)=>
        obj = {}
        @config.multichannelCSV.fields.forEach (field)=>
          _value = ''
          switch field.name
            # common fields && slide fields
            when 'external_id__v'         then _value = field._get(item.presentation_name)
            when 'presentationLink'       then _value = field._get(item.presentation_link)
            when 'document_id__v'         then _value = field._get(item.document_id)
            when 'type'                   then _value = field._get(item.type)
            when 'lifecycle__v'           then _value = field._get(item.type)
            when 'name__v'                then _value = field._get(item.full_name || item.presentation_name)
            when 'slideTitle'             then _value = field._get(item.name)
            when 'slidefilename'          then _value = field._get(item.media_file_name)
            when 'productName'            then _value = field._get(@product, item.type)
            when 'slideCountry'           then _value = field._get(@country, item.type)
            when 'clmContent'             then _value = field._get(item.type)
            when 'engageContent'          then _value = field._get(item.type)
            when 'crmMediaType'           then _value = field._get(item.type)
            when 'relatedSharedResource'  then _value = field._get(@relatedResourceId, item.type)
            when 'crm_SharedResource'     then _value = field._get('', item.type)
            when 'disableActions'         then _value = field._get('', item.type)
            # presentation fields
            when 'presentationId'         then _value = field._get(item.presentation_id)
            when 'hiddenId'               then _value = field._get(item.hidden)
            when 'productNamePres'        then _value = field._get(@product, item.type)
            when 'countryName'            then _value = field._get(@country, item.type)
            when 'content'                then _value = field._get('YES', item.type)
            when 'engageContentPres'      then _value = field._get('NO', item.type)
            when 'coBrowse'               then _value = field._get('NO', item.type)
            else                         _value = field._get()
          obj[field.name] = _value
        obj
    multichannelSlidesData = createSlidesData itemsCollections
    jsoncsv.csvBuffered multichannelSlidesData, {fields: @config.multichannelCSV.fields}, createCsvFile

module.exports = VVCSVBuild
