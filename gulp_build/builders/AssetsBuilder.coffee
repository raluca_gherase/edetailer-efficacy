gulp = require 'gulp'
path = require 'path'
es = require 'event-stream'

BaseBuilder = require './BaseBuilder'

PATH_SEPARATOR = path.sep

IMAGES_MASK = '*.{png,jpg,jpeg}'
FONTS_MASK = '*.{woff,ttf,svg,eot,woff2,ico}'

class AssetsBuilder extends BaseBuilder
  constructor: (@config)->

  errorHandler: (error)->


  slidesAssets: ()=>
    console.log "\n======== Start Assets =======\n"

    processStreams = @loopOverSlides (folder)=> @oneSlideAssets(folder)

    mergedStream = es.merge.apply(@, processStreams)
    mergedStream.on 'end', ()=>
      console.log "\n======== Compiled ASSETS =======\n"

  oneSlideAssets: (folder)=>
    slideFolder = @config.dir.source + @config.dir.slides_root + folder + PATH_SEPARATOR
    assetsSource = slideFolder + PATH_SEPARATOR + 'assets/**/' + PATH_SEPARATOR + IMAGES_MASK

    dest = @config.dir.dest + @config.dir.slides_root + folder + '/assets/'

    console.log "\tAssets for #{folder}"

    gulp.src(assetsSource)
    .pipe(gulp.dest(dest))

  modulesAssets: =>
    source = @config.dir.source + @config.dir.modules_root + '**/' + IMAGES_MASK
    dest = @config.dir.dest + @config.dir.modules_root

    gulp.src(source)
    .pipe(gulp.dest(dest))

  globalAssets: =>
    source = @config.dir.source + @config.dir.global_styles + '**/'
    imagesSource = source + IMAGES_MASK
    fontsSource = source + FONTS_MASK #In this case get fonts too
    #Get video and pdf should be defined here
    dest = @config.dir.dest + @config.dir.global_styles

    #Please add others assets into array
    gulp.src([imagesSource, fontsSource])
    .pipe(gulp.dest(dest))

module.exports = AssetsBuilder