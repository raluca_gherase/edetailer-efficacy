gulp        =  require 'gulp'
rename      = require 'gulp-rename'
del         = require('del')
plumber     = require 'gulp-plumber'
changed     = require 'gulp-changed' #pipe trough only changed files
exec        = require('child_process').exec
path        = require 'path'
es          = require 'event-stream'
fse         = require 'fs-extra'
fileExists  = require 'file-exists'

imagemin = require 'gulp-imagemin'
pngquant = require 'imagemin-pngquant'
jpegoptim = require 'imagemin-jpegoptim'

jsonlint = require("gulp-jsonlint") # Validation JSON

BaseBuilder = require './BaseBuilder'

EXTENSIONS = '.{png,jpg,jpeg}'
IMAGES_MASK = '*' + EXTENSIONS
PATH_SEPARATOR = path.sep
THUMBNAILS_NAMES =
  agnitio: 'thumbnail-agnitio'

class CommonBuilder extends BaseBuilder

  constructor: (@config)->

  errorHandler: (err)->
    console.log(err)
    @emit('end')

  copyJSONS: =>
    source = @config.dir.json_sources
    dest = @config.dir.dest

    gulp.src(source)
    .pipe(plumber(handleError: @errorHandler))
      .pipe(jsonlint())
      .pipe(jsonlint.reporter( (file)->
        console.error("File #{file.path} is not valid JSON.")
      ))
      .pipe(jsonlint.failOnError())
    .pipe(changed(dest))
    .pipe(gulp.dest(dest))
    .on('end', ->
      console.log("copy json data ready"))

  copyShared: =>
    source = @config.dir.source + @config.dir.shared + '**'
    dest = @config.dir.dest + @config.dir.shared

    gulp.src(source)
    .pipe(plumber(handleError: @errorHandler))
    .pipe(changed(dest))
    .pipe(gulp.dest(dest))
    .on('end', ->
      console.log("copy shared data ready"))

  copyIcons: =>
    dest = @config.dir.dest
    icons = @config.dir.source + @config.dir.shared + @config.dir.icons + IMAGES_MASK

    gulp.src(icons)
    .pipe(plumber(handleError: @errorHandler))
    .pipe(changed(dest))
    .pipe(gulp.dest(dest))
    .on('end', ->
      console.log('copy icons '))

  copyVendor: =>
    source = @config.dir.source + @config.dir.vendor + "**/*.*"
    dest = @config.dir.dest + @config.dir.vendor

    gulp.src(source)
    .pipe(plumber(handleError: @errorHandler))
    .pipe(changed(dest))
    .pipe(gulp.dest(dest))
    .on('end', ->
      console.log("copy vendor ready"))

  copyAccelerator: =>
    source = @config.dir.source + @config.dir.accelerator + "**/*.*"
    dest = @config.dir.dest + @config.dir.accelerator

    gulp.src(source)
    .pipe(plumber(handleError: @errorHandler))
    .pipe(changed(dest))
    .pipe(gulp.dest(dest))
    .on('end', ->
      console.log("copy accelerator ready"))

  slidesThumbnails: ()=>
    console.log "\n======== Start Thumbnails =======\n"

    processStreams = @loopOverSlides (folder)=> @oneSlideThumbnails(folder)

    mergedStream = es.merge.apply(@, processStreams)
    mergedStream.on 'end', ()=>
      console.log "\n======== Compiled Thumbnails =======\n"

  imgOptimization:  =>
    source = @config.dir.source + "**/**/" + IMAGES_MASK
    dest = @config.dir.source

    gulp.src(source)
      .pipe(imagemin([
        pngquant(),
        jpegoptim({ progressive: true, max: 70})
      ],{
        verbose: true
      }))
      .pipe(gulp.dest(dest))

  oneSlideThumbnails: (folder)=>
    slideFolder = @config.dir.source + @config.dir.slides_root + folder + PATH_SEPARATOR
    thumbnailSource = slideFolder + PATH_SEPARATOR + 'thumbnails' + PATH_SEPARATOR + THUMBNAILS_NAMES.agnitio + EXTENSIONS

    dest = @config.dir.dest + @config.dir.slides_root + folder

    console.log "\tThumbnails for #{folder}"

    gulp.src(thumbnailSource)
    .pipe(plumber(handleError: @errorHandler))
    .pipe(changed(dest))
    .pipe(rename((file)=>
      file.basename = folder
    ))
    .pipe(gulp.dest(dest))

  agnitioRun: =>
    agnitioRainmakerProcces = exec('agnitio run', {cwd: @config.dir.dest})
    agnitioRainmakerProcces.stdout.on 'data', (data)->
      console.log data
    agnitioRainmakerProcces.on 'exit', (code)->
      console.log "\x1b[31m'agnitio run dev' closed with code #{code}\x1b[0m"
    agnitioRainmakerProcces.on 'error', (error)->
      console.log "error"
    killProcess = ->
      agnitioRainmakerProcces.kill()

    process.on 'close', killProcess
    process.on 'uncaughtException', killProcess
    process.on 'error', killProcess
    process.on 'exit', killProcess

  clean: =>
    del(@config.dir.dest)

  getNumbersFormatter: =>
    localizerFolder = @config.dir.localizer_module
    return unless fileExists localizerFolder+"/cldr.json"
    data = fse.readJsonSync(@config.dir.configs, {throws: false})
    localeKey = data.locale
    dataForJson =
      "main": require("cldr-data/main/#{localeKey}/numbers.json").main
      "supplemental": require("cldr-data/supplemental/likelySubtags.json").supplemental

    fse.writeJsonSync(localizerFolder+"/cldr.json", dataForJson)
    console.log("------------------------------------")
    console.log("generate formatter for localization")
    console.log("------------------------------------")

module.exports = CommonBuilder
