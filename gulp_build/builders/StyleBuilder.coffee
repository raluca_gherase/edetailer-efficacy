gulp = require 'gulp'
path = require 'path'
es = require 'event-stream'
changed = require 'gulp-changed' #pipe trough only changed files
plumber = require 'gulp-plumber'

BaseBuilder = require './BaseBuilder'

PATH_SEPARATOR = path.sep

class StyleBuilder extends BaseBuilder
  constructor: (@config)->

  errorHandler: (err)->
    console.log(err)
    @emit('end')

  slidesStyles: ()=>
    console.log "\n======== Start Styles =======\n"

    processStreams = @loopOverSlides (folder)=> @oneSlideStyles(folder)

    mergedStream = es.merge.apply(@, processStreams)
    mergedStream.on 'end', ()=>
      console.log "\n======== Compiled css/stylus =======\n"

  oneSlideStyles: (folder)=>
    slideFolder = @config.dir.source + @config.dir.slides_root + folder + PATH_SEPARATOR
    stylesSource = slideFolder + folder + '.styl'
    dest = @config.dir.dest + @config.dir.slides_root + folder

    console.log "Styles for #{folder}"

    gulp.src(stylesSource)
    .pipe(plumber(handleError: @errorHandler))
    .pipe(changed(dest))
    .pipe(gulp.dest(dest))

  modulesStyles: =>
    source = @config.dir.source + @config.dir.modules_root + '**/*.{css,styl}'
    dest = @config.dir.dest + @config.dir.modules_root

    gulp.src(source)
    .pipe(plumber(handleError: @errorHandler))
    .pipe(changed(dest))
    .pipe(gulp.dest(dest))
    .on('end', ->
      console.log("modules styles ready"))

  globalStyles: =>
    source = @config.dir.source + @config.dir.global_styles + '**/*.{css,styl}'
    dest = @config.dir.dest + @config.dir.global_styles

    gulp.src(source)
    .pipe(plumber(handleError: @errorHandler))
    .pipe(changed(dest))
    .pipe(gulp.dest(dest))
    .on('end', ->
      console.log("globals style ready"))

module.exports = StyleBuilder    