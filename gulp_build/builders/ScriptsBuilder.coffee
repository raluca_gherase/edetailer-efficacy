gulp = require 'gulp'
path = require 'path'
es = require 'event-stream'
changed = require 'gulp-changed' #pipe trough only changed files
plumber = require 'gulp-plumber'

BaseBuilder = require './BaseBuilder'

PATH_SEPARATOR = path.sep

class ScriptBuilder extends BaseBuilder
  constructor: (@config)->

  errorHandler: (err)->
    console.log(err)
    @emit('end')

  slidesScripts: ()=>
    console.log "\n======== Start Styles =======\n"

    processStreams = @loopOverSlides (folder)=> @oneSlideScripts(folder)

    mergedStream = es.merge.apply(@, processStreams)
    mergedStream.on 'end', ()=>
      console.log "\n======== Compiled css/stylus =======\n"

  oneSlideScripts: (folder)=>
    slideFolder = @config.dir.source + @config.dir.slides_root + folder + PATH_SEPARATOR
    scriptsSource = slideFolder + '**/*.{js,coffee}'
    dest = @config.dir.dest + @config.dir.slides_root + folder

    console.log "Scripts for #{folder}"

    gulp.src(scriptsSource)
    .pipe(plumber(handleError: @errorHandler))
    .pipe(changed(dest))
    .pipe(gulp.dest(dest))

  modulesScripts: =>
    source = @config.dir.source + @config.dir.modules_root + '**/*.{js,coffee}'
    dest = @config.dir.dest + @config.dir.modules_root

    gulp.src(source)
    .pipe(plumber(handleError: @errorHandler))
    .pipe(changed(dest))
    .pipe(gulp.dest(dest))
    .on('end', ->
      console.log("modules scripts"))

module.exports = ScriptBuilder