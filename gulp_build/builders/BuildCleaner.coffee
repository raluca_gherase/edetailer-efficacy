gulp = require 'gulp'
del = require 'del'
exec = require('child_process').exec
uglify = require 'gulp-uglify'
cleanCSS = require 'gulp-clean-css'

BaseBuilder = require './BaseBuilder'

class BuildCleaner extends BaseBuilder
  constructor: (@config)->

  errorHandler: (err)->
    console.log(err)
    @emit('end')

  build: ()=>
    destFiles = @config.dir.dest

    agnitioRainmakerProcces = exec('agnitio build',{cwd: destFiles, watch: false})
    agnitioRainmakerProcces.stdout.on 'data', (data)=>
      console.log data

  cleaner: ()=>
    destFiles = @config.dir.dest
    @_cleanJS destFiles
    @_cleanCSS destFiles


    del([destFiles+'slides/**/*.{coffee,styl}', destFiles+@config.dir.global_styles+'*.styl', destFiles+'modules/**/*.{coffee,styl}']).then (paths)=>
      console.log 'Deleted files and folders:\n', paths.join '\n'
      
  _cleanCSS: (destFiles)->
    gulp.src(destFiles+'build/presentation/*.css')
    .pipe(cleanCSS())
    .pipe(gulp.dest(destFiles+'build/presentation/'))

  _cleanJS: (destFiles)->
    gulp.src(destFiles+'build/presentation/*.js')
    .pipe(uglify())
    .pipe(gulp.dest(destFiles+'build/presentation/'))

module.exports = BuildCleaner