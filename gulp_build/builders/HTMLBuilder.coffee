gulp         = require 'gulp'
path         = require 'path'
es           = require 'event-stream'
pug          = require 'gulp-pug'
plumber      = require 'gulp-plumber'
dataInjector = require 'gulp-data'
changed      = require 'gulp-changed' #pipe trough only changed files
fse          = require('fs-extra')
fileExists   = require 'file-exists'
filterFiles  = require 'gulp-tap'

jsonlint      = require("gulp-jsonlint") # Validation JSON

BaseBuilder = require './BaseBuilder'

PATH_SEPARATOR = path.sep

class HTMLBuilder extends BaseBuilder
  constructor: (@config)->
    @stringdata = {}

  errorHandler: (err)->
    console.log(err)
    @emit('end')

  readModulesStrings: ()=>
    source = @config.dir.source + @config.dir.modules_root + '**/*-strings.json'
    gulp.src(source)
    .pipe filterFiles (file, t)=>
      console.log "parse: ",file.path
      ahStringName = path.parse(file.path).name.split('-strings')[0]
      console.log "create ahstrings.#{ahStringName}"
      @stringdata[ahStringName] = @readFilesCommon(file.path)

  globalStringInjection: ()=>
    globalStringsFile = path.join @config.dir.source + "global-strings.json"
    @globalString = fse.readJsonSync(globalStringsFile, {throws: false})
    @globalString

  readFilesCommon: (file)=>
    globalNotesFile = path.join file
    globalString = fse.readJsonSync(globalNotesFile, {throws: false})
    globalString

  stringsInjector: (file)=>
    slideString = fse.readJsonSync(path.dirname(file.path) + PATH_SEPARATOR + 'strings.json', {throws: false})
    slideString.globals = @globalStringInjection()
    slideString

  moduleStringInjection: (file)=>
    pathArray = path.dirname(file.path).split(PATH_SEPARATOR)
    dirName = pathArray.pop()
    stringPath =  path.dirname(file.path) + PATH_SEPARATOR + dirName + '-strings.json'
    moduleString = if fileExists stringPath then fse.readJsonSync(stringPath, {throws: false}) else {}
    @stringdata[dirName] = moduleString
    @stringdata.globals = @globalStringInjection()
    moduleString

  readGlobalStrings: ()=>
    source = @config.dir.source + '*-strings.json'
    gulp.src(source)
    .pipe filterFiles (file, t)=>
      return if path.basename(file.path) is "global-strings.json"
      #ahStringName = path.basename(file.path, "-strings.json")
      console.log "parse: ",file.path
      ahStringName = path.parse(file.path).name.split('-strings')[0]
      console.log "create ahstrings.#{ahStringName}"
      @stringdata[ahStringName] = @readFilesCommon(file.path)

  indexPug: ()=>
    source = @config.dir.source + @config.dir.platform + 'index.pug'
    dest = @config.dir.dest
    localdata =
      stringdata: JSON.stringify( @stringdata )

    gulp.src(source)
    .pipe(plumber(handleError: @errorHandler))
    .pipe(pug({
      doctype:'html'
      pretty: true
      locals:localdata
    }))
    .pipe(gulp.dest(dest))
    .on('end', ->
      console.log("index pug ready"))

  modulesHtml: =>
    source = @config.dir.source + @config.dir.modules_root + '**/*.html'
    dest = @config.dir.dest + @config.dir.modules_root
    gulp.src(source)
    .pipe(plumber(handleError: @errorHandler))
    .pipe(changed(dest))
    .pipe(gulp.dest(dest))
    .on('end', =>
      console.log("modules html ready"))

  modulesPug: =>
    source = @config.dir.source + @config.dir.modules_root + '**/*.pug'
    dest = @config.dir.dest + @config.dir.modules_root

    gulp.src(source)
    .pipe(plumber(handleError: @errorHandler))
    .pipe(changed(dest))
    .pipe(dataInjector(@moduleStringInjection))
    .pipe(pug({
      pretty: true
    }))
    .pipe(gulp.dest(dest))
    .on('end', =>
      console.log("modules pug ready"))

  slidesStringsJSONs: ()=>
    console.log "\n======== Start JSON =======\n"

    processStreams = @loopOverSlides (folder)=> @oneSlidesStringsJSONs(folder)

    mergedStream = es.merge.apply(@, processStreams)
    mergedStream.on 'end', ()=>
      console.log "\n======== Compiled JSON =======\n"

  oneSlidesStringsJSONs: (folder)=>
    slideFolder = @config.dir.source + @config.dir.slides_root + folder + PATH_SEPARATOR
    stringsSource = slideFolder + 'strings.json'
    dest = @config.dir.dest + @config.dir.slides_root + folder

    console.log "JSON for #{folder}"

    gulp.src(stringsSource)
    .pipe(plumber(handleError: @errorHandler))
    .pipe(jsonlint())
    .pipe(jsonlint.reporter( (file)->
      console.error("File #{file.path} is not valid JSON.")
    ))
    .pipe(jsonlint.failOnError())
    .pipe(changed(dest))
    .pipe(gulp.dest(dest))

  slidesPug: ()=>
    console.log "\n======== Start Pug/HTML =======\n"

    processStreams = @loopOverSlides (folder)=> @oneSlidePug(folder)

    mergedStream = es.merge.apply(@, processStreams)
    mergedStream.on 'end', ()=>
      console.log "\n======== Compiled Pug/HTML =======\n"

  oneSlidePug: (folder)=>
    slideFolder = @config.dir.source + @config.dir.slides_root + folder + PATH_SEPARATOR
    pugSource = slideFolder + folder + '.pug'
    dest = @config.dir.dest + @config.dir.slides_root + folder

    console.log "Pug for #{folder}"

    gulp.src(pugSource)
    .pipe(plumber(handleError: @errorHandler))
    .pipe(changed(dest))
    .pipe(dataInjector(@stringsInjector))
    .pipe(pug({
      pretty: true
    }))
    .pipe(gulp.dest(dest))

module.exports = HTMLBuilder