module.exports =
  platformname: "cegedim"
  platformname_short: "cg"

  dir:
    platform_files:
      app_config:
        src:"./app/platforms/cegedim/"
        glob:["**"]
        dest:""
    thumbnail_simple:   'media/images/thumbnails'
    build:              './build/cegedim/'
    distribution:       './dist/cegedim/'
    build_content_folder:'./build'
    build_path_single:  './build/'
    zip_name: 'distribution.zip'