module.exports =
  platformname: "rainmaker"
  platformname_short: "rm"

  dir:
    source: './app/'
    platform: 'platforms/rainmaker/'
    slides_root: 'slides/'
    modules_root: 'modules/rainmaker_modules/'
    shared: 'shared/'
    icons: 'images/icons/'
    global_styles: 'global_styles/'
    accelerator: 'accelerator/'
    vendor: '_vendor/'

    json_sources: [
      './app/platforms/rainmaker/*.json',
      './app/**/*.json',
      '!./app/slides/**/',
      '!./app/_vendor/',
      '!./app/accelerator/',
      '!./app/platforms/agnitio/',
      '!./app/platforms/cegedim/',
      '!./app/platforms/veeva/'
    ]
    configs: './app/platforms/rainmaker/config.json'
    localizer_module: 'app/modules/rainmaker_modules/ah-localizer'
    dest: './build/'
