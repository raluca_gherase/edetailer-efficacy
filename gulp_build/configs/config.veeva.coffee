module.exports =
  platformname: "veeva"
  platformname_short: "vv"
  multichannelCSV:
    fields:[
      {
        name: 'external_id__v'
        label: 'external_id__v'
        _get:(name)->name or ''
      }
      {
        name: 'presentationLink'
        label: 'Presentation Link'
        _get:(id)-> id or ''
      }
      {
        name: 'document_id__v'
        label: 'document_id__v'
        _get:(id)-> id or ''
      }
      {
        name: 'type'
        label: 'Type'
        _get:(type)-> type or 'Presentation'
      }
      {
        name: 'lifecycle__v'
        label: 'lifecycle__v'
        _get:(type)-> if type then 'CRM Content Lifecycle' else 'Binder Lifecycle'
      }
      {
        name: 'name__v'
        label: 'name__v'
        _get:(name__v)-> name__v or ''
      }
      {
        name: 'slideTitle'
        label: 'slide.title__v'
        _get:(slideTitle)-> slideTitle or ''
      }
      {
        name: 'slidefilename'
        label: 'slide.filename'
        _get:(slidefilename)-> slidefilename or ''
      }
      {
        name:'productName'
        label:'slide.product__v.name__v',
        _get:(productName, type)-> if type then productName else ''
      }
      {
        name:'slideCountry'
        label:'slide.country__v.name__v',
        _get:(slideCountry, type)-> if type then slideCountry else ''
      }
      {
        name:'clmContent'
        label:'slide.clm_content__v',
        _get:(type)-> if type then 'YES' else ''
      }
      {
        name:'engageContent'
        label:'slide.engage_content__v',
        _get:(type)-> if type then 'NO' else ''
      }
      {
        name:'crmMediaType'
        label:'slide.crm_media_type__v',
        _get:(type)-> if type then 'HTML' else ''
      }
      {
        name:'relatedSharedResource'
        label:'slide.related_shared_resource__v',
        _get:(sharedFolder, type)-> if type then sharedFolder else ''
      }
      {
        name:'crm_SharedResource'
        label:'slide.crm_shared_resource__v',
        _get:(CRMsharedFolder, type)-> if type then CRMsharedFolder else ''
      }
      {
        name:'disableActions'
        label:'slide.crm_disable_actions__v',
        _get:(disableActions, type)-> if type then disableActions else ''
      }
      {
        name:'presentationId'
        label:'pres.crm_presentation_id__v',
        _get:(presentationId)-> presentationId or ''
      }
      {
        name:'hiddenId'
        label:'pres.crm_hidden__v',
        _get:(hiddenId)-> hiddenId or ''
      }
      {
        name:'productNamePres'
        label:'pres.product__v.name__v',
        _get:(productNamePres, type)-> if type then '' else productNamePres
      }
      {
        name:'countryName'
        label:'pres.country__v.name__v',
        _get:(countryName, type)-> if type then '' else countryName
      }
      {
        name:'content'
        label:'pres.clm_content__v',
        _get:(content, type)-> if type then '' else content
      }
      {
        name:'engageContentPres'
        label:'pres.engage_content__v',
        _get:(engageContentPres, type)-> if type then '' else engageContentPres
      }
      {
        name:'coBrowse'
        label:'pres.cobrowse_content__v',
        _get:(coBrowse, type)-> if type then '' else coBrowse
      }
    ]
  dir:
    source:             './app/'
    platform_files:
      app_config:
        src:"./app/platforms/veeva/"
        configs: "./app/platforms/veeva/configs.json"
        key_message: "key_message_name"
        glob:["**"]
        dest:""

    build_content_folder:'./build'
    distribution_build: './dist/veeva/build/'
    build_path_single:  './build/'
    dest: './dist/veeva/'
