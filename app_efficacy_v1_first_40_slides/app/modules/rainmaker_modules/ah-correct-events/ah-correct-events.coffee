app.register 'ah-correct-events', ->
  {
    publish: {
    }
    states: []


    onRender: () ->
      @slides = app.model.get().slides
      app.listenTo(app.slide, 'slide:render', @validator.bind(@))
      app.listenTo(app.slideshow, 'update:current', @validatorSlideshow.bind(@))

    validator: (el)->
      app.slide.trigger 'slide:anthill-render', el if @slides[el.id]

    validatorSlideshow: (el)->
      app.slideshow.trigger 'update:anthill-current', el if el.current.id isnt el.prev.id

  }