app.register "ah-listener", ()->
  onRender: ()->
    app.listenTo app.slideshow, 'update:anthill-current', (el)=>
      app.view.get("overlay-popup-custom").reset()
      app.view.get("viewer").closeViewer()
