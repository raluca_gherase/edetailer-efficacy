
/**
*
* BHC Template Module: GPC Number
*
* A module that reads the 'gpc' setting in config.json and displays it.
*
* Usage:
* Insert <div data-module="bhc-gpc-number"></div> where number should be displayed
*
*/

app.register("bhc-gpc-number", function() {

  return {
    template: false,
    publish: {

    },
    events: {

    },
    states: [],
    onRender: function(el) {
      var gpc = app.config.get('gpc');
      var existingText = this.el.innerHTML;

      if (!gpc || gpc === "999-999-999") {
        gpc = "Not Applied"
      }

      if (gpc) {
        this.el.innerHTML = existingText + " " + gpc;
      }

      this.monitorUsage();
    },
    onRemove: function(el) {

    },
    monitorUsage: function() {
      if (ag) {
        ag.submit.data({
          label: "Registered Module",
          value: "bhc-gpc-number",
          category: "BHC Template Modules",
          isUnique: true,
          labelId: "bhc_registered_module",
          categoryId: "bhc_template_modules"
        });
      }
    }
  }

});
