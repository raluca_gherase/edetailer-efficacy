/**
 * Provides a reference library section.
 * -------------------------------------
 * Implements a user interface for viewing media entries that have a "referenceId" attribute.
 *
 * @module ah-reference-library.js
 * @requires module.js, jquery.js, iscroll.js, media-repository.js
 * @author Andreas Tietz antwerpes ag
 */
app.register("ah-reference-library", function() {

  var self, $list;
  return {
    publish: {

    },
    events: {

    },
    states: [
      {
        id: "visible"
      }
    ],
    onRender: function(el) {
      $list = $(el).find("ul");
      self = this;

      app.$.on('open:ah-reference-library', function () {
        if(this.stateIsnt("visible"))
          this.show();
      }.bind(this));

      app.$.on('close:ah-reference-library', function () {
        this.hide();
      }.bind(this));

      app.$.on('toolbar:hidden', function () {
        this.hide();
        this.unload();
      }.bind(this));

      this.load(el);


    },
    onRemove: function(el) {

    },
    onEnter: function(el) {

    },
    onExit: function(el) {

    },

    hide: function () {
      app.unlock();
      this.reset();
    },

    show: function () {
      app.lock();
      this.goTo('visible');
      this.load();
    },

    unload: function() {
      if(this.stateIs("visible"))
        if(self.scroll) self.scroll.destroy();
    },
    sortReferences: function(object){
      var tempArray = [], tempArraySort, result = {};
      for(item in object){
        if(object.hasOwnProperty(item))
          tempArray.push(object[item].title.toLowerCase())
      }
      tempArraySort = tempArray.sort();
      for(index in tempArraySort){
        for(item in object){
          if(object.hasOwnProperty(item) && tempArraySort[index] == object[item].title.toLowerCase())
            result[item] = object[item]
        }
      }
      return result
    },
    filterTrack: function(references) {
      var currentTrack = app.slideshow.getId(),
          filterReferences = {}, isFilterTrack = true;
      if(isFilterTrack){
        for(var key in references){
          var currentFlow = references[key].flow.split(',');
          if(currentFlow.indexOf(currentTrack) != -1)
            filterReferences[key] = references[key]
        }
      }
      
      return Object.keys(filterReferences).length != 0 ? filterReferences : references
    },
    load: function(el){
      $list.empty();
      // Initialize scrolling:
      if(self.scroll) self.scroll.destroy();
      self.scroll = new IScroll(self.$(".scroll")[0], {scrollbars: true});

      // Fill the list with all media entries that have a reference id:
      var references = window.mediaRepository.find("", "referenceId");
      // var references = this.filterTrack(window.mediaRepository.find("", "referenceId"));
      if (references) {
        $.each(references, function (file, meta) {
          $list.append(window.mediaRepository.render(file, meta));
        });
        self.scroll.refresh();
      }
    }
  }

});