'use strict'
window.LinearSlider = class LinearSlider
  constructor: (element)->
    slider = element.querySelector '.linear-slider'
    dragHandler = slider.querySelector '.arrow-btn'
    dragLine = slider.querySelector '.drag-line'
    itemsArray = slider.querySelectorAll '.text-item'
    @dragLimit = dragLine.offsetWidth - dragHandler.offsetWidth
    @curElements = (itemsArray.length - 1)
    @curPosition = 0
    @dragElement = new Draggy dragHandler, {
      restrictY: true
      limitsX: [0, @dragLimit]
      onChange: @onSliderMove.bind(@)
    }
    element.addEventListener "onDrop", ()=>
      dragHandler.classList.remove('active')
      @dragElement.moveTo(((@dragLimit*@curPosition)/@curElements),0)

  onSliderMove: (x,y)->
    console.log(x)
    @curPosition = Math.round(x / (@dragLimit / @curElements))