app.register 'ah-slide-analytics', ->
  {
    publish: {}
    events: {}
    states: []

    onRender: (element) ->
      @modelData = app.model.get()
      @analyticsExtend = app.module.get 'agAnalytics'
      app.on('slideEnter:inlineSlideshow', @submitMonitoring.bind(@))

    isSkippedSlide: (id)-> app.model.getSlide(id).isSkipped

    submitMonitoring: (data)->
      slideId = data.slide.id
      return if @isSkippedSlide slideId
      slide = @analyticsExtend.assignValues slideId, 'slide'
      components = app.slideshow.resolve()
      @sendMonitoring slide.id, slide.name, components.slideshow, components.chapter

    submitCustomSlideMonitoring: (elementData)->
      elementId = elementData.id
      elementName = elementData.name
      components = app.slideshow.resolve()
      @sendMonitoring elementId, elementName, components.slideshow, components.chapter

    getChapterName: (slideshowId)-> @modelData.storyboards[slideshowId].name

    sendMonitoring: (id, name, slideshow, chapter)->
      return unless window.ag
      ag.submit.slide
        id: id
        name: name
        path: app.slideshow.getPath()
        chapter: @getChapterName slideshow
        chapterId: chapter
      console.log id, name
  }