app.register 'ah-bottom-logo', ->
  {
    events: {
      "tap" : "openNotes"
    }
    states: []

    onRender: () ->
      @notesModule = app.module.get 'notes'

    openNotes: ()->
      @notesModule.show()
  }