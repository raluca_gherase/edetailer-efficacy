/**
 * Implements a function that will open the specific-product-characteristics.pdf file with the agnitio in-app PDF viewer.
 * -----------------------------------------------------------------
 * @module ap-specific-product-characteristics.js
 * @requires agnitio.js
 * @author Andreas Tietz, antwerpes ag
 */
app.register("ap-specific-product-characteristics", function() {

  /**
   * Implements a function that will open the specific-product-characteristics.pdf file with the agnitio in-app PDF viewer.
   * ----------------------------------------------------------------------------------------------------------------------
   *
   * @class ap-specific-product-characteristics.js
   * @constructor
   */
  return {
    publish: {

    },
    events: {

    },
    states: [],
    onRender: function(el) {
      app.$.specificProductCharacteristics = this;
      this.monitorUsage();
    },
    onRemove: function(el) {

    },
    monitorUsage: function (el) {
      if (ag) {
        ag.submit.data({
          label: "Registered Module",
          value: "ap-specific-product-characteristics",
          category: "BHC Template Modules",
          isUnique: true,
          labelId: "bhc_registered_module",
          categoryId: "bhc_template_modules"
        });
      }
    },

    openPdf: function() {
      console.log('openPDF("shared/pdfs/EyleaSPCAug2016.pdf", "Specific Product Characteristics"');
      ag.openPDF("shared/pdfs/EyleaSPCAug2016.pdf", "Specific Product Characteristics");
      var $joystick = $('.joystick');
      var $addSlideButton = $('[data-module="ap-add-slide-button"]');
      $joystick.fadeIn();
      $addSlideButton.fadeIn();

    }
  }

});
