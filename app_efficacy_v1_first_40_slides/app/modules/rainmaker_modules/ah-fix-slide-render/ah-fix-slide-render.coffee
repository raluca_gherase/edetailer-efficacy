app.register "ah-fix-slide-render", ()->
  onRender: ()->
    document.addEventListener "touchmove", (event)->
      event.preventDefault()

    app.listenTo app.slideshow, 'update:anthill-current', (el)=>
      current = document.querySelectorAll('.slide.present')
      if current.length > 1
        app.util.toArray(current).forEach (slide)=>
          return if slide.id.indexOf('PopupSlide') > -1 or slide.id is el.current.id
          slide.classList.remove 'present'