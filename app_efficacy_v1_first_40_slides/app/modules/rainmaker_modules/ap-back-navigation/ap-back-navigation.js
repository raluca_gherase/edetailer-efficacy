/**
 * Provides a function to navigate backwards
 * ---------------------------------------------------------
 *
 * Navigates backwards through the sequence of app.goTo calls.
 *
 * @module ap-back-navigation.js
 * @requires agnitio.js
 * @author David Buezas, antwerpes ag
 */


app.register("ap-back-navigation", function () {

    var self;
    return {
        publish: {},
        events: {
          "tap": "back"
        },
        states: [],
        history: [],
        active: false,
        path: null,
        onRender: function (el) {
            self = this;


            app.$.BackNavigation = this;
            app.slide.on('slide:enter',this.update.bind(this));
            app.slide.on('slide:exit',this.save.bind(this));

            this.monitorUsage();

        },
        onRemove: function (el) {
          self = null;
        },
        monitorUsage: function (el) {
          if (ag) {
            ag.submit.data({
              label: "Registered Module",
              value: "ap-back-navigation",
              category: "BHC Template Modules",
              isUnique: true,
              labelId: "bhc_registered_module",
              categoryId: "bhc_template_modules"
            });
          }
        },
        // Save the path of the slide we are currently on
        update: function(data) {
          this.path = app.getPath();
        },
        save: function(data) {
          if (!this.active && this.path) {
            this.history.push(this.path);
          }
          else {
            this.active = false;
          }
        },
        storeLastCollection: function(data) {
          self.prevCollection = data.id;
        },

        storePrevSlide: function (data) {
            self.prevSlide = data.prev.id;
        },

        setPrevCollection: function (id) {
          self.prevCollection = id;
        },

        back: function () {
          // When moving in history we will not add the slides we are moving to
          this.active = true;
          var path = this.history.pop();
          app.goTo(path);
        }
    }

});
