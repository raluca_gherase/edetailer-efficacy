###
   app.module.get("ah-utils").getSlideData( SLIDE_ID, ( data )=>
      console.log( data )
    )
###

class AhUtils
  constructor: ->
    @getSlideData = ( slideId, callback ) ->
      path = "slides/#{slideId}/strings.json"
      @getData( path, callback )
      return

    @getData = ( path, callback ) ->
      xhr = new XMLHttpRequest
      xhr.open 'GET', path, false

      xhr.onreadystatechange = ->
        if xhr.readyState != 4
          return
        if xhr.status != 0 and xhr.status != 200
          if xhr.status == 400
            console.log "Could not locate #{path}"
          else
            console.error "getSlideData #{path} HTTP error: #{xhr.status}"
          return
        callback JSON.parse( xhr.responseText )

      xhr.send()
      return

app.register "ah-utils", -> new AhUtils