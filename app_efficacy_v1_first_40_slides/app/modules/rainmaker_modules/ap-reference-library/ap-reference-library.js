/**
 * Provides a reference library section.
 * -------------------------------------
 * Implements a user interface for viewing media entries that have a "referenceId" attribute.
 *
 * @module ap-reference-library.js
 * @requires module.js, jquery.js, iscroll.js, media-repository.js
 * @author Andreas Tietz antwerpes ag
 */
app.register("ap-reference-library", function() {

  var self;

  return {
    publish: {

    },
    events: {

    },
    states: [
      {
        id: "visible"
      }
    ],
    onRender: function(el) {
      self = this;

      app.$.on('open:ap-reference-library', function () {
        if(this.stateIsnt("visible")){
          this.show();
        }
      }.bind(this));

      app.$.on('close:ap-reference-library', function () {
        this.hide();
      }.bind(this));

      app.$.on('toolbar:hidden', function () {
        this.hide();
        this.unload();
      }.bind(this));

      this.load(el);

      this.monitorUsage();

    },
    onRemove: function(el) {
      self = null;
    },
    monitorUsage: function (el) {
      if (ag) {
        ag.submit.data({
          label: "Registered Module",
          value: "ap-reference-library",
          category: "BHC Template Modules",
          isUnique: true,
          labelId: "bhc_registered_module",
          categoryId: "bhc_template_modules"
        });
      }
    },

    hide: function () {
      app.unlock();
      this.reset();
    },

    show: function () {
      app.lock();
      this.goTo('visible');
      this.load();
    },

    unload: function() {
      if(this.stateIs("visible"))
        if(self.scroll) self.scroll.destroy();
    },

    load: function(el){
      var $list = $(el).find("ul");

      $list.empty();
      // Initialize scrolling:
      if(self.scroll) self.scroll.destroy();
      self.scroll = new IScroll(self.$(".scroll")[0], {scrollbars: true});

      // Fill the list with all media entries that have a reference id:
      var references = window.mediaRepository.find("", "referenceId");
      if (references) {
        $.each(references, function (file, meta) {
          $list.append(window.mediaRepository.render(file, meta));
        });
        self.scroll.refresh();
      }
    }
  }

});
