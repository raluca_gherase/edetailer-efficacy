'use strict'
window.BeforeAfterSlider = class BeforeAfterSlider
  constructor: (slider, controlPoints)->
    @$after = slider.getElementsByClassName("element-after")[0]
    sliderOffsetX = $(slider).offset().left

    slider.addEventListener "touchmove", (event)=>
      event.preventDefault()
      touch = event.touches[0]
      offX = touch.pageX - sliderOffsetX
      @$after.style.width = "#{offX}px"

    slider.addEventListener "touchend", (event)=>
      event.preventDefault()
      touch = event.changedTouches[0]
      offX = touch.pageX - sliderOffsetX
      if offX > 0
        for point, index in controlPoints
          if offX <= point
            @$after.style.width = "#{point}px"
            @touchEnd(index+1)
            break
      else
        @$after.style.width = "0px"
        @touchEnd(0)

  reset: ()-> @$after.style.width = 0

  touchEnd: (index)->