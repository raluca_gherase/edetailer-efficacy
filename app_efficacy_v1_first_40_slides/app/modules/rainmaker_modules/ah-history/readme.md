# ah-history module
-------------------------------------
### Description
##### This module the triggered event when current view(slide or popup) updated.
##### In callback you receive data about current view.
-------------------------------------

### How to use:

##### Include module to presentation.json
```
"ah-history": {
            "id": "ah-history",
            "files": {
                "scripts": [
                    "modules/rainmaker_modules/ah-history/ah-history.coffee"
                ]
            }
 }
 ```    
##### Include module to index.jade/index.pug
```
#ahHistory(data-module='ah-history')
```

##### Example of using - use event 'history:update' to monitor changes current view:

```
app.listenTo app, 'history:update', (data)=> do something, data - info about current view
```
