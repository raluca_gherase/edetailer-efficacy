app.register "ah-history", ()->
  publish: {}

  onRender: ()->
    @stack = []

    app.listenTo app.slide, 'slide:enter', @add.bind(@)
    app.listenTo app, 'slideEnter:inlineSlideshow', @add.bind(@)
    app.listenTo app.slide, 'slide:exit', @remove.bind(@)
    app.listenTo app, 'slideExit:inlineSlideshow', @remove.bind(@)

  _getSlideId: (data)->
    # TODO update popup, inlineSlideshow - create same callback for receiving id
    # for slide 'data.id' for popup 'data.slideId' for inlineSlideshow 'data.slide.id'
    data.slideId or data.id or data.slide.id

  add: (data)->
    slideId = @_getSlideId data
    @stack.push
      id: slideId
      slide: app.slide.get slideId
    @trigger()

  remove: (data)->
    slideId = @_getSlideId data
    item = @stack.find (item)-> slideId is item.id
    @stack.splice @stack.indexOf(item), 1
    @trigger() unless @stack.length is 0

  getCurrentView: ()->
    @stack[@stack.length - 1]

  getStack: ()->
    @stack

  trigger: ()->
    app.trigger 'history:update', @getCurrentView()