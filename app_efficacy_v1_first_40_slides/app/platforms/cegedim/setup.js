
(function(global){
	console.log('cegedim shim!')
	$(document).ready(function(){
			window.referencespath = "references.json"

			global.app = {		
				currentSlide:null,
				slides:[],
				slide:{},
				goTo:function(slideID){
					if(this.currentSlide!=null)
						this.currentSlide.onExit()
					this.currentSlide=this.slide[slideID]
					this.currentSlide.onEnter()

				},
				init:function(){
					window.presentationInit = document.createEvent('UIEvents');
					presentationInit.initEvent('presentationInit', false, false);

					window.slideEnter = document.createEvent('UIEvents');
					slideEnter.initEvent('slideEnter', false, false);

					try{
						document.dispatchEvent(presentationInit);
						$('article')[0].dispatchEvent(window.slideEnter);

						app.goTo(primary_slide_name)
						
					}catch(err){console.warn('Err dispatching init + slide enter events',err)}
				}
			}
			//Object.defineProperty(global.app,"currentSlide",{get:function(){return app.slide[Object.keys(app.slide)[0]]}})
			global.app.init()

		})
})(window);