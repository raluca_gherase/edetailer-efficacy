
class StayStrong40Slide
  constructor: ()->
    @events = {
      "tap .arrow-btn": ()-> @changeState()
    }
    @states = [
      {
        id:"default",
        name: "DME_Eff_StayStrongMaintainGains_State1_Slide",
        onEnter:()-> @submitMonitoring()
      }
      {
        id:"first",
        name: "DME_Eff_StayStrongMaintainGains_State2_Slide",
        onEnter:()-> @submitMonitoring()
      }
      {
        id:"second",
        name: "DME_Eff_StayStrongMaintainGains_State3_Slide",
        onEnter:()-> @submitMonitoring()
      }
      {
        id:"third",
        name: "DME_Eff_StayStrongMaintainGains_State4_Slide",
        onEnter:()-> @submitMonitoring()
      }
    ]
    @$buttons = null

  onRender: (element)->
    @element = element
    @analitycsModule = app.module.get 'ahAnalitycs'
    @$buttons = element.getElementsByClassName("state-btn")

  changeState: ()->
    button = event.target
    if button.classList.contains("plus")
      @resetButtons()
      button.classList.remove("plus")
      button.classList.add("minus")
    else
      @resetButtons()
      @reset()

  resetButtons: ()->
    for button in @$buttons
      button.classList.remove("minus")
      button.classList.add("plus")

  onEnter: (element)->

  onExit: (element)->
    @resetButtons()
    @reset()

  submitMonitoring: ()->
    @analitycsModule.submitCustomSlideMonitoring {id: @element.id, name: @getState().name}

app.register('StayStrong40Slide', -> new StayStrong40Slide())