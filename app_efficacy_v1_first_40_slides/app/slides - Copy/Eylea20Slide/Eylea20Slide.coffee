
class Eylea20Slide
  constructor: ()->
    @events = { }
    @states = []

  onRender: (element)->
    @element = element
    @buttonsList = @element.getElementsByClassName 'show-description'
    @descriptionsList = @element.getElementsByClassName 'description'
    @analitycsModule = app.module.get 'ahAnalitycs'

    for button, index in @buttonsList
      do (button, index)=>
        button.addEventListener 'tap', ()=>
          button.classList.add "hidden"
          @descriptionsList[index]?.classList.add "active"
          monitoringName = button.getAttribute 'data-name'
          @submitMonitoring(monitoringName)

  onEnter: (element)->

  onExit: (element)->
    do @resetActiveDescriptions

  resetActiveDescriptions: ()->
    for button, index in @buttonsList
      do (button, index)=>
        button.classList.remove "hidden"
        @descriptionsList[index]?.classList.remove "active"

  submitMonitoring: (monitoringName)->
    @analitycsModule.submitCustomSlideMonitoring {id: @element.id, name: monitoringName}

app.register('Eylea20Slide', -> new Eylea20Slide())