
class MultiTargetApproachSlide
  constructor: ()->
    @events = {}
    @states = [
      {"id": "first"},
      {"id": "second"},
      {"id": "third"}
    ]

  onRender: (element)->
    @next()

  onEnter: (element)->

  onExit: (element)->

app.register('MultiTargetApproachSlide', -> new MultiTargetApproachSlide())