
class StartStrong35Slide
  constructor: ()->
    @events = {}
    @states = [
      {
        id: "default",
        name: "DME_Eff_StartStrongProtocolT_State1_Slide",
        onEnter: ()-> @submitMonitoring()
      },
      {
        id: "detail-1",
        name: "DME_Eff_StartStrongProtocolT_State2_Slide",
        onEnter: ()-> @submitMonitoring()
      },
      {
        id: "detail-2",
        name: "DME_Eff_StartStrongProtocolT_State3_Slide",
        onEnter: ()-> @submitMonitoring()
      }
    ]

  onRender: (element)->
    @element = element
    @analitycsModule = app.module.get 'ahAnalitycs'

  onEnter: (element)->

  onExit: (element)->
    @reset()

  submitMonitoring: ()->
    @analitycsModule.submitCustomSlideMonitoring {id: @element.id, name: @getState().name}

app.register('StartStrong35Slide', -> new StartStrong35Slide())