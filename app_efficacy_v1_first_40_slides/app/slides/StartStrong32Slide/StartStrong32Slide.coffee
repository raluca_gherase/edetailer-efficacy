
class StartStrong32Slide
  constructor: ()->
    @events = {
      "tap .next-slide": "goToNextSlide"
    }
    @states = []

  goToNextSlide: ->
    app.slideshow.next()

  onRender: (element)->

  onEnter: (element)->

  onExit: (element)->

app.register('StartStrong32Slide', -> new StartStrong32Slide())