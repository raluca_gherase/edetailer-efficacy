
class StartStrong33Slide
  constructor: ()->
    @events = {
      "tap .previous-slide": "goToPreviousSlide"
    }
    @states = []

  goToPreviousSlide: ()->
    app.slideshow.prev()

  onRender: (element)->

  onEnter: (element)->

  onExit: (element)->

app.register('StartStrong33Slide', -> new StartStrong33Slide())